package com.snail.oa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.common.mq.core.dto.MqResultDto;
import com.snail.oa.domain.LeaveApply;
import com.snail.oa.query.LeaveApplyQuery;
import com.snail.workflow.api.common.WorkflowBasicService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Description: 请假申请Service接口
 * @Author: snail
 * @CreateDate: 2023-09-04
 * @Version: V1.0
 */
public interface ILeaveApplyService extends IService<LeaveApply>, IExcelService<LeaveApply>, WorkflowBasicService {

    /**
     * 查询请假申请列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<LeaveApply> selectLeaveApplyPage(LeaveApplyQuery query);

    /**
     * 根据id获取请假申请详情
     *
     * @param applyId 主键id
     * @return 结果
     */
    LeaveApply getLeaveApplyById(String applyId);

    /**
     * 保存请假申请数据
     *
     * @param leaveApply 请假申请信息
     * @return 结果
     */
    String insertLeaveApply(LeaveApply leaveApply);

    /**
     * 更新请假申请数据
     *
     * @param leaveApply 请假申请信息
     * @return 结果
     */
    boolean updateLeaveApply(LeaveApply leaveApply);

    /**
     * 根据id批量删除请假申请数据
     *
     * @param applyIds 主键ids
     * @return 结果
     */
    boolean deleteLeaveApplyByIds(String[] applyIds);

    /**
     * 查询请假申请数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<LeaveApply> selectLeaveApplyList(LeaveApplyQuery query);

    /**
     * 发送MQ消息
     *
     * @param applyId 申请id
     * @return 结果
     */
    MqResultDto sendMq(String applyId);

    /**
     * 发送文件消息
     *
     * @param file    文件
     * @param applyId 申请id
     * @return 结果
     */
    MqResultDto sendFileMq(MultipartFile file, String applyId);
}
