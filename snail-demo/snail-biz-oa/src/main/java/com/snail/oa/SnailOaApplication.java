package com.snail.oa;


import com.snail.common.cloud.annotation.SnailCloudApplication;
import com.snail.common.job.annotation.JobRegistrarScan;
import com.snail.common.security.annotation.EnableSnailFeignClients;
import org.springframework.boot.SpringApplication;

/**
 * @author Snail
 */
@EnableSnailFeignClients
@JobRegistrarScan("com.snail.oa.job")
@SnailCloudApplication
public class SnailOaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailOaApplication.class, args);
    }
}
