package com.snail.oa.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.common.datascope.annotation.DataScope;
import com.snail.oa.domain.LeaveApply;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 请假申请Mapper接口
 * @Author: snail
 * @CreateDate: 2023-09-04
 * @Version: V1.0
 */
@Mapper
@DataScope(includeMethod = {"selectPage", "selectList"})
public interface LeaveApplyMapper extends BaseMapper<LeaveApply> {

}
