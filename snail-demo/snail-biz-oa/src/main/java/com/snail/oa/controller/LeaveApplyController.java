package com.snail.oa.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.mq.core.dto.MqResultDto;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.oa.domain.LeaveApply;
import com.snail.oa.query.LeaveApplyQuery;
import com.snail.oa.service.ILeaveApplyService;
import com.snail.workflow.api.common.WorkflowBasicController;
import com.snail.workflow.api.common.WorkflowBasicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 请假申请Controller
 * @Author: snail
 * @CreateDate: 2023-09-04
 * @Version: V1.0
 */
@Api(value = "请假申请", tags = "请假申请")
@RestController
@RequestMapping("/leave")
public class LeaveApplyController implements WorkflowBasicController {
    @Autowired
    private ILeaveApplyService leaveApplyService;

    @Log(title = "查询请假申请分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询请假申请列表")
    @RequiresPermissions("apply:leave:query")
    @GetMapping("/page")
    public R<PageResult<LeaveApply>> getLeaveApplyPage(LeaveApplyQuery query) {
        return R.ok(leaveApplyService.selectLeaveApplyPage(query));
    }

    @Log(title = "获取请假申请详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取请假申请详细信息")
    @RequiresPermissions("apply:leave:query")
    @GetMapping(value = "/{applyId}")
    public R<LeaveApply> getLeaveApplyById(@PathVariable("applyId") String applyId) {
        return R.ok(leaveApplyService.getLeaveApplyById(applyId));
    }

    @ApiOperation(value = "新增请假申请")
    @RequiresPermissions("apply:leave:add")
    @Log(title = "新增请假申请", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveLeaveApply(@RequestBody LeaveApply leaveApply) {
        return R.ok(leaveApplyService.insertLeaveApply(leaveApply));
    }

    @ApiOperation(value = "修改请假申请")
    @RequiresPermissions("apply:leave:edit")
    @Log(title = "修改请假申请", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateLeaveApply(@RequestBody LeaveApply leaveApply) {
        return R.ok(leaveApplyService.updateLeaveApply(leaveApply));
    }


    @ApiOperation(value = "删除请假申请")
    @RequiresPermissions("apply:leave:delete")
    @Log(title = "删除请假申请", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{applyIds}")
    public R<Boolean> deleteLeaveApplyByIds(@PathVariable String[] applyIds) {
        return R.ok(leaveApplyService.deleteLeaveApplyByIds(applyIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("apply:leave:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "请假申请导入模板", LeaveApply.class);
    }

    @ApiOperation(value = "导出请假申请数据")
    @Log(title = "导出请假申请", businessType = BusinessType.EXPORT)
    @RequiresPermissions("apply:leave:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, LeaveApplyQuery query) {
        EasyExcelUtils.writeExcel(response, "请假申请信息", LeaveApply.class, leaveApplyService.selectLeaveApplyList(query));
    }

    @ApiOperation(value = "导入请假申请数据")
    @Log(title = "导入请假申请", businessType = BusinessType.IMPORT)
    @RequiresPermissions("apply:leave:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, LeaveApply.class, leaveApplyService);
    }

    @ApiOperation(value = "发送MQ消息")
    @Log(title = "发送MQ消息", businessType = BusinessType.OTHER)
    @PostMapping("/sendMq/{applyId}")
    public R<MqResultDto> sendMq(@PathVariable("applyId") String applyId){
        return R.ok(leaveApplyService.sendMq(applyId));
    }

    @ApiOperation(value = "发送MQ文件消息")
    @Log(title = "发送MQ文件消息", businessType = BusinessType.OTHER)
    @PostMapping("/sendFileMq/{applyId}")
    public R<MqResultDto> uploadFile(@RequestPart("file") MultipartFile file,@PathVariable("applyId") String applyId) {
        return R.ok(leaveApplyService.sendFileMq(file,applyId));
    }

    @Override
    public WorkflowBasicService getService() {
        return leaveApplyService;
    }
}
