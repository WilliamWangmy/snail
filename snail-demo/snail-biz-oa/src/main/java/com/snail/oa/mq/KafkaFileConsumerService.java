package com.snail.oa.mq;

import com.snail.common.core.exception.ServiceException;
import com.snail.common.mq.core.IMqProducerAndConsumerHandler;
import com.snail.common.mq.core.dto.MqDto;
import com.snail.common.mq.exception.MqException;
import com.snail.common.mq.kafka.factory.AbstractKafkaMqFactory;
import lombok.SneakyThrows;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @Description: Kafka消息处理
 * @Author: Snail
 * @CreateDate: 2023/11/27 10:12
 * @Version: V1.0
 */
@Component
@KafkaListener(topics = {KafkaFileConsumerService.TOPIC})
public class KafkaFileConsumerService extends AbstractKafkaMqFactory<String> {

    public static final String TOPIC = "OA_LEAVE_APPLY_FILE";
    /**
     * 这里可以注入自定义handler进行处理
     * @param kafkaProducerAndConsumerHandler Kafka消息处理handler
     */
    public KafkaFileConsumerService(IMqProducerAndConsumerHandler kafkaProducerAndConsumerHandler) {
        super(kafkaProducerAndConsumerHandler);
    }


    /**
     * 业务消费处理
     *
     * @param mqDto 消息体
     * @throws ServiceException 业务异常
     * @throws MqException      mq异常
     */
    @SneakyThrows
    @Override
    protected void consumeMq(MqDto<String> mqDto) throws ServiceException, MqException {
        System.out.println("Kafka消息监听："+mqDto);
    }
}
