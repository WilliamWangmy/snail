package com.snail.oa.job;

import com.snail.common.core.utils.DateUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.job.annotation.JobTask;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

/**
 * @Description: 定时任务处理器
 * @Author: Snail
 * @CreateDate: 2023/9/12 13:57
 * @Version: V1.0
 */
@Component
public class JobHandler {

    @XxlJob("jobDemo")
    @JobTask(value = "定时任务demo", cron = "0/5 * * * * ?")
    public void jobDemo(String param) {
        XxlJobHelper.log("定时任务jobDemo执行:{},参数:{}", DateUtils.getTime(), param);
        System.out.println(StringUtils.format("定时任务jobDemo执行:{},参数:{}", DateUtils.getTime(), param));
    }
}
