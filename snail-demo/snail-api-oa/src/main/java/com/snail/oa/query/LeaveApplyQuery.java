package com.snail.oa.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;


/**
 * @Description: 请假申请查询对象
 * @Author: snail
 * @CreateDate: 2023-09-04
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "请假申请")
public class LeaveApplyQuery extends BaseQuery {
    @ApiModelProperty(value = "单据编码")
    private String applyCode;
    @ApiModelProperty(value = "请假天数")
    private BigDecimal leaveDay;
    @ApiModelProperty(value = "开始时间")
    private String leaveBegin;
    @ApiModelProperty(value = "结束时间")
    private String leaveEnd;
    @ApiModelProperty(value = "流程状态")
    private String workflowStatus;
    @ApiModelProperty(value = "请假人")
    private String createUserName;
}
