package com.snail.oa.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 请假申请对象 snail_leave_apply
 * @Author: snail
 * @CreateDate: 2023-09-04
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "请假申请")
@TableName("snail_leave_apply")
public class LeaveApply implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String applyId;

    /**
     * 单据名称
     */
    @ApiModelProperty(value = "单据名称")
    @Excel(value = "单据名称")
    private String applyName;

    /**
     * 单据编码
     */
    @ApiModelProperty(value = "单据编码")
    @Excel(value = "单据编码")
    private String applyCode;

    /**
     * 请假天数
     */
    @ApiModelProperty(value = "请假天数")
    @Excel(value = "请假天数")
    private BigDecimal leaveDay;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(value = "开始时间",template = true)
    private LocalDateTime leaveBegin;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(value = "结束时间",template = true)
    private LocalDateTime leaveEnd;

    /**
     * 请假原因
     */
    @ApiModelProperty(value = "请假原因")
    @Excel(value = "请假原因",template = true)
    private String leaveReason;

    /**
     * 流程状态
     */
    @ApiModelProperty(value = "流程状态")
    @Excel(value = "流程状态")
    private String workflowStatus;

    /**
     * 租户
     */
    @ApiModelProperty(value = "租户")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 是否逻辑删除
     */
    @ApiModelProperty(value = "是否逻辑删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 请假人
     */
    @ApiModelProperty(value = "请假人")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "请假人")
    private String createUserName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新人名称
     */
    @ApiModelProperty(value = "更新人名称")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserName;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人组织id
     */
    @ApiModelProperty(value = "创建人组织id")
    @TableField(fill = FieldFill.INSERT)
    private String deptId;

    /**
     * 创建人组织名称
     */
    @ApiModelProperty(value = "创建人组织名称")
    @TableField(fill = FieldFill.INSERT)
    private String deptName;

    /**
     * 创建人组织全路径id
     */
    @ApiModelProperty(value = "创建人组织全路径id")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullId;

    /**
     * 创建人组织全路径名称
     */
    @ApiModelProperty(value = "创建人组织全路径名称")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullName;


}
