<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Snail v3.6.3</h1>
<h4 align="center">基于 Spring Boot/Spring Cloud & Alibaba 前后端分离的分布式微服务架构</h4>


## 平台简介

Snail是一套全部开源的快速开发平台，基于 [若依/RuoYi-Cloud](https://gitee.com/zhangmrit/ruoyi-cloud) 为基础版本进行迭代孵化Snail平台。

* 采用前后端分离的模式，微服务版本前端(Snail-Antdv)。
* 后端采用Spring Boot、Spring Cloud & Alibaba。
* 注册中心、配置中心选型Nacos，权限认证使用Redis。
* 流量控制框架选型Sentinel，分布式事务选型Seata。
* 消息中间件使用了kafka、RabbitMQ
* 流程中心使用了flowable

#### 友情链接 [若依/RuoYi-Cloud](https://gitee.com/y_project/RuoYi-Cloud) 微服务版本。

[前端snail-ui](https://gitee.com/WilliamWangmy/snail-ui)

[功能详细说明文档](docs/功能介绍.md)

## 系统模块

~~~
com.snail     
├── snail-dependencies        // Maven依赖管理
├── snail-gateway             // 网关模块 [8080]
├── snail-auth                // 认证中心 [9200]
├── snail-common              // 通用模块
│       └── snail-common-core                          // 核心模块
│       └── snail-common-datascope                     // 权限范围
│       └── snail-common-datasource                    // 多数据源
│       └── snail-common-log                           // 日志记录
│       └── snail-common-redis                         // 缓存服务
│       └── snail-common-seata                         // 分布式事务
│       └── snail-common-security                      // 安全模块
│       └── snail-common-swagger                       // swagger knife4j接口
│       └── snail-common-cloud                         // 微服务模块
│       └── snail-common-excel                         // Excel导入导出
│       └── snail-common-job                           // 定时任务模块
│       └── snail-common-lock                          // 分布式锁
│       └── snail-common-sequence                      // 序列号模块
│       └── snail-common-tenant                        // 多租户模块
│       └── snail-common-mq                            // mq消息
│       └── snail-common-mq-kafka                      // kafka模块
│       └── snail-common-mq-rabbitmq                   // rabbitMQ模块
├── snail-modules             // 业务模块
│       └── snail-modules-system                       // 系统管理模块 [9201]
│       └── snail-modules-system-api                   // 系统管理接口
│       └── snail-modules-gen                          // 代码生成 [9202]
│       └── snail-modules-job                          // 定时任务 [9203]
│       └── snail-modules-file                         // 文件服务 [9204]
│       └── snail-modules-workflow                     // 流程服务 [9205]
│       └── snail-modules-workflow-api                 // 流程接口
│       └── snail-modules-job-admin                    // 定时任务(xxl-job)服务 [9206]
│       └── snail-modules-thirdparty                   // 第三方服务 [9207]
│       └── snail-modules-thirdparty-api               // 第三方服务接口 
│       └── snail-modules-monitor                      // 监控中心 [9100]
├── snail-wms                 // wms功能
│       └── snail-wms-base-api                         // wms基础数据接口
│       └── snail-wms-base-api                         // wms基础数据业务服务[11000]
│       └── snail-wms-business-api                     // wms业务接口
│       └── snail-wms-business-biz                     // wms业务服务[11001]
│       └── snail-wms-stock-api                        // wms库存接口
│       └── snail-wms-stock-biz                        // wms库存业务服务[11002]
├──pom.xml                    // 公共依赖
~~~

## 架构图

<img src="https://oscimg.oschina.net/oscnet/up-82e9722ecb846786405a904bafcf19f73f3.png"/>

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10.  登录日志：系统登录日志记录查询包含登录异常。
11.  在线用户：当前系统中活跃用户状态监控。
12.  定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13.  代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14.  系统接口：根据业务代码自动生成相关的api接口文档。
15.  服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16.  在线构建器：拖动表单元素生成相应的HTML代码。
17.  连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
18.  序列号管理：序列号生成规则维护。
19.  MQ消息管理：对MQ消息的生产、消费过程记录和处理异常消息。
20.  多租户管理：[使用方法](https://gitee.com/WilliamWangmy/snail/tree/master/snail-common/snail-common-tenant)
     - 数据隔离：根据租户查询不同的数据，将表里面的数据进行切割，达到数据隔离的效果。
     - 数据库隔离：根据租户连接不同的数据库，达到数据库隔离的效果。


## 演示图

<table>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E7%99%BB%E5%BD%95%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E5%BE%85%E5%8A%9E%E3%80%81%E5%B7%B2%E5%8A%9E%E3%80%81%E5%BE%85%E9%98%85%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E9%83%A8%E9%97%A8%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%B5%81%E7%A8%8B%E8%AE%BE%E8%AE%A1%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%B5%81%E7%A8%8B%E5%8F%91%E5%B8%83%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E8%AF%A6%E6%83%85%E9%A1%B5%E9%9D%A2.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E6%8F%90%E4%BA%A4%E9%A1%B5%E9%9D%A2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E9%85%8D%E7%BD%AE%E6%95%B0%E6%8D%AE%E6%BA%90.png"/></td>
        <td><img src="https://gitee.com/WilliamWangmy/snail-knowledge/raw/master/images/snail/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E9%85%8D%E7%BD%AE%E5%B1%9E%E6%80%A7.png"/></td>
    </tr>
</table>