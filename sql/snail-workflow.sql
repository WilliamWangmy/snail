-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: snail-workflow
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_evt_log`
--

DROP TABLE IF EXISTS `act_evt_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_evt_log` (
  `LOG_NR_` bigint NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DATA_` longblob,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint DEFAULT '0',
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_evt_log`
--

LOCK TABLES `act_evt_log` WRITE;
/*!40000 ALTER TABLE `act_evt_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_evt_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ge_bytearray`
--

DROP TABLE IF EXISTS `act_ge_bytearray`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ge_bytearray` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  `GENERATED_` tinyint DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ge_bytearray`
--

LOCK TABLES `act_ge_bytearray` WRITE;
/*!40000 ALTER TABLE `act_ge_bytearray` DISABLE KEYS */;
INSERT INTO `act_ge_bytearray` VALUES ('172f5342d6d2f95ec57a246d9a4ed4e8',1,'var-assigneeList',NULL,_binary '�\�\0sr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0t\0 00b6fea67bb10175f39158cb9e00db1et\0 9a8ccb159ac609b176825e9a6b4039d3x',NULL),('419937cbe011d8cfc90aa43bf35d5a43',1,'hist.var-assigneeList',NULL,_binary '�\�\0sr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0t\0 00b6fea67bb10175f39158cb9e00db1et\0 9a8ccb159ac609b176825e9a6b4039d3x',NULL),('85cf8da4082216350fa6ad4f8acc7918',1,'hist.var-assigneeList',NULL,_binary '�\�\0sr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0t\0 00b6fea67bb10175f39158cb9e00db1ex',NULL),('9cd852e641dab0f877881ee0440689a2',1,'请假申请.bpmn','21a2f1ff1536a9d3f8ecee118eb0287b',_binary '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:bioc=\"http://bpmn.io/schema/bpmn/biocolor/1.0\" xmlns:flowable=\"http://flowable.org/bpmn\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" targetNamespace=\"http://www.flowable.org/processdef\">\n  <process id=\"LeaveApply\" name=\"请假申请\" flowable:processCategory=\"leave\">\n    <startEvent id=\"startNode1\" name=\"开始\">\n      <outgoing>Flow_0nfqvai</outgoing>\n    </startEvent>\n    <exclusiveGateway id=\"Gateway_08fh2up\">\n      <incoming>Flow_1wxc6l4</incoming>\n      <outgoing>Flow_014zhnw</outgoing>\n      <outgoing>Reject_Flow_0yh56fs</outgoing>\n      <outgoing>Flow_0vhm2e4</outgoing>\n    </exclusiveGateway>\n    <exclusiveGateway id=\"Gateway_1bsklt0\">\n      <incoming>Flow_0i3x4ud</incoming>\n      <outgoing>Flow_071mxw1</outgoing>\n      <outgoing>Reject_Flow_1mi5648</outgoing>\n    </exclusiveGateway>\n    <endEvent id=\"endNode\" name=\"结束\">\n      <incoming>Flow_071mxw1</incoming>\n      <incoming>Flow_0vhm2e4</incoming>\n    </endEvent>\n    <sequenceFlow id=\"Flow_071mxw1\" sourceRef=\"Gateway_1bsklt0\" targetRef=\"endNode\" />\n    <userTask id=\"mangerNode\" name=\"主管审批\" flowable:candidateGroups=\"945cc85588578f74143ff672085c25a7\" flowable:userType=\"candidateGroups\" flowable:assignee=\"${assignee}\">\n      <incoming>Flow_0mw6ofi</incoming>\n      <outgoing>Flow_1wxc6l4</outgoing>\n    </userTask>\n    <userTask id=\"deptLeaderNode\" name=\"部门领导审批\" flowable:candidateGroups=\"51d63df3702720193c17209e1d18eaa5\" flowable:userType=\"candidateGroups\" flowable:assignee=\"${assignee}\">\n      <incoming>Flow_014zhnw</incoming>\n      <outgoing>Flow_0i3x4ud</outgoing>\n      <multiInstanceLoopCharacteristics flowable:collection=\"assigneeList\" flowable:elementVariable=\"assignee\">\n        <completionCondition>${nrOfCompletedInstances/nrOfInstances == 1} </completionCondition>\n      </multiInstanceLoopCharacteristics>\n    </userTask>\n    <userTask id=\"applyNode\" name=\"申请人修改\" flowable:assignee=\"${INITIATOR}\" flowable:userType=\"initiator\">\n      <incoming>Flow_0nfqvai</incoming>\n      <incoming>Reject_Flow_1mi5648</incoming>\n      <incoming>Reject_Flow_0yh56fs</incoming>\n      <outgoing>Flow_0mw6ofi</outgoing>\n    </userTask>\n    <sequenceFlow id=\"Flow_0nfqvai\" name=\"\" sourceRef=\"startNode1\" targetRef=\"applyNode\" />\n    <sequenceFlow id=\"Flow_0mw6ofi\" name=\"\" sourceRef=\"applyNode\" targetRef=\"mangerNode\" />\n    <sequenceFlow id=\"Flow_1wxc6l4\" name=\"\" sourceRef=\"mangerNode\" targetRef=\"Gateway_08fh2up\" />\n    <sequenceFlow id=\"Flow_014zhnw\" name=\"\" sourceRef=\"Gateway_08fh2up\" targetRef=\"deptLeaderNode\">\n      <conditionExpression xsi:type=\"tFormalExpression\">${day&gt;2}</conditionExpression>\n    </sequenceFlow>\n    <sequenceFlow id=\"Flow_0i3x4ud\" name=\"\" sourceRef=\"deptLeaderNode\" targetRef=\"Gateway_1bsklt0\" />\n    <sequenceFlow id=\"Reject_Flow_1mi5648\" name=\"驳回\" sourceRef=\"Gateway_1bsklt0\" targetRef=\"applyNode\" />\n    <sequenceFlow id=\"Reject_Flow_0yh56fs\" name=\"驳回\" sourceRef=\"Gateway_08fh2up\" targetRef=\"applyNode\" />\n    <sequenceFlow id=\"Flow_0vhm2e4\" sourceRef=\"Gateway_08fh2up\" targetRef=\"endNode\">\n      <conditionExpression xsi:type=\"tFormalExpression\">${day&lt;=2}</conditionExpression>\n    </sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_flow\">\n    <bpmndi:BPMNPlane id=\"BPMNPlane_flow\" bpmnElement=\"LeaveApply\">\n      <bpmndi:BPMNEdge id=\"Flow_0yh56fs_di\" bpmnElement=\"Reject_Flow_0yh56fs\">\n        <di:waypoint x=\"120\" y=\"95\" />\n        <di:waypoint x=\"120\" y=\"10\" />\n        <di:waypoint x=\"-190\" y=\"10\" />\n        <di:waypoint x=\"-190\" y=\"80\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"-45\" y=\"-8\" width=\"21\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_1mi5648_di\" bpmnElement=\"Reject_Flow_1mi5648\">\n        <di:waypoint x=\"380\" y=\"145\" />\n        <di:waypoint x=\"380\" y=\"210\" />\n        <di:waypoint x=\"-190\" y=\"210\" />\n        <di:waypoint x=\"-190\" y=\"160\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"85\" y=\"192\" width=\"21\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_0i3x4ud_di\" bpmnElement=\"Flow_0i3x4ud\">\n        <di:waypoint x=\"300\" y=\"120\" />\n        <di:waypoint x=\"355\" y=\"120\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"311\" y=\"95\" width=\"34\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_014zhnw_di\" bpmnElement=\"Flow_014zhnw\">\n        <di:waypoint x=\"145\" y=\"120\" />\n        <di:waypoint x=\"200\" y=\"120\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"156\" y=\"95\" width=\"34\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_1wxc6l4_di\" bpmnElement=\"Flow_1wxc6l4\">\n        <di:waypoint x=\"40\" y=\"120\" />\n        <di:waypoint x=\"95\" y=\"120\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"51\" y=\"95\" width=\"34\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_0mw6ofi_di\" bpmnElement=\"Flow_0mw6ofi\">\n        <di:waypoint x=\"-140\" y=\"120\" />\n        <di:waypoint x=\"-60\" y=\"120\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"-117\" y=\"95\" width=\"34\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_0nfqvai_di\" bpmnElement=\"Flow_0nfqvai\">\n        <di:waypoint x=\"-315\" y=\"120\" />\n        <di:waypoint x=\"-240\" y=\"120\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"-294\" y=\"95\" width=\"34\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_071mxw1_di\" bpmnElement=\"Flow_071mxw1\">\n        <di:waypoint x=\"405\" y=\"120\" />\n        <di:waypoint x=\"462\" y=\"120\" />\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge id=\"Flow_0vhm2e4_di\" bpmnElement=\"Flow_0vhm2e4\">\n        <di:waypoint x=\"120\" y=\"145\" />\n        <di:waypoint x=\"120\" y=\"300\" />\n        <di:waypoint x=\"480\" y=\"300\" />\n        <di:waypoint x=\"480\" y=\"138\" />\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNShape id=\"BPMNShape_startNode1\" bpmnElement=\"startNode1\" bioc:stroke=\"\">\n        <omgdc:Bounds x=\"-345\" y=\"105\" width=\"30\" height=\"30\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"-342\" y=\"142\" width=\"22\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Gateway_08fh2up_di\" bpmnElement=\"Gateway_08fh2up\" isMarkerVisible=\"true\">\n        <omgdc:Bounds x=\"95\" y=\"95\" width=\"50\" height=\"50\" />\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Gateway_1bsklt0_di\" bpmnElement=\"Gateway_1bsklt0\" isMarkerVisible=\"true\">\n        <omgdc:Bounds x=\"355\" y=\"95\" width=\"50\" height=\"50\" />\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Event_1axfiw1_di\" bpmnElement=\"endNode\">\n        <omgdc:Bounds x=\"462\" y=\"102\" width=\"36\" height=\"36\" />\n        <bpmndi:BPMNLabel>\n          <omgdc:Bounds x=\"468\" y=\"78\" width=\"23\" height=\"14\" />\n        </bpmndi:BPMNLabel>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Activity_1nmaqro_di\" bpmnElement=\"mangerNode\">\n        <omgdc:Bounds x=\"-60\" y=\"80\" width=\"100\" height=\"80\" />\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Activity_06xr9gr_di\" bpmnElement=\"deptLeaderNode\">\n        <omgdc:Bounds x=\"200\" y=\"80\" width=\"100\" height=\"80\" />\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape id=\"Activity_157q91y_di\" bpmnElement=\"applyNode\">\n        <omgdc:Bounds x=\"-240\" y=\"80\" width=\"100\" height=\"80\" />\n      </bpmndi:BPMNShape>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>\n',0),('c86a833eadc9eda8671263427b6dacda',1,'hist.var-assigneeList',NULL,_binary '�\�\0sr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0t\0 00b6fea67bb10175f39158cb9e00db1ex',NULL),('d19e241886a6a8f5ad81bbd64a59316b',1,'请假申请.LeaveApply.png','21a2f1ff1536a9d3f8ecee118eb0287b',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0U\0\0>\0\0\0eS:v\0\0$IDATx\�\�\���e�/�Y���\�\nY��\�K�dCqK�(n\�\�\�\�f�+e��\�I!- \�\�*6pc�t\\+\\+���%�\�.�tf�b��\�\n\�O)�K�l�\"羿�y\�==gf\�̙�3�\�\'y2\�3�\�\�y����\��ﴴ\0\0\0\00}tu�N�\�\�S�\�qmi�\������M�t�\�&=oiw\�\�uu��Sz~qtt��\�\�\��«	\0\0L;��\�?<>v��\�Z\��㿊f���\�������qZ_�ژ���\�\0\0���\�\�\�\��ֹ�tq�yٚ��\�\�\���8*\�\�\�\�=\�|���}\����0xt+{\�;����J\0\0\�R4S+W�|W\�,]�lM\�خ���H_�S��ϋ�\�}W\�Ǖ+��\�;��:�tm\�_w��Z��\0\0��MTǚ���z�ϋ\�\�T��k�O���#X�MUw��y#�=\�\�Ju���s\\��=\��&\0\00\�\��S麩�\��+\�U\�4��7[=��\��+\�\�\�\��ƈǢ�ʛ�\��e����\0\0���7DY\�MRi�\���f��\�\�\�S�d�\���\�tu��\\\��\�-�&\0\00-�#LqÉ��K\�ǝ��?g�u�,4U�t�)ͪv;�Ξ�^I\0\0`z6U\�J\�ͯ�\�\�q��\�츦��x<n�\�\�\�w}\�\�޿�?��X\�d\�\�B�$\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\�N?���aƁ6�;\00�M�W�k\0\0\0r\r\0��\0�k\0�\��\\\0,>\0\�\Z\0�\� \�\0\0,>\0\�\Z\0`� \�\0\0�\0�\0`�\�5\0\0�\0�\0X|\0\�5\0�\�@�\0\0r\r\0�\��kr\r\0��\0�k\0�\��\\\0,>\0\�\Z\0�\� \�\0\0,>\0\�\Z\0`� \�\0\0�\0�\0`�\�5\0\0�\0�\0X|\0\�5\0�\�@�\0\0r\r\0�\��k\0\0\0r\r\0��\0�k\0�\��\\\0��\0\�\Z\0�\��\\\0,>\0\�\Z\0`� \�\0\0��@�\0X|\0\�5\0�\�@�\0\0r\r\0��\0�k\0\0�\\\0��\0�k\0�\��\\\0,>\0\�\Z\0�\� \�\0\0,>\0\�\Z\0`� \�\0\0�\0�\0X|\0\�5\0\0�@�\�5\0�\�@�\0\0r\r\0��\0�k\0\0�\\\0��\0�k\0�\��\\\0,>\0\�\Z\0�\� \�\0\0,>\0\�\Z\00q���\�fl�J�*\0�\��\�\�;TS\�\�\�:\�+h�\0\0j/6\���\�<s\�̃�J��\n\0��bcF�����m�W\�T\0�\�vm\�G�\0M\0�\��][\�Z*@S\0P����\�Q*@S\00��\�µT��\"��\n\0���G~m��T��\n\0`t���\n\0�\�r����z\�\�|�O}}}\�\r6<z{{\��\���\�\�3\�|6�\�T\�y\00\�D�ٴiSy\�Ν\�7\�xØ��\�K/�7n\��JV�N5+\�g�M�<�\0SH\��Sp���aÆ\�\�J�\�|FS%\���D�\�Y\�\�kV�\�\�3�*y \0��8\�Y\�7Uѱ�2�\�g4U�@\0�E\��\�Q~j˺�oz/\�G|�)���l�Ϛ*y`\�@\�&_ݵ��\�OW�u����X|M�Pt\�g\�|\�T\�C\0�\�\�\��\�~\'�\��^�Pt\�g\�|\�T\�C\0�\�P\�ҷj���b�\�φ�����<\0�!ƣV\�,:�5\�B\�1�\r�YS%y\0(:���c>�\�泦J\�y\00>E\'\�T�\�\�\�E\�|6\�gM�<0\��\�1�|\�ښE\'��X(:\�a>k�\�!\0Eg�����\�G\�d�S#�\�\�k���c>泦J�\0Pt�O����N<�P(:\�a>k�\�!\�\��^�\�E\��\�\�O��\��?5\"{,��X(:\�a>7\�)M\�,/�<�� �\0�rщw�⁞�\�\�׼뼢c>\�sC\�\�Ʈl�|�\�%��@\���E\��\�\�;�\�/��\'_�Yp҈\�\�s\�\�St\�g\�|n\�闲\�^��.{�\�<��\0�X\�nz���l�\�\�bV�=\�iae�<��@�T):#\�{7\�^=\�C\�1�\r�n3�\�]k��<��\0hƢ3ڂ��\�\�φ�\\��{��r\�5�@\��ي��\�φ�<a\�5sF��Y-����@ȃ	\�\�\�~�駟~j6z��9;\�ά�\�\�\�\�z}[[ۂl\�UStE\�|6\�\�uR\�讍��׫R�@\�y0>�&iv\�,�Ϛ��*��aG�w6dc\�̙3�J*:��c>\����\�2�S|�\�C\�\�y \�<h��)��5D\�\�H\�[�\�\�UtE\�|6\�\�q�@s\��}N����@ȃt�3g�5@߬vdꢋ.*\�z\�\�x��\�ϖ�\�\�So��f�\�|�|�\���W�XQ��\�ioo\��\'��\�[�n]r\�7n\\�j\�+˖-ۛ�\���`���\�=�\�7.��⧾��oݜ��a�+:�\��<�}���\�@���<��@��������\�\�g�Q^�n]yǎ\�zl۶�|\�\r7�S/S��gܚ�\�n�\��\�/�|ogggy\�ڵy��}���0>\�ܹ��\�C����\�\�\��vWW׶ŋ�m\n(:�\��<P�\�9u��t;\�D\�y \�5S\�̛7\�b�\�o~3\�?\�\�\�_._v\�e��\�s\��pC�Y�\�\��\�\�_���魷\�\Z��\�#���򕯼�dɒ\�\�Ν{�\�\�(:�\�\���N\�9u���\�\�<��#Tņ*�N\�w\�}u�%\�ٴiS�}\�Xe\������s\�)�_�~L?�O~��E�v;j�\�(:�\�\��\�#��u��l,�8վ�S\�<��`��\Z<\�/k�ʏ?�xy<ęx��z\�ܹ9��*\��~�t\�\��O<ѐ2\�[\�\Z�\�g�}��LEG\��|6��t�N�\����8�q�������\�y \��0\�\�#T\�\�P�h܊7��G���\Z\�9��\��-^��\�x\�-SD\�Qt\�g�\�|n\"iA�N�Y8��)�a!U\\@�qf\�sҩE�\�\�Cy \���n�>x��8\�o\"ĩ�\�gU�6�k�┿F��v\�\�\�3\�\���*���c>\�s�Ye!SmT\�B�\�߿�\�ߟ���\�y \�A��ToJ\�\�k��Sq�-u�Ap\�\�/�o\�5T\�\�\�\�\�=��ͦ���\�φ�<\�Nj���3�]Hճ��\�3>[�@�8����uv�F�A7����\�\Z���?\�w\��\�.�Y\�\���\�{\�i����c>\��$J�ڴ׹ \Zj!5�T\�\�3[�@L�\�*kd֧�&އj2\�|�\�ŦjÈ�R\��P\�m\�\'¯~��7�Rt�\�0�\'\�\�:,#]H�eU\�C�R\�4��B\�y �\�\�\�.^KU\��6�~śVd?ӡ\���[�n]7��\�s-Z�b\�\�Xk\n�\�7�Tt�\�0�?ת�\�R��\�.�\Z��*.������@ȃ\�l��?8552]tQy2�X��\�T\r�ݍ7޸�n�\�r�\�Տd?\��\�}Z<�\�\�\�z���\�φ�ܸ�Ve�2\�)>�.�\�\��Tr@��#\�<��\�Vq���[o�uR��\�o�}��\�~\�\���Z�ꕸ/�D��\��-Η�=�\\1\�\��+:���l�\��ϵ*��\�񭶐�]�P\�ߔ��<��@�f[e�mN_\��&������\�Ҳe\��N���/��\�\�\�{\�\"t�\�5\��\'���\�φ�\\�\\0���j�F/�*��ϖ�<�\�졶U�\�\��g�}vR��\�۷\�\��㖁{�\�\�2��\�\�cZ}r;�8<�\�(:\�a>\�?\�Z�����7�W�=ҿx�Ѧ\��\�y \��h�U�\�qW�Ŀ_�yF��#xa���\�4\�0\'\�>\�#�y�FV\����k���L\����R}3\�<�yP9�A]M\�ߞ\�#U�w\�~ё�!;�-�\�g\��\'�\�\�q\�\�#�<�\�\��\�ߖ����\0��~0\���\�͛7o\�\�<�\���|�\��\��\�sO�\�\�~w]����\�]w�=y\�|>ך[q��Pϋ�\�}�\��ͩ��\�ܻꪫ�}΅^�~\�-�\�G\�\�\���L7n�5����\\C\�\�ԁx��\�y��W\\�Ϝ�Zs\�]w�?��\��\�k/��B��x,\�\�\�>��|\�s��}\�{\�\�\�{G͋\�\�\�ۼys��\�N\�Ǚg�����Z�\��\��(\�_|N�\�5k~\�y0\�۪��D�\'c>R�t\�\�7&���,�~皪���2\0Z\�{������\�\�~6��u\�!�R��\��@�\�@��\�\�\�7X�\�\���\�\�n�-���矯Y>�\�O[0\��\�?w\�q��w�)�\'�pB>\�k\�{l�����\�_�r��\�\"tz\�\�\�F4LC\�\�4\�>�я\��\�#�\�\�{<�\�/~qp���̟�q\�\����p:��\�\�\'�|r�y����\�G>�\����#}~\�M7\r~�$MU\�\�Z\�Bd,�Z�I�v��F-�\���W[�k�\Z�\��\��\�/��ɾ>�	�l6�A*�J\�O<1�}�\�\�Ԕ\�a�߈�z�\�\�Z��\�\�N�bǇ>���u�Z\�ӌ�!\��do�\�5U۶m�Z\�Te\�\'\'�\�?�\�O\�\��L�\�mmmsj��.B\���?�a�y��QG�\�\�*C:\�\�饗�\�Ul�\�c4[C�[Qpj}=\nV<\'\n\�\�?��(~\�\�ſ_�\�K��\���\�}��\�\�\'>���\������j�\�\�\�Mv,�RCG�>��\�9\�����\�\�%�`\�|�\�<`\�w�����;�^qA�կ~5��駟�7?��O\�\�+>OG\��[�\�׬Y��j�\\+\�����3#}C\�z\�\�F\�x\\ j�W_}u�#\�qfC�]�\�\��Zz�x�\�\�{\�r\'c��\��\�,��MN44\�d�w\�y�\�dI;\'�/_�ߑ��>\�:͘�@L���\�w���\�+o�\���\�\�w{��|\��7e��}\�c5�TŢ2���{\�\��ϋ{��S�az\�{\�S�g�S#\�9\�~>�/��\���8�/�\�_|~4_\�\�)\�-�u��[ݲ�>�ڼ+��裏Ώn\�z~,�b>\�\�\�h�bA;\�c�9&\�Q<\���h\��pѢE�\�q\nP,���\��4v\�a�\"/>��ұ�ڑ�\�̵\Z{�\�YH��<��:P\��5!y�u��E\��?�\�3�MU�W\�q�)��oV6U�\�e˖\�\�+�\�%K�\�^y\��z\�Rqjp옌���{\�\�p׮]M��@L��*�OU\�t�R\�S�����g�\�?��\�[v�y�\��>�?l:2\�\�T�=eC�J{\�\�ް�s�\Z��\�|f� \�Q�b+�8�0�n4m�\�T\�\�ߊEo4Uq�*��KD��\�(5�Q\�*��E��&+\nW�땍��\���\�Í����\���<�L�\�=\���XO/-6\�\�9{\�c�v�߹Xt�SU\�)��y�\�\���qU\�Bj��F.��\�x\�Au*��\rQ\��\�\�\�_��%Ճ/���T\��8�7�n\�N�\�#B�<�\�~MU���X���\�\�~7�[�1jd|^�͔�@4A\�ujjd\�r�\�\n�}ّ\�\�˗���zh�N��9�\�6YZN���V�\�xzæM����*�b\�\�\�\ndq�_)��\��\�-Ne�\� �\�*^p\\���\�[N���q\Z�E\��n�\�\�;�\��EJ\�Ս�5-��{�\�܊\�+\�\�t�`�SI\��t\�iqOs\\��\�~�y��\�[\n�W�>��jBR���j\�B*~�ў�4-� ��h�\�u�Q\'*O�\�\�)��4U1b\�I|\�\�?>o�R����\�*jV\�H4R1Ҏ\�bm�:MO\�ѳf\�y &[{{��Y��753Qw\'\�Ν;\�\�o��CG�8묳\�^p�{\'\�hծ\�|x\�\�\�T�\���\��#\�ө\�S\rZj�n�\Z�➾U�V\��y\�NLq:a쉋��0�be1,�t\�{�[Q�\�)[���8\�&5NÝ�Wk.ǼJ{�G\�Tő�(Žѕ���H\�F\Z\�sC\�B\�\Z�\�.�Ʋ�J�\��`\�#v�T6P\�w(\���y�MU�F��\�ѡt\'\�\�V�gt�e�뜚-?\�<h��U\�S3�v\�\�Ii�\"7�oN\\\�`ɒ%[ׯ_?�?`\\�G�\�<\�\�\"t���ts�\�#U\�\�\�\�;!\�\�q��\�T�������U^�_l�\�\�7N\�F/\�eO{ \�\�\�[n\����S\��H?\�P7Ͱ�>MU=\�T\�a:R�\�w\���[�\�\��\�O-�E\�p\�D\�B(-\�bgE,���\"��\Z��T�=\�ǰ�\Zj!uf�\�\�l�\�I��<H;ӊMI\�\��Sq\�Pj~F\�T\�\r�b]�\��}㱨7qG\�\�\�T�^�ƥ�h\�T5[~\�y\�⺫\�Мq\��Q��Yo9Uh�\�\�\�~xѢE��x\�q��_��\�v�\�v�ees.B+��Z�\��\�\�=t��T\�cq�m\���=\0�)+�~\�R��8.��\�$\��x�(x\�#g��V~\�h�R��kT4USc>\�܉R�\�z\��\�tzN�ke\�\�\�՚���\�U\�\��\�0-\������\�\�\�-\�c�㗦j\�U;���\0\Z\���\�s\���S\��\�Bw��nP� F���W\�\���\r�\�w�+~-��B�~7��|�\�R3S펴q�S��D�V6s~\�y\�DG�\�x\�e��\'�\���W�\���-X�`��%K�\�\���\��ټy�q\�_s/B\�4���i�\�Ğ�b��iq�A��k�Jڸve�7LM����X\��WQ�\��8b�=o��.N,)K�J4���5US\�H\�h�\�Ƽ�=ԕ����Jwl�����\�\\S5)�\�k�l`O�X\�_f�\�N9�-\ZF\�x\�ȷڵ�iG\\:M��b\�L\�|�l\�\"\'j���I\�n�\��\�f\�y �\�i��vJ�=�\�\�	i�bY�w�\�e�G\r\�ϟ�h�\�ů6\�U�������+�H�/B\rE\�|6�\��w\��^�@\�yP\�Ѫ+Ss��\'y\�3\�\�K�`���MUϘ�Ytz�M_��\�;_\�\�]\����\�\r�3r���\�(:�\�\�s��=\�\���^�@\�yP�8\��x\Z`4V\����\�P-\\��O��\�\�s\�\��\�Fu��7o^ߒ%K^|�\�w\��>T�\�{�lKܔ\�5T�����l>O��\�x.p\���GZ\�y F k��&\�\'�,6Vq*`#���#`G����Ӎ7\�檽�}\�U�~��\�\�۷\�s�\�\�ݻ_|\�~\�\�\���+W��\'�\�>\�T�r�?EG\��|6����:�\'�\�c��<��@\�y��\�X��W��q3���R�_CU\�)���uf�Э\�x:{��=?@\�S�\�q�AC\�1�\r�yj-�\�^;쑖�@ȃ1�\�\\l�\�v\�\�֭+\�ر�\�7��;�o��N�\�\�(:��c>\�s\��$7\�ԟ�P�@\�y0vq\\kk\�\�F(_�\�\�ʷ\�rK~\�նm\�\�o��f\�@\�\�\�۷\�\�L\�y\�\�+V�\�͛�v�\�\�Ӱk�PtE\�|6�i\�\����\�<��@4\��\�\�7Wk�F1��\�\�(:��c>\�s]�Y��{\�\�%\�<�㤵�u��\�G{\�m��FjC6\����c(:\�a>O\�B����z\�y \��ioo?8\�*N\�8������?oΞs}\�H-�;w\��):��c>\��\�\��ָ��<��@L��y:)�rj���c>\�ss.��Z \�l�\�/\�C\�F\�T\�Q�f\�+���\�φ�\�|j���\�w���@ȃI�5Q3*���k��2(:���l�\�͹�*\�N�^\Zy \�<�Ԧj}�;��!���c>\�ss/��lq�ϴȃ\�^{��裏\�\��<��[C5�֝\0\�N^!,B�\�0��Ӭ(\�-.B�y\rR�#\�\��<��[Su\��Q\�\�\�\"T\�1�\r�y��\�OS��BL�k?c���rm����l�\�Q�@S%\��_��#x\�_wTt,B�\�0�-�䁦J\�l,B-B1�\�gEM�<�\�(:��c>�\�(ښ*M���@\�1�\�0�my������@\�1�\�0�my����\0\��P�P�\�0�my����\0\�E\�\"�\�|V�\�Ti�\�����\�\�\�3��<\�T!lC\�1�\r�Yі�*y l��\�\�\��k��\'+:{\�J�\�|Fў^y0\r�*y ll�\����;wZ\06�ؾ}��Ί\�cf��l>�hO�<x\��\��)>�\�qy �=�$�����\���\��\�n�\�ۃ�����l�jV�\�\�3��<�\�ۃ)&�\�6l\��\�\�gc\�G�\�)8\����-\�����ɚ\�\�crϜ9� ��h�\0\�\�\�\�\�zR6��\�\�nkk�\�m@`{@}M\�1�\���h�\0\�F>�g��TidM\�l��h�\0\�F6�\��t�ʵU��\r\�l~2\�w�*����9^!@\�\��=�\�\�P\r�\�^!@\�\��Ԟ\�5�R��\nP�y�\�\�O\��C5T\�(ڀ<��\0��kr\r���\�\0r\r���\�\0r\r�\��\�\0r\r�\�`rr\r�\�`r\�5@\�`r\�5@\�`r\�5@`{�\�\r\�5@`{�\�\r \�\0y`{�\�\r \�\0y`{�\�\r \�\0y�\�\�\r \�\0y�\�&7 \�\0y l0�\�\Z l0�\�\Z �=0�\�\Z �=�\�\�\Z �=�\��k�<�=�\��k�<�=�\��k�<���\��*\0r\r�\�`r\�5@\�`rS[Ww\��=�9זf_pm\�Q���\�\�tH皾k\��v��]Ww\�?�\�GGw��.^��/\�\Z`�c{�\�\rL;��\�?<>v��\�Z\��㿊f���\�������qZ_�ژ���#\�\0\�\�Ln`Z\�\�.]\�qM\�a�kJǟ��\���\�\����R�=�݃\�WO�\�w�\���G��\��X\�*�X\�\�`r\�R4S+W�|W\�,]�lM\�خ���H_�S��ϋ�\�}W\�Ǖ+��\�;��:�tm\�_w��Z\�\Z`�c{�\�\rL;\�Du�鿺�����~*N츦�\��Z:�5\�Tu�_�7R\��޹��tQGO�?\�uX\��N�k�u�\�&70\�\��S麩�\��+\�U\�4��7[=��\��+\�\�\�\��ƈǢ�ʛ�\��e��\�\Z`�c{�\�\rL+yC�5N\�$ő�8�/o�zJ��=�K�ynOiNWw\�\�վOђk�u�\�&70-�#LqÉ��K\�ǝ��?g�u�,4U�t�)ͪv;�Ξ�r\r�α=�\��gS���\��Z�\����\��ǎkz��\�\�\�\�\�}\�w�\��\��s:�UM\�l-�k�u�\�&7�\\\��&7�\\\���܀\\\���\�\0r\r���\�\0r\r���\�\0r\r�\�`rr\r�\�`r\�5@\�\��\�0\�0\�@V{�*\0@\��\�\0��\r �\�3\0(\�\0\�g\0P��\�\0��\r �\0E@>\0�6\0�\0m\0\�3\0�h\�g\0@\��\�\0��\r�|\0E\0�\0�6\0�\0P�\�3\0�h\�g\0@\�@>��\r�|\0m\0�\0(\�\0�\0P��\�\0�h �@\��*\0\�g\0@\��\�\0��\r�|\0E\0�\0�6\0�\0P�\�3\0�h\�g\0@\�@>��\r�|\0m\0�\0(\�\0�\0P��\�\0�h �@\�@>\0�6�|\0m\0�,�@\�@>��\r�|\0m\0�\0(\�\0�\0P��\�\0�h �\0E@>\0�6�|\0m\0\�3\0(\�\0\�g\0P��\�\0��\r �\0E@>\0�6\0�\0m\0\�3\0�h\�g\0@\��\�\0��\r�|\0E\0�\0���\r �\0E@>\0�6\0�\0m\0\�3\0(\�\0\�g\0@\��\�\0��\r �\0E\0�\0�6\0�\0P�\�3\0�h\�g\0@\�@>��\r�|\0E\0�\0(\�\0�\0P��\�\0�h �@\�@>\0�6�|\0m\0�\0(\�\0\�g\0P��\�\0��\r �\0E@>\0�6\0�\0m\0\�3\0(\�\0\�g\0@\��\�\0��\r �\0E\0�\0�6\0�\0P�\�3\0�h\�g\0����a�ќC�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�I��\0\�;dk\�J\0\0\0\0IEND�B`�',1);
/*!40000 ALTER TABLE `act_ge_bytearray` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ge_property`
--

DROP TABLE IF EXISTS `act_ge_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ge_property` (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ge_property`
--

LOCK TABLES `act_ge_property` WRITE;
/*!40000 ALTER TABLE `act_ge_property` DISABLE KEYS */;
INSERT INTO `act_ge_property` VALUES ('batch.schema.version','6.7.2.0',1),('cfg.execution-related-entities-count','true',1),('cfg.task-related-entities-count','true',1),('common.schema.version','6.7.2.0',1),('entitylink.schema.version','6.7.2.0',1),('eventsubscription.schema.version','6.7.2.0',1),('identitylink.schema.version','6.7.2.0',1),('job.schema.version','6.7.2.0',1),('next.dbid','1',1),('schema.history','create(6.7.2.0)',1),('schema.version','6.7.2.0',1),('task.schema.version','6.7.2.0',1),('variable.schema.version','6.7.2.0',1);
/*!40000 ALTER TABLE `act_ge_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_actinst`
--

DROP TABLE IF EXISTS `act_hi_actinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_actinst` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `TRANSACTION_ORDER_` int DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`) USING BTREE,
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`) USING BTREE,
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_actinst`
--

LOCK TABLES `act_hi_actinst` WRITE;
/*!40000 ALTER TABLE `act_hi_actinst` DISABLE KEYS */;
INSERT INTO `act_hi_actinst` VALUES ('03ce57248d066a3cbf33d8c112e8d8c8',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','2ca32e0c26b8581d475842a1fb90b8ef','applyNode','b3fdebe0c00f0c293b16bda52fab48b1',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:49:53.234','2023-09-15 20:50:16.209',1,22975,'Change activity to endNode',''),('067a0ba9bbfd2b01cb88193da59019b2',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-18 19:00:41.621','2023-09-18 19:00:41.623',1,2,NULL,''),('0734deaa04b998a819f57cc2a38a3480',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','05fccf74e5499bdd34c768a84c314896','applyNode','046ac3cb8721b39d3ff0afc3cd18152f',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:16:53.062',NULL,1,NULL,NULL,''),('0b7d6fa3668bae53e7f05ed37dfaa437',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:57:04.655','2023-09-11 21:57:04.661',1,6,NULL,''),('0bd8cbb5-5249-11ee-9316-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','mangerNode','9888383b5a147f488efd2bc4cd0dd9e2',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-13 22:22:17.491','2023-09-13 23:20:18.144',2,3480653,'委托',NULL),('0c1723d9-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','3b18cd53bc89c3fff32b094b0b6939cf','Flow_1wxc6l4','5742df4c2826308d73db85d9184bfe55',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-12 22:38:09.737','2023-09-12 22:38:31.237',2,21500,'同意',NULL),('0c172f34-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','3b18cd53bc89c3fff32b094b0b6939cf','Flow_014zhnw','5742df4c2826308d73db85d9184bfe55',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-12 22:38:09.737','2023-09-12 22:38:31.237',2,21500,'同意',NULL),('0c17fdfe-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','3b18cd53bc89c3fff32b094b0b6939cf','Gateway_08fh2up','5742df4c2826308d73db85d9184bfe55',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-12 22:38:09.737','2023-09-12 22:38:31.237',2,21500,'同意',NULL),('0c2621849a8d870a238ccb0766cd9bda',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:25:27.267','2023-09-11 21:25:27.275',1,8,NULL,''),('0d62c184-561b-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','4655b44e3e75e87d6c34bc401df1ddec','mangerNode','d25650926a27824c9146bf8e414cca74',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:59:14.795','2023-09-18 20:01:08.392',2,113597,'同意',NULL),('0d62c4b8-561b-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','4655b44e3e75e87d6c34bc401df1ddec','Flow_0mw6ofi','d25650926a27824c9146bf8e414cca74',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 19:59:14.795','2023-09-18 20:01:08.392',2,113597,'同意',NULL),('0d76456a70b46b9ebe58c2e4a12f7820',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:49:02.394','2023-09-11 21:49:02.404',1,10,NULL,''),('117eb924c45bcbcb0a6b9d11a888b5e5',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','applyNode','d7fdd950a11638d9fb65d8accdd098e0',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 22:00:49.044','2023-09-14 22:18:44.368',3,1075324,'Change activity to mangerNode',''),('12ff1eb59f7ae6e958b5e67222398562',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:51:52.739','2023-09-11 21:51:52.739',1,0,NULL,''),('13941faf5918d094012ac27a6e9e2a71',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a15dc5534f126d84a6437e2897d8c397','deptLeaderNode','b4835657e917e572f1cef1e11ba8810d',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:01:19.459','2023-09-18 20:01:32.893',2,13434,'Change parent activity to applyNode',''),('14143aee-561b-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5','Flow_1wxc6l4','03f82c30eb7300efe00f29a221ea2bc9',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293','2023-09-18 20:01:19.623',2,11330,'同意',NULL),('14143e08-561b-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5','Flow_014zhnw','03f82c30eb7300efe00f29a221ea2bc9',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293','2023-09-18 20:01:19.623',2,11330,'同意',NULL),('14143ef2-561b-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5','Gateway_08fh2up','03f82c30eb7300efe00f29a221ea2bc9',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293','2023-09-18 20:01:19.623',2,11330,'同意',NULL),('14f7910c97da7da57ab9f70b79403b68',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','applyNode','2e76ed0b06f0306b43ae073ef7b62719',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:42.924',NULL,3,NULL,NULL,''),('1762d972acd32ad74eff705f72df4c85',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b','deptLeaderNode','ed3c147ac8082ec56206d93a5d82989b',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-12 22:38:31.127','2023-09-12 22:42:44.108',1,252981,'Change parent activity to endNode',''),('207024738290b8a074b02917afc3d0c0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 22:08:06.464','2023-09-11 22:08:06.470',1,6,NULL,''),('213b203d2a39126126e0b90215948973',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403','applyNode','dd83cde660ec9b26ad7a27e3ab8da95c',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 21:56:26.634','2023-09-12 22:38:09.721',3,2503087,'Change activity to mangerNode',''),('289e6fff24ebc790e0fbbdc612cee2e4',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','3ea49e2387678e791f3cc42cbd62bdd1','mangerNode','1e0d31a3f4452161a51320a448fc753d',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:53:17.102','2023-09-15 21:08:17.605',1,900503,'Change activity to deptLeaderNode',''),('28f49711633b7ca022c15f7d19afb705',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','8a4a43ddfdb246b27dccf3cbbcf460b1','endNode',NULL,NULL,'结束','endEvent',NULL,'2023-09-18 18:58:11.243','2023-09-18 18:58:11.247',1,4,NULL,''),('2ebff706512a9677e3682400f81b341e',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','applyNode','aebb9acc0a2518e35c32714be66758ba',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:25:27.279',NULL,3,NULL,NULL,''),('332e65497f106c0714846567c075b3c2',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','67503605cf2d047750cf17d1ec1190f8','applyNode','0a20cedab4f2ad8e3a0dc756d833d1fd',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 20:39:20.725','2023-09-14 21:39:15.451',1,3594726,'Change activity to endNode',''),('35dce84c59b15243e0697d6f361ed6e5',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 21:56:26.622','2023-09-12 21:56:26.630',1,8,NULL,''),('36879930402419facf87289e121032ab',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6e9d34ed662a1bdeeccc8e779e9c8243','mangerNode','ead9f2e0f519793db70e0831c22afdef',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:45:11.797','2023-09-15 20:49:53.213',1,281416,'Change activity to applyNode',''),('38129e5b00f2d05efbc305cdde8a9180',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','9cb9c09b8e748dd0d9743cbf3e174a41','deptLeaderNode','7ad75c81b9ba14c310316e745119bcb7',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:13:14.001','2023-09-18 20:16:53.049',2,219048,'Change parent activity to applyNode',''),('38e2e3560d8cc93c8bdfcfcfe3c8fd69',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','applyNode','3c9991d349f1d44c616ee8d724d7fb74',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:12.991',NULL,3,NULL,NULL,''),('3981face26572694302df630e28f2e7d',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:37:18.050','2023-09-12 22:37:18.050',2,0,NULL,''),('3b61e51e053db7c9835d55b80a80489c',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','2d202e18fe57628c9f77a3e136210bb8','deptLeaderNode','d6f11daa33af21fb33b0cf00d7531e79',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:01:19.456','2023-09-18 20:01:32.916',1,13460,'Change parent activity to applyNode',''),('42e0a2aab0f1068d86c41504e09bf7ab',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-15 20:09:27.481','2023-09-15 20:09:27.489',1,8,NULL,''),('49403746109d625ba2a529486dafe58f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:50:12.991','2023-09-11 21:50:12.991',2,0,NULL,''),('4eebe99d29add4a4c70aab134ed3b1b0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.863',1,0,NULL,''),('52462b9475ef8cd78cc7b59340e7df95',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','f47767b3297cdc62cbec1835e9873d9b','deptLeaderNode','304e1eba7ff0593e6697d5cfb1eeb4da',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 19:02:41.444','2023-09-18 19:59:14.724',1,3393280,'Change parent activity to applyNode',''),('54a6508d952c51bbd1910fc5d719e7c8',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-15 20:51:49.316','2023-09-15 20:51:49.316',2,0,NULL,''),('561af9d2c74eb2cb809d918319923840',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-15 20:09:27.493','2023-09-15 20:09:27.493',2,0,NULL,''),('59b29f80d5ba29f0e72880b292f32afd',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','applyNode','896bc6db02674af24a5d6f050a2f3db0',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:06.473','2023-09-11 22:08:25.804',3,19331,'Change activity to mangerNode',''),('5d83ad1a-53c6-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6e9d34ed662a1bdeeccc8e779e9c8243','Flow_1wxc6l4','ead9f2e0f519793db70e0831c22afdef',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:45:11.797','2023-09-15 20:49:53.319',2,281522,'不同意',NULL),('5d83b2d9-53c6-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6e9d34ed662a1bdeeccc8e779e9c8243','Reject_Flow_0yh56fs','ead9f2e0f519793db70e0831c22afdef',NULL,'驳回','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:45:11.797','2023-09-15 20:49:53.319',2,281522,'不同意',NULL),('5d83b762-53c6-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6e9d34ed662a1bdeeccc8e779e9c8243','Gateway_08fh2up','ead9f2e0f519793db70e0831c22afdef',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-15 20:45:11.797','2023-09-15 20:49:53.319',2,281522,'不同意',NULL),('5e01d372171990f0e9a5ba349ffdd9f2',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:51:52.739','2023-09-11 21:51:52.739',2,0,NULL,''),('5e8f49c300d46e18e45eb641e3d0f942',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','applyNode','1be90845469a774c0830dc4cba2009be',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:30:45.579',NULL,3,NULL,NULL,''),('5e9143e1f5316a85a64ae9a9ad6e5f3c',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-14 21:59:15.758','2023-09-14 21:59:15.758',2,0,NULL,''),('5ee37d9514ec23a0d0e00d4e40f71afe',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf','mangerNode','09ef88659150862142596dd67eda5a78',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629','2023-09-18 20:13:13.987',1,27358,'Change activity to deptLeaderNode',''),('634e1771c7976455d0ba760e149659ae',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-14 22:00:49.044','2023-09-14 22:00:49.044',1,0,NULL,''),('7204038084a46009705d6cc7a8619f6a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 22:30:45.578','2023-09-11 22:30:45.578',2,0,NULL,''),('764514ea3ae9787972f1fffad28652b0',4,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','mangerNode','9888383b5a147f488efd2bc4cd0dd9e2',NULL,'主管审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-13 22:22:17.490','2023-09-14 20:39:20.702',1,80223212,'Change activity to applyNode',''),('7e67c7d00fdd6e82ca9ba14a7a6d2deb',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5','mangerNode','03f82c30eb7300efe00f29a221ea2bc9',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293','2023-09-18 20:01:19.435',1,11142,'Change activity to deptLeaderNode',''),('83dacadc0c8d10766f60b47377e0a703',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:52:30.137','2023-09-11 21:52:30.137',1,0,NULL,''),('83e52e520d12dc430f748b4cc4625da4',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','applyNode','fb2577c0df7cea22e8f8a6203fa6e2fe',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:41.628','2023-09-18 19:00:47.735',3,6107,'Change activity to mangerNode',''),('8699ac0e1ced2bf686dde789a6b9723a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-14 21:59:15.750','2023-09-14 21:59:15.754',1,4,NULL,''),('86a23053dd3b5f8c7af01c8091dab965',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:37:18.050','2023-09-12 22:37:18.050',1,0,NULL,''),('86bbbf4f360f4786fea76eb58402f1c6',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:25:27.279','2023-09-11 21:25:27.279',2,0,NULL,''),('8950718a4ce7fa0ef8f7b2145a96f6ea',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','3b18cd53bc89c3fff32b094b0b6939cf','mangerNode','5742df4c2826308d73db85d9184bfe55',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:38:09.737','2023-09-12 22:38:31.111',1,21374,'Change activity to deptLeaderNode',''),('8e4e8546865fa0f313db67d317383efa',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','fee5801bd4d8cd57251025e757b2400f','deptLeaderNode','042ddaeb139fcb06547fdcda8a7ad71d',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-15 21:08:17.619','2023-09-15 23:21:03.223',1,7965604,'Change parent activity to applyNode',''),('905ea6f79b70aa4ddaef82f643b7ef9a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','applyNode','578f6b194aa3e1371e0ee45eb49e7d78',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:53:26.863',NULL,3,NULL,NULL,''),('96c354755cd056580241fc6b3ccc2a17',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-15 20:51:49.310','2023-09-15 20:51:49.313',1,3,NULL,''),('96dc51f1d1bcbdd927971988ce675374',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','applyNode','9b931808d034cb271160bc1390ccf248',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:03.755',NULL,3,NULL,NULL,''),('97dd85d02b724cb171d8e2762d61019a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','applyNode','67d94a5144c1fe8e89e70c134f9d4de1',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 21:59:15.758',NULL,3,NULL,NULL,''),('99d1384cc2989b650427b4dc6b4d7394',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','4655b44e3e75e87d6c34bc401df1ddec','applyNode','d25650926a27824c9146bf8e414cca74',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:59:14.794','2023-09-18 20:01:08.285',1,113491,'Change activity to mangerNode',''),('9af4a2a8ac0fad885fb700fad7d26772',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','applyNode','265377b48122d2ec995d6beedcf08f17',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:57:04.665',NULL,3,NULL,NULL,''),('9ca697ce-5309-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','mangerNode','d7fdd950a11638d9fb65d8accdd098e0',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 22:00:49.044','2023-09-14 22:18:44.483',2,1075439,'同意',NULL),('9ca69a7f-5309-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','Flow_0mw6ofi','d7fdd950a11638d9fb65d8accdd098e0',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-14 22:00:49.044','2023-09-14 22:18:44.483',2,1075439,'同意',NULL),('9f5e29c6dee479bf560c7d89e0de3111',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:03.755',2,0,NULL,''),('a08a7ad0f711c632068355d1782f7a2b',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','390a2bd8ff5147f12a1a792ca883ae9e','endNode',NULL,NULL,'结束','endEvent',NULL,'2023-09-15 20:50:16.218','2023-09-15 20:50:16.223',1,5,NULL,''),('a14c934de8c1506137438112b4d00d5e',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710','applyNode','6faa03d0f5a26f404ad19a16c1c380ae',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:51:52.739','2023-09-11 22:09:28.952',3,1056213,'Change activity to mangerNode',''),('a2e8ac61-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b','Flow_0i3x4ud','ed3c147ac8082ec56206d93a5d82989b',NULL,'','sequenceFlow','00b6fea67bb10175f39158cb9e00db1e','2023-09-12 22:38:31.127','2023-09-12 22:42:44.273',2,253146,'同意',NULL),('a2e8b018-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b','Flow_071mxw1','ed3c147ac8082ec56206d93a5d82989b',NULL,NULL,'sequenceFlow','00b6fea67bb10175f39158cb9e00db1e','2023-09-12 22:38:31.127','2023-09-12 22:42:44.273',2,253146,'同意',NULL),('a2e8b0f7-517a-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b','Gateway_1bsklt0','ed3c147ac8082ec56206d93a5d82989b',NULL,NULL,'exclusiveGateway','00b6fea67bb10175f39158cb9e00db1e','2023-09-12 22:38:31.127','2023-09-12 22:42:44.273',2,253146,'同意',NULL),('a2ffe936-5612-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','mangerNode','fb2577c0df7cea22e8f8a6203fa6e2fe',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:41.628','2023-09-18 19:00:47.822',2,6194,'同意',NULL),('a2ffecdd-5612-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','Flow_0mw6ofi','fb2577c0df7cea22e8f8a6203fa6e2fe',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:41.628','2023-09-18 19:00:47.822',2,6194,'同意',NULL),('a30399a5-50ad-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','5b679daadec6e5c140f986aebc7c70d6','Flow_1wxc6l4','ee4aa48739d93756d7e72e56fd37510a',NULL,'','sequenceFlow','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:09:28.958','2023-09-11 22:15:17.456',2,348498,'请批准',NULL),('a3039f5e-50ad-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','5b679daadec6e5c140f986aebc7c70d6','Flow_0vhm2e4','ee4aa48739d93756d7e72e56fd37510a',NULL,NULL,'sequenceFlow','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:09:28.958','2023-09-11 22:15:17.456',2,348498,'请批准',NULL),('a303a09f-50ad-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','5b679daadec6e5c140f986aebc7c70d6','Gateway_08fh2up','ee4aa48739d93756d7e72e56fd37510a',NULL,NULL,'exclusiveGateway','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:09:28.958','2023-09-11 22:15:17.456',2,348498,'请批准',NULL),('a46604261b3ca89037e5874e70b2bb39',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1','applyNode','5bcc6d4fafc02f1f64c7b3ffcc808329',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:09:27.494','2023-09-15 20:45:11.777',3,2144283,'Change activity to mangerNode',''),('a6928bfa2cc48cb6b16c971f619d7b92',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:49:02.408','2023-09-11 21:49:02.408',2,0,NULL,''),('a73ea3a5ad885c6da1e4f2d0bdc83394',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','9dea8978754d867d28e261c3429eb799','deptLeaderNode','251de12881c8555f6c1f63f61a4c4be3',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:13:13.999','2023-09-18 20:16:53.038',1,219039,'Change parent activity to applyNode',''),('a811a11ffaae6af75c547dfe682c96a0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','a420dc015b277aba101e81ee2a429c52','endNode',NULL,NULL,'结束','endEvent',NULL,'2023-09-11 22:15:17.311','2023-09-11 22:15:17.314',1,3,NULL,''),('abc52aaf48c8e811826529b0e5ac613e',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:50:12.990','2023-09-11 21:50:12.991',1,1,NULL,''),('ac4044adaae01a457a45dfd262cfe0f0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','98eb86b1fbe6e10b65ed8d6ebf6dcf86','endNode',NULL,NULL,'结束','endEvent',NULL,'2023-09-12 22:42:44.141','2023-09-12 22:42:44.144',1,3,NULL,''),('ad99f866-561c-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','95a49737bdee7652e74ec5eb0c0a922c','mangerNode','d413ed765799eaa1c7ea6abb70988d56',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:32.950','2023-09-18 20:12:46.693',2,673743,'同意',NULL),('ad99fde3-561c-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','95a49737bdee7652e74ec5eb0c0a922c','Flow_0mw6ofi','d413ed765799eaa1c7ea6abb70988d56',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:32.950','2023-09-18 20:12:46.693',2,673743,'同意',NULL),('adb7f920-50ac-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','mangerNode','896bc6db02674af24a5d6f050a2f3db0',NULL,'主管审批','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:06.473','2023-09-11 22:08:25.918',2,19445,'请批准',NULL),('adb7fe55-50ac-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','Flow_0mw6ofi','896bc6db02674af24a5d6f050a2f3db0',NULL,'','sequenceFlow','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:06.473','2023-09-11 22:08:25.918',2,19445,'请批准',NULL),('ae8c76c66ed7ff9aa4859c0bb0288aa9',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:42.924',1,0,NULL,''),('aea789074cbe2607a54cfc58aef468db',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','5b679daadec6e5c140f986aebc7c70d6','mangerNode','ee4aa48739d93756d7e72e56fd37510a',NULL,'主管审批','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:09:28.958','2023-09-11 22:15:17.288',1,348330,'Change activity to endNode',''),('b3708f559d2bea9ea1954d2dcc32b194',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 22:30:45.566','2023-09-11 22:30:45.575',1,9,NULL,''),('b4308fdc66d09d701a873472f30e5e41',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','95a49737bdee7652e74ec5eb0c0a922c','applyNode','d413ed765799eaa1c7ea6abb70988d56',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:32.949','2023-09-18 20:12:46.618',1,673669,'Change activity to mangerNode',''),('b4added7e63914aa7a73d44de102f9d1',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','d5eb46d7099e367012959854ae72759f','mangerNode','1800d422aaf3f7847dbca50ae6ff861d',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 22:18:44.373',NULL,1,NULL,NULL,''),('b5c8471a-53c5-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1','mangerNode','5bcc6d4fafc02f1f64c7b3ffcc808329',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:09:27.494','2023-09-15 20:45:11.915',2,2144421,'同意',NULL),('b5c84a6a-53c5-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1','Flow_0mw6ofi','5bcc6d4fafc02f1f64c7b3ffcc808329',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:09:27.494','2023-09-15 20:45:11.915',2,2144421,'同意',NULL),('b6f1f51af9ab4d7d33734c39687d9b97',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','60ced1591f149f909e5ea13d1819087a','endNode',NULL,NULL,'结束','endEvent',NULL,'2023-09-14 21:39:15.456','2023-09-14 21:39:15.456',1,0,NULL,''),('ba1d6ef9-52fb-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','Flow_1wxc6l4','9888383b5a147f488efd2bc4cd0dd9e2',NULL,'','sequenceFlow','9a8ccb159ac609b176825e9a6b4039d3','2023-09-13 22:22:17.491','2023-09-14 20:39:20.864',2,80223373,'不同意',NULL),('ba1d7304-52fb-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','Reject_Flow_0yh56fs','9888383b5a147f488efd2bc4cd0dd9e2',NULL,'驳回','sequenceFlow','9a8ccb159ac609b176825e9a6b4039d3','2023-09-13 22:22:17.491','2023-09-14 20:39:20.864',2,80223373,'不同意',NULL),('ba1d743f-52fb-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','Gateway_08fh2up','9888383b5a147f488efd2bc4cd0dd9e2',NULL,NULL,'exclusiveGateway','9a8ccb159ac609b176825e9a6b4039d3','2023-09-13 22:22:17.491','2023-09-14 20:39:20.864',2,80223373,'不同意',NULL),('babb262f7eb2bc409f8a9d6dd74a36ef',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','applyNode','0a413d9c6ef53463e2e21852493e9790',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:33:59.049',NULL,3,NULL,NULL,''),('bdf010a8-561c-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf','Flow_1wxc6l4','09ef88659150862142596dd67eda5a78',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629','2023-09-18 20:13:14.102',2,27473,'同意',NULL),('bdf01434-561c-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf','Flow_014zhnw','09ef88659150862142596dd67eda5a78',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629','2023-09-18 20:13:14.102',2,27473,'同意',NULL),('bdf0152b-561c-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf','Gateway_08fh2up','09ef88659150862142596dd67eda5a78',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629','2023-09-18 20:13:14.102',2,27473,'同意',NULL),('be35da3a9dcec515253e925c944572ed',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','applyNode','d2bda77e898d40290f8e7d96ec70cf17',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:52:30.137',NULL,3,NULL,NULL,''),('c32d8234c6f06c9f7b178c5f59d63d36',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:52:30.137','2023-09-11 21:52:30.137',2,0,NULL,''),('c5506344997b284e809e0aa697b953ac',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4','applyNode','860bb68fe25317abfb84d13c6736117a',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:34:30.122','2023-09-13 22:22:17.467',3,85667345,'Change activity to mangerNode',''),('c8cb773877ad374198acebb46d0e1b1a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-14 22:00:49.044','2023-09-14 22:00:49.044',2,0,NULL,''),('c98ef4e8cec65272051be1f692989d66',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:03.755',1,0,NULL,''),('cd537922-52f9-11ee-94a3-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51','mangerNode','9888383b5a147f488efd2bc4cd0dd9e2',NULL,'主管审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-13 22:22:17.491','2023-09-14 20:25:34.098',2,79396607,'转办',NULL),('d190af06f3d9db385f421a93c9354cae',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 21:56:26.634','2023-09-12 21:56:26.634',2,0,NULL,''),('d357554d-50ac-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710','mangerNode','6faa03d0f5a26f404ad19a16c1c380ae',NULL,'主管审批','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:51:52.739','2023-09-11 22:09:29.040',2,1056301,'请批准',NULL),('d357584d-50ac-11ee-a373-0242ac110003',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710','Flow_0mw6ofi','6faa03d0f5a26f404ad19a16c1c380ae',NULL,'','sequenceFlow','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:51:52.739','2023-09-11 22:09:29.040',2,1056301,'请批准',NULL),('d4143849b075014a17f26984fa6aa45b',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:42.924',2,0,NULL,''),('d519f9d4436653cdda419e6b378eecca',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','df56e6bf53ded205fe1db016a01818a3','applyNode','15a5091105cb9b1ec2685d20ef986486',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 23:21:03.274','2023-09-18 18:58:11.212',1,243427938,'Change activity to endNode',''),('d7058fa0-53c6-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430','mangerNode','cb3dbea44e2dda84e6413791f83c8119',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:51:49.318','2023-09-15 20:53:17.178',2,87860,'同意',NULL),('d7059f68-53c6-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430','Flow_0mw6ofi','cb3dbea44e2dda84e6413791f83c8119',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:51:49.318','2023-09-15 20:53:17.178',2,87860,'同意',NULL),('d84a9d233a79b32f8f9ecd633d3bc27f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:33:59.049','2023-09-12 22:33:59.049',1,0,NULL,''),('d9c728ec368de13cb2e518db84eddf6f',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3','mangerNode','d870f96180591307aab4f7256dd44306',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.741','2023-09-18 19:02:41.429',1,113688,'Change activity to deptLeaderNode',''),('e074256a865a4db1fbe6d9beb2d8d284',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','e1045e978e8fef0ac700073b5115f8fa','mangerNode','c28cc4716c70d343fb9adb4de728bfea',NULL,'主管审批','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:25.819',NULL,1,NULL,NULL,''),('e201f1a92aa5745b375aabfe65185104',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:33:59.049','2023-09-12 22:33:59.049',2,0,NULL,''),('e31bd7bc50ec5bab67a93b611cf94b2c',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:34:30.122','2023-09-12 22:34:30.122',1,0,NULL,''),('e59890453140325179407e622dc4ce3a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','applyNode','3e55fbc84ca581895a74a36f66149acb',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:49:02.408',NULL,3,NULL,NULL,''),('e6cead8d-5612-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3','Flow_1wxc6l4','d870f96180591307aab4f7256dd44306',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.742','2023-09-18 19:02:41.579',2,113837,'同意',NULL),('e6ceb410-5612-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3','Flow_014zhnw','d870f96180591307aab4f7256dd44306',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.742','2023-09-18 19:02:41.579',2,113837,'同意',NULL),('e6ceb54c-5612-11ee-94d2-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3','Gateway_08fh2up','d870f96180591307aab4f7256dd44306',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.742','2023-09-18 19:02:41.579',2,113837,'同意',NULL),('e881225b01e8ebeff73f50e2006536e2',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.863',2,0,NULL,''),('ec067be7f432f4d104d41ea96767a720',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','37e99c1669a1c61f10d7fcee86116e03','deptLeaderNode','64131a55a62f212ce843f08d6adfe365',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 19:02:41.448','2023-09-18 19:59:14.758',2,3393310,'Change parent activity to applyNode',''),('efc72760-53c8-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','3ea49e2387678e791f3cc42cbd62bdd1','Flow_1wxc6l4','1e0d31a3f4452161a51320a448fc753d',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:53:17.103','2023-09-15 21:08:17.713',2,900610,'同意',NULL),('efc72761-53c8-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','3ea49e2387678e791f3cc42cbd62bdd1','Flow_014zhnw','1e0d31a3f4452161a51320a448fc753d',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-15 20:53:17.103','2023-09-15 21:08:17.713',2,900610,'同意',NULL),('efc72762-53c8-11ee-bf5f-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','3ea49e2387678e791f3cc42cbd62bdd1','Gateway_08fh2up','1e0d31a3f4452161a51320a448fc753d',NULL,NULL,'exclusiveGateway','78f97db459233bc461df091b862fb7e3','2023-09-15 20:53:17.103','2023-09-15 21:08:17.713',2,900610,'同意',NULL),('f159a097-5240-11ee-9316-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4','mangerNode','860bb68fe25317abfb84d13c6736117a',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:34:30.122','2023-09-13 22:22:17.686',2,85667564,'同意',NULL),('f159a3cb-5240-11ee-9316-0242ac110002',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4','Flow_0mw6ofi','860bb68fe25317abfb84d13c6736117a',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-12 22:34:30.122','2023-09-13 22:22:17.686',2,85667564,'同意',NULL),('f27617a54a230a95dca72fe98caec511',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','applyNode','9b1fd3de1a8bce92eb9e93b9804c6b1f',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:18.050',NULL,3,NULL,NULL,''),('f2cd5dd6549e79fa3acd987bd2aefc68',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:57:04.665','2023-09-11 21:57:04.665',2,0,NULL,''),('f917d3912cd14a4c3e5d6fcf80ea41e9',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430','applyNode','cb3dbea44e2dda84e6413791f83c8119',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-15 20:51:49.318','2023-09-15 20:53:17.086',3,87768,'Change activity to mangerNode',''),('f9817e4c65107563b890abf079f0aadf',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 22:08:06.473','2023-09-11 22:08:06.473',2,0,NULL,''),('fa84a3eabf09c02556bca28b1d8f73f4',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-18 19:00:41.628','2023-09-18 19:00:41.628',2,0,NULL,''),('fd9fbe996466cc8a91293238fe982fd8',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:34:30.122','2023-09-12 22:34:30.122',2,0,NULL,''),('ff54c79b-5179-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403','mangerNode','dd83cde660ec9b26ad7a27e3ab8da95c',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 21:56:26.634','2023-09-12 22:38:09.834',2,2503200,'请审批',NULL),('ff54cc28-5179-11ee-b715-0242ac110004',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403','Flow_0mw6ofi','dd83cde660ec9b26ad7a27e3ab8da95c',NULL,'','sequenceFlow','78f97db459233bc461df091b862fb7e3','2023-09-12 21:56:26.634','2023-09-12 22:38:09.834',2,2503200,'请审批',NULL);
/*!40000 ALTER TABLE `act_hi_actinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_attachment`
--

DROP TABLE IF EXISTS `act_hi_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_attachment` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_attachment`
--

LOCK TABLES `act_hi_attachment` WRITE;
/*!40000 ALTER TABLE `act_hi_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_comment`
--

DROP TABLE IF EXISTS `act_hi_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_comment` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_comment`
--

LOCK TABLES `act_hi_comment` WRITE;
/*!40000 ALTER TABLE `act_hi_comment` DISABLE KEYS */;
INSERT INTO `act_hi_comment` VALUES ('0c5427f166cbdc70fbc465cf9ab21897','S','2023-09-14 22:18:44.358','78f97db459233bc461df091b862fb7e3','d7fdd950a11638d9fb65d8accdd098e0','2c43cad6e817cb50a9131351a79b6e4a','AddComment','同意',_binary '同意'),('16daa00a7f1c57e6a95a7138486ed65f','S','2023-09-18 20:12:46.593',NULL,'d413ed765799eaa1c7ea6abb70988d56','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('1c770a9f8dc825c57439acc1a06949b8','S','2023-09-18 20:01:08.252',NULL,'d25650926a27824c9146bf8e414cca74','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('1eee9b692601d0479100e9b5af6591fe','C','2023-09-18 18:58:11.178',NULL,'15a5091105cb9b1ec2685d20ef986486','2fadd67483a667cf3780274d6ad2c490','AddComment','作废',_binary '作废'),('26c74aea6a6547501cc9946efdf8b05a','S','2023-09-13 22:22:17.421',NULL,'860bb68fe25317abfb84d13c6736117a','f9e728a4640b74e0ec0f501346b794ff','AddComment','同意',_binary '同意'),('295f9464169ef3f03f3888f5044802df','S','2023-09-18 19:02:41.419',NULL,'d870f96180591307aab4f7256dd44306','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('2ad9e605893323bb1c76ce81e15d7a84','S','2023-09-15 20:53:17.075',NULL,'cb3dbea44e2dda84e6413791f83c8119','2fadd67483a667cf3780274d6ad2c490','AddComment','同意',_binary '同意'),('2eab2711f5c7fef65f66c50bee225ed6','S','2023-09-11 22:15:17.267',NULL,'ee4aa48739d93756d7e72e56fd37510a','0c0ae280fcaf4d43a4659c6d2a2e4da0','AddComment','请批准',_binary '请批准'),('31313a9d6a18ecf6df2794b7ca6663ee','W','2023-09-18 19:59:14.643',NULL,'304e1eba7ff0593e6697d5cfb1eeb4da','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','撤回',_binary '撤回'),('3aa1381c8e2ea428dfac268fbe5ae8e9','S','2023-09-18 20:01:19.410',NULL,'03f82c30eb7300efe00f29a221ea2bc9','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('3f0fbca3e3e8ea3472f12ace4299b887','C','2023-09-15 20:50:16.181',NULL,'b3fdebe0c00f0c293b16bda52fab48b1','5b05d462a25f6e49e5ff3e97c9bb41f4','AddComment','取消',_binary '取消'),('44914017d92a75a9ef0089832fef3bc9','S','2023-09-15 21:08:17.583','78f97db459233bc461df091b862fb7e3','1e0d31a3f4452161a51320a448fc753d','2fadd67483a667cf3780274d6ad2c490','AddComment','同意',_binary '同意'),('4cf486a39fc65b612fa1f30a5d827e0b','S','2023-09-12 22:38:31.063','78f97db459233bc461df091b862fb7e3','5742df4c2826308d73db85d9184bfe55','9de499a6f8d8307a34cb9640d6e63d4e','AddComment','同意',_binary '同意'),('4d9fddcf4cbf973fa52ba43258a2bde9','W','2023-09-18 20:16:53.003',NULL,'7ad75c81b9ba14c310316e745119bcb7','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','信息错误撤回重新填写',_binary '信息错误撤回重新填写'),('53a2a77130c4252402ac0e318c72552f','S','2023-09-12 22:42:44.091','78f97db459233bc461df091b862fb7e3','ed3c147ac8082ec56206d93a5d82989b','9de499a6f8d8307a34cb9640d6e63d4e','AddComment','同意',_binary '同意'),('60b36e30abf90d45c6d70d2c9819bccb','C','2023-09-14 21:39:15.435',NULL,'0a20cedab4f2ad8e3a0dc756d833d1fd','f9e728a4640b74e0ec0f501346b794ff','AddComment','作废',_binary '作废'),('660279ae859d8468f10bfba7800d55ac','S','2023-09-15 20:45:11.746',NULL,'5bcc6d4fafc02f1f64c7b3ffcc808329','5b05d462a25f6e49e5ff3e97c9bb41f4','AddComment','同意',_binary '同意'),('6c1843a9e04afc518ba594a37c025e1d','D','2023-09-13 23:20:18.139','78f97db459233bc461df091b862fb7e3','6c789455b90e6502ff4d518e3f96c29f','f9e728a4640b74e0ec0f501346b794ff','AddComment','委托',_binary '委托'),('81152c067777f85cd37390d0ddb1138e','W','2023-09-18 20:01:32.843',NULL,'d6f11daa33af21fb33b0cf00d7531e79','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','撤回',_binary '撤回'),('85408a9c4eea9f342d564cde81946249','W','2023-09-18 20:16:52.981',NULL,'251de12881c8555f6c1f63f61a4c4be3','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','信息错误撤回重新填写',_binary '信息错误撤回重新填写'),('88be68ee0e1461f1a077e018c248cc68','S','2023-09-15 20:49:53.167',NULL,'ead9f2e0f519793db70e0831c22afdef','5b05d462a25f6e49e5ff3e97c9bb41f4','AddComment','不同意',_binary '不同意'),('9c5607509163aff030523dfff56806f5','S','2023-09-11 22:08:25.779',NULL,'896bc6db02674af24a5d6f050a2f3db0','9b15f4abc8ba5dcf905885c5f73489e9','AddComment','请批准',_binary '请批准'),('9cde430cbc1ad6a09b3c91d24d81f4fa','W','2023-09-15 23:21:03.188',NULL,'042ddaeb139fcb06547fdcda8a7ad71d','2fadd67483a667cf3780274d6ad2c490','AddComment','撤回',_binary '撤回'),('9e30bf6857f3bd3366ff0d00dafbbef9','S','2023-09-18 19:00:47.717',NULL,'fb2577c0df7cea22e8f8a6203fa6e2fe','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('b353abac84362a66b8ee132db959f95d','S','2023-09-11 22:09:28.930',NULL,'6faa03d0f5a26f404ad19a16c1c380ae','0c0ae280fcaf4d43a4659c6d2a2e4da0','AddComment','请批准',_binary '请批准'),('b865ecbc4e1d7c5eeace744dea7cee50','T','2023-09-14 20:25:34.087','00b6fea67bb10175f39158cb9e00db1e','64ea0905503233a823593d39289af99d','f9e728a4640b74e0ec0f501346b794ff','AddComment','转办',_binary '转办'),('c538856aaaa3a55ca3b16fb690228641','S','2023-09-14 20:39:20.615',NULL,'9888383b5a147f488efd2bc4cd0dd9e2','f9e728a4640b74e0ec0f501346b794ff','AddComment','不同意',_binary '不同意'),('d653d1b94cfbcecda18a19a67e73ffb0','S','2023-09-12 22:38:09.692',NULL,'dd83cde660ec9b26ad7a27e3ab8da95c','9de499a6f8d8307a34cb9640d6e63d4e','AddComment','请审批',_binary '请审批'),('d938d154dba265aff6699b6363285ef6','S','2023-09-18 20:13:13.965',NULL,'09ef88659150862142596dd67eda5a78','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','同意',_binary '同意'),('de399ef37e1325d8cc5fa377faaf692f','event','2023-09-14 20:25:34.011',NULL,'9888383b5a147f488efd2bc4cd0dd9e2',NULL,'AddUserLink','9a8ccb159ac609b176825e9a6b4039d3_|_assignee',NULL),('e23b6007553ffaf2de1786a12f09e807','W','2023-09-18 20:01:32.808',NULL,'b4835657e917e572f1cef1e11ba8810d','3d11f7f23d377f4967cf1550ff19ea5a','AddComment','撤回',_binary '撤回');
/*!40000 ALTER TABLE `act_hi_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_detail`
--

DROP TABLE IF EXISTS `act_hi_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_detail` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`) USING BTREE,
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`) USING BTREE,
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_detail`
--

LOCK TABLES `act_hi_detail` WRITE;
/*!40000 ALTER TABLE `act_hi_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_entitylink`
--

DROP TABLE IF EXISTS `act_hi_entitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_entitylink` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_ENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_ENT_LNK_REF_SCOPE` (`REF_SCOPE_ID_`,`REF_SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_ENT_LNK_ROOT_SCOPE` (`ROOT_SCOPE_ID_`,`ROOT_SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_ENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_entitylink`
--

LOCK TABLES `act_hi_entitylink` WRITE;
/*!40000 ALTER TABLE `act_hi_entitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_entitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_identitylink`
--

DROP TABLE IF EXISTS `act_hi_identitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_identitylink` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_identitylink`
--

LOCK TABLES `act_hi_identitylink` WRITE;
/*!40000 ALTER TABLE `act_hi_identitylink` DISABLE KEYS */;
INSERT INTO `act_hi_identitylink` VALUES ('01b5e4b9c1881bb5790ae69e4968706a',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','1be90845469a774c0830dc4cba2009be','2023-09-11 22:30:45.603',NULL,NULL,NULL,NULL,NULL),('02e5e5d62b49b6ce1989959ff5ad4359',NULL,'assignee','78f97db459233bc461df091b862fb7e3','b3fdebe0c00f0c293b16bda52fab48b1','2023-09-15 20:49:53.235',NULL,NULL,NULL,NULL,NULL),('0426eab06af829717c0de0a9a758b71b',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','9888383b5a147f488efd2bc4cd0dd9e2','2023-09-13 23:20:18.114',NULL,NULL,NULL,NULL,NULL),('070c7000ee417c8dba7fbddf014a1511',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','aebb9acc0a2518e35c32714be66758ba','2023-09-11 21:25:27.307',NULL,NULL,NULL,NULL,NULL),('075a44dc76d72256ba9746c9494a6e69',NULL,'assignee','78f97db459233bc461df091b862fb7e3','dd83cde660ec9b26ad7a27e3ab8da95c','2023-09-12 21:56:26.656',NULL,NULL,NULL,NULL,NULL),('0968c9f45e1ed299c3434a96f11d8c23',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:34:30.123','f9e728a4640b74e0ec0f501346b794ff',NULL,NULL,NULL,NULL),('0a18dffc5adaad96cd8dcd85e2416814',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','6faa03d0f5a26f404ad19a16c1c380ae','2023-09-11 21:51:52.739',NULL,NULL,NULL,NULL,NULL),('0aff610fc27a0c0038585802b542ea75',NULL,'participant','9a8ccb159ac609b176825e9a6b4039d3',NULL,'2023-09-18 19:02:41.448','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL),('0c7003861b852a8c92f1d762e0efa36c',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:52:30.137','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL),('0c8502db93cecf5c7c99b833dc5648de',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:33:59.049','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL),('0dd2f50aa908fbf93823521f06eee1b2',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','ee4aa48739d93756d7e72e56fd37510a','2023-09-11 22:09:28.959',NULL,NULL,NULL,NULL,NULL),('1097c30702cff693b03f229599036265',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:18.049','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL),('10cc30d92a927390f274c786a3400761',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','2e76ed0b06f0306b43ae073ef7b62719','2023-09-11 21:50:42.925',NULL,NULL,NULL,NULL,NULL),('123ad962966abae96b90c9fa791f04e1',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:30:45.559','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL),('1ad2a2ad9dc719459401c2c6428be55d','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'b4835657e917e572f1cef1e11ba8810d','2023-09-18 20:01:19.459',NULL,NULL,NULL,NULL,NULL),('1f5ba3fed0d3208cb0332e39966acd03',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','251de12881c8555f6c1f63f61a4c4be3','2023-09-18 20:13:13.999',NULL,NULL,NULL,NULL,NULL),('1fd95e19e67a9a7687ec4d47a225940c',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:03.755','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL),('232ec36017c3cea13f8dae2272e8c097',NULL,'participant','00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-18 19:02:41.448','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL),('25c898f57dc831a4eadfc9e334af9199',NULL,'assignee','78f97db459233bc461df091b862fb7e3','5bcc6d4fafc02f1f64c7b3ffcc808329','2023-09-15 20:09:27.516',NULL,NULL,NULL,NULL,NULL),('26141198d33b7b2b1568c7262afe8edc','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'7ad75c81b9ba14c310316e745119bcb7','2023-09-18 20:13:14.001',NULL,NULL,NULL,NULL,NULL),('273471c1232d43e7d20eca9220d0032d',NULL,'assignee','78f97db459233bc461df091b862fb7e3','d870f96180591307aab4f7256dd44306','2023-09-18 19:00:47.742',NULL,NULL,NULL,NULL,NULL),('27d409995a06024ddb5d33c7d639f0f5','945cc85588578f74143ff672085c25a7','candidate',NULL,'ee4aa48739d93756d7e72e56fd37510a','2023-09-11 22:09:28.960',NULL,NULL,NULL,NULL,NULL),('27f7010da425b0b9551a01c4793904f9',NULL,'assignee','9a8ccb159ac609b176825e9a6b4039d3','64131a55a62f212ce843f08d6adfe365','2023-09-18 19:02:41.448',NULL,NULL,NULL,NULL,NULL),('33685135539e19e370d1460e1990f163',NULL,'participant','00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-12 22:38:31.129','9de499a6f8d8307a34cb9640d6e63d4e',NULL,NULL,NULL,NULL),('3846e62b94c1a4207beb71e426f62738',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','d6f11daa33af21fb33b0cf00d7531e79','2023-09-18 20:01:19.457',NULL,NULL,NULL,NULL,NULL),('3abfad257db42d93d31e49f1d0a870eb',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 21:56:26.613','9de499a6f8d8307a34cb9640d6e63d4e',NULL,NULL,NULL,NULL),('3e2a75f9ba15ed748f388a360ba8103b',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:57:04.648','678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL),('3f41ac9478f736d19045f11097d689d2',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:49:02.385','b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL),('3f44dac3ad7e7a17ce5a608d1f967ae0',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','3e55fbc84ca581895a74a36f66149acb','2023-09-11 21:49:02.431',NULL,NULL,NULL,NULL,NULL),('429461a918b31042f0edd7cf679b629b',NULL,'assignee','78f97db459233bc461df091b862fb7e3','0a20cedab4f2ad8e3a0dc756d833d1fd','2023-09-14 20:39:20.747',NULL,NULL,NULL,NULL,NULL),('438ac0061a5a4ebd1ee56d9228992c98',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:51:52.739','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,NULL,NULL,NULL),('4391f1b1b82e1b9e1c3d70ab466402d1',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','042ddaeb139fcb06547fdcda8a7ad71d','2023-09-15 21:08:17.620',NULL,NULL,NULL,NULL,NULL),('49ef9cd4717e93ad177545ab92ae1d02','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'ed3c147ac8082ec56206d93a5d82989b','2023-09-12 22:38:31.129',NULL,NULL,NULL,NULL,NULL),('4c8646bf37d475df313481f55f683ddf',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','304e1eba7ff0593e6697d5cfb1eeb4da','2023-09-18 19:02:41.444',NULL,NULL,NULL,NULL,NULL),('4fe130faaa663110f8cb7408540bbfc9',NULL,'assignee','78f97db459233bc461df091b862fb7e3','9b1fd3de1a8bce92eb9e93b9804c6b1f','2023-09-12 22:37:18.050',NULL,NULL,NULL,NULL,NULL),('5a2678493d630c283b5da6ca7c75b506',NULL,'assignee','78f97db459233bc461df091b862fb7e3','09ef88659150862142596dd67eda5a78','2023-09-18 20:12:46.629',NULL,NULL,NULL,NULL,NULL),('5c19ec7f756e930a8eda5bc61f7dd1b7','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'d6f11daa33af21fb33b0cf00d7531e79','2023-09-18 20:01:19.459',NULL,NULL,NULL,NULL,NULL),('5d045fecd42eb752f22aa8aaad87766b',NULL,'assignee','78f97db459233bc461df091b862fb7e3','d413ed765799eaa1c7ea6abb70988d56','2023-09-18 20:01:32.952',NULL,NULL,NULL,NULL,NULL),('62af1c0dbdf1fff0c6063bfbc9bdca9b',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:25:27.258','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL),('6392638faea9d1eee0f54657241b8f19','945cc85588578f74143ff672085c25a7','candidate',NULL,'03f82c30eb7300efe00f29a221ea2bc9','2023-09-18 20:01:08.302',NULL,NULL,NULL,NULL,NULL),('6400efc6c67ad0eef944540866a16ce3','945cc85588578f74143ff672085c25a7','candidate',NULL,'5742df4c2826308d73db85d9184bfe55','2023-09-12 22:38:09.741',NULL,NULL,NULL,NULL,NULL),('6659bf4a1c8e171625f2dc1a8bb5f190',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:03.754','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL),('66d9194317978ad5163fed7269d51563',NULL,'assignee','78f97db459233bc461df091b862fb7e3','d7fdd950a11638d9fb65d8accdd098e0','2023-09-14 22:00:49.044',NULL,NULL,NULL,NULL,NULL),('67bb28fd308f8f9e10becd441291fcaa',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:42.924','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL),('68361055c16df69d718f0d0ac290fa9a',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:52:30.136','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL),('6effcb5b145f79b3683f353e2242e596',NULL,'assignee','78f97db459233bc461df091b862fb7e3','ead9f2e0f519793db70e0831c22afdef','2023-09-15 20:45:11.821',NULL,NULL,NULL,NULL,NULL),('6f1132fc6f9512fd537aeabf53c60644','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'251de12881c8555f6c1f63f61a4c4be3','2023-09-18 20:13:14.000',NULL,NULL,NULL,NULL,NULL),('72dba11abd53d112ee3593750a17e5c3',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:09:27.477','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,NULL,NULL,NULL),('7420b0580587a70e3d269bcf5d228ce0',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-18 19:00:41.651','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL),('74d2504731ce79b0dd8267a76f1c4081',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','896bc6db02674af24a5d6f050a2f3db0','2023-09-11 22:08:06.495',NULL,NULL,NULL,NULL,NULL),('7567d516b082781a36cd668cd6672823',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-18 19:00:41.618','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL),('779d315c6e690a9d8172d4dffbd614e2',NULL,'assignee','78f97db459233bc461df091b862fb7e3','1800d422aaf3f7847dbca50ae6ff861d','2023-09-14 22:18:44.373',NULL,NULL,NULL,NULL,NULL),('7bab3cf5e7fba8e49234fa947e426a71',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','578f6b194aa3e1371e0ee45eb49e7d78','2023-09-11 21:53:26.863',NULL,NULL,NULL,NULL,NULL),('7ca68312a51028eb6c84698193926254','945cc85588578f74143ff672085c25a7','candidate',NULL,'ead9f2e0f519793db70e0831c22afdef','2023-09-15 20:45:11.831',NULL,NULL,NULL,NULL,NULL),('7d3a789e398c57f5eca471532917b489','945cc85588578f74143ff672085c25a7','candidate',NULL,'d870f96180591307aab4f7256dd44306','2023-09-18 19:00:47.744',NULL,NULL,NULL,NULL,NULL),('7dd189748cf42c67b3ca1890b0a7c85d',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:57:04.687','678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL),('7de603a9f4fcb6f7632dfeaeb98ef124',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:53:26.863','c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL),('7e9742ed699825a6d3679f8cc026b8d1',NULL,'assignee','78f97db459233bc461df091b862fb7e3','0a413d9c6ef53463e2e21852493e9790','2023-09-12 22:33:59.049',NULL,NULL,NULL,NULL,NULL),('7f0c46e894775de265932895271422a7','945cc85588578f74143ff672085c25a7','candidate',NULL,'1e0d31a3f4452161a51320a448fc753d','2023-09-15 20:53:17.104',NULL,NULL,NULL,NULL,NULL),('7f3d31e30dca6fb5e8dbbf8bf5eb595d',NULL,'participant','00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-15 21:08:17.621','2fadd67483a667cf3780274d6ad2c490',NULL,NULL,NULL,NULL),('805f446e518a02435587a1cfd780c5c8','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'64131a55a62f212ce843f08d6adfe365','2023-09-18 19:02:41.448',NULL,NULL,NULL,NULL,NULL),('8096e5a72c68f99e826669dded94ed2f',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 21:56:26.656','9de499a6f8d8307a34cb9640d6e63d4e',NULL,NULL,NULL,NULL),('81d19eadc4b77e18747d11c0e1fd3c9d',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','265377b48122d2ec995d6beedcf08f17','2023-09-11 21:57:04.686',NULL,NULL,NULL,NULL,NULL),('83a135ed6d65a6b631ddf7c21ccdc147',NULL,'assignee','78f97db459233bc461df091b862fb7e3','d25650926a27824c9146bf8e414cca74','2023-09-18 19:59:14.801',NULL,NULL,NULL,NULL,NULL),('8659df4649ef985a3bdbdcdbfb7caf33',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:12.990','62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL),('8742404eb7ffce2cf5a742a14f9cf360',NULL,'assignee','78f97db459233bc461df091b862fb7e3','5742df4c2826308d73db85d9184bfe55','2023-09-12 22:38:09.738',NULL,NULL,NULL,NULL,NULL),('8cf78a1de710fd320e8f3baeec4114a0',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:51:49.318','2fadd67483a667cf3780274d6ad2c490',NULL,NULL,NULL,NULL),('8db94bbff0f89c3092e48f13efea905a',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:09:27.516','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,NULL,NULL,NULL),('8fad82f4b884fbdbbdeff7335f7cdc40',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','c28cc4716c70d343fb9adb4de728bfea','2023-09-11 22:08:25.819',NULL,NULL,NULL,NULL,NULL),('93247ac02b5a59788b8a04dba4b0060a',NULL,'assignee','9a8ccb159ac609b176825e9a6b4039d3','9888383b5a147f488efd2bc4cd0dd9e2','2023-09-14 20:25:33.984',NULL,NULL,NULL,NULL,NULL),('9339de1ec3e06f9d199d9f7749a26371',NULL,'assignee','78f97db459233bc461df091b862fb7e3','67d94a5144c1fe8e89e70c134f9d4de1','2023-09-14 21:59:15.758',NULL,NULL,NULL,NULL,NULL),('97490042b45557cc1e8fa67a7e979be8',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:12.991','62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL),('9a5d24df2512162f82687287ec3b2523',NULL,'assignee','78f97db459233bc461df091b862fb7e3','860bb68fe25317abfb84d13c6736117a','2023-09-12 22:34:30.123',NULL,NULL,NULL,NULL,NULL),('9e12cd282137b707fa79a44bd9483f53',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:53:26.862','c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL),('9e4fc4eec29886980977c079d53589c7',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 21:59:15.747','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL),('a26e0025e0e8af579464166394f588e4',NULL,'owner','78f97db459233bc461df091b862fb7e3','9888383b5a147f488efd2bc4cd0dd9e2','2023-09-13 23:20:18.114',NULL,NULL,NULL,NULL,NULL),('a7a21f9e7d19a17d52ed7166398a9f2b',NULL,'assignee','78f97db459233bc461df091b862fb7e3','03f82c30eb7300efe00f29a221ea2bc9','2023-09-18 20:01:08.293',NULL,NULL,NULL,NULL,NULL),('adc2440ff202a44aeb71b3e513165a75','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'042ddaeb139fcb06547fdcda8a7ad71d','2023-09-15 21:08:17.621',NULL,NULL,NULL,NULL,NULL),('adc81bdb52b54e42bcf47a9232744af1','945cc85588578f74143ff672085c25a7','candidate',NULL,'9888383b5a147f488efd2bc4cd0dd9e2','2023-09-13 22:22:17.531',NULL,NULL,NULL,NULL,NULL),('af10b53e2b57f9da31333b32b6c319f6',NULL,'participant','9a8ccb159ac609b176825e9a6b4039d3',NULL,'2023-09-14 20:25:34.009','f9e728a4640b74e0ec0f501346b794ff',NULL,NULL,NULL,NULL),('b08ff6264502eab59e15383a7b73d043',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:42.925','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL),('b0bcb9723ff64153c252a9a759b260af',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:49:02.432','b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL),('b103ba70a08ca806d088c38f3a017f20',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:18.050','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL),('b249f86c6d9323497ba2f0854c9472c2',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:51:52.738','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,NULL,NULL,NULL),('b38f775d5860b4af30d085ea7cd28c3c',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','d2bda77e898d40290f8e7d96ec70cf17','2023-09-11 21:52:30.137',NULL,NULL,NULL,NULL,NULL),('b3ab80510441946d659f35b33cfdbc7f',NULL,'assignee','78f97db459233bc461df091b862fb7e3','046ac3cb8721b39d3ff0afc3cd18152f','2023-09-18 20:16:53.064',NULL,NULL,NULL,NULL,NULL),('b43978fc258de19f47d73a6e964ae683',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 22:00:49.044','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL),('b47aea358480dd145d5bb1ee1b29011a',NULL,'assignee','78f97db459233bc461df091b862fb7e3','9888383b5a147f488efd2bc4cd0dd9e2','2023-09-13 22:22:17.520',NULL,NULL,NULL,NULL,NULL),('bb329e1802f75dc59d4b3235862ae5d2',NULL,'assignee','a50f1994deb7e358e04ca0b1eb938ee1','3c9991d349f1d44c616ee8d724d7fb74','2023-09-11 21:50:12.991',NULL,NULL,NULL,NULL,NULL),('bb4d200904db769f015f9381994f6f59',NULL,'assignee','9a8ccb159ac609b176825e9a6b4039d3','b4835657e917e572f1cef1e11ba8810d','2023-09-18 20:01:19.459',NULL,NULL,NULL,NULL,NULL),('be1d215b329e57985e96be0ac6eeacc1',NULL,'assignee','78f97db459233bc461df091b862fb7e3','fb2577c0df7cea22e8f8a6203fa6e2fe','2023-09-18 19:00:41.650',NULL,NULL,NULL,NULL,NULL),('be99d54f937451d9f3ab5d5edc10a0cf',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:33:59.048','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL),('c202be75df95cfdd10c96049c2ed71c8',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:08:06.496','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL),('c2421c1d71b5d9183d0bba612d291154',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 22:00:49.044','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL),('c2565236ffe41553e6d3893f55f5182c',NULL,'participant','00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-13 23:20:18.118','f9e728a4640b74e0ec0f501346b794ff',NULL,NULL,NULL,NULL),('c40e8d02943c4c019a8fca109098c4cd',NULL,'assignee','00b6fea67bb10175f39158cb9e00db1e','ed3c147ac8082ec56206d93a5d82989b','2023-09-12 22:38:31.127',NULL,NULL,NULL,NULL,NULL),('c9011efa442d31cb5a62cfd53375a7a6',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:34:30.122','f9e728a4640b74e0ec0f501346b794ff',NULL,NULL,NULL,NULL),('ca028c28fd79342c8d15f3862990657b',NULL,'assignee','78f97db459233bc461df091b862fb7e3','cb3dbea44e2dda84e6413791f83c8119','2023-09-15 20:51:49.318',NULL,NULL,NULL,NULL,NULL),('cb2ac4e322c11f81fb2a1a4251a1182b',NULL,'assignee','78f97db459233bc461df091b862fb7e3','15a5091105cb9b1ec2685d20ef986486','2023-09-15 23:21:03.301',NULL,NULL,NULL,NULL,NULL),('cb8c77185775b12d1e9d3734c1556314',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:30:45.603','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL),('d636d8845ec1aef5f2ef44a63b52755c','51d63df3702720193c17209e1d18eaa5','candidate',NULL,'304e1eba7ff0593e6697d5cfb1eeb4da','2023-09-18 19:02:41.448',NULL,NULL,NULL,NULL,NULL),('e16f75012ab82314ba76463796c3c10c',NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:08:06.462','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL),('e2295639759efec3e766b1a68ec770c1',NULL,'assignee','9a8ccb159ac609b176825e9a6b4039d3','7ad75c81b9ba14c310316e745119bcb7','2023-09-18 20:13:14.001',NULL,NULL,NULL,NULL,NULL),('e96065cb7c50847ccf7a285e7c9a9309',NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:51:49.306','2fadd67483a667cf3780274d6ad2c490',NULL,NULL,NULL,NULL),('e987c2c43de6e66e2dadca29690941b9',NULL,'assignee','78f97db459233bc461df091b862fb7e3','9b931808d034cb271160bc1390ccf248','2023-09-12 22:37:03.755',NULL,NULL,NULL,NULL,NULL),('ec1a7642c306184605db1384f24ec5af',NULL,'assignee','78f97db459233bc461df091b862fb7e3','1e0d31a3f4452161a51320a448fc753d','2023-09-15 20:53:17.103',NULL,NULL,NULL,NULL,NULL),('f0e198226eef42d9f000097d8440fd0f','945cc85588578f74143ff672085c25a7','candidate',NULL,'c28cc4716c70d343fb9adb4de728bfea','2023-09-11 22:08:25.821',NULL,NULL,NULL,NULL,NULL),('f25eec945c7a2f007dac976e419aff74',NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 21:59:15.758','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL),('f89c8879a35b249d8b030802a78f7cf9',NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:25:27.308','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL),('fad9f449220398be24d45ecc1827b44f','945cc85588578f74143ff672085c25a7','candidate',NULL,'1800d422aaf3f7847dbca50ae6ff861d','2023-09-14 22:18:44.376',NULL,NULL,NULL,NULL,NULL),('fc979228817c37b2b9d8ab9f823b261e','945cc85588578f74143ff672085c25a7','candidate',NULL,'09ef88659150862142596dd67eda5a78','2023-09-18 20:12:46.631',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `act_hi_identitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_procinst`
--

DROP TABLE IF EXISTS `act_hi_procinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_procinst` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`) USING BTREE,
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_procinst`
--

LOCK TABLES `act_hi_procinst` WRITE;
/*!40000 ALTER TABLE `act_hi_procinst` DISABLE KEYS */;
INSERT INTO `act_hi_procinst` VALUES ('0c0ae280fcaf4d43a4659c6d2a2e4da0',2,'0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:51:52.738','2023-09-11 22:15:17.350',1404612,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1','endNode',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1b8c6cf5877ddf134f4b149408024b37',1,'1b8c6cf5877ddf134f4b149408024b37',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-12 22:33:59.048',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2c43cad6e817cb50a9131351a79b6e4a',1,'2c43cad6e817cb50a9131351a79b6e4a',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-14 22:00:49.043',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('2fadd67483a667cf3780274d6ad2c490',2,'2fadd67483a667cf3780274d6ad2c490',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-15 20:51:49.306','2023-09-18 18:58:11.295',252381989,'78f97db459233bc461df091b862fb7e3','startNode1','endNode',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3d11f7f23d377f4967cf1550ff19ea5a',1,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-18 19:00:41.615',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('48cb4c54ce008b4ac292c3ba18044a56',1,'48cb4c54ce008b4ac292c3ba18044a56',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-14 21:59:15.747',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('5b05d462a25f6e49e5ff3e97c9bb41f4',2,'5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-15 20:09:27.472','2023-09-15 20:50:16.253',2448781,'78f97db459233bc461df091b862fb7e3','startNode1','endNode',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('62580c574add9b784f1b6e2114561a0d',1,'62580c574add9b784f1b6e2114561a0d',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:50:12.990',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('678f8c7260b7992967d4175d5694b4b5',1,'678f8c7260b7992967d4175d5694b4b5',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:57:04.644',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('861e6ffa7abfee87cd95c08f4fcdc6c8',1,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-12 22:37:18.049',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('86a2f9b8410887c3f77a136b68a96c77',1,'86a2f9b8410887c3f77a136b68a96c77',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-12 22:37:03.754',NULL,NULL,'78f97db459233bc461df091b862fb7e3','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9790b57ffcc326c11d1867972ff55876',1,'9790b57ffcc326c11d1867972ff55876',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:52:30.136',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9b15f4abc8ba5dcf905885c5f73489e9',1,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 22:08:06.458',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9de499a6f8d8307a34cb9640d6e63d4e',2,'9de499a6f8d8307a34cb9640d6e63d4e',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-12 21:56:26.609','2023-09-12 22:42:44.164',2777555,'78f97db459233bc461df091b862fb7e3','startNode1','endNode',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('b971297aac58a442a089dd33612510be',1,'b971297aac58a442a089dd33612510be',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:49:02.381',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c0b03c7d121b3ca701b5433bce783533',1,'c0b03c7d121b3ca701b5433bce783533',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:53:26.862',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('cf9b32c4462a2ee9a295ce6343ce82d0',1,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:50:42.924',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e2657e6406c2a9c2e7ef4f8b61d06a12',1,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 21:25:27.257',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e73736cde5d5a8e1a04b900134cfd846',1,'e73736cde5d5a8e1a04b900134cfd846',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-11 22:30:45.555',NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','startNode1',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('f9e728a4640b74e0ec0f501346b794ff',2,'f9e728a4640b74e0ec0f501346b794ff',NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2023-09-12 22:34:30.122','2023-09-14 21:39:15.516',169485394,'78f97db459233bc461df091b862fb7e3','startNode1','endNode',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `act_hi_procinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_taskinst`
--

DROP TABLE IF EXISTS `act_hi_taskinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_taskinst` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_TASK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_TASK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_TASK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_TASK_INST_PROCINST` (`PROC_INST_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_taskinst`
--

LOCK TABLES `act_hi_taskinst` WRITE;
/*!40000 ALTER TABLE `act_hi_taskinst` DISABLE KEYS */;
INSERT INTO `act_hi_taskinst` VALUES ('03f82c30eb7300efe00f29a221ea2bc9',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293',NULL,'2023-09-18 20:01:19.441',11148,'Change activity to deptLeaderNode',50,NULL,NULL,NULL,'','2023-09-18 20:01:19.441'),('042ddaeb139fcb06547fdcda8a7ad71d',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','2fadd67483a667cf3780274d6ad2c490','fee5801bd4d8cd57251025e757b2400f',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e','2023-09-15 21:08:17.620',NULL,'2023-09-15 23:21:03.263',7965643,'Change parent activity to applyNode',50,'2023-09-15 23:21:03.193',NULL,NULL,'','2023-09-15 23:21:03.263'),('046ac3cb8721b39d3ff0afc3cd18152f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','3d11f7f23d377f4967cf1550ff19ea5a','05fccf74e5499bdd34c768a84c314896',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 20:16:53.062',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-18 20:16:53.064'),('09ef88659150862142596dd67eda5a78',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629',NULL,'2023-09-18 20:13:13.993',27364,'Change activity to deptLeaderNode',50,NULL,NULL,NULL,'','2023-09-18 20:13:13.993'),('0a20cedab4f2ad8e3a0dc756d833d1fd',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','f9e728a4640b74e0ec0f501346b794ff','67503605cf2d047750cf17d1ec1190f8',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-14 20:39:20.725',NULL,'2023-09-14 21:39:15.453',3594728,'Change activity to endNode',50,NULL,NULL,NULL,'','2023-09-14 21:39:15.453'),('0a413d9c6ef53463e2e21852493e9790',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 22:33:59.049',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-12 22:33:59.049'),('15a5091105cb9b1ec2685d20ef986486',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','2fadd67483a667cf3780274d6ad2c490','df56e6bf53ded205fe1db016a01818a3',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 23:21:03.274',NULL,'2023-09-18 18:58:11.224',243427950,'Change activity to endNode',50,NULL,NULL,NULL,'','2023-09-18 18:58:11.224'),('1800d422aaf3f7847dbca50ae6ff861d',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','2c43cad6e817cb50a9131351a79b6e4a','d5eb46d7099e367012959854ae72759f',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-14 22:18:44.373',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-14 22:18:44.373'),('1be90845469a774c0830dc4cba2009be',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:30:45.579',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 22:30:45.602'),('1e0d31a3f4452161a51320a448fc753d',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','2fadd67483a667cf3780274d6ad2c490','3ea49e2387678e791f3cc42cbd62bdd1',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 20:53:17.103',NULL,'2023-09-15 21:08:17.612',900509,'Change activity to deptLeaderNode',50,NULL,NULL,NULL,'','2023-09-15 21:08:17.612'),('251de12881c8555f6c1f63f61a4c4be3',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','9dea8978754d867d28e261c3429eb799',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:13:13.999',NULL,'2023-09-18 20:16:53.045',219046,'Change parent activity to applyNode',50,'2023-09-18 20:16:52.984',NULL,NULL,'','2023-09-18 20:16:53.045'),('265377b48122d2ec995d6beedcf08f17',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:57:04.665',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:57:04.686'),('2e76ed0b06f0306b43ae073ef7b62719',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:42.924',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:50:42.924'),('304e1eba7ff0593e6697d5cfb1eeb4da',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','f47767b3297cdc62cbec1835e9873d9b',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e','2023-09-18 19:02:41.444',NULL,'2023-09-18 19:59:14.751',3393307,'Change parent activity to applyNode',50,'2023-09-18 19:59:14.651',NULL,NULL,'','2023-09-18 19:59:14.751'),('3c9991d349f1d44c616ee8d724d7fb74',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:12.991',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:50:12.991'),('3e55fbc84ca581895a74a36f66149acb',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:49:02.408',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:49:02.431'),('5742df4c2826308d73db85d9184bfe55',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','9de499a6f8d8307a34cb9640d6e63d4e','3b18cd53bc89c3fff32b094b0b6939cf',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 22:38:09.737',NULL,'2023-09-12 22:38:31.118',21381,'Change activity to deptLeaderNode',50,NULL,NULL,NULL,'','2023-09-12 22:38:31.118'),('578f6b194aa3e1371e0ee45eb49e7d78',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:53:26.863',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:53:26.863'),('5bcc6d4fafc02f1f64c7b3ffcc808329',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','5b05d462a25f6e49e5ff3e97c9bb41f4','6aeece9a41fa2c2fc915c814814709d1',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 20:09:27.494',NULL,'2023-09-15 20:45:11.783',2144289,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-15 20:45:11.783'),('64131a55a62f212ce843f08d6adfe365',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','37e99c1669a1c61f10d7fcee86116e03',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 19:02:41.448',NULL,'2023-09-18 19:59:14.779',3393331,'Change parent activity to applyNode',50,NULL,NULL,NULL,'','2023-09-18 19:59:14.779'),('64ea0905503233a823593d39289af99d',1,NULL,NULL,'mangerNode','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,'78f97db459233bc461df091b862fb7e3','00b6fea67bb10175f39158cb9e00db1e','2023-09-13 22:22:17.491',NULL,'2023-09-14 20:25:34.079',NULL,NULL,50,NULL,NULL,NULL,'','2023-09-14 20:25:34.079'),('67d94a5144c1fe8e89e70c134f9d4de1',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-14 21:59:15.758',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-14 21:59:15.758'),('6c789455b90e6502ff4d518e3f96c29f',1,NULL,NULL,'mangerNode','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-13 22:22:17.491',NULL,'2023-09-13 23:20:18.133',NULL,NULL,50,NULL,NULL,NULL,'','2023-09-13 23:20:18.133'),('6faa03d0f5a26f404ad19a16c1c380ae',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','0c0ae280fcaf4d43a4659c6d2a2e4da0','6bb51fac3f17b893f20ea249922d8710',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:51:52.739',NULL,'2023-09-11 22:09:28.955',1056216,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-11 22:09:28.955'),('7ad75c81b9ba14c310316e745119bcb7',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','9cb9c09b8e748dd0d9743cbf3e174a41',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:13:14.001',NULL,'2023-09-18 20:16:53.054',219053,'Change parent activity to applyNode',50,'2023-09-18 20:16:53.005',NULL,NULL,'','2023-09-18 20:16:53.054'),('860bb68fe25317abfb84d13c6736117a',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','f9e728a4640b74e0ec0f501346b794ff','82845a6288efdc6f48f2af3fcfb702b4',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 22:34:30.122',NULL,'2023-09-13 22:22:17.474',85667352,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-13 22:22:17.474'),('896bc6db02674af24a5d6f050a2f3db0',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:06.473',NULL,'2023-09-11 22:08:25.810',19337,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-11 22:08:25.810'),('9888383b5a147f488efd2bc4cd0dd9e2',4,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','f9e728a4640b74e0ec0f501346b794ff','07e52f286a8bb976f5e4f52ce7088d51',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,'78f97db459233bc461df091b862fb7e3','9a8ccb159ac609b176825e9a6b4039d3','2023-09-13 22:22:17.491',NULL,'2023-09-14 20:39:20.712',80223221,'Change activity to applyNode',50,NULL,NULL,NULL,'','2023-09-14 20:39:20.712'),('9b1fd3de1a8bce92eb9e93b9804c6b1f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:18.050',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-12 22:37:18.050'),('9b931808d034cb271160bc1390ccf248',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:03.755',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-12 22:37:03.755'),('aebb9acc0a2518e35c32714be66758ba',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:25:27.279',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:25:27.307'),('b3fdebe0c00f0c293b16bda52fab48b1',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','5b05d462a25f6e49e5ff3e97c9bb41f4','2ca32e0c26b8581d475842a1fb90b8ef',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 20:49:53.234',NULL,'2023-09-15 20:50:16.215',22981,'Change activity to endNode',50,NULL,NULL,NULL,'','2023-09-15 20:50:16.215'),('b4835657e917e572f1cef1e11ba8810d',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','a15dc5534f126d84a6437e2897d8c397',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:01:19.459',NULL,'2023-09-18 20:01:32.905',13446,'Change parent activity to applyNode',50,'2023-09-18 20:01:32.814',NULL,NULL,'','2023-09-18 20:01:32.905'),('c28cc4716c70d343fb9adb4de728bfea',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','9b15f4abc8ba5dcf905885c5f73489e9','e1045e978e8fef0ac700073b5115f8fa',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:25.819',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 22:08:25.819'),('cb3dbea44e2dda84e6413791f83c8119',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','2fadd67483a667cf3780274d6ad2c490','4c6e8bf82491bbc3e4a2447e72096430',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 20:51:49.318',NULL,'2023-09-15 20:53:17.100',87782,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-15 20:53:17.100'),('d25650926a27824c9146bf8e414cca74',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','3d11f7f23d377f4967cf1550ff19ea5a','4655b44e3e75e87d6c34bc401df1ddec',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 19:59:14.795',NULL,'2023-09-18 20:01:08.288',113493,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-18 20:01:08.288'),('d2bda77e898d40290f8e7d96ec70cf17',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:52:30.137',NULL,NULL,NULL,NULL,50,NULL,NULL,NULL,'','2023-09-11 21:52:30.137'),('d413ed765799eaa1c7ea6abb70988d56',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','3d11f7f23d377f4967cf1550ff19ea5a','95a49737bdee7652e74ec5eb0c0a922c',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:32.950',NULL,'2023-09-18 20:12:46.622',673672,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-18 20:12:46.622'),('d6f11daa33af21fb33b0cf00d7531e79',3,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','3d11f7f23d377f4967cf1550ff19ea5a','2d202e18fe57628c9f77a3e136210bb8',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:01:19.457',NULL,'2023-09-18 20:01:32.925',13468,'Change parent activity to applyNode',50,'2023-09-18 20:01:32.847',NULL,NULL,'','2023-09-18 20:01:32.925'),('d7fdd950a11638d9fb65d8accdd098e0',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-14 22:00:49.044',NULL,'2023-09-14 22:18:44.370',1075326,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-14 22:18:44.370'),('d870f96180591307aab4f7256dd44306',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.742',NULL,'2023-09-18 19:02:41.434',113692,'Change activity to deptLeaderNode',50,NULL,NULL,NULL,'','2023-09-18 19:02:41.434'),('dd83cde660ec9b26ad7a27e3ab8da95c',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','9de499a6f8d8307a34cb9640d6e63d4e','0adfac8b35abebea0ea07251f2b23403',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-12 21:56:26.634',NULL,'2023-09-12 22:38:09.725',2503091,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-12 22:38:09.725'),('ead9f2e0f519793db70e0831c22afdef',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','5b05d462a25f6e49e5ff3e97c9bb41f4','6e9d34ed662a1bdeeccc8e779e9c8243',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-15 20:45:11.797',NULL,'2023-09-15 20:49:53.227',281430,'Change activity to applyNode',50,NULL,NULL,NULL,'','2023-09-15 20:49:53.227'),('ed3c147ac8082ec56206d93a5d82989b',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'deptLeaderNode','9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b',NULL,NULL,NULL,NULL,NULL,'部门领导审批',NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e','2023-09-12 22:38:31.127',NULL,'2023-09-12 22:42:44.135',253008,'Change parent activity to endNode',50,NULL,NULL,NULL,'','2023-09-12 22:42:44.135'),('ee4aa48739d93756d7e72e56fd37510a',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'mangerNode','0c0ae280fcaf4d43a4659c6d2a2e4da0','5b679daadec6e5c140f986aebc7c70d6',NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:09:28.958',NULL,'2023-09-11 22:15:17.301',348343,'Change activity to endNode',50,NULL,NULL,NULL,'','2023-09-11 22:15:17.301'),('fb2577c0df7cea22e8f8a6203fa6e2fe',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'applyNode','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07',NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:41.628',NULL,'2023-09-18 19:00:47.738',6110,'Change activity to mangerNode',50,NULL,NULL,NULL,'','2023-09-18 19:00:47.738');
/*!40000 ALTER TABLE `act_hi_taskinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_tsk_log`
--

DROP TABLE IF EXISTS `act_hi_tsk_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_tsk_log` (
  `ID_` bigint NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DATA_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_tsk_log`
--

LOCK TABLES `act_hi_tsk_log` WRITE;
/*!40000 ALTER TABLE `act_hi_tsk_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_hi_tsk_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_hi_varinst`
--

DROP TABLE IF EXISTS `act_hi_varinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_hi_varinst` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_VAR_SCOPE_ID_TYPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_VAR_SUB_ID_TYPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_PROCVAR_TASK_ID` (`TASK_ID_`) USING BTREE,
  KEY `ACT_IDX_HI_PROCVAR_EXE` (`EXECUTION_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_hi_varinst`
--

LOCK TABLES `act_hi_varinst` WRITE;
/*!40000 ALTER TABLE `act_hi_varinst` DISABLE KEYS */;
INSERT INTO `act_hi_varinst` VALUES ('044b4e75b27e47421a03e5468ddf6721',0,'9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:08:25.797','2023-09-11 22:08:25.797'),('0517aa053ed3e76a435f9444abe9820b',2,'9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:52:30.136','2023-09-11 22:15:25.355'),('0570fe22948a6e50d3083cc459cb8e3f',4,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'assigneeList','serializable',NULL,NULL,NULL,'c86a833eadc9eda8671263427b6dacda',NULL,NULL,NULL,NULL,'2023-09-15 21:08:17.601','2023-09-18 18:58:11.087'),('065eafba90bbedf581039c43516e12a6',0,'3d11f7f23d377f4967cf1550ff19ea5a','bd2008f11f8c450857638ba18bfd40ea',NULL,'nrOfActiveInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'bd2008f11f8c450857638ba18bfd40ea','active','2023-09-18 19:02:41.441','2023-09-18 19:02:41.441'),('077a16db65511cdcb6c3bdf0dfe61fa6',6,'48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-14 21:59:15.748','2023-10-10 21:21:41.689'),('0afcd4b916ba7af3db330d7055bd1a1b',7,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:53:17.082','2023-09-18 18:58:11.110'),('0f59598d173e8f7ade6febab52f923a9',2,'86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,'day','double',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:13.020'),('107220c1c9d88ce82ff64edf656c141e',1,'cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:43.079'),('1235f17a966836b916c552d9ea0a9f33',3,'9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-12 22:38:31.127','2023-09-12 22:42:43.990'),('12670db4b1f48640030dc3023488acc8',22,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-18 19:00:47.727','2023-09-18 20:25:50.120'),('12cce0aa76fe36b8f0f621f96b631e32',22,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-18 19:00:41.618','2023-09-18 20:25:50.084'),('14a233482c3026205bd08ff2a60b083f',20,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'day','double',NULL,NULL,NULL,NULL,18,NULL,NULL,NULL,'2023-09-12 21:56:26.620','2023-09-12 22:42:44.000'),('171c58cd4aa96dc92258ff245ef4d1cd',27,'2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-14 22:00:49.285','2023-09-15 20:09:12.132'),('18cba817dad86bb06beecccd5835bb9b',1,'b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:49:02.611','2023-09-11 21:49:56.095'),('1d9cacf7dee839161edb4af66a418eb2',6,'0c0ae280fcaf4d43a4659c6d2a2e4da0','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:51:52.738','2023-09-11 22:15:17.234'),('1e8472a94eeddcdaa181e0e95ffb9e08',0,'3d11f7f23d377f4967cf1550ff19ea5a','2d202e18fe57628c9f77a3e136210bb8',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-18 20:01:19.456','2023-09-18 20:01:19.456'),('247613ed446335fed6edeb9596b439f4',19,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'assigneeList','serializable',NULL,NULL,NULL,'419937cbe011d8cfc90aa43bf35d5a43',NULL,NULL,NULL,NULL,'2023-09-18 19:02:41.425','2023-09-18 20:25:50.098'),('2693f049a1c9d48cbf73196d52ef4f4d',0,'3d11f7f23d377f4967cf1550ff19ea5a','2d202e18fe57628c9f77a3e136210bb8',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-18 20:01:19.453','2023-09-18 20:01:19.453'),('272dfa7805d6cd1ae29db02e6294b52d',1,'2fadd67483a667cf3780274d6ad2c490','fee5801bd4d8cd57251025e757b2400f',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-15 21:08:17.619','2023-09-15 22:18:35.504'),('27e40ee3f88dd0eee086b9d69dab76df',7,'5b05d462a25f6e49e5ff3e97c9bb41f4','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:45:11.771','2023-09-15 20:50:16.123'),('28a6bfbe2243e929497799e8ea1cc79d',1,'2fadd67483a667cf3780274d6ad2c490','3bae7874c276f6a3df8e82007d7970e4',NULL,'nrOfCompletedInstances','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-15 21:08:17.618','2023-09-15 22:18:35.522'),('2b959e6a689155b55d6fd4fd4c81e095',3,'9de499a6f8d8307a34cb9640d6e63d4e','98550be4e2968e014abfa2e2513c8e2b',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-12 22:38:31.125','2023-09-12 22:42:43.986'),('2e51d522adb9964c6fd9f3b71aa776bb',0,'3d11f7f23d377f4967cf1550ff19ea5a','f47767b3297cdc62cbec1835e9873d9b',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-18 19:02:41.444','2023-09-18 19:02:41.444'),('300ffe514eb77217e23feb0b2c428316',1,'9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 22:08:06.648','2023-09-11 22:08:25.731'),('34463928d4732bafc89e5ac477f3c069',20,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 21:56:26.620','2023-09-12 22:42:44.009'),('3664b29a7dfd2927da4fd72d6e2ad699',48,'f9e728a4640b74e0ec0f501346b794ff','f9e728a4640b74e0ec0f501346b794ff',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'C',NULL,'2023-09-12 22:34:30.243','2023-09-14 21:39:15.373'),('3786d210efdf5d49b8c519480d02ffe2',6,'48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 21:59:15.748','2023-10-10 21:21:41.703'),('37b19cfd8814c12bfbad89b9b5e4b19c',2,'86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:13.025'),('387923c02c452cb8ceee6c002b814cb7',23,'2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 22:18:44.365','2023-09-15 20:09:12.136'),('38888676b1eb8c7882c5ae34f9bd9beb',21,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-18 19:00:41.754','2023-09-18 20:25:50.110'),('3a8e384486a6d2c6942fb6db5d3287f1',3,'9de499a6f8d8307a34cb9640d6e63d4e','841a9ef7d798527331ae10d8728c3af9',NULL,'nrOfCompletedInstances','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-12 22:38:31.125','2023-09-12 22:42:44.019'),('3c5d49c061fb8048a108b3de981483d9',2,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:38:09.714','2023-09-12 22:38:31.021'),('3cb5ae2c8b5f72ea1efb541ed3ea9c3a',0,'3d11f7f23d377f4967cf1550ff19ea5a','37e99c1669a1c61f10d7fcee86116e03',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-18 19:02:41.448','2023-09-18 19:02:41.448'),('3d13de24158076f41d09dd16b18016e8',11,'e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:25:27.263','2023-09-11 21:40:38.535'),('3f7e453f9b00396739c5bea2d520d613',5,'48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-14 21:59:15.867','2023-10-10 21:21:41.693'),('403f76a317a817b7be15f5acca06c662',3,'678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:57:04.866','2023-09-11 22:08:30.765'),('446b64ad47ac02e6b668d68ac39740bc',3,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'assigneeList','serializable',NULL,NULL,NULL,'85cf8da4082216350fa6ad4f8acc7918',NULL,NULL,NULL,NULL,'2023-09-12 22:38:31.104','2023-09-12 22:42:43.972'),('4ab049401a199b64948aea245ab6d29e',0,'3d11f7f23d377f4967cf1550ff19ea5a','9dea8978754d867d28e261c3429eb799',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,0,'0',NULL,'2023-09-18 20:13:13.999','2023-09-18 20:13:13.999'),('4cb0e002d097eb60f30e710087d44951',1,'62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:50:12.990','2023-09-11 21:50:13.087'),('4fa5763450669866724b1d5264e45331',2,'861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'day','double',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,'2023-09-12 22:37:18.049','2023-09-12 22:37:26.592'),('5147c92af231f31e86f7a5c047f30066',1,'2fadd67483a667cf3780274d6ad2c490','3bae7874c276f6a3df8e82007d7970e4',NULL,'nrOfInstances','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-15 21:08:17.617','2023-09-15 22:18:35.526'),('51e6723d707761b830bf91fc8242f913',1,'c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.965'),('5847d2d9ac7f555699b590d82d447d5b',11,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:51:49.306','2023-09-18 18:58:11.127'),('5b117d86ffc226c3fd74c92531e7a1e9',10,'5b05d462a25f6e49e5ff3e97c9bb41f4','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-15 20:09:27.479','2023-09-15 20:50:16.136'),('6317a6b1a4bb01416c09ab77fdaa5b8e',3,'0c0ae280fcaf4d43a4659c6d2a2e4da0','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:09:28.943','2023-09-11 22:15:17.222'),('647a0c143e8a1ca9e1c500765504ce2e',1,'9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:52:30.272','2023-09-11 22:15:25.343'),('66c0844e7976be37910948d46471f8bf',1,'0c0ae280fcaf4d43a4659c6d2a2e4da0','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'day','double',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,'2023-09-11 22:15:05.286','2023-09-11 22:15:17.228'),('677e133da4db32589edc670037e45f37',4,'678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:57:04.653','2023-09-11 22:08:30.759'),('67987ae65981e5332d57a0e855f4187e',22,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'day','double',NULL,NULL,NULL,NULL,7,NULL,NULL,NULL,'2023-09-18 19:00:41.618','2023-09-18 20:25:50.132'),('6aa8dcb9b5c028dee48b133744e630db',3,'e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 22:30:45.564','2023-09-11 22:48:25.597'),('6bfab9bafa925e4d654fc839c16d9e37',2,'1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:33:59.048','2023-09-12 22:36:57.133'),('6c1ca631aee3cccb104484aade70810f',0,'3d11f7f23d377f4967cf1550ff19ea5a','9cb9c09b8e748dd0d9743cbf3e174a41',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3',NULL,'2023-09-18 20:13:13.999','2023-09-18 20:13:13.999'),('6c41f0dec0c1007ef3bebfd0939dfb74',1,'861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-12 22:37:18.233','2023-09-12 22:37:26.580'),('719f835218e685147cc0098b559f24d7',11,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'day','double',NULL,NULL,NULL,NULL,21,NULL,NULL,NULL,'2023-09-15 20:51:49.306','2023-09-18 18:58:11.117'),('71a5cc566fbdcd287c10c6dd31187af8',0,'3d11f7f23d377f4967cf1550ff19ea5a','a15dc5534f126d84a6437e2897d8c397',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-18 20:01:19.459','2023-09-18 20:01:19.459'),('72919b6226d21d6092990ecd5710f583',28,'2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,'day','double',NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,'2023-09-14 22:00:49.044','2023-09-15 20:09:12.142'),('729dff692685065e7ea4e568aa7941bc',2,'86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:13.011'),('72a62636e4d6154c4b3356b46436abfa',3,'e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,'day','double',NULL,NULL,NULL,NULL,17,NULL,NULL,NULL,'2023-09-11 22:30:45.564','2023-09-11 22:48:25.608'),('747551465cf8d47663de88f78c8a8b07',10,'e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:25:27.466','2023-09-11 21:40:38.544'),('76b4f19d0ab6ba9cf586b9e94c7cb2cd',6,'48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,'day','double',NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,'2023-09-14 21:59:15.748','2023-10-10 21:21:41.698'),('7818cf7604691e013abf864b1e03581b',1,'2fadd67483a667cf3780274d6ad2c490','3bae7874c276f6a3df8e82007d7970e4',NULL,'nrOfActiveInstances','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-15 21:08:17.618','2023-09-15 22:18:35.475'),('795e320cef539eccff9843d05bafb720',1,'cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:43.067'),('7a81adcfc3b5f33ee070e1289e164351',49,'f9e728a4640b74e0ec0f501346b794ff','f9e728a4640b74e0ec0f501346b794ff',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-12 22:34:30.122','2023-09-14 21:39:15.367'),('7bcc9133aad55ae1c0534351a459a445',49,'f9e728a4640b74e0ec0f501346b794ff','f9e728a4640b74e0ec0f501346b794ff',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:34:30.122','2023-09-14 21:39:15.390'),('7cba7a681fd3c5901f26da9ef161b733',49,'f9e728a4640b74e0ec0f501346b794ff','f9e728a4640b74e0ec0f501346b794ff',NULL,'day','double',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,'2023-09-12 22:34:30.122','2023-09-14 21:39:15.385'),('7d1d0a9ac53f7bd96979c083f9b45424',0,'3d11f7f23d377f4967cf1550ff19ea5a','9dea8978754d867d28e261c3429eb799',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-18 20:13:13.998','2023-09-18 20:13:13.998'),('871c563e9fcbd509a03f36460889edff',11,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-15 20:51:49.306','2023-09-18 18:58:11.077'),('8a570bb76021dd5edb344016a10a7cb0',28,'2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-14 22:00:49.044','2023-09-15 20:09:12.126'),('8f772e07173cd6f76235099ad472e9b1',0,'3d11f7f23d377f4967cf1550ff19ea5a','bd2008f11f8c450857638ba18bfd40ea',NULL,'nrOfInstances','integer',NULL,NULL,NULL,NULL,NULL,2,'2',NULL,'2023-09-18 19:02:41.441','2023-09-18 19:02:41.441'),('9193f8c03dd0881b62ea34f462acf296',20,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-12 21:56:26.620','2023-09-12 22:42:43.960'),('93a86db1b3e0061a8f1c7b2e28c9e4c1',2,'861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-12 22:37:18.049','2023-09-12 22:37:26.604'),('9811c4d49842e6620d7ba1d6f6e114b1',11,'e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:25:27.263','2023-09-11 21:40:38.554'),('9891958a5a8c6a6545879f2e011a8c6d',0,'3d11f7f23d377f4967cf1550ff19ea5a','a15dc5534f126d84a6437e2897d8c397',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3',NULL,'2023-09-18 20:01:19.456','2023-09-18 20:01:19.456'),('98d9d0be83ad4045252f09c188a7de86',0,'3d11f7f23d377f4967cf1550ff19ea5a','8adbfde6e58d0ff7aacd21fa87807b9a',NULL,'nrOfInstances','integer',NULL,NULL,NULL,NULL,NULL,2,'2',NULL,'2023-09-18 20:01:19.453','2023-09-18 20:01:19.453'),('9ed7ec69d26fcd08494d4dbfc870f511',3,'9de499a6f8d8307a34cb9640d6e63d4e','841a9ef7d798527331ae10d8728c3af9',NULL,'nrOfActiveInstances','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-12 22:38:31.125','2023-09-12 22:42:43.964'),('a00e9884f9f2560ba39719f0197ef098',1,'2fadd67483a667cf3780274d6ad2c490','fee5801bd4d8cd57251025e757b2400f',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-15 21:08:17.618','2023-09-15 22:18:35.501'),('a3df8c053bcc88cb70130ae00343c81b',10,'2fadd67483a667cf3780274d6ad2c490','2fadd67483a667cf3780274d6ad2c490',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'C',NULL,'2023-09-15 20:51:49.416','2023-09-18 18:58:11.096'),('a55ece25531017215e61649be12463e8',10,'5b05d462a25f6e49e5ff3e97c9bb41f4','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'day','double',NULL,NULL,NULL,NULL,7,NULL,NULL,NULL,'2023-09-15 20:09:27.479','2023-09-15 20:50:16.131'),('ad61ac242c0c3fc44e43b6cc8d7cbd15',0,'3d11f7f23d377f4967cf1550ff19ea5a','37e99c1669a1c61f10d7fcee86116e03',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'9a8ccb159ac609b176825e9a6b4039d3',NULL,'2023-09-18 19:02:41.444','2023-09-18 19:02:41.444'),('b68413b264e641d4994471f8cf0e85d9',10,'5b05d462a25f6e49e5ff3e97c9bb41f4','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-15 20:09:27.479','2023-09-15 20:50:16.111'),('b8f4cdf64d9371d4826d50a636afe919',2,'e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 22:30:45.889','2023-09-11 22:48:25.603'),('be5128485317ea435feb84b920941402',2,'9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:52:30.137','2023-09-11 22:15:25.336'),('c0699b149a7eba0b20a030b3fce678f9',0,'3d11f7f23d377f4967cf1550ff19ea5a','745dd9d6da838b1c9887447d4230000c',NULL,'nrOfActiveInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'745dd9d6da838b1c9887447d4230000c','active','2023-09-18 20:13:13.998','2023-09-18 20:13:13.998'),('c3dd714a5459be63c23a96cf32a97332',0,'3d11f7f23d377f4967cf1550ff19ea5a','9cb9c09b8e748dd0d9743cbf3e174a41',NULL,'loopCounter','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-18 20:13:14.001','2023-09-18 20:13:14.001'),('c5fb9f9caab85f0a365d47754394cac5',0,'9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,'day','double',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,'2023-09-11 22:15:25.349','2023-09-11 22:15:25.349'),('c65cab4a76aa0c46689891fa932cdf1d',2,'861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-12 22:37:18.050','2023-09-12 22:37:26.570'),('c6e2b20ea0f94732e7743d6d48be67c4',28,'2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-14 22:00:49.044','2023-09-15 20:09:12.145'),('c86b0ee13e2eb0a9522915a3a3519959',2,'b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:49:02.392','2023-09-11 21:49:56.089'),('c9819b97d94bcd589a8fc2f96a90dc7a',3,'e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:30:45.564','2023-09-11 22:48:25.612'),('cbd56d7a219f7984343b4a2d40106438',22,'3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-18 19:00:41.618','2023-09-18 20:25:50.142'),('cfae8e9e13accebaa1d3262d8e41db03',2,'9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 22:08:06.463','2023-09-11 22:08:25.738'),('d2740933a4e48941c46683eb9268af71',0,'3d11f7f23d377f4967cf1550ff19ea5a','745dd9d6da838b1c9887447d4230000c',NULL,'nrOfCompletedInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'745dd9d6da838b1c9887447d4230000c','completed','2023-09-18 20:13:13.998','2023-09-18 20:13:13.998'),('d86f5dd264fb5de8a381f7d6df3deacb',0,'cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:50:43.074','2023-09-11 21:50:43.074'),('dc4b12404dee3c2d85998c08e49850a3',1,'c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.949'),('dcc33da13e19f52c4b257eeb52cd60d8',2,'1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-12 22:33:59.048','2023-09-12 22:36:57.121'),('e2d6c7f0519499276be4754fbb123d68',0,'3d11f7f23d377f4967cf1550ff19ea5a','745dd9d6da838b1c9887447d4230000c',NULL,'nrOfInstances','integer',NULL,NULL,NULL,NULL,NULL,2,'2',NULL,'2023-09-18 20:13:13.998','2023-09-18 20:13:13.998'),('e3cdf065c9e2f9abf6b177bec262fa75',6,'0c0ae280fcaf4d43a4659c6d2a2e4da0','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 21:51:52.738','2023-09-11 22:15:17.210'),('e49277ce2a9e53515d828036d6ef9429',1,'86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-12 22:37:03.906','2023-09-12 22:37:13.014'),('e74cfa341ead9241420d4c60c68fe353',1,'1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-12 22:33:59.306','2023-09-12 22:36:57.125'),('ece8ee5402ad912fb6e2825928e2c72e',0,'3d11f7f23d377f4967cf1550ff19ea5a','f47767b3297cdc62cbec1835e9873d9b',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'00b6fea67bb10175f39158cb9e00db1e',NULL,'2023-09-18 19:02:41.442','2023-09-18 19:02:41.442'),('ece9fc5b98e15eee1c1948abde997c06',3,'9de499a6f8d8307a34cb9640d6e63d4e','841a9ef7d798527331ae10d8728c3af9',NULL,'nrOfInstances','integer',NULL,NULL,NULL,NULL,NULL,1,'1',NULL,'2023-09-12 22:38:31.124','2023-09-12 22:42:44.014'),('edc45f9895363a0105bc12aced2d63fc',19,'9de499a6f8d8307a34cb9640d6e63d4e','9de499a6f8d8307a34cb9640d6e63d4e',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-12 21:56:26.840','2023-09-12 22:42:43.982'),('f145440dcf2f8591e310e8f25c641bc3',2,'b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:49:02.391','2023-09-11 21:49:56.102'),('f15acef87c9558d5b3de6326eeb85566',2,'1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,'day','double',NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,'2023-09-12 22:33:59.048','2023-09-12 22:36:57.129'),('f5e9e97f777851e244727dea57cc20e4',0,'62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:50:13.093','2023-09-11 21:50:13.093'),('f86b1215b20c3a80744be0eb7f6f0979',0,'c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:53:26.956','2023-09-11 21:53:26.956'),('f973e50a9cdf4b0261a4185821bd1747',9,'5b05d462a25f6e49e5ff3e97c9bb41f4','5b05d462a25f6e49e5ff3e97c9bb41f4',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'C',NULL,'2023-09-15 20:09:27.644','2023-09-15 20:50:16.117'),('fa6e4a0df4ba71494c1b4475628fba52',1,'62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:50:12.990','2023-09-11 21:50:13.096'),('fb6e85989c60ac224f2e86b08a4d03f4',0,'3d11f7f23d377f4967cf1550ff19ea5a','bd2008f11f8c450857638ba18bfd40ea',NULL,'nrOfCompletedInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'bd2008f11f8c450857638ba18bfd40ea','completed','2023-09-18 19:02:41.441','2023-09-18 19:02:41.441'),('fb977c4d15520a3a94bb453495c2f068',0,'3d11f7f23d377f4967cf1550ff19ea5a','8adbfde6e58d0ff7aacd21fa87807b9a',NULL,'nrOfActiveInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'8adbfde6e58d0ff7aacd21fa87807b9a','active','2023-09-18 20:01:19.453','2023-09-18 20:01:19.453'),('fd031115af6728c9bdaf804984b12f6b',4,'678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,'INITIATOR','string',NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,'2023-09-11 21:57:04.653','2023-09-11 22:08:30.771'),('fdbee101383a2ac46c8fb3abf4c27feb',0,'3d11f7f23d377f4967cf1550ff19ea5a','8adbfde6e58d0ff7aacd21fa87807b9a',NULL,'nrOfCompletedInstances','bpmnParallelMultiInstanceCompleted',NULL,NULL,NULL,NULL,NULL,NULL,'8adbfde6e58d0ff7aacd21fa87807b9a','completed','2023-09-18 20:01:19.453','2023-09-18 20:01:19.453'),('fe25de64f41f36def6e08ce972983fa7',35,'f9e728a4640b74e0ec0f501346b794ff','f9e728a4640b74e0ec0f501346b794ff',NULL,'assignee','string',NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL,'2023-09-13 22:22:17.451','2023-09-14 21:39:15.380'),('feb17f42ec5e9338e88849c5ce338c4d',5,'0c0ae280fcaf4d43a4659c6d2a2e4da0','0c0ae280fcaf4d43a4659c6d2a2e4da0',NULL,'submitType','string',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,'2023-09-11 21:51:59.394','2023-09-11 22:15:17.216'),('fee66f42fcd1438e6e9f11dd2a3d5655',2,'9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,'_FLOWABLE_SKIP_EXPRESSION_ENABLED','boolean',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2023-09-11 22:08:06.463','2023-09-11 22:08:25.724');
/*!40000 ALTER TABLE `act_hi_varinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_bytearray`
--

DROP TABLE IF EXISTS `act_id_bytearray`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_bytearray` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_bytearray`
--

LOCK TABLES `act_id_bytearray` WRITE;
/*!40000 ALTER TABLE `act_id_bytearray` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_bytearray` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_group`
--

DROP TABLE IF EXISTS `act_id_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_group` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_group`
--

LOCK TABLES `act_id_group` WRITE;
/*!40000 ALTER TABLE `act_id_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_info`
--

DROP TABLE IF EXISTS `act_id_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_info` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_info`
--

LOCK TABLES `act_id_info` WRITE;
/*!40000 ALTER TABLE `act_id_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_membership`
--

DROP TABLE IF EXISTS `act_id_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_membership` (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`) USING BTREE,
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_membership`
--

LOCK TABLES `act_id_membership` WRITE;
/*!40000 ALTER TABLE `act_id_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_priv`
--

DROP TABLE IF EXISTS `act_id_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_priv` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `ACT_UNIQ_PRIV_NAME` (`NAME_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_priv`
--

LOCK TABLES `act_id_priv` WRITE;
/*!40000 ALTER TABLE `act_id_priv` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_priv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_priv_mapping`
--

DROP TABLE IF EXISTS `act_id_priv_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_priv_mapping` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PRIV_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_FK_PRIV_MAPPING` (`PRIV_ID_`) USING BTREE,
  KEY `ACT_IDX_PRIV_USER` (`USER_ID_`) USING BTREE,
  KEY `ACT_IDX_PRIV_GROUP` (`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `act_id_priv` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_priv_mapping`
--

LOCK TABLES `act_id_priv_mapping` WRITE;
/*!40000 ALTER TABLE `act_id_priv_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_priv_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_property`
--

DROP TABLE IF EXISTS `act_id_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_property` (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REV_` int DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_property`
--

LOCK TABLES `act_id_property` WRITE;
/*!40000 ALTER TABLE `act_id_property` DISABLE KEYS */;
INSERT INTO `act_id_property` VALUES ('schema.version','6.7.2.0',1);
/*!40000 ALTER TABLE `act_id_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_token`
--

DROP TABLE IF EXISTS `act_id_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_token` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NULL DEFAULT NULL,
  `IP_ADDRESS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `USER_AGENT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_token`
--

LOCK TABLES `act_id_token` WRITE;
/*!40000 ALTER TABLE `act_id_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_id_user`
--

DROP TABLE IF EXISTS `act_id_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_id_user` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_id_user`
--

LOCK TABLES `act_id_user` WRITE;
/*!40000 ALTER TABLE `act_id_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_id_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_procdef_info`
--

DROP TABLE IF EXISTS `act_procdef_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_procdef_info` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `ACT_UNIQ_INFO_PROCDEF` (`PROC_DEF_ID_`) USING BTREE,
  KEY `ACT_IDX_INFO_PROCDEF` (`PROC_DEF_ID_`) USING BTREE,
  KEY `ACT_FK_INFO_JSON_BA` (`INFO_JSON_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_procdef_info`
--

LOCK TABLES `act_procdef_info` WRITE;
/*!40000 ALTER TABLE `act_procdef_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_procdef_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_deployment`
--

DROP TABLE IF EXISTS `act_re_deployment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_deployment` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_deployment`
--

LOCK TABLES `act_re_deployment` WRITE;
/*!40000 ALTER TABLE `act_re_deployment` DISABLE KEYS */;
INSERT INTO `act_re_deployment` VALUES ('21a2f1ff1536a9d3f8ecee118eb0287b','请假申请','leave',NULL,'','2023-09-11 21:04:25.171',NULL,NULL,'21a2f1ff1536a9d3f8ecee118eb0287b',NULL);
/*!40000 ALTER TABLE `act_re_deployment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_model`
--

DROP TABLE IF EXISTS `act_re_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_model` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `META_INFO_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_FK_MODEL_SOURCE` (`EDITOR_SOURCE_VALUE_ID_`) USING BTREE,
  KEY `ACT_FK_MODEL_SOURCE_EXTRA` (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) USING BTREE,
  KEY `ACT_FK_MODEL_DEPLOYMENT` (`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_model`
--

LOCK TABLES `act_re_model` WRITE;
/*!40000 ALTER TABLE `act_re_model` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_re_model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_re_procdef`
--

DROP TABLE IF EXISTS `act_re_procdef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_re_procdef` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DERIVED_VERSION_` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`DERIVED_VERSION_`,`TENANT_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_re_procdef`
--

LOCK TABLES `act_re_procdef` WRITE;
/*!40000 ALTER TABLE `act_re_procdef` DISABLE KEYS */;
INSERT INTO `act_re_procdef` VALUES ('LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',2,'leave','请假申请','LeaveApply',1,'21a2f1ff1536a9d3f8ecee118eb0287b','请假申请.bpmn','请假申请.LeaveApply.png',NULL,0,1,1,'',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `act_re_procdef` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_actinst`
--

DROP TABLE IF EXISTS `act_ru_actinst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_actinst` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT '1',
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint DEFAULT NULL,
  `TRANSACTION_ORDER_` int DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_START` (`START_TIME_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_END` (`END_TIME_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_PROC` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_PROC_ACT` (`PROC_INST_ID_`,`ACT_ID_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_EXEC` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_EXEC_ACT` (`EXECUTION_ID_`,`ACT_ID_`) USING BTREE,
  KEY `ACT_IDX_RU_ACTI_TASK` (`TASK_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_actinst`
--

LOCK TABLES `act_ru_actinst` WRITE;
/*!40000 ALTER TABLE `act_ru_actinst` DISABLE KEYS */;
INSERT INTO `act_ru_actinst` VALUES ('067a0ba9bbfd2b01cb88193da59019b2',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-18 19:00:41.621','2023-09-18 19:00:41.623',2,1,NULL,''),('0734deaa04b998a819f57cc2a38a3480',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','05fccf74e5499bdd34c768a84c314896','applyNode','046ac3cb8721b39d3ff0afc3cd18152f',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:16:53.062',NULL,NULL,1,NULL,''),('0b7d6fa3668bae53e7f05ed37dfaa437',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:57:04.655','2023-09-11 21:57:04.661',6,1,NULL,''),('0c2621849a8d870a238ccb0766cd9bda',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:25:27.267','2023-09-11 21:25:27.275',8,1,NULL,''),('0d76456a70b46b9ebe58c2e4a12f7820',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:49:02.394','2023-09-11 21:49:02.404',10,1,NULL,''),('117eb924c45bcbcb0a6b9d11a888b5e5',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','applyNode','d7fdd950a11638d9fb65d8accdd098e0',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 22:00:49.044','2023-09-14 22:18:44.368',1075324,3,'Change activity to mangerNode',''),('13941faf5918d094012ac27a6e9e2a71',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a15dc5534f126d84a6437e2897d8c397','deptLeaderNode','b4835657e917e572f1cef1e11ba8810d',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:01:19.459','2023-09-18 20:01:32.893',13434,2,'Change parent activity to applyNode',''),('14f7910c97da7da57ab9f70b79403b68',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','applyNode','2e76ed0b06f0306b43ae073ef7b62719',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:42.924',NULL,NULL,3,NULL,''),('207024738290b8a074b02917afc3d0c0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 22:08:06.464','2023-09-11 22:08:06.470',6,1,NULL,''),('2ebff706512a9677e3682400f81b341e',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','applyNode','aebb9acc0a2518e35c32714be66758ba',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:25:27.279',NULL,NULL,3,NULL,''),('38129e5b00f2d05efbc305cdde8a9180',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','9cb9c09b8e748dd0d9743cbf3e174a41','deptLeaderNode','7ad75c81b9ba14c310316e745119bcb7',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 20:13:14.001','2023-09-18 20:16:53.049',219048,2,'Change parent activity to applyNode',''),('38e2e3560d8cc93c8bdfcfcfe3c8fd69',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','applyNode','3c9991d349f1d44c616ee8d724d7fb74',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:50:12.991',NULL,NULL,3,NULL,''),('3981face26572694302df630e28f2e7d',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:37:18.050','2023-09-12 22:37:18.050',0,2,NULL,''),('3b61e51e053db7c9835d55b80a80489c',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','2d202e18fe57628c9f77a3e136210bb8','deptLeaderNode','d6f11daa33af21fb33b0cf00d7531e79',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:01:19.456','2023-09-18 20:01:32.916',13460,1,'Change parent activity to applyNode',''),('49403746109d625ba2a529486dafe58f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:50:12.991','2023-09-11 21:50:12.991',0,2,NULL,''),('4eebe99d29add4a4c70aab134ed3b1b0',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.863',0,1,NULL,''),('52462b9475ef8cd78cc7b59340e7df95',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','f47767b3297cdc62cbec1835e9873d9b','deptLeaderNode','304e1eba7ff0593e6697d5cfb1eeb4da',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 19:02:41.444','2023-09-18 19:59:14.724',3393280,1,'Change parent activity to applyNode',''),('59b29f80d5ba29f0e72880b292f32afd',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','applyNode','896bc6db02674af24a5d6f050a2f3db0',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:06.473','2023-09-11 22:08:25.804',19331,3,'Change activity to mangerNode',''),('5e8f49c300d46e18e45eb641e3d0f942',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','applyNode','1be90845469a774c0830dc4cba2009be',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:30:45.579',NULL,NULL,3,NULL,''),('5e9143e1f5316a85a64ae9a9ad6e5f3c',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-14 21:59:15.758','2023-09-14 21:59:15.758',0,2,NULL,''),('5ee37d9514ec23a0d0e00d4e40f71afe',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','0430a904e50699051bbb092cf766e9bf','mangerNode','09ef88659150862142596dd67eda5a78',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:12:46.629','2023-09-18 20:13:13.987',27358,1,'Change activity to deptLeaderNode',''),('634e1771c7976455d0ba760e149659ae',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-14 22:00:49.044','2023-09-14 22:00:49.044',0,1,NULL,''),('7204038084a46009705d6cc7a8619f6a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 22:30:45.578','2023-09-11 22:30:45.578',0,2,NULL,''),('7e67c7d00fdd6e82ca9ba14a7a6d2deb',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','a8bc69357fafd4c1f03d0fbe3b1b1de5','mangerNode','03f82c30eb7300efe00f29a221ea2bc9',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:08.293','2023-09-18 20:01:19.435',11142,1,'Change activity to deptLeaderNode',''),('83dacadc0c8d10766f60b47377e0a703',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:52:30.137','2023-09-11 21:52:30.137',0,1,NULL,''),('83e52e520d12dc430f748b4cc4625da4',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','applyNode','fb2577c0df7cea22e8f8a6203fa6e2fe',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:41.628','2023-09-18 19:00:47.735',6107,3,'Change activity to mangerNode',''),('8699ac0e1ced2bf686dde789a6b9723a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-14 21:59:15.750','2023-09-14 21:59:15.754',4,1,NULL,''),('86a23053dd3b5f8c7af01c8091dab965',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:37:18.050','2023-09-12 22:37:18.050',0,1,NULL,''),('86bbbf4f360f4786fea76eb58402f1c6',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e2657e6406c2a9c2e7ef4f8b61d06a12','ae401bd6eb8517ebcec57f7159eae912','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:25:27.279','2023-09-11 21:25:27.279',0,2,NULL,''),('905ea6f79b70aa4ddaef82f643b7ef9a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','applyNode','578f6b194aa3e1371e0ee45eb49e7d78',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:53:26.863',NULL,NULL,3,NULL,''),('96dc51f1d1bcbdd927971988ce675374',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','applyNode','9b931808d034cb271160bc1390ccf248',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:03.755',NULL,NULL,3,NULL,''),('97dd85d02b724cb171d8e2762d61019a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','48cb4c54ce008b4ac292c3ba18044a56','41da80b1a9dedbed14c6470971727a18','applyNode','67d94a5144c1fe8e89e70c134f9d4de1',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 21:59:15.758',NULL,NULL,3,NULL,''),('99d1384cc2989b650427b4dc6b4d7394',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','4655b44e3e75e87d6c34bc401df1ddec','applyNode','d25650926a27824c9146bf8e414cca74',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:59:14.794','2023-09-18 20:01:08.285',113491,1,'Change activity to mangerNode',''),('9af4a2a8ac0fad885fb700fad7d26772',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','applyNode','265377b48122d2ec995d6beedcf08f17',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:57:04.665',NULL,NULL,3,NULL,''),('9f5e29c6dee479bf560c7d89e0de3111',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:03.755',0,2,NULL,''),('a6928bfa2cc48cb6b16c971f619d7b92',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:49:02.408','2023-09-11 21:49:02.408',0,2,NULL,''),('a73ea3a5ad885c6da1e4f2d0bdc83394',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','9dea8978754d867d28e261c3429eb799','deptLeaderNode','251de12881c8555f6c1f63f61a4c4be3',NULL,'部门领导审批','userTask','00b6fea67bb10175f39158cb9e00db1e','2023-09-18 20:13:13.999','2023-09-18 20:16:53.038',219039,1,'Change parent activity to applyNode',''),('abc52aaf48c8e811826529b0e5ac613e',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','62580c574add9b784f1b6e2114561a0d','3e1fe020967f17ff93afa84e1f194cc8','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:50:12.990','2023-09-11 21:50:12.991',1,1,NULL,''),('ae8c76c66ed7ff9aa4859c0bb0288aa9',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:42.924',0,1,NULL,''),('b3708f559d2bea9ea1954d2dcc32b194',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','e73736cde5d5a8e1a04b900134cfd846','cbba13eda6e646f5acf7e7a4efc7c6c4','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-11 22:30:45.566','2023-09-11 22:30:45.575',9,1,NULL,''),('b4308fdc66d09d701a873472f30e5e41',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','95a49737bdee7652e74ec5eb0c0a922c','applyNode','d413ed765799eaa1c7ea6abb70988d56',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 20:01:32.949','2023-09-18 20:12:46.618',673669,1,'Change activity to mangerNode',''),('b4added7e63914aa7a73d44de102f9d1',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','d5eb46d7099e367012959854ae72759f','mangerNode','1800d422aaf3f7847dbca50ae6ff861d',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-14 22:18:44.373',NULL,NULL,1,NULL,''),('babb262f7eb2bc409f8a9d6dd74a36ef',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','applyNode','0a413d9c6ef53463e2e21852493e9790',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:33:59.049',NULL,NULL,3,NULL,''),('be35da3a9dcec515253e925c944572ed',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','applyNode','d2bda77e898d40290f8e7d96ec70cf17',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:52:30.137',NULL,NULL,3,NULL,''),('c32d8234c6f06c9f7b178c5f59d63d36',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9790b57ffcc326c11d1867972ff55876','3c74956a66bdb1737db9a91da1b2baf5','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:52:30.137','2023-09-11 21:52:30.137',0,2,NULL,''),('c8cb773877ad374198acebb46d0e1b1a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','2c43cad6e817cb50a9131351a79b6e4a','da944bee1b84932fcc512f5a985e0d7f','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-14 22:00:49.044','2023-09-14 22:00:49.044',0,2,NULL,''),('c98ef4e8cec65272051be1f692989d66',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','86a2f9b8410887c3f77a136b68a96c77','380aa891f7112eaa3ced5712daa25dab','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:37:03.755','2023-09-12 22:37:03.755',0,1,NULL,''),('d4143849b075014a17f26984fa6aa45b',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','cf9b32c4462a2ee9a295ce6343ce82d0','037f0352764982500f0581f94ae46dd0','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:50:42.924','2023-09-11 21:50:42.924',0,2,NULL,''),('d84a9d233a79b32f8f9ecd633d3bc27f',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','startNode1',NULL,NULL,'开始','startEvent',NULL,'2023-09-12 22:33:59.049','2023-09-12 22:33:59.049',0,1,NULL,''),('d9c728ec368de13cb2e518db84eddf6f',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','36a3b2c4854b0651e2151a965cc73cb3','mangerNode','d870f96180591307aab4f7256dd44306',NULL,'主管审批','userTask','78f97db459233bc461df091b862fb7e3','2023-09-18 19:00:47.741','2023-09-18 19:02:41.429',113688,1,'Change activity to deptLeaderNode',''),('e074256a865a4db1fbe6d9beb2d8d284',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','e1045e978e8fef0ac700073b5115f8fa','mangerNode','c28cc4716c70d343fb9adb4de728bfea',NULL,'主管审批','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 22:08:25.819',NULL,NULL,1,NULL,''),('e201f1a92aa5745b375aabfe65185104',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','1b8c6cf5877ddf134f4b149408024b37','95e1735a6d8fed8aff73189a9caec1f1','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-12 22:33:59.049','2023-09-12 22:33:59.049',0,2,NULL,''),('e59890453140325179407e622dc4ce3a',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','b971297aac58a442a089dd33612510be','3e3dd6f3017bdacde40295da82de62b6','applyNode','3e55fbc84ca581895a74a36f66149acb',NULL,'申请人修改','userTask','a50f1994deb7e358e04ca0b1eb938ee1','2023-09-11 21:49:02.408',NULL,NULL,3,NULL,''),('e881225b01e8ebeff73f50e2006536e2',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','c0b03c7d121b3ca701b5433bce783533','72d80cd2cf99408da4e95c4d8bfc78ba','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:53:26.863','2023-09-11 21:53:26.863',0,2,NULL,''),('ec067be7f432f4d104d41ea96767a720',2,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','37e99c1669a1c61f10d7fcee86116e03','deptLeaderNode','64131a55a62f212ce843f08d6adfe365',NULL,'部门领导审批','userTask','9a8ccb159ac609b176825e9a6b4039d3','2023-09-18 19:02:41.448','2023-09-18 19:59:14.758',3393310,2,'Change parent activity to applyNode',''),('f27617a54a230a95dca72fe98caec511',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','861e6ffa7abfee87cd95c08f4fcdc6c8','38bd4213921cdfb88d44cd8e405f2c61','applyNode','9b1fd3de1a8bce92eb9e93b9804c6b1f',NULL,'申请人修改','userTask','78f97db459233bc461df091b862fb7e3','2023-09-12 22:37:18.050',NULL,NULL,3,NULL,''),('f2cd5dd6549e79fa3acd987bd2aefc68',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','678f8c7260b7992967d4175d5694b4b5','27313e568fef209e9132c3a6d13be830','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 21:57:04.665','2023-09-11 21:57:04.665',0,2,NULL,''),('f9817e4c65107563b890abf079f0aadf',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','9b15f4abc8ba5dcf905885c5f73489e9','148537b31d7c759d695237189cc61816','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-11 22:08:06.473','2023-09-11 22:08:06.473',0,2,NULL,''),('fa84a3eabf09c02556bca28b1d8f73f4',1,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','3d11f7f23d377f4967cf1550ff19ea5a','c3187c90b9954f0669aecc4b995c7d07','Flow_0nfqvai',NULL,NULL,'','sequenceFlow',NULL,'2023-09-18 19:00:41.628','2023-09-18 19:00:41.628',0,2,NULL,'');
/*!40000 ALTER TABLE `act_ru_actinst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_deadletter_job`
--

DROP TABLE IF EXISTS `act_ru_deadletter_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_deadletter_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`) USING BTREE,
  KEY `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`) USING BTREE,
  KEY `ACT_IDX_DEADLETTER_JOB_CORRELATION_ID` (`CORRELATION_ID_`) USING BTREE,
  KEY `ACT_IDX_DJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_DJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_DJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_DEADLETTER_JOB_EXECUTION` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`) USING BTREE,
  KEY `ACT_FK_DEADLETTER_JOB_PROC_DEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_deadletter_job`
--

LOCK TABLES `act_ru_deadletter_job` WRITE;
/*!40000 ALTER TABLE `act_ru_deadletter_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_deadletter_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_entitylink`
--

DROP TABLE IF EXISTS `act_ru_entitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_entitylink` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_ENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_ENT_LNK_REF_SCOPE` (`REF_SCOPE_ID_`,`REF_SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_ENT_LNK_ROOT_SCOPE` (`ROOT_SCOPE_ID_`,`ROOT_SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE,
  KEY `ACT_IDX_ENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`,`LINK_TYPE_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_entitylink`
--

LOCK TABLES `act_ru_entitylink` WRITE;
/*!40000 ALTER TABLE `act_ru_entitylink` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_entitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_event_subscr`
--

DROP TABLE IF EXISTS `act_ru_event_subscr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_event_subscr` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CONFIGURATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_EVENT_SUBSCR_CONFIG_` (`CONFIGURATION_`) USING BTREE,
  KEY `ACT_FK_EVENT_EXEC` (`EXECUTION_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_event_subscr`
--

LOCK TABLES `act_ru_event_subscr` WRITE;
/*!40000 ALTER TABLE `act_ru_event_subscr` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_event_subscr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_execution`
--

DROP TABLE IF EXISTS `act_ru_execution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_execution` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint DEFAULT NULL,
  `IS_CONCURRENT_` tinyint DEFAULT NULL,
  `IS_SCOPE_` tinyint DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint DEFAULT NULL,
  `IS_MI_ROOT_` tinyint DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `CACHED_ENT_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int DEFAULT NULL,
  `TASK_COUNT_` int DEFAULT NULL,
  `JOB_COUNT_` int DEFAULT NULL,
  `TIMER_JOB_COUNT_` int DEFAULT NULL,
  `SUSP_JOB_COUNT_` int DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int DEFAULT NULL,
  `EXTERNAL_WORKER_JOB_COUNT_` int DEFAULT NULL,
  `VAR_COUNT_` int DEFAULT NULL,
  `ID_LINK_COUNT_` int DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`) USING BTREE,
  KEY `ACT_IDC_EXEC_ROOT` (`ROOT_PROC_INST_ID_`) USING BTREE,
  KEY `ACT_IDX_EXEC_REF_ID_` (`REFERENCE_ID_`) USING BTREE,
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`) USING BTREE,
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`) USING BTREE,
  KEY `ACT_FK_EXE_PROCDEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_execution`
--

LOCK TABLES `act_ru_execution` WRITE;
/*!40000 ALTER TABLE `act_ru_execution` DISABLE KEYS */;
INSERT INTO `act_ru_execution` VALUES ('037f0352764982500f0581f94ae46dd0',1,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,'cf9b32c4462a2ee9a295ce6343ce82d0','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'cf9b32c4462a2ee9a295ce6343ce82d0','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:50:42.924',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('05fccf74e5499bdd34c768a84c314896',1,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,'3d11f7f23d377f4967cf1550ff19ea5a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'3d11f7f23d377f4967cf1550ff19ea5a','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-18 20:16:53.059',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('1b8c6cf5877ddf134f4b149408024b37',1,'1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'1b8c6cf5877ddf134f4b149408024b37',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-12 22:33:59.048','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('27313e568fef209e9132c3a6d13be830',1,'678f8c7260b7992967d4175d5694b4b5',NULL,'678f8c7260b7992967d4175d5694b4b5','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'678f8c7260b7992967d4175d5694b4b5','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:57:04.655',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('2c43cad6e817cb50a9131351a79b6e4a',1,'2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'2c43cad6e817cb50a9131351a79b6e4a',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-14 22:00:49.043','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('380aa891f7112eaa3ced5712daa25dab',1,'86a2f9b8410887c3f77a136b68a96c77',NULL,'86a2f9b8410887c3f77a136b68a96c77','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'86a2f9b8410887c3f77a136b68a96c77','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-12 22:37:03.755',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('38bd4213921cdfb88d44cd8e405f2c61',1,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,'861e6ffa7abfee87cd95c08f4fcdc6c8','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'861e6ffa7abfee87cd95c08f4fcdc6c8','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-12 22:37:18.050',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('3c74956a66bdb1737db9a91da1b2baf5',1,'9790b57ffcc326c11d1867972ff55876',NULL,'9790b57ffcc326c11d1867972ff55876','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'9790b57ffcc326c11d1867972ff55876','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:52:30.137',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('3d11f7f23d377f4967cf1550ff19ea5a',1,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-18 19:00:41.615','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('3e1fe020967f17ff93afa84e1f194cc8',1,'62580c574add9b784f1b6e2114561a0d',NULL,'62580c574add9b784f1b6e2114561a0d','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'62580c574add9b784f1b6e2114561a0d','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:50:12.990',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('3e3dd6f3017bdacde40295da82de62b6',1,'b971297aac58a442a089dd33612510be',NULL,'b971297aac58a442a089dd33612510be','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'b971297aac58a442a089dd33612510be','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:49:02.394',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('41da80b1a9dedbed14c6470971727a18',1,'48cb4c54ce008b4ac292c3ba18044a56',NULL,'48cb4c54ce008b4ac292c3ba18044a56','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'48cb4c54ce008b4ac292c3ba18044a56','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-14 21:59:15.750',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('48cb4c54ce008b4ac292c3ba18044a56',1,'48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'48cb4c54ce008b4ac292c3ba18044a56',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-14 21:59:15.747','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('62580c574add9b784f1b6e2114561a0d',1,'62580c574add9b784f1b6e2114561a0d',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'62580c574add9b784f1b6e2114561a0d',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:50:12.990','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('678f8c7260b7992967d4175d5694b4b5',1,'678f8c7260b7992967d4175d5694b4b5',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'678f8c7260b7992967d4175d5694b4b5',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:57:04.644','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('72d80cd2cf99408da4e95c4d8bfc78ba',1,'c0b03c7d121b3ca701b5433bce783533',NULL,'c0b03c7d121b3ca701b5433bce783533','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'c0b03c7d121b3ca701b5433bce783533','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:53:26.863',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('861e6ffa7abfee87cd95c08f4fcdc6c8',1,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-12 22:37:18.049','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('86a2f9b8410887c3f77a136b68a96c77',1,'86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'86a2f9b8410887c3f77a136b68a96c77',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-12 22:37:03.754','78f97db459233bc461df091b862fb7e3',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('95e1735a6d8fed8aff73189a9caec1f1',1,'1b8c6cf5877ddf134f4b149408024b37',NULL,'1b8c6cf5877ddf134f4b149408024b37','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'1b8c6cf5877ddf134f4b149408024b37','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-12 22:33:59.048',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('9790b57ffcc326c11d1867972ff55876',1,'9790b57ffcc326c11d1867972ff55876',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'9790b57ffcc326c11d1867972ff55876',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:52:30.136','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('9b15f4abc8ba5dcf905885c5f73489e9',1,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 22:08:06.458','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('ae401bd6eb8517ebcec57f7159eae912',1,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,'e2657e6406c2a9c2e7ef4f8b61d06a12','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'e2657e6406c2a9c2e7ef4f8b61d06a12','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 21:25:27.266',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('b971297aac58a442a089dd33612510be',1,'b971297aac58a442a089dd33612510be',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'b971297aac58a442a089dd33612510be',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:49:02.381','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('c0b03c7d121b3ca701b5433bce783533',1,'c0b03c7d121b3ca701b5433bce783533',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'c0b03c7d121b3ca701b5433bce783533',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:53:26.862','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('cbba13eda6e646f5acf7e7a4efc7c6c4',1,'e73736cde5d5a8e1a04b900134cfd846',NULL,'e73736cde5d5a8e1a04b900134cfd846','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'e73736cde5d5a8e1a04b900134cfd846','applyNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 22:30:45.565',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('cf9b32c4462a2ee9a295ce6343ce82d0',1,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:50:42.924','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('d5eb46d7099e367012959854ae72759f',1,'2c43cad6e817cb50a9131351a79b6e4a',NULL,'2c43cad6e817cb50a9131351a79b6e4a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'2c43cad6e817cb50a9131351a79b6e4a','mangerNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-14 22:18:44.371',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('e1045e978e8fef0ac700073b5115f8fa',1,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,'9b15f4abc8ba5dcf905885c5f73489e9','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'9b15f4abc8ba5dcf905885c5f73489e9','mangerNode',1,0,0,0,0,1,NULL,'',NULL,NULL,'2023-09-11 22:08:25.816',NULL,NULL,NULL,1,0,1,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('e2657e6406c2a9c2e7ef4f8b61d06a12',1,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 21:25:27.257','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL),('e73736cde5d5a8e1a04b900134cfd846',1,'e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,'LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,'e73736cde5d5a8e1a04b900134cfd846',NULL,1,0,1,0,0,1,NULL,'',NULL,'startNode1','2023-09-11 22:30:45.555','a50f1994deb7e358e04ca0b1eb938ee1',NULL,NULL,1,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `act_ru_execution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_external_job`
--

DROP TABLE IF EXISTS `act_ru_external_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_external_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_EXTERNAL_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`) USING BTREE,
  KEY `ACT_IDX_EXTERNAL_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`) USING BTREE,
  KEY `ACT_IDX_EXTERNAL_JOB_CORRELATION_ID` (`CORRELATION_ID_`) USING BTREE,
  KEY `ACT_IDX_EJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_EJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_EJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_external_job`
--

LOCK TABLES `act_ru_external_job` WRITE;
/*!40000 ALTER TABLE `act_ru_external_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_external_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_history_job`
--

DROP TABLE IF EXISTS `act_ru_history_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_history_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_history_job`
--

LOCK TABLES `act_ru_history_job` WRITE;
/*!40000 ALTER TABLE `act_ru_history_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_history_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_identitylink`
--

DROP TABLE IF EXISTS `act_ru_identitylink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_identitylink` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`) USING BTREE,
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`) USING BTREE,
  KEY `ACT_IDX_IDENT_LNK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_IDENT_LNK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_IDENT_LNK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_ATHRZ_PROCEDEF` (`PROC_DEF_ID_`) USING BTREE,
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`) USING BTREE,
  KEY `ACT_FK_IDL_PROCINST` (`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_identitylink`
--

LOCK TABLES `act_ru_identitylink` WRITE;
/*!40000 ALTER TABLE `act_ru_identitylink` DISABLE KEYS */;
INSERT INTO `act_ru_identitylink` VALUES ('0aff610fc27a0c0038585802b542ea75',1,NULL,'participant','9a8ccb159ac609b176825e9a6b4039d3',NULL,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL),('0c7003861b852a8c92f1d762e0efa36c',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL),('0c8502db93cecf5c7c99b833dc5648de',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL),('1097c30702cff693b03f229599036265',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL),('123ad962966abae96b90c9fa791f04e1',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL),('1fd95e19e67a9a7687ec4d47a225940c',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL),('232ec36017c3cea13f8dae2272e8c097',1,NULL,'participant','00b6fea67bb10175f39158cb9e00db1e',NULL,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL),('3e2a75f9ba15ed748f388a360ba8103b',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL,NULL),('3f41ac9478f736d19045f11097d689d2',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL,NULL),('62af1c0dbdf1fff0c6063bfbc9bdca9b',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL,NULL),('6659bf4a1c8e171625f2dc1a8bb5f190',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL),('67bb28fd308f8f9e10becd441291fcaa',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL,NULL),('68361055c16df69d718f0d0ac290fa9a',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL),('7420b0580587a70e3d269bcf5d228ce0',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL),('7567d516b082781a36cd668cd6672823',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL),('7dd189748cf42c67b3ca1890b0a7c85d',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL,NULL),('7de603a9f4fcb6f7632dfeaeb98ef124',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL,NULL),('8659df4649ef985a3bdbdcdbfb7caf33',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL,NULL),('97490042b45557cc1e8fa67a7e979be8',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL,NULL),('9e12cd282137b707fa79a44bd9483f53',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL,NULL),('9e4fc4eec29886980977c079d53589c7',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL),('b08ff6264502eab59e15383a7b73d043',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL,NULL),('b0bcb9723ff64153c252a9a759b260af',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL,NULL),('b103ba70a08ca806d088c38f3a017f20',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL),('b43978fc258de19f47d73a6e964ae683',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL),('be99d54f937451d9f3ab5d5edc10a0cf',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL),('c202be75df95cfdd10c96049c2ed71c8',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL),('c2421c1d71b5d9183d0bba612d291154',1,NULL,'starter','78f97db459233bc461df091b862fb7e3',NULL,'2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL),('cb8c77185775b12d1e9d3734c1556314',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL),('e16f75012ab82314ba76463796c3c10c',1,NULL,'starter','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL),('f0e198226eef42d9f000097d8440fd0f',1,'945cc85588578f74143ff672085c25a7','candidate',NULL,'c28cc4716c70d343fb9adb4de728bfea',NULL,NULL,NULL,NULL,NULL,NULL),('f25eec945c7a2f007dac976e419aff74',1,NULL,'participant','78f97db459233bc461df091b862fb7e3',NULL,'48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL),('f89c8879a35b249d8b030802a78f7cf9',1,NULL,'participant','a50f1994deb7e358e04ca0b1eb938ee1',NULL,'e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL,NULL),('fad9f449220398be24d45ecc1827b44f',1,'945cc85588578f74143ff672085c25a7','candidate',NULL,'1800d422aaf3f7847dbca50ae6ff861d',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `act_ru_identitylink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_job`
--

DROP TABLE IF EXISTS `act_ru_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`) USING BTREE,
  KEY `ACT_IDX_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`) USING BTREE,
  KEY `ACT_IDX_JOB_CORRELATION_ID` (`CORRELATION_ID_`) USING BTREE,
  KEY `ACT_IDX_JOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_JOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_JOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_JOB_EXECUTION` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`) USING BTREE,
  KEY `ACT_FK_JOB_PROC_DEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_job`
--

LOCK TABLES `act_ru_job` WRITE;
/*!40000 ALTER TABLE `act_ru_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_suspended_job`
--

DROP TABLE IF EXISTS `act_ru_suspended_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_suspended_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`) USING BTREE,
  KEY `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`) USING BTREE,
  KEY `ACT_IDX_SUSPENDED_JOB_CORRELATION_ID` (`CORRELATION_ID_`) USING BTREE,
  KEY `ACT_IDX_SJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_SJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_SJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_SUSPENDED_JOB_EXECUTION` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`) USING BTREE,
  KEY `ACT_FK_SUSPENDED_JOB_PROC_DEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_suspended_job`
--

LOCK TABLES `act_ru_suspended_job` WRITE;
/*!40000 ALTER TABLE `act_ru_suspended_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_suspended_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_task`
--

DROP TABLE IF EXISTS `act_ru_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_task` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUSPENSION_STATE_` int DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint DEFAULT NULL,
  `VAR_COUNT_` int DEFAULT NULL,
  `ID_LINK_COUNT_` int DEFAULT NULL,
  `SUB_TASK_COUNT_` int DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`) USING BTREE,
  KEY `ACT_IDX_TASK_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_TASK_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_TASK_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`) USING BTREE,
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_task`
--

LOCK TABLES `act_ru_task` WRITE;
/*!40000 ALTER TABLE `act_ru_task` DISABLE KEYS */;
INSERT INTO `act_ru_task` VALUES ('046ac3cb8721b39d3ff0afc3cd18152f',7,'05fccf74e5499bdd34c768a84c314896','3d11f7f23d377f4967cf1550ff19ea5a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-18 20:16:53.062',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('0a413d9c6ef53463e2e21852493e9790',3,'95e1735a6d8fed8aff73189a9caec1f1','1b8c6cf5877ddf134f4b149408024b37','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-12 22:33:59.049',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('1800d422aaf3f7847dbca50ae6ff861d',24,'d5eb46d7099e367012959854ae72759f','2c43cad6e817cb50a9131351a79b6e4a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,'mangerNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-14 22:18:44.373',NULL,NULL,1,'',NULL,NULL,1,0,1,0),('1be90845469a774c0830dc4cba2009be',4,'cbba13eda6e646f5acf7e7a4efc7c6c4','e73736cde5d5a8e1a04b900134cfd846','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 22:30:45.579',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('265377b48122d2ec995d6beedcf08f17',5,'27313e568fef209e9132c3a6d13be830','678f8c7260b7992967d4175d5694b4b5','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:57:04.665',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('2e76ed0b06f0306b43ae073ef7b62719',2,'037f0352764982500f0581f94ae46dd0','cf9b32c4462a2ee9a295ce6343ce82d0','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:50:42.924',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('3c9991d349f1d44c616ee8d724d7fb74',2,'3e1fe020967f17ff93afa84e1f194cc8','62580c574add9b784f1b6e2114561a0d','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:50:12.991',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('3e55fbc84ca581895a74a36f66149acb',3,'3e3dd6f3017bdacde40295da82de62b6','b971297aac58a442a089dd33612510be','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:49:02.408',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('578f6b194aa3e1371e0ee45eb49e7d78',2,'72d80cd2cf99408da4e95c4d8bfc78ba','c0b03c7d121b3ca701b5433bce783533','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:53:26.863',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('67d94a5144c1fe8e89e70c134f9d4de1',7,'41da80b1a9dedbed14c6470971727a18','48cb4c54ce008b4ac292c3ba18044a56','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-14 21:59:15.758',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('9b1fd3de1a8bce92eb9e93b9804c6b1f',3,'38bd4213921cdfb88d44cd8e405f2c61','861e6ffa7abfee87cd95c08f4fcdc6c8','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-12 22:37:18.050',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('9b931808d034cb271160bc1390ccf248',3,'380aa891f7112eaa3ced5712daa25dab','86a2f9b8410887c3f77a136b68a96c77','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'78f97db459233bc461df091b862fb7e3',NULL,50,'2023-09-12 22:37:03.755',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('aebb9acc0a2518e35c32714be66758ba',12,'ae401bd6eb8517ebcec57f7159eae912','e2657e6406c2a9c2e7ef4f8b61d06a12','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:25:27.279',NULL,NULL,1,'',NULL,NULL,1,0,0,0),('c28cc4716c70d343fb9adb4de728bfea',1,'e1045e978e8fef0ac700073b5115f8fa','9b15f4abc8ba5dcf905885c5f73489e9','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'主管审批',NULL,NULL,'mangerNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 22:08:25.819',NULL,NULL,1,'',NULL,NULL,1,0,1,0),('d2bda77e898d40290f8e7d96ec70cf17',3,'3c74956a66bdb1737db9a91da1b2baf5','9790b57ffcc326c11d1867972ff55876','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c',NULL,NULL,NULL,NULL,NULL,NULL,'申请人修改',NULL,NULL,'applyNode',NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL,50,'2023-09-11 21:52:30.137',NULL,NULL,1,'',NULL,NULL,1,0,0,0);
/*!40000 ALTER TABLE `act_ru_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_timer_job`
--

DROP TABLE IF EXISTS `act_ru_timer_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_timer_job` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID` (`EXCEPTION_STACK_ID_`) USING BTREE,
  KEY `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID` (`CUSTOM_VALUES_ID_`) USING BTREE,
  KEY `ACT_IDX_TIMER_JOB_CORRELATION_ID` (`CORRELATION_ID_`) USING BTREE,
  KEY `ACT_IDX_TIMER_JOB_DUEDATE` (`DUEDATE_`) USING BTREE,
  KEY `ACT_IDX_TJOB_SCOPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_TJOB_SUB_SCOPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_TJOB_SCOPE_DEF` (`SCOPE_DEFINITION_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_TIMER_JOB_EXECUTION` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`) USING BTREE,
  KEY `ACT_FK_TIMER_JOB_PROC_DEF` (`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_timer_job`
--

LOCK TABLES `act_ru_timer_job` WRITE;
/*!40000 ALTER TABLE `act_ru_timer_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_ru_timer_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_ru_variable`
--

DROP TABLE IF EXISTS `act_ru_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `act_ru_variable` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `ACT_IDX_RU_VAR_SCOPE_ID_TYPE` (`SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_IDX_RU_VAR_SUB_ID_TYPE` (`SUB_SCOPE_ID_`,`SCOPE_TYPE_`) USING BTREE,
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`) USING BTREE,
  KEY `ACT_IDX_VARIABLE_TASK_ID` (`TASK_ID_`) USING BTREE,
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`) USING BTREE,
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_ru_variable`
--

LOCK TABLES `act_ru_variable` WRITE;
/*!40000 ALTER TABLE `act_ru_variable` DISABLE KEYS */;
INSERT INTO `act_ru_variable` VALUES ('044b4e75b27e47421a03e5468ddf6721',1,'string','assignee','9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('0517aa053ed3e76a435f9444abe9820b',1,'string','INITIATOR','9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('077a16db65511cdcb6c3bdf0dfe61fa6',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('0f59598d173e8f7ade6febab52f923a9',1,'double','day','86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL),('107220c1c9d88ce82ff64edf656c141e',1,'string','INITIATOR','cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('12670db4b1f48640030dc3023488acc8',1,'string','assignee','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('12cce0aa76fe36b8f0f621f96b631e32',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('171c58cd4aa96dc92258ff245ef4d1cd',1,'string','submitType','2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('18cba817dad86bb06beecccd5835bb9b',1,'string','submitType','b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('247613ed446335fed6edeb9596b439f4',1,'serializable','assigneeList','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,'172f5342d6d2f95ec57a246d9a4ed4e8',NULL,NULL,NULL,NULL),('300ffe514eb77217e23feb0b2c428316',1,'string','submitType','9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('3786d210efdf5d49b8c519480d02ffe2',1,'string','INITIATOR','48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('37b19cfd8814c12bfbad89b9b5e4b19c',1,'string','INITIATOR','86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('387923c02c452cb8ceee6c002b814cb7',1,'string','assignee','2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('38888676b1eb8c7882c5ae34f9bd9beb',1,'string','submitType','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('3d13de24158076f41d09dd16b18016e8',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('3f7e453f9b00396739c5bea2d520d613',1,'string','submitType','48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('403f76a317a817b7be15f5acca06c662',1,'string','submitType','678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('4cb0e002d097eb60f30e710087d44951',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('4fa5763450669866724b1d5264e45331',1,'double','day','861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL),('51e6723d707761b830bf91fc8242f913',1,'string','INITIATOR','c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('647a0c143e8a1ca9e1c500765504ce2e',1,'string','submitType','9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('677e133da4db32589edc670037e45f37',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('67987ae65981e5332d57a0e855f4187e',1,'double','day','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL,7,NULL,NULL,NULL),('6aa8dcb9b5c028dee48b133744e630db',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('6bfab9bafa925e4d654fc839c16d9e37',1,'string','INITIATOR','1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('6c41f0dec0c1007ef3bebfd0939dfb74',1,'string','submitType','861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('72919b6226d21d6092990ecd5710f583',1,'double','day','2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL),('729dff692685065e7ea4e568aa7941bc',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('72a62636e4d6154c4b3356b46436abfa',1,'double','day','e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL,17,NULL,NULL,NULL),('747551465cf8d47663de88f78c8a8b07',1,'string','submitType','e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('76b4f19d0ab6ba9cf586b9e94c7cb2cd',1,'double','day','48cb4c54ce008b4ac292c3ba18044a56','48cb4c54ce008b4ac292c3ba18044a56',NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL),('795e320cef539eccff9843d05bafb720',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('8a570bb76021dd5edb344016a10a7cb0',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('93a86db1b3e0061a8f1c7b2e28c9e4c1',1,'string','INITIATOR','861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('9811c4d49842e6620d7ba1d6f6e114b1',1,'string','INITIATOR','e2657e6406c2a9c2e7ef4f8b61d06a12','e2657e6406c2a9c2e7ef4f8b61d06a12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('b8f4cdf64d9371d4826d50a636afe919',1,'string','submitType','e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('be5128485317ea435feb84b920941402',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('c5fb9f9caab85f0a365d47754394cac5',1,'double','day','9790b57ffcc326c11d1867972ff55876','9790b57ffcc326c11d1867972ff55876',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL),('c65cab4a76aa0c46689891fa932cdf1d',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','861e6ffa7abfee87cd95c08f4fcdc6c8','861e6ffa7abfee87cd95c08f4fcdc6c8',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('c6e2b20ea0f94732e7743d6d48be67c4',1,'string','INITIATOR','2c43cad6e817cb50a9131351a79b6e4a','2c43cad6e817cb50a9131351a79b6e4a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('c86b0ee13e2eb0a9522915a3a3519959',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('c9819b97d94bcd589a8fc2f96a90dc7a',1,'string','INITIATOR','e73736cde5d5a8e1a04b900134cfd846','e73736cde5d5a8e1a04b900134cfd846',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('cbd56d7a219f7984343b4a2d40106438',1,'string','INITIATOR','3d11f7f23d377f4967cf1550ff19ea5a','3d11f7f23d377f4967cf1550ff19ea5a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'78f97db459233bc461df091b862fb7e3',NULL),('cfae8e9e13accebaa1d3262d8e41db03',1,'string','INITIATOR','9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('d86f5dd264fb5de8a381f7d6df3deacb',1,'string','submitType','cf9b32c4462a2ee9a295ce6343ce82d0','cf9b32c4462a2ee9a295ce6343ce82d0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('dc4b12404dee3c2d85998c08e49850a3',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('dcc33da13e19f52c4b257eeb52cd60d8',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL),('e49277ce2a9e53515d828036d6ef9429',1,'string','submitType','86a2f9b8410887c3f77a136b68a96c77','86a2f9b8410887c3f77a136b68a96c77',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('e74cfa341ead9241420d4c60c68fe353',1,'string','submitType','1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('f145440dcf2f8591e310e8f25c641bc3',1,'string','INITIATOR','b971297aac58a442a089dd33612510be','b971297aac58a442a089dd33612510be',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('f15acef87c9558d5b3de6326eeb85566',1,'double','day','1b8c6cf5877ddf134f4b149408024b37','1b8c6cf5877ddf134f4b149408024b37',NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL),('f5e9e97f777851e244727dea57cc20e4',1,'string','submitType','62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('f86b1215b20c3a80744be0eb7f6f0979',1,'string','submitType','c0b03c7d121b3ca701b5433bce783533','c0b03c7d121b3ca701b5433bce783533',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL),('fa6e4a0df4ba71494c1b4475628fba52',1,'string','INITIATOR','62580c574add9b784f1b6e2114561a0d','62580c574add9b784f1b6e2114561a0d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('fd031115af6728c9bdaf804984b12f6b',1,'string','INITIATOR','678f8c7260b7992967d4175d5694b4b5','678f8c7260b7992967d4175d5694b4b5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a50f1994deb7e358e04ca0b1eb938ee1',NULL),('fee66f42fcd1438e6e9f11dd2a3d5655',1,'boolean','_FLOWABLE_SKIP_EXPRESSION_ENABLED','9b15f4abc8ba5dcf905885c5f73489e9','9b15f4abc8ba5dcf905885c5f73489e9',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `act_ru_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_channel_definition`
--

DROP TABLE IF EXISTS `flw_channel_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_channel_definition` (
  `ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `IMPLEMENTATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `ACT_IDX_CHANNEL_DEF_UNIQ` (`KEY_`,`VERSION_`,`TENANT_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_channel_definition`
--

LOCK TABLES `flw_channel_definition` WRITE;
/*!40000 ALTER TABLE `flw_channel_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_channel_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ev_databasechangelog`
--

DROP TABLE IF EXISTS `flw_ev_databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ev_databasechangelog` (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ev_databasechangelog`
--

LOCK TABLES `flw_ev_databasechangelog` WRITE;
/*!40000 ALTER TABLE `flw_ev_databasechangelog` DISABLE KEYS */;
INSERT INTO `flw_ev_databasechangelog` VALUES ('1','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-09-11 13:03:20',1,'EXECUTED','8:1b0c48c9cf7945be799d868a2626d687','createTable tableName=FLW_EVENT_DEPLOYMENT; createTable tableName=FLW_EVENT_RESOURCE; createTable tableName=FLW_EVENT_DEFINITION; createIndex indexName=ACT_IDX_EVENT_DEF_UNIQ, tableName=FLW_EVENT_DEFINITION; createTable tableName=FLW_CHANNEL_DEFIN...','',NULL,'4.9.1',NULL,NULL,'4437398892'),('2','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-09-11 13:03:20',2,'EXECUTED','8:0ea825feb8e470558f0b5754352b9cda','addColumn tableName=FLW_CHANNEL_DEFINITION; addColumn tableName=FLW_CHANNEL_DEFINITION','',NULL,'4.9.1',NULL,NULL,'4437398892'),('3','flowable','org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml','2023-09-11 13:03:20',3,'EXECUTED','8:3c2bb293350b5cbe6504331980c9dcee','customChange','',NULL,'4.9.1',NULL,NULL,'4437398892');
/*!40000 ALTER TABLE `flw_ev_databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ev_databasechangeloglock`
--

DROP TABLE IF EXISTS `flw_ev_databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ev_databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ev_databasechangeloglock`
--

LOCK TABLES `flw_ev_databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `flw_ev_databasechangeloglock` DISABLE KEYS */;
INSERT INTO `flw_ev_databasechangeloglock` VALUES (1,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `flw_ev_databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_definition`
--

DROP TABLE IF EXISTS `flw_event_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_definition` (
  `ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `VERSION_` int DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE KEY `ACT_IDX_EVENT_DEF_UNIQ` (`KEY_`,`VERSION_`,`TENANT_ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_definition`
--

LOCK TABLES `flw_event_definition` WRITE;
/*!40000 ALTER TABLE `flw_event_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_deployment`
--

DROP TABLE IF EXISTS `flw_event_deployment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_deployment` (
  `ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_deployment`
--

LOCK TABLES `flw_event_deployment` WRITE;
/*!40000 ALTER TABLE `flw_event_deployment` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_deployment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_event_resource`
--

DROP TABLE IF EXISTS `flw_event_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_event_resource` (
  `ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `RESOURCE_BYTES_` longblob,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_event_resource`
--

LOCK TABLES `flw_event_resource` WRITE;
/*!40000 ALTER TABLE `flw_event_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_event_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ru_batch`
--

DROP TABLE IF EXISTS `flw_ru_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ru_batch` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `BATCH_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ru_batch`
--

LOCK TABLES `flw_ru_batch` WRITE;
/*!40000 ALTER TABLE `flw_ru_batch` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_ru_batch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flw_ru_batch_part`
--

DROP TABLE IF EXISTS `flw_ru_batch_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flw_ru_batch_part` (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int DEFAULT NULL,
  `BATCH_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `RESULT_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  KEY `FLW_IDX_BATCH_PART` (`BATCH_ID_`) USING BTREE,
  CONSTRAINT `FLW_FK_BATCH_PART_PARENT` FOREIGN KEY (`BATCH_ID_`) REFERENCES `flw_ru_batch` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flw_ru_batch_part`
--

LOCK TABLES `flw_ru_batch_part` WRITE;
/*!40000 ALTER TABLE `flw_ru_batch_part` DISABLE KEYS */;
/*!40000 ALTER TABLE `flw_ru_batch_part` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `w_workflow_business`
--

DROP TABLE IF EXISTS `w_workflow_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `w_workflow_business` (
  `wf_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程表单id',
  `biz_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务id',
  `biz_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务名称',
  `biz_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务url',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `finish_time` datetime DEFAULT NULL COMMENT '结束时间',
  `workflow_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程状态',
  `instance_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程实例id',
  `proc_def_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程定义id',
  `proc_def_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程定义key',
  `proc_def_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '流程定义名称',
  `task_node_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '任务节点key',
  `create_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人id',
  `create_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_dept_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建部门',
  PRIMARY KEY (`wf_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='流程表单信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `w_workflow_business`
--

LOCK TABLES `w_workflow_business` WRITE;
/*!40000 ALTER TABLE `w_workflow_business` DISABLE KEYS */;
INSERT INTO `w_workflow_business` VALUES ('00841a947c6f53593c09951431adc9df','47b4fbeee25480f596bd3312c10057ed','韦一笑的1.0天请假申请','/leave-apply-view?id=47b4fbeee25480f596bd3312c10057ed','2023-09-11 21:49:03',NULL,'draft','b971297aac58a442a089dd33612510be','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('00fb348924b2bfd3796ed2efba3786a9','39f5053f89739403efff3483e78a896b','虚竹的7.0天请假申请','/leave-apply-view?id=39f5053f89739403efff3483e78a896b','2023-09-15 20:09:28',NULL,'cancel','5b05d462a25f6e49e5ff3e97c9bb41f4','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('0666bfe144389548b509e9b9e361024a','71ea7933680f17688e96181afe6050e8','虚竹的2.0天请假申请','/leave-apply-view?id=71ea7933680f17688e96181afe6050e8','2023-09-12 22:37:04',NULL,'draft','86a2f9b8410887c3f77a136b68a96c77','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('0b1bef4f56ae045bc15ba411ad400ca0','739daafea8087fe1abc3aa184b48e270','韦一笑的2.0天请假申请','/leave-apply-view?id=739daafea8087fe1abc3aa184b48e270','2023-09-11 21:53:27',NULL,'draft','c0b03c7d121b3ca701b5433bce783533','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('11f5b03f7669b3133ed1693b79676fb9','73dbe081e7e024067ab7ed061ee998eb','虚竹的18.0天请假申请','/leave-apply-view?id=73dbe081e7e024067ab7ed061ee998eb','2023-09-12 21:56:27',NULL,'complete','9de499a6f8d8307a34cb9640d6e63d4e','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('3545fd027b4b3327f56afd93d7cd8498','bfea7085c9468cd82845a478c32f9988','虚竹的7.0天请假申请','/leave-apply-view?id=bfea7085c9468cd82845a478c32f9988','2023-09-18 19:00:42',NULL,'in_process','3d11f7f23d377f4967cf1550ff19ea5a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('4b2ab9cd4635c78d15da0cd8d76ead58','5a3f4d09ac67862bcf1503a03c987938','韦一笑的25.0天请假申请','/leave-apply-view?id=5a3f4d09ac67862bcf1503a03c987938','2023-09-11 22:08:07',NULL,'in_process','9b15f4abc8ba5dcf905885c5f73489e9','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('5fae39090fff36861236a14770bb50f2','aab0fe8bed396487c4da846202d7234e','韦一笑的1.0天请假申请','/leave-apply-view?id=aab0fe8bed396487c4da846202d7234e','2023-09-11 21:52:30',NULL,'draft','9790b57ffcc326c11d1867972ff55876','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('6100bfb40a717df58dfcdf54c65f33aa','0179ef0813336305335c1035778c779c','虚竹的10.0天请假申请','/leave-apply-view?id=0179ef0813336305335c1035778c779c','2023-09-14 22:00:49',NULL,'in_process','2c43cad6e817cb50a9131351a79b6e4a','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('7a386e30f1fa076ae9b3850225695c8e','16dd6484fcffbd1a72bee10d8eff2f30','韦一笑的4.0天请假申请','/leave-apply-view?id=16dd6484fcffbd1a72bee10d8eff2f30','2023-09-11 21:50:43',NULL,'draft','cf9b32c4462a2ee9a295ce6343ce82d0','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('836f0f004dc0f33d6fb5c6c374be1d88','7ec6579fff4bba5c172de99c156d8534','虚竹的1.0天请假申请','/leave-apply-view?id=7ec6579fff4bba5c172de99c156d8534','2023-09-12 22:37:18',NULL,'draft','861e6ffa7abfee87cd95c08f4fcdc6c8','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('a06027c51def38f886fdc024aa5bfe99','982f29710dcb1198b11a002d14cc7827','韦一笑的17.0天请假申请','/leave-apply-view?id=982f29710dcb1198b11a002d14cc7827','2023-09-11 22:30:46',NULL,'draft','e73736cde5d5a8e1a04b900134cfd846','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('a753749f05506580efd3e19d4478011d','544c69d7f52db547ee64fb5847ef89c6','韦一笑的19.0天请假申请','/leave-apply-view?id=544c69d7f52db547ee64fb5847ef89c6','2023-09-11 21:57:05',NULL,'draft','678f8c7260b7992967d4175d5694b4b5','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('b7d16784d3e94879b6a08bd62fb3b1c1','14e9ec5a9856508e30e1c4e41474faba','韦一笑的3.0天请假申请','/leave-apply-view?id=14e9ec5a9856508e30e1c4e41474faba','2023-09-11 21:50:13',NULL,'draft','62580c574add9b784f1b6e2114561a0d','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('c24de2b889faaefef4ee41c479d6e923','d7993a958619810609a05d9e343b2423','虚竹的3.0天请假申请','/leave-apply-view?id=d7993a958619810609a05d9e343b2423','2023-09-12 22:33:59',NULL,'draft','1b8c6cf5877ddf134f4b149408024b37','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('d0b364312a0ffe24e491fe732ff64ce5','510fe1a2729339cc4ff5efcedb4ba8ac','weiyixiao的1.0请假申请','/leave-apply-view?id=510fe1a2729339cc4ff5efcedb4ba8ac','2023-09-11 21:25:27',NULL,'draft','e2657e6406c2a9c2e7ef4f8b61d06a12','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('e45cc79d99efaf2ec0972e3cef8f88db','d0e443e6d5cad014bea91fd062001a77','虚竹的3.0天请假申请','/leave-apply-view?id=d0e443e6d5cad014bea91fd062001a77','2023-09-14 21:59:16',NULL,'draft','48cb4c54ce008b4ac292c3ba18044a56','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('f467d4a8b071635a337383c112ef9ed3','fd82e12fcf8d958b768a2b5d54662dbc','虚竹的2.0天请假申请','/leave-apply-view?id=fd82e12fcf8d958b768a2b5d54662dbc','2023-09-12 22:34:30',NULL,'C','f9e728a4640b74e0ec0f501346b794ff','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门'),('f591c9ac5b57fed58865d1a061a6d822','5c2e773cbc005cad011ebfdb8ca08d96','韦一笑的1.0天请假申请','/leave-apply-view?id=5c2e773cbc005cad011ebfdb8ca08d96','2023-09-11 21:51:59',NULL,'complete','0c0ae280fcaf4d43a4659c6d2a2e4da0','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'a50f1994deb7e358e04ca0b1eb938ee1','韦一笑','Sanil科技/成都分公司/市场部门'),('fccdd30482521d6b137f23250d35aa05','db7a5b02aa2c2ae6bff68aa1d1456d27','虚竹的21.0天请假申请','/leave-apply-view?id=db7a5b02aa2c2ae6bff68aa1d1456d27','2023-09-15 20:51:49',NULL,'cancel','2fadd67483a667cf3780274d6ad2c490','LeaveApply:1:eafc57fa28f231d3fa6b187cd1f47a5c','LeaveApply','请假申请',NULL,'78f97db459233bc461df091b862fb7e3','虚竹','Sanil科技/重庆分公司/市场部门');
/*!40000 ALTER TABLE `w_workflow_business` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-10 13:08:39
