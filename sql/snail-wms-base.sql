-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: snail-wms-base
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `base_attr`
--

DROP TABLE IF EXISTS `base_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_attr` (
  `attr_id` varchar(64) NOT NULL COMMENT '属性id',
  `attr_name` varchar(255) NOT NULL COMMENT '属性名称',
  `category_id` varchar(64) NOT NULL COMMENT '分类id',
  `category_code` varchar(100) NOT NULL COMMENT '分类编码',
  `category_name` varchar(50) NOT NULL COMMENT '分类名称',
  `category_full_name` varchar(500) NOT NULL COMMENT '分类全路径',
  `status` varchar(2) NOT NULL COMMENT '状态：1.启用，0.禁用',
  `del_flag` varchar(2) NOT NULL COMMENT '逻辑删除：1.删除，0.未删除',
  `tenant_id` varchar(100) DEFAULT NULL COMMENT '租户标识',
  `create_user_id` varchar(64) NOT NULL COMMENT '创建人id',
  `create_user_name` varchar(20) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `dept_id` varchar(64) NOT NULL COMMENT '组织id',
  `dept_full_id` varchar(500) NOT NULL COMMENT '组织全路径id',
  `dept_full_name` varchar(500) NOT NULL COMMENT '组织全路径',
  `update_user_id` varchar(64) DEFAULT NULL COMMENT '更新人id',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`attr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_attr`
--

LOCK TABLES `base_attr` WRITE;
/*!40000 ALTER TABLE `base_attr` DISABLE KEYS */;
INSERT INTO `base_attr` VALUES ('2f10f2170358a57a765ca04a46f11d02','年份','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-08 14:36:08','100','100','Sanil科技','1000','Snail','2024-06-10 11:45:21'),('709d6c45858d9ece9a5f414cbbafe1bb','CPU型号','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-08 14:36:08','100','100','Sanil科技',NULL,NULL,NULL),('ebc44e0078b3231499c56fe081b4ba4e','内存','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-05 22:22:59','100','100','Sanil科技',NULL,NULL,NULL),('f9cb05f76e300f68f16a42f073f7e96e','颜色','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-04 22:13:21','100','100','Sanil科技','1000','Snail','2024-06-05 22:19:19');
/*!40000 ALTER TABLE `base_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_attr_value`
--

DROP TABLE IF EXISTS `base_attr_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_attr_value` (
  `attr_value_id` varchar(64) NOT NULL COMMENT '主键',
  `attr_id` varchar(64) NOT NULL COMMENT '属性id',
  `attr_value` varchar(255) NOT NULL COMMENT '属性值',
  `status` varchar(2) NOT NULL COMMENT '状态：1.启用，0.禁用',
  PRIMARY KEY (`attr_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='属性值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_attr_value`
--

LOCK TABLES `base_attr_value` WRITE;
/*!40000 ALTER TABLE `base_attr_value` DISABLE KEYS */;
INSERT INTO `base_attr_value` VALUES ('1f2dad848dfc13452de8f8773f3f3663','ebc44e0078b3231499c56fe081b4ba4e','8+512G','1'),('336982caf2ecc879e999e03f525955f3','709d6c45858d9ece9a5f414cbbafe1bb','骁龙600系列','1'),('384b88a7cae480ed9b480f2e7d80a677','f9cb05f76e300f68f16a42f073f7e96e','粉色','1'),('3b5dcd179b821751361ac62280fddfd7','2f10f2170358a57a765ca04a46f11d02','2024年','1'),('3ef719d5e751d3272acf3326b826d4a4','ebc44e0078b3231499c56fe081b4ba4e','16+1T','1'),('4ec7d1ecc678dfd9fb0a9efb8a09941e','2f10f2170358a57a765ca04a46f11d02','2023年','1'),('50bb680b96a220c9ac15a7c1df8c56d6','2f10f2170358a57a765ca04a46f11d02','2025年','1'),('517183b54d026fe50d6c1e7e2790b2e2','f9cb05f76e300f68f16a42f073f7e96e','白色','1'),('6a77ad4c30c2f52556bfdce8c1aef91d','709d6c45858d9ece9a5f414cbbafe1bb','第一代骁龙4','1'),('74e103059167ec3c5ca644befe12f3fd','f9cb05f76e300f68f16a42f073f7e96e','紫色','1'),('7f0124ac7d1ef8817083f12ca2d93eea','2f10f2170358a57a765ca04a46f11d02','2026年','1'),('7f40299fb1f91125809e55b5d75aba9c','709d6c45858d9ece9a5f414cbbafe1bb','A15','1'),('9fd2cf0e668dd4063cbae1b5cdb8783a','ebc44e0078b3231499c56fe081b4ba4e','12+512G','1'),('b33ebd063157f1edba2bbe7404c0b6c6','f9cb05f76e300f68f16a42f073f7e96e','墨绿色','1'),('cefd9bc81e18f49387ffece6a419919e','ebc44e0078b3231499c56fe081b4ba4e','6+128G','1'),('cf8b0a817dea500de99622a80672b938','709d6c45858d9ece9a5f414cbbafe1bb','A17','1'),('dbafa4ef125e5b6060d860251c45184e','f9cb05f76e300f68f16a42f073f7e96e','蓝色','1'),('f5e4a0979119ab64c8bf3492c07ac8ee','f9cb05f76e300f68f16a42f073f7e96e','红色','1');
/*!40000 ALTER TABLE `base_attr_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_category`
--

DROP TABLE IF EXISTS `base_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_category` (
  `category_id` varchar(64) NOT NULL COMMENT '分类id',
  `category_code` varchar(100) NOT NULL COMMENT '分类编码',
  `category_name` varchar(50) NOT NULL COMMENT '分类名称',
  `parent_id` varchar(64) NOT NULL COMMENT '父分类id',
  `category_level` int NOT NULL COMMENT '分类层级',
  `category_status` varchar(2) NOT NULL COMMENT '状态：1.启用，0.禁用',
  `category_full_name` varchar(500) NOT NULL COMMENT '分类全路径',
  `del_flag` varchar(2) NOT NULL COMMENT '逻辑删除：1.删除，0.未删除',
  `tenant_id` varchar(100) DEFAULT NULL COMMENT '租户标识',
  `create_user_id` varchar(64) NOT NULL COMMENT '创建人id',
  `create_user_name` varchar(20) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `dept_id` varchar(64) NOT NULL COMMENT '组织id',
  `dept_full_id` varchar(500) NOT NULL COMMENT '组织全路径id',
  `dept_full_name` varchar(500) NOT NULL COMMENT '组织全路径',
  `update_user_id` varchar(64) DEFAULT NULL COMMENT '更新人id',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='产品分类';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_category`
--

LOCK TABLES `base_category` WRITE;
/*!40000 ALTER TABLE `base_category` DISABLE KEYS */;
INSERT INTO `base_category` VALUES ('-1','0','根目录','0',0,'1','根目录','0','Snail','1000','Snail','2024-05-01 21:16:52','100','100','Sanil科技',NULL,NULL,NULL),('2df7b83a91f8e0ec403f4fbc31204f0d','11','家用电器','-1',1,'1','家用电器','0','Snail','1000','Snail','2024-05-01 21:22:13','100','100','Sanil科技',NULL,NULL,NULL),('34059af9e2406d2aa9c5b641aebce583','12','汽车用品','-1',1,'1','汽车用品','0','Snail','1000','Snail','2024-05-02 13:11:31','100','100','Sanil科技',NULL,NULL,NULL),('416499d7d89ed95c228087893c01e917','12102','汽车服务','34059af9e2406d2aa9c5b641aebce583',2,'1','汽车用品/汽车服务','0','Snail','1000','Snail','2024-05-02 19:54:05','100','100','Sanil科技',NULL,NULL,NULL),('5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','5e87a1016ca99b6675fe4fc92bb6db0d',2,'1','通讯设备/手机','0','Snail','1000','Snail','2024-05-01 21:23:01','100','100','Sanil科技',NULL,NULL,NULL),('5e87a1016ca99b6675fe4fc92bb6db0d','10','通讯设备','-1',1,'1','通讯设备','0','Snail','1000','Snail','2024-05-01 21:22:13','100','100','Sanil科技',NULL,NULL,NULL),('6cab21a1b305f33a31d2843e16f79637','11101','电视','2df7b83a91f8e0ec403f4fbc31204f0d',2,'1','家用电器/电视','0','Snail','1000','Snail','2024-05-01 21:23:01','100','100','Sanil科技',NULL,NULL,NULL);
/*!40000 ALTER TABLE `base_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_sku`
--

DROP TABLE IF EXISTS `base_sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_sku` (
  `sku_id` varchar(64) NOT NULL COMMENT '主键',
  `sku_code` varchar(255) NOT NULL COMMENT 'sku编码',
  `sku_name` varchar(255) NOT NULL COMMENT 'sku名称',
  `spu_id` varchar(255) NOT NULL COMMENT '主键',
  `spu_code` varchar(255) NOT NULL COMMENT 'spu编码',
  `spu_name` varchar(255) NOT NULL COMMENT 'spu名称',
  `category_id` varchar(64) NOT NULL COMMENT '分类id',
  `category_code` varchar(100) NOT NULL COMMENT '分类编码',
  `category_name` varchar(50) NOT NULL COMMENT '分类名称',
  `category_full_name` varchar(500) NOT NULL COMMENT '分类全路径',
  `sku_status` varchar(2) NOT NULL COMMENT '状态：1.启用，0.禁用',
  `del_flag` varchar(2) NOT NULL COMMENT '逻辑删除：1.删除，0.未删除',
  `tenant_id` varchar(100) DEFAULT NULL COMMENT '租户标识',
  `create_user_id` varchar(64) NOT NULL COMMENT '创建人id',
  `create_user_name` varchar(20) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `dept_id` varchar(64) NOT NULL COMMENT '组织id',
  `dept_full_id` varchar(500) NOT NULL COMMENT '组织全路径id',
  `dept_full_name` varchar(500) NOT NULL COMMENT '组织全路径',
  `update_user_id` varchar(64) DEFAULT NULL COMMENT '更新人id',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`sku_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='sku信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_sku`
--

LOCK TABLES `base_sku` WRITE;
/*!40000 ALTER TABLE `base_sku` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_sku_attr_relation`
--

DROP TABLE IF EXISTS `base_sku_attr_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_sku_attr_relation` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `sku_id` varchar(64) NOT NULL COMMENT '主键',
  `attr_id` varchar(64) NOT NULL COMMENT '属性id',
  `attr_name` varchar(255) NOT NULL COMMENT '属性名称',
  `attr_value_id` varchar(64) NOT NULL COMMENT '属性值id',
  `attr_value` varchar(255) NOT NULL COMMENT '属性值',
  PRIMARY KEY (`id`,`sku_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='sku属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_sku_attr_relation`
--

LOCK TABLES `base_sku_attr_relation` WRITE;
/*!40000 ALTER TABLE `base_sku_attr_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_sku_attr_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_spu`
--

DROP TABLE IF EXISTS `base_spu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_spu` (
  `spu_id` varchar(255) NOT NULL COMMENT '主键',
  `spu_code` varchar(255) NOT NULL COMMENT 'spu编码',
  `spu_name` varchar(255) NOT NULL COMMENT 'spu名称',
  `category_id` varchar(64) NOT NULL COMMENT '分类id',
  `category_code` varchar(100) NOT NULL COMMENT '分类编码',
  `category_name` varchar(50) NOT NULL COMMENT '分类名称',
  `category_full_name` varchar(500) NOT NULL COMMENT '分类全路径',
  `spu_status` varchar(2) NOT NULL COMMENT '状态：1.启用，0.禁用',
  `del_flag` varchar(2) NOT NULL COMMENT '逻辑删除：1.删除，0.未删除',
  `tenant_id` varchar(100) DEFAULT NULL COMMENT '租户标识',
  `create_user_id` varchar(64) NOT NULL COMMENT '创建人id',
  `create_user_name` varchar(20) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `dept_id` varchar(64) NOT NULL COMMENT '组织id',
  `dept_full_id` varchar(500) NOT NULL COMMENT '组织全路径id',
  `dept_full_name` varchar(500) NOT NULL COMMENT '组织全路径',
  `update_user_id` varchar(64) DEFAULT NULL COMMENT '更新人id',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`spu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='spu信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_spu`
--

LOCK TABLES `base_spu` WRITE;
/*!40000 ALTER TABLE `base_spu` DISABLE KEYS */;
INSERT INTO `base_spu` VALUES ('460b812371cd83cc1cb51f448797763f','SPU101000001','iPhone 17','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-10 17:41:14','100','100','Sanil科技','1000','Snail','2024-06-10 18:14:54'),('7d85484d0fda102edb5d3e57ac211a38','SPU101000002','小米13','5ac5e6e6e466e6ada71f8d83671351bd','10100','手机','通讯设备/手机','1','0','Snail','1000','Snail','2024-06-10 20:28:09','100','100','Sanil科技',NULL,NULL,NULL);
/*!40000 ALTER TABLE `base_spu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_spu_attr_relation`
--

DROP TABLE IF EXISTS `base_spu_attr_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `base_spu_attr_relation` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `spu_id` varchar(255) NOT NULL COMMENT '主键',
  `attr_id` varchar(64) NOT NULL COMMENT '属性id',
  `attr_name` varchar(255) NOT NULL COMMENT '属性名称',
  `attr_value_id` varchar(64) NOT NULL COMMENT '属性值id',
  `attr_value` varchar(255) NOT NULL COMMENT '属性值',
  PRIMARY KEY (`id`,`spu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='spu属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_spu_attr_relation`
--

LOCK TABLES `base_spu_attr_relation` WRITE;
/*!40000 ALTER TABLE `base_spu_attr_relation` DISABLE KEYS */;
INSERT INTO `base_spu_attr_relation` VALUES ('11c4bc33634f00f97c74a57a65d9a882','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','74e103059167ec3c5ca644befe12f3fd','紫色'),('2ac5a2974bbf53fea516ee98783ec0d2','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','f5e4a0979119ab64c8bf3492c07ac8ee','红色'),('2b8e76a58b9f24128f9f3eaf667674e9','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','384b88a7cae480ed9b480f2e7d80a677','粉色'),('2cca377a13322d416c447e2db8d146e3','460b812371cd83cc1cb51f448797763f','f9cb05f76e300f68f16a42f073f7e96e','颜色','f5e4a0979119ab64c8bf3492c07ac8ee','红色'),('48a780b31bca9e3c4809041e8158a1d8','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','b33ebd063157f1edba2bbe7404c0b6c6','墨绿色'),('4bdb94b1d9c9a5056bf012f428ada14f','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','517183b54d026fe50d6c1e7e2790b2e2','白色'),('51cca66eb86b73e7eb37b09401c81e52','460b812371cd83cc1cb51f448797763f','2f10f2170358a57a765ca04a46f11d02','年份','3b5dcd179b821751361ac62280fddfd7','2024年'),('51d24a3cfc381b1dc9654fe0a0cfd48f','7d85484d0fda102edb5d3e57ac211a38','ebc44e0078b3231499c56fe081b4ba4e','内存','9fd2cf0e668dd4063cbae1b5cdb8783a','12+512G'),('563ff627a7c4f25dda30b7a0c6f8623f','7d85484d0fda102edb5d3e57ac211a38','f9cb05f76e300f68f16a42f073f7e96e','颜色','dbafa4ef125e5b6060d860251c45184e','蓝色'),('69988d6273ddbf1b2d5e18e487ab3898','7d85484d0fda102edb5d3e57ac211a38','2f10f2170358a57a765ca04a46f11d02','年份','3b5dcd179b821751361ac62280fddfd7','2024年'),('6a009f71b11152bf85f4d3b8fbcf60ef','7d85484d0fda102edb5d3e57ac211a38','2f10f2170358a57a765ca04a46f11d02','年份','50bb680b96a220c9ac15a7c1df8c56d6','2025年'),('784dae33fda2f703a9630fcc60817983','7d85484d0fda102edb5d3e57ac211a38','709d6c45858d9ece9a5f414cbbafe1bb','CPU型号','336982caf2ecc879e999e03f525955f3','骁龙600系列'),('9fd783dd1f1316ed596593d33e36abf0','7d85484d0fda102edb5d3e57ac211a38','709d6c45858d9ece9a5f414cbbafe1bb','CPU型号','6a77ad4c30c2f52556bfdce8c1aef91d','第一代骁龙4'),('a68b902b7babde5a47b5f3bbecf95623','7d85484d0fda102edb5d3e57ac211a38','ebc44e0078b3231499c56fe081b4ba4e','内存','1f2dad848dfc13452de8f8773f3f3663','8+512G'),('c7b2b51a622a958fbd9de345950c4586','460b812371cd83cc1cb51f448797763f','709d6c45858d9ece9a5f414cbbafe1bb','CPU型号','cf8b0a817dea500de99622a80672b938','A17'),('d294731520286a1f78a5c53ffafd39da','460b812371cd83cc1cb51f448797763f','ebc44e0078b3231499c56fe081b4ba4e','内存','9fd2cf0e668dd4063cbae1b5cdb8783a','12+512G'),('f428aeba2bafacdb4e1afd0842e296e5','7d85484d0fda102edb5d3e57ac211a38','2f10f2170358a57a765ca04a46f11d02','年份','7f0124ac7d1ef8817083f12ca2d93eea','2026年');
/*!40000 ALTER TABLE `base_spu_attr_relation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-10 13:08:33
