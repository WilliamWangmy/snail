/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.56.11
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 192.168.56.11:3306
 Source Schema         : ry-oa

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 05/12/2023 20:59:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for snail_leave_apply
-- ----------------------------
DROP TABLE IF EXISTS `snail_leave_apply`;
CREATE TABLE `snail_leave_apply`  (
                                      `apply_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键id',
                                      `apply_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据名称',
                                      `apply_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编码',
                                      `leave_day` decimal(3, 1) NULL DEFAULT NULL COMMENT '请假天数',
                                      `leave_begin` datetime NULL DEFAULT NULL COMMENT '开始时间',
                                      `leave_end` datetime NULL DEFAULT NULL COMMENT '结束时间',
                                      `leave_Reason` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请假原因',
                                      `workflow_status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程状态',
                                      `tenant_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户',
                                      `del_flag` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否逻辑删除',
                                      `create_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
                                      `create_user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
                                      `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                      `update_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人id',
                                      `update_user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人名称',
                                      `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                      `dept_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人组织id',
                                      `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人组织名称',
                                      `dept_full_id` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人组织全路径id',
                                      `dept_full_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人组织全路径名称',
                                      PRIMARY KEY (`apply_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '请假申请' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of snail_leave_apply
-- ----------------------------
INSERT INTO `snail_leave_apply` VALUES ('0179ef0813336305335c1035778c779c', '虚竹的10.0天请假申请', 'QJSQ20230914106', 10.0, '2023-09-19 22:00:35', '2023-09-29 22:00:38', '10', 'in_process', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-14 22:00:49', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-14 22:18:45', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('14e9ec5a9856508e30e1c4e41474faba', '韦一笑的3.0天请假申请', 'QJSQ20230911083', 3.0, '2023-09-11 21:50:01', '2023-09-14 21:50:05', '3', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:50:12', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('16dd6484fcffbd1a72bee10d8eff2f30', '韦一笑的4.0天请假申请', 'QJSQ20230911084', 4.0, '2023-09-11 21:50:29', '2023-09-15 21:50:31', '2', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:50:42', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('39f5053f89739403efff3483e78a896b', '虚竹的7.0天请假申请', 'QJSQ20230915115', 7.0, '2023-09-15 20:09:17', '2023-09-22 20:09:19', '7', 'cancel', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-15 20:09:27', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-15 20:50:16', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('47b4fbeee25480f596bd3312c10057ed', '韦一笑的1.0天请假申请', 'QJSQ20230911082', 1.0, '2023-09-11 21:48:51', '2023-09-12 21:48:54', '1', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:49:01', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('510fe1a2729339cc4ff5efcedb4ba8ac', 'weiyixiao的1.0请假申请', 'QJSQ20230911081', 1.0, '2023-09-11 21:04:50', '2023-09-12 21:04:52', '1', 'draft', '', '1', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:04:58', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('544c69d7f52db547ee64fb5847ef89c6', '韦一笑的19.0天请假申请', 'QJSQ20230911088', 19.0, '2023-09-11 21:56:42', '2023-09-30 21:56:45', '5', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:57:02', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('5a3f4d09ac67862bcf1503a03c987938', '韦一笑的25.0天请假申请', 'QJSQ20230911089', 25.0, '2023-09-11 22:07:53', '2023-10-06 22:07:56', '25', 'in_process', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 22:08:05', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 22:08:26', '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('5c2e773cbc005cad011ebfdb8ca08d96', '韦一笑的1.0天请假申请', 'QJSQ20230911085', 1.0, '2023-09-11 21:51:35', '2023-09-12 21:51:38', '2', 'complete', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:51:49', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 22:15:18', '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('71ea7933680f17688e96181afe6050e8', '虚竹的2.0天请假申请', 'QJSQ20230912097', 2.0, '2023-09-12 22:28:02', '2023-09-14 22:28:06', '3', 'draft', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-12 22:28:15', NULL, NULL, NULL, 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('739daafea8087fe1abc3aa184b48e270', '韦一笑的2.0天请假申请', 'QJSQ20230911087', 2.0, '2023-09-11 21:53:06', '2023-09-13 21:53:13', '2', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:53:21', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('73dbe081e7e024067ab7ed061ee998eb', '虚竹的18.0天请假申请', 'QJSQ20230912095', 18.0, '2023-09-12 21:03:13', '2023-09-30 21:03:15', '1', 'complete', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-12 21:03:26', '00b6fea67bb10175f39158cb9e00db1e', '王语嫣', '2023-09-12 22:42:44', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('7ec6579fff4bba5c172de99c156d8534', '虚竹的1.0天请假申请', 'QJSQ20230912096', 1.0, '2023-09-12 22:25:25', '2023-09-13 22:25:27', '2', 'draft', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-12 22:25:35', NULL, NULL, NULL, 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('982f29710dcb1198b11a002d14cc7827', '韦一笑的17.0天请假申请', 'QJSQ20230911090', 17.0, '2023-09-11 22:30:27', '2023-09-28 22:30:32', '17', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 22:30:44', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('aab0fe8bed396487c4da846202d7234e', '韦一笑的1.0天请假申请', 'QJSQ20230911086', 1.0, '2023-09-11 21:52:04', '2023-09-12 21:52:07', '1', 'draft', '', '0', 'a50f1994deb7e358e04ca0b1eb938ee1', '韦一笑', '2023-09-11 21:52:13', NULL, NULL, NULL, '18b9ea24518d558f157a41632c8d39d7', '市场部门', '100,a4b25e44a83953563a1f5ce7ab6fdaef,18b9ea24518d558f157a41632c8d39d7', 'Sanil科技/成都分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('bfea7085c9468cd82845a478c32f9988', '虚竹的7.0天请假申请', 'QJSQ20230918125', 7.0, '2023-09-18 19:00:24', '2023-09-25 19:00:28', '7', 'in_process', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-18 19:00:40', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-18 20:13:14', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('d0e443e6d5cad014bea91fd062001a77', '虚竹的3.0天请假申请', 'QJSQ20230914105', 3.0, '2023-09-14 21:59:05', '2023-09-17 21:59:07', '3', 'draft', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-14 21:59:15', NULL, NULL, NULL, 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('d7993a958619810609a05d9e343b2423', '虚竹的3.0天请假申请', 'QJSQ20230912098', 3.0, '2023-09-12 22:32:27', '2023-09-15 22:32:31', '4', 'draft', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-12 22:32:39', NULL, NULL, NULL, 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('db7a5b02aa2c2ae6bff68aa1d1456d27', '虚竹的21.0天请假申请', 'QJSQ20230915116', 21.0, '2023-09-15 20:51:37', '2023-10-06 20:51:40', '21', 'cancel', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-15 20:51:49', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-18 18:58:12', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');
INSERT INTO `snail_leave_apply` VALUES ('fd82e12fcf8d958b768a2b5d54662dbc', '虚竹的2.0天请假申请', 'QJSQ20230912099', 2.0, '2023-09-12 22:34:16', '2023-09-14 22:34:19', '2', 'C', '', '0', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-12 22:34:29', '78f97db459233bc461df091b862fb7e3', '虚竹', '2023-09-14 21:39:16', 'e8d96ba9907786a146ef8b090e9cbefc', '市场部门', '100,84c1c819e8b1e0d11484333e0fca6f9b,e8d96ba9907786a146ef8b090e9cbefc', 'Sanil科技/重庆分公司/市场部门');

-- ----------------------------
-- Table structure for snail_mq_consumer
-- ----------------------------
DROP TABLE IF EXISTS `snail_mq_consumer`;
CREATE TABLE `snail_mq_consumer`  (
                                      `consumer_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
                                      `pri_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息唯一标识',
                                      `mq_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息类型：RabbitMq，Kafka',
                                      `topic` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息主题',
                                      `exchange` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交换器',
                                      `queue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '队列',
                                      `content` longblob NULL COMMENT '内容',
                                      `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态：1.成功，0.失败',
                                      `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误信息',
                                      `create_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
                                      `create_user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                      `create_time` datetime NOT NULL COMMENT '创建时间',
                                      `retry_count` int NULL DEFAULT NULL COMMENT '重试次数',
                                      PRIMARY KEY (`consumer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'mq消息消费记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of snail_mq_consumer
-- ----------------------------

-- ----------------------------
-- Table structure for snail_mq_producer
-- ----------------------------
DROP TABLE IF EXISTS `snail_mq_producer`;
CREATE TABLE `snail_mq_producer`  (
                                      `producer_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
                                      `pri_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息唯一标识',
                                      `mq_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息类型：RabbitMq，Kafka',
                                      `topic` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息主题',
                                      `exchange` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交换器',
                                      `queue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '队列',
                                      `content` longblob NULL COMMENT '内容',
                                      `create_user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人id',
                                      `create_user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                      `create_time` datetime NOT NULL COMMENT '创建时间',
                                      PRIMARY KEY (`producer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'mq消息生产记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of snail_mq_producer
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
