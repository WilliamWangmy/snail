package com.snail.gateway;

import com.snail.common.core.utils.ServerUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 网关启动程序
 *
 * @author snail
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SnailGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(SnailGatewayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail网关服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _                     _\n" +
                "                   (_)| |                   | |\n" +
                " ___  _ ___   __ _  _ | |       __ _   __ _ | |_   ___ __      __  __ _  _   _\n" +
                "/ __|| '_  \\ / _` || || |      / _` | / _` || __| / _ \\\\ \\ /\\ / / / _` || | | |\n" +
                "\\__ \\| | | || (_| || || |     | (_| || (_| || |_ |  __/ \\ V  V / | (_| || |_| |\n" +
                "|___/|_| |_| \\__,_||_||_|      \\__, | \\__,_| \\__| \\___|  \\_/\\_/   \\__,_| \\__, |\n" +
                "                                __/ |                                     __/ |\n" +
                "                               |___/                                     |___/\n" +
                ServerUtil.printInfo());
    }
}
