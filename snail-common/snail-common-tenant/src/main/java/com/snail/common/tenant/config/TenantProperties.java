package com.snail.common.tenant.config;

import com.snail.common.tenant.constants.TenantConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 多租户属性配置
 * @Author: Snail
 * @CreateDate: 2023/8/3 15:25
 * @Version: V1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "snail.tenant")
public class TenantProperties {

    /**
     * 是否开启多租户
     */
    private Boolean enabled = false;

    /**
     * 多租户类型，table表数据隔离，db数据库隔离
     */
    private String type = TenantConstants.TENANT_TYPE_TABLE;


    /**
     * 不需要多租户处理的请求
     */
    private String excludeUrls = "";

    /**
     * 多租户忽略的表结构
     */
    private String ignoreTable = "";

    /**
     * 默认租住标识
     */
    private String defaultTenantVal = "Snail";

    /**
     * 查询所有数据的租户值
     */
    private String allTenantVal = "Snail";

    public Boolean isOpen() {
        return this.enabled;
    }

}