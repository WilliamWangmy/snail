package com.snail.common.tenant.constants;

/**
 * @Description: 多租户常量
 * @Author: Snail
 * @CreateDate: 2023/8/3 16:21
 * @Version: V1.0
 */
public class TenantConstants {
    /**
     * 多租户类型，table表数据隔离，db数据库隔离
     */
    public static final String TENANT_TYPE_TABLE = "table";
    public static final String TENANT_TYPE_DB = "db";
    /**
     * 忽略多租户的表
     */
    public  static final String[] SYSTEM_INCLUDE_TABLE = {"sys_dept","sys_file","sys_login_info","sys_menu","sys_notice","sys_opera_log","sys_post","sys_role","sys_sequence_rule","sys_user"};
}
