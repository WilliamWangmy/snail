# snail-common-tenant使用方法

1、引用模块

```xml
<!--多租户功能-->
<dependency>
    <groupId>com.snail</groupId>
    <artifactId>snail-common-tenant</artifactId>
</dependency>
```

也可以直接引用微服务公共模块

```xml
<dependency>
    <groupId>com.snail</groupId>
    <artifactId>snail-common-cloud</artifactId>
</dependency>
```

2、配置文件开启多租户

详细的配置说明请查看：TenantProperties

```yml
snail:
  tenant:
    #开启多租户
    enabled: true
    #隔离类型：db数据库隔离，table表数据隔离（默认）
    type: db
```

> **注意：**
>
> **可以开启全系统的多租户模式，但是必须所有表都得加上多租户字段（或者排除不需要多租户的表）。也可以只开启对应业务服务的多租户功能，但是对应的数据库表必须都得有多租户字段。**
>
> **租户信息是前端请求头传递到后端**