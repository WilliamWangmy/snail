package com.snail.common.cloud.annotation;


import com.snail.common.cloud.config.SnailCloudConfig;
import com.snail.common.security.annotation.EnableCustomConfig;
import com.snail.common.security.annotation.EnableSnailFeignClients;
import com.snail.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 微服务模块启动类
 * @author Snail
 */

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@EnableCustomConfig
@EnableCustomSwagger2
@EnableSnailFeignClients
@SpringBootApplication
@Import(SnailCloudConfig.class)
public @interface SnailCloudApplication {
}
