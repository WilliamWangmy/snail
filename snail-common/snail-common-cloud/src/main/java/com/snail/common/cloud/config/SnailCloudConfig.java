package com.snail.common.cloud.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.datascope.handler.DataScopePermissionHandler;
import com.snail.common.datascope.interceptor.DataScopePermissionInterceptor;
import com.snail.common.security.utils.SecurityUtils;
import com.snail.common.tenant.config.TenantProperties;
import com.snail.common.tenant.config.TenantWebConfig;
import com.snail.common.tenant.constants.TenantConstants;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Arrays;

/**
 * @Description: 配置类
 * @Author: Snail
 * @CreateDate: 2023/8/10 16:05
 * @Version: V1.0
 */
@Configuration
@EnableConfigurationProperties(TenantProperties.class)
@Import({TenantWebConfig.class})
public class SnailCloudConfig {

    @Autowired
    private TenantProperties tenantProperties;

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //多租户插件
        addTenantInterceptor(interceptor);
        //数据权限查询
        addDataScopeInterceptor(interceptor);
        //分页插件
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

    /**
     * 添加数据权限插件
     *
     * @param interceptor 拦截器
     */
    private void addDataScopeInterceptor(MybatisPlusInterceptor interceptor) {
        interceptor.addInnerInterceptor(new DataScopePermissionInterceptor(new DataScopePermissionHandler()));
    }

    /**
     * 添加多租户插件
     *
     * @param interceptor 拦截器
     */
    private void addTenantInterceptor(MybatisPlusInterceptor interceptor) {
        //多租户未开启
        if (!tenantProperties.isOpen()) {
            return;
        }
        //非表数据隔离
        if (!TenantConstants.TENANT_TYPE_TABLE.equals(tenantProperties.getType())) {
            return;
        }
        //添加多租户配置内容
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new TenantLineHandler() {
            @Override
            public Expression getTenantId() {
                String tenantId = SecurityUtils.getTenantId();
                return new StringValue(StringUtils.defaultIfEmpty(tenantId,tenantProperties.getDefaultTenantVal()));
            }

            @Override
            public boolean ignoreTable(String tableName) {
                String tenantId = SecurityUtils.getTenantId();
                return  tenantId.equals(tenantProperties.getAllTenantVal()) || tenantProperties.getIgnoreTable().contains(tableName) || Arrays.stream(TenantConstants.SYSTEM_INCLUDE_TABLE).noneMatch(item -> item.equalsIgnoreCase(tableName));
            }
        }));
    }
}
