package com.snail.common.core.web.domain;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 查询基类
 * @Author: Snail
 * @CreateDate: 2022/7/7 11:18
 * @Version: V1.0
 */
@Data
@ApiModel(value = "查询基类")
public class BaseQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *第几页
     */
    @ApiModelProperty(value = "第几页")
    private Integer pageNum = 1;
    /**
     * 每页大小
     */
    @ApiModelProperty(value = "每页大小")
    private Integer pageSize = 10;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private String beginTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private String endTime;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private String orderByColumn;
    /**
     * asc.升序，desc.降序
     */
    @ApiModelProperty(value = "排序类型：asc.升序，desc.降序")
    private String orderByType;
    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    private String tenantId;

}
