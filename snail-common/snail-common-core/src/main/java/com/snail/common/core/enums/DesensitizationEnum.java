package com.snail.common.core.enums;

import cn.hutool.core.util.DesensitizedUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @Description: 数据脱敏枚举类
 * @Author: Snail
 * @CreateDate: 2024/8/9
 * @Version: V1.0
 */
public enum DesensitizationEnum {

    /**
     * 自定义
     */
    CUSTOM {
        @Override
        public String desensitize(String str, Integer start, Integer end) {
            return StrUtil.hide(str, start, end);
        }
    },
    /**
     * 身份证
     */
    ID_CRAD {
        @Override
        public String desensitize(String str, Integer start, Integer end) {
            return String.valueOf(DesensitizedUtil.idCardNum(str, start, end));
        }
    },
    /**
     * 邮箱
     */
    EMAIL {
        @Override
        public String desensitize(String str, Integer start, Integer end) {
            return DesensitizedUtil.email(str);
        }
    },
    /**
     * 电话
     */
    PHONE {
        @Override
        public String desensitize(String str, Integer start, Integer end) {
            return DesensitizedUtil.mobilePhone(str);
        }
    };

    public abstract String desensitize(String str, Integer start, Integer end);


}
