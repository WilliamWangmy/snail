package com.snail.common.core.constant;

/**
 * Token的Key常量
 * 
 * @author snail
 */
public class TokenConstants
{
    /**
     * 令牌自定义标识
     */
    public static final String AUTHENTICATION = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String PREFIX = "Bearer ";

    /**
     * 令牌秘钥
     */
    public final static String SECRET = "abcdefghijklmnopqrstuvwxyz";

    /**
     * 登录类型
     */
    public final static String ACCOUNT_LOGIN = "account";
    public final static String PHONE_LOGIN = "mobile";


}
