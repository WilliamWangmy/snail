package com.snail.common.core.web.page;

import lombok.Data;

import java.util.List;

/**
 * @Description: 分页数据返回
 * @Author: Snail
 * @CreateDate: 2022/4/30 15:00
 * @Version: V1.0
 */
@Data
public class PageResult<T> {

    /**
     *第几页
     */
    private Long pageNum;
    /**
     * 分页大小
     */
    private Long pageSize;
    /**
     * 总记录数
     */
    private Long total;

    /**
     * 分页数据
     */
    private List<T> rows;

}
