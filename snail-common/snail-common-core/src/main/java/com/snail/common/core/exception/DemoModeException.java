package com.snail.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author snail
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
