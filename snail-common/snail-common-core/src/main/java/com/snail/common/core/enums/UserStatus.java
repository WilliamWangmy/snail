package com.snail.common.core.enums;

import java.util.Arrays;

/**
 * 用户状态
 *
 * @author snail
 */
public enum UserStatus {
    /**
     * 正常
     */
    OK("0", "正常"),
    /**
     * 停用
     */
    DISABLE("1", "停用"),
    /**
     * 删除
     */
    DELETED("2", "删除");

    private final String code;
    private final String info;

    UserStatus(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public static String converterInfo(String code){
        return Arrays.stream(UserStatus.values())
                .filter(item -> item.getCode().equals(code))
                .map(UserStatus::getInfo)
                .findFirst()
                .orElse(null);
    }

    public static String converterCode(String info){
        return Arrays.stream(UserStatus.values())
                .filter(item -> item.getInfo().equals(info))
                .map(UserStatus::getCode)
                .findFirst()
                .orElse(null);
    }
}
