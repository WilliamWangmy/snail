package com.snail.common.core.enums;

import lombok.AllArgsConstructor;

import java.util.Arrays;

/**
 * @Description: 流程状态枚举
 * @Author: Snail
 * @CreateDate: 2023/8/17 15:49
 * @Version: V1.0
 */
@AllArgsConstructor
public enum WorkflowStatus {
    /**
     * 待提交
     */
    draft("draft", "待提交"),
    /**
     * 审批中
     */
    in_process("in_process", "审批中"),
    /**
     * 作废
     */
    cancel("cancel", "作废"),
    /**
     * 已完成
     */
    complete("complete", "已完成"),
    /**
     * 驳回
     */
    reject("reject", "驳回"),
    ;

    private String code;
    private String info;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static String converterInfo(String code) {
        return Arrays.stream(WorkflowStatus.values())
                .filter(item -> item.getCode().equals(code))
                .map(WorkflowStatus::getInfo)
                .findFirst()
                .orElse(null);
    }

    public static String converterCode(String info) {
        return Arrays.stream(WorkflowStatus.values())
                .filter(item -> item.getInfo().equals(info))
                .map(WorkflowStatus::getCode)
                .findFirst()
                .orElse(null);
    }
}
