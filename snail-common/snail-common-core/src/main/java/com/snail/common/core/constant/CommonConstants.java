package com.snail.common.core.constant;

/**
 * @Description: 公共常量类
 * @Author: Snail
 * @CreateDate: 2023/8/7 11:03
 * @Version: V1.0
 */
public class CommonConstants {

    /**
     * 分割字符
     */
    public static final String SPLIT_CHAR_1 = ",";
    public static final String SPLIT_CHAR_2 = "/";
    /**
     * 状态
     */
    public static final String STATUS_YES = "0";
    public static final String STATUS_NO = "1";

    /**
     * 是否逻辑删除
     */
    public static final String DELETE_YES = "1";
    public static final String DELETE_NO = "0";
    /**
     * 是否
     */
    public static final String YES = "Y";
    public static final String NO = "N";

    /**
     * 状态启用、禁用
     */
    public static final String STATUS_ENABLE = "1";
    public static final String STATUS_DISABLE = "0";
    /**
     * 状态成功、失败
     */
    public static final String STATUS_SUCCESS = "1";
    public static final String STATUS_FAIL = "0";
}
