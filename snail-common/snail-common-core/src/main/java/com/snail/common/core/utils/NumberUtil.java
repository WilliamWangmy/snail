package com.snail.common.core.utils;

/**
 * @Description: 数字工具类
 * @Author: Snail
 * @CreateDate: 2023/8/21 15:39
 * @Version: V1.0
 */
public class NumberUtil extends cn.hutool.core.util.NumberUtil {

    /**
     * 判断数字是多少位
     *
     * @param number 数字
     * @return 结果
     */
    public static int bit(Long number) {
        if (number == 0) {
            return 1;
        }
        int bit = 0;
        while (number > 0) {
            number = number / 10;
            bit++;
        }
        return bit;
    }
}
