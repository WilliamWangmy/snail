package com.snail.common.core.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.snail.common.core.enums.DesensitizationEnum;
import com.snail.common.core.serialize.DesensitizationSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据脱敏注解
 *
 * @author snail
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = DesensitizationSerializer.class)
public @interface Desensitization {

    /**
     * 类型
     */
    DesensitizationEnum type() default DesensitizationEnum.CUSTOM;

    /**
     * 脱敏开始位置
     */
    int start() default 0;

    /**
     * 脱敏结束位置
     */
    int end() default 0;

}
