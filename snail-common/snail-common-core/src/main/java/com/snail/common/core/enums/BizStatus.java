package com.snail.common.core.enums;

import java.util.Arrays;

/**
 * 用户状态
 *
 * @author snail
 */
public enum BizStatus {
    /**
     * 正常
     */
    OK("1", "启用"),
    /**
     * 停用
     */
    DISABLE("0", "禁用");

    private final String code;
    private final String info;

    BizStatus(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public static String converterInfo(String code){
        return Arrays.stream(BizStatus.values())
                .filter(item -> item.getCode().equals(code))
                .map(BizStatus::getInfo)
                .findFirst()
                .orElse(null);
    }

    public static String converterCode(String info){
        return Arrays.stream(BizStatus.values())
                .filter(item -> item.getInfo().equals(info))
                .map(BizStatus::getCode)
                .findFirst()
                .orElse(null);
    }
}
