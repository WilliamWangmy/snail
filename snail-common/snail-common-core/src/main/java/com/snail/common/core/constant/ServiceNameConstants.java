package com.snail.common.core.constant;

/**
 * 服务名称
 * 
 * @author snail
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "snail-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "snail-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "snail-file";
    /**
     * 流程服务的serviceid
     */
    public static final String WORKFLOW_SERVICE = "snail-workflow";
    /**
     * 第三方接口服务
     */
    public static final String THIRDPARTY_SERVICE = "snail-thirdparty";
    /**
     * 定时任务服务
     */
    public static final String JOB_ADMIN_SERVICE = "snail-job-admin";
}
