package com.snail.common.job.enums;


/**
 * 路由策略
 */
public enum RouteStrategyEnum {

    /**
     * 第一个
     */
    FIRST("FIRST"),
    /**
     * 最后一个
     */
    LAST("LAST"),
    /**
     * 轮询
     */
    ROUND("ROUND"),
    /**
     * 随机
     */
    RANDOM("RANDOM"),
    /**
     * 一致性HASH
     */
    CONSISTENT_HASH("CONSISTENT_HASH"),
    /**
     * 最不经常使用
     */
    LEAST_FREQUENTLY_USED("LEAST_FREQUENTLY_USED"),
    /**
     * 最近最久未使用
     */
    LEAST_RECENTLY_USED("LEAST_RECENTLY_USED"),
    /**
     * 故障转移
     */
    FAILOVER("FAILOVER"),
    /**
     * 忙碌转移
     */
    BUSYOVER("BUSYOVER"),
    /**
     * 分片广播
     */
    SHARDING_BROADCAST("SHARDING_BROADCAST");

    RouteStrategyEnum(String value) {
        this.value = value;
    }

    private String value;

}
