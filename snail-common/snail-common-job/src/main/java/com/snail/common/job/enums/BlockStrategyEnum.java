package com.snail.common.job.enums;

public enum BlockStrategyEnum {

    SERIAL_EXECUTION("SERIAL_EXECUTION"),
    DISCARD_LATER("DISCARD_LATER"),
    COVER_EARLY("COVER_EARLY");

    private String value;

    BlockStrategyEnum(String value) {
        this.value = value;
    }
}
