package com.snail.common.job.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({JobScannerRegistrar.class})
public @interface JobRegistrarScan {

    String value();

}
