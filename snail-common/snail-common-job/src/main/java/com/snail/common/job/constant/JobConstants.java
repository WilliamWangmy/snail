package com.snail.common.job.constant;

/**
 * @Description: Job常量
 * @Author: Snail
 * @CreateDate: 2024/9/24
 * @Version: V1.0
 */
public class JobConstants {

    public static final String LOCAL_KEY_JOB_EXECUTOR = "jobExecutor";

    public static final String LOCAL_KEY_JOB_INFO = "jobInfo";


}
