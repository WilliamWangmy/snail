package com.snail.common.job.enums;

public enum ScheduleTypeEnum {

    NONE("NONE"),

    /**
     * schedule by cron
     */
    CRON("CRON"),

    /**
     * schedule by fixed rate (in seconds)
     */
    FIX_RATE("FIX_RATE");

    ScheduleTypeEnum(String value) {
        this.value = value;
    }

    private String value;
}
