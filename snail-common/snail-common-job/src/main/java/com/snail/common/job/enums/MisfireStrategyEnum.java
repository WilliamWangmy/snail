package com.snail.common.job.enums;

public enum MisfireStrategyEnum {

    DO_NOTHING("DO_NOTHING"),

    FIRE_ONCE_NOW("FIRE_ONCE_NOW");

    MisfireStrategyEnum(String value) {
        this.value = value;
    }

    private String value;
}
