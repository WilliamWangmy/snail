package com.snail.common.job.annotation;

import com.snail.common.job.enums.BlockStrategyEnum;
import com.snail.common.job.enums.MisfireStrategyEnum;
import com.snail.common.job.enums.RouteStrategyEnum;
import com.snail.common.job.enums.ScheduleTypeEnum;
import com.xxl.job.core.glue.GlueTypeEnum;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface JobTask {

    /**
     * 任务描述
     */
    String value();

    /**
     * 路由策略
     */
    RouteStrategyEnum routeStrategy() default RouteStrategyEnum.FIRST;

    /**
     * cron表达式
     */
    String cron();

    /**
     * 阻塞策略
     */
    BlockStrategyEnum blockStrategy() default BlockStrategyEnum.SERIAL_EXECUTION;

    /**
     * 作者
     */
    String auth() default "admin";

    /**
     * 调度类型
     */
    ScheduleTypeEnum scheduleType() default ScheduleTypeEnum.CRON;

    /**
     * GLUE类型
     */
    GlueTypeEnum glueType() default GlueTypeEnum.BEAN;

    /**
     * 调度过期策略
     */
    MisfireStrategyEnum misfireStrategy() default MisfireStrategyEnum.DO_NOTHING;
}
