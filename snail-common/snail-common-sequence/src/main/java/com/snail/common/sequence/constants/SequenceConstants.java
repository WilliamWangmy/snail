package com.snail.common.sequence.constants;

/**
 * @Description: 序列号常量
 * @Author: Snail
 * @CreateDate: 2023/8/21 14:05
 * @Version: V1.0
 */
public class SequenceConstants {

    /**
     * 策略:
     * 1.纯数字，前缀+数字生成策略+后缀。eg:SQ0001SQ
     * 2.日期+数字，前缀+日期+数字+后缀。eg:SQ202306060001SQ
     *
     */
    public static final String STRATEGY_1 = "1";
    public static final String STRATEGY_2 = "2";
    /**
     * 序列号缓存key
     */
    public static final String SEQUENCE_CODE_KEY = "snail:sequence:{}:code:";
    /**
     * 序列号缓存key
     */
    public static final String SEQUENCE_CONFIG_KEY = "snail:sequence:{}:config:";
    /**
     * 序列号分布式锁key
     */
    public static final String LOCK_KEY = "snail:sequence:lock:{}";
}

