package com.snail.common.sequence.feign;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description: 序列号feign接口
 * @Author: Snail
 * @CreateDate: 2023/8/22 10:29
 * @Version: V1.0
 */
@FeignClient(contextId = "remoteSequenceService", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface RemoteSequenceService {


    /**
     * 生成序列号
     *
     * @param sequenceCode 序列号编码
     * @param source       来源
     * @return 结果
     */
    @GetMapping("/sequence/generate")
    String generateSequence(@RequestParam("sequenceCode") String sequenceCode, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 批量生成序列号
     *
     * @param sequenceCode 序列号编码
     * @param generateNum  生成数量
     * @param source       来源
     * @return 结果
     */
    @GetMapping("/sequence/generateBatch")
    Boolean generateBatchSequence(@RequestParam("sequenceCode")String sequenceCode, @RequestParam("generateNum")Long generateNum, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
