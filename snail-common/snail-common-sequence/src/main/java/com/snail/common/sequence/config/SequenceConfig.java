package com.snail.common.sequence.config;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 序列号配置类
 * @Author: Snail
 * @CreateDate: 2023/8/22 10:59
 * @Version: V1.0
 */
@Data
@Builder
public class SequenceConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 规则id
     */
    private String ruleId;
    /**
     * 序列号名称
     */
    private String sequenceName;
    /**
     * 序列号编码
     */
    private String sequenceCode;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 后缀
     */
    private String suffix;
    /**
     * 策略:
     * 1.纯数字，前缀+数字生成策略+后缀。eg:SQ0001SQ
     * 2.日期+数字，前缀+日期+数字+后缀。eg:SQ202306060001SQ
     */
    private String strategy;

    /**
     * 日期格式
     */
    private String dateFormat;
    /**
     * 最小值
     */
    private Long minValue;
    /**
     * 最大值
     */
    private Long maxValue;
    /**
     * 当前值
     */
    private Long currentValue;
    /**
     * 每次生成多少个
     */
    private Long generateNum;
    /**
     * 是否重置
     */
    private String reset;
    /**
     * 补值
     */
    private String repairValue;
    /**
     * 序列号样例
     */
    private String maxSequence;

    /**
     * 达到多少数量后进行补值
     */
    private Long threshold;
    /**
     * 父序列号规则
     */
    private String parentSequenceCode;
}
