package com.snail.common.sequence.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 序列号配置类
 * @Author: Snail
 * @CreateDate: 2023/8/22 10:09
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties("snail.sequence")
public class SequenceProperties {

}
