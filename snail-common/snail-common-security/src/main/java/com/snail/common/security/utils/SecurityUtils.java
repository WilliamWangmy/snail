package com.snail.common.security.utils;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.TokenConstants;
import com.snail.common.core.context.SecurityContextHolder;
import com.snail.common.core.utils.ServletUtils;
import com.snail.common.core.utils.SpringUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.security.config.SecurityProperties;
import com.snail.system.api.domain.SysDept;
import com.snail.system.api.domain.SysUser;
import com.snail.system.api.model.LoginUser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;

/**
 * 权限获取工具类
 *
 * @author snail
 */
public class SecurityUtils {
    /**
     * 获取用户ID
     */
    public static String getUserId() {
        return SecurityContextHolder.getUserId();
    }

    /**
     * 获取用户名称
     */
    public static String getUserName() {
        return SecurityContextHolder.getUserName();
    }
    /**
     * 获取用户名称
     */
    public static String getNickName() {
        return getSysUser().getNickName();
    }

    /**
     * 获取当前登录用户信息
     */
    public static SysUser getSysUser() {
        return getLoginUser().getSysUser();
    }

    /**
     * 获取当前登录用户组织
     */
    public static SysDept getSysDept() {
        return getLoginUser().getSysDept();
    }

    /**
     * 获取组织id
     */
    public static String getDeptId() {
        return getLoginUser().getSysDept().getDeptId();
    }

    /**
     * 获取组织名称
     */
    public static String getDeptName() {
        return getLoginUser().getSysDept().getDeptName();
    }

    /**
     * 获取用户key
     */
    public static String getUserKey() {
        return SecurityContextHolder.getUserKey();
    }

    /**
     * 获取登录用户信息
     */
    public static LoginUser getLoginUser() {
        return SecurityContextHolder.get(SecurityConstants.LOGIN_USER, LoginUser.class);
    }

    /**
     * 获取请求token
     */
    public static String getToken() {
        return getToken(ServletUtils.getRequest());
    }

    /**
     * 根据request获取请求token
     */
    public static String getToken(HttpServletRequest request) {
        // 从header获取token标识
        String token = request.getHeader(TokenConstants.AUTHENTICATION);
        return replaceTokenPrefix(token);
    }

    /**
     * 裁剪token前缀
     */
    public static String replaceTokenPrefix(String token) {
        // 如果前端设置了令牌前缀，则裁剪掉前缀
        if (StringUtils.isNotEmpty(token) && token.startsWith(TokenConstants.PREFIX)) {
            token = token.replaceFirst(TokenConstants.PREFIX, "");
        }
        return token;
    }

    /**
     * 是否超级管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdminUser(String userId) {
        return userId != null && SpringUtils.getBean(SecurityProperties.class).getAdminUserId().equals(userId);
    }

    /**
     * 当前登录人是否超级管理员
     *
     * @return 结果
     */
    public static boolean isAdminUser() {
        return isAdminUser(SecurityUtils.getUserId());
    }

    /**
     * 是否超级管理员角色
     *
     * @param roleId 角色id
     * @return 结果
     */
    public static boolean isAdminRole(String roleId) {
        return roleId != null && SpringUtils.getBean(SecurityProperties.class).getAdminRoleId().equals(roleId);
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 获取组织全路径下的人名
     *
     * @return
     */
    public static String getFullUserName() {
        LoginUser loginUser = getLoginUser();
        if (loginUser == null) {
            return "";
        }
        SysUser sysUser = loginUser.getSysUser();
        SysDept dept = loginUser.getSysDept();
        String fullName = dept.getDeptName();
        if (StringUtils.isNotEmpty(dept.getDeptFullName())) {
            fullName = dept.getDeptFullName();
        }
        return fullName + "/" + sysUser.getNickName();
    }


    /**
     * 获取租户标识
     *
     * @return 租户标识
     */
    public static String getTenantId() {
        return SecurityContextHolder.getTenantId();
    }
}
