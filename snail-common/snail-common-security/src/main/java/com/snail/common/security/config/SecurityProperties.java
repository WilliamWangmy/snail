package com.snail.common.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 安全配置属性
 * @Author: Snail
 * @CreateDate: 2023/8/3 11:09
 * @Version: V1.0
 */
@Configuration
@ConfigurationProperties(prefix = "snail.security")
public class SecurityProperties {

    /**
     * 是否开启方法权限
     */
    private Boolean permissions = true;

    /**
     * 超级管理员id
     */
    private String adminUserId = "1000";

    /**
     * 超级管理员角色id
     */
    private String adminRoleId = "1";

    public Boolean getPermissions() {
        return permissions;
    }

    public void setPermissions(Boolean permissions) {
        this.permissions = permissions;
    }

    public String getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(String adminUserId) {
        this.adminUserId = adminUserId;
    }

    public String getAdminRoleId() {
        return adminRoleId;
    }

    public void setAdminRoleId(String adminRoleId) {
        this.adminRoleId = adminRoleId;
    }
}
