package com.snail.common.log.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.snail.common.core.constant.SecurityConstants;
import com.snail.system.api.RemoteLogService;
import com.snail.system.api.domain.SysOperaLog;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 异步调用日志服务
 * 
 * @author snail
 */
@Service
public class AsyncLogService
{
    @Autowired
    private RemoteLogService remoteLogService;

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(SysOperaLog sysOperaLog,ServletRequestAttributes requestAttributes) throws Exception
    {
        RequestContextHolder.setRequestAttributes(requestAttributes, true);
        remoteLogService.saveLog(sysOperaLog, SecurityConstants.INNER);
    }
}
