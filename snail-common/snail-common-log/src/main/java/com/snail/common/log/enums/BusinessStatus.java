package com.snail.common.log.enums;

/**
 * 操作状态
 * 
 * @author snail
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
