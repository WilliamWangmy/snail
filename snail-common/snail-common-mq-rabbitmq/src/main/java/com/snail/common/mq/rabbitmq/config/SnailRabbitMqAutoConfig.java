package com.snail.common.mq.rabbitmq.config;

import com.snail.common.mq.aspect.MqLogAspect;
import com.snail.common.mq.core.IMqProducerAndConsumerHandler;
import com.snail.common.mq.core.SnailMqProperties;
import com.snail.common.mq.rabbitmq.handler.RabbitMqProducerAndConsumerHandler;
import com.snail.common.mq.service.ISnailMqConsumerService;
import com.snail.common.mq.service.ISnailMqProducerService;
import com.snail.common.mq.service.impl.SnailMqConsumerServiceImpl;
import com.snail.common.mq.service.impl.SnailMqProducerServiceImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Description: MQ配置类
 * @Author: Snail
 * @CreateDate: 2023/11/27 9:49
 * @Version: V1.0
 */
@Configuration
@EnableRabbit
@EnableConfigurationProperties(SnailMqProperties.class)
@ConditionalOnClass(RabbitAutoConfiguration.class)
public class SnailRabbitMqAutoConfig {

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean("rabbitMqProducerAndConsumerHandler")
    public IMqProducerAndConsumerHandler kafkaProducerAndConsumerHandler() {
        return new RabbitMqProducerAndConsumerHandler();
    }


    @Configuration
    @ConditionalOnProperty(prefix = "snail.mq.log", name = "enable", havingValue = "true", matchIfMissing = true)
    @MapperScan("com.snail.common.mq.mapper")
    @Import({MqLogAspect.class})
    static class SnailMqLogAutoConfig {

        @Bean
        public ISnailMqProducerService snailMqProducerService() {
            return new SnailMqProducerServiceImpl();
        }

        @Bean
        public ISnailMqConsumerService snailMqConsumerService() {
            return new SnailMqConsumerServiceImpl();
        }

    }

}
