package com.snail.common.lock.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 分布式锁配置
 * @Author: Snail
 * @CreateDate: 2023/8/22 17:16
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties("snail.lock")
public class LockConfig {

    /**
     * 锁过期时间，单位毫秒
     */
    private Long lockTimeOut = 30000L;

    /**
     * 锁等待超时时间，单位毫秒
     */
    private Long waitTimeOut = 10000L;
}
