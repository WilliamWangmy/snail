package com.snail.common.mq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.mq.core.domain.SnailMqProducer;
import com.snail.common.mq.core.dto.MqDto;

import java.util.List;

/**
 * @Description: MQ消息记录service
 * @Author: Snail
 * @CreateDate: 2023/11/21 14:29
 * @Version: V1.0
 */
public interface ISnailMqProducerService extends IService<SnailMqProducer> {


    /**
     * 获取mq消息生产列表
     *
     * @param producer 生产信息
     * @return 结果
     */
    List<SnailMqProducer> selectMqProducerList(SnailMqProducer producer);

    /**
     * 查询消息生产记录数
     *
     * @param mqDto 消息对象
     * @return 结果
     */
    <T> Long selectMqProducerCount(MqDto<T> mqDto);

    /**
     * 插入消息生产记录
     *
     * @param mqDto    消息体
     * @param result   发送结果
     * @param errorMsq 错误信息
     */
    <T> void insertProducerInfo(MqDto<T> mqDto, Boolean result, String errorMsq);

    /**
     * 删除生产记录
     * @param mqDto 消息对象
     */
    <T> void deleteMqProducer(MqDto<T> mqDto);
}
