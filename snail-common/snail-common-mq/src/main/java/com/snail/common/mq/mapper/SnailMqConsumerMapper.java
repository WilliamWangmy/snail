package com.snail.common.mq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.common.mq.core.domain.SnailMqConsumer;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 消息生产记录mapper
 * @Author: Snail
 * @CreateDate: 2023/11/20 14:19
 * @Version: V1.0
 */
@Mapper
public interface SnailMqConsumerMapper extends BaseMapper<SnailMqConsumer> {
}
