package com.snail.common.mq.core;

import com.snail.common.mq.constants.MqConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: MQ消息配置属性
 * @Author: Snail
 * @CreateDate: 2023/11/29 10:02
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "snail.mq")
public class SnailMqProperties {

    static class Log{

        /**
         * 是否开启消息日志
         */
        private boolean enable;
    }



}
