package com.snail.common.mq.core;

import com.snail.common.mq.core.dto.MqDto;
import com.snail.common.mq.core.dto.MqResultDto;

/**
 * @Description: MQ生产和消费处理器
 * @Author: Snail
 * @CreateDate: 2023/11/21 14:24
 * @Version: V1.0
 */
public interface IMqProducerAndConsumerHandler {

    /**
     * 发送MQ消息
     *
     * @param mqDto 消息体
     * @return 结果
     */
    <T> MqResultDto sendMq(MqDto<T> mqDto);
}
