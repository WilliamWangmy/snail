package com.snail.common.mq.constants;

/**
 * @Description: MQ常量
 * @Author: Snail
 * @CreateDate: 2023/11/20 10:48
 * @Version: V1.0
 */
public interface MqConstants {

    String LOCK_KEY = "snail:mq:{}";
    /**
     * 消息类型
     */
    String KAFKA_TYPE = "Kafka";
    String RABBITMQ_TYPE = "RabbitMQ";

    /**
     *  生产者， 消费者
     */
    String PRODUCER ="1";
    String CONSUMER ="2";
}
