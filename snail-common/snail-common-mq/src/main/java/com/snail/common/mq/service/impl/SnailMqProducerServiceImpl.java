package com.snail.common.mq.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.mq.constants.MqConstants;
import com.snail.common.mq.core.domain.SnailMqProducer;
import com.snail.common.mq.core.dto.MqDto;
import com.snail.common.mq.mapper.SnailMqProducerMapper;
import com.snail.common.mq.service.ISnailMqProducerService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description: MQ消息处理
 * @Author: Snail
 * @CreateDate: 2023/11/21 14:29
 * @Version: V1.0
 */
@Service
public class SnailMqProducerServiceImpl extends ServiceImpl<SnailMqProducerMapper, SnailMqProducer> implements ISnailMqProducerService {
    @Override
    public List<SnailMqProducer> selectMqProducerList(SnailMqProducer producer) {
        LambdaQueryWrapper<SnailMqProducer> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getTopic()), SnailMqProducer::getTopic, producer.getTopic());
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getPriKey()), SnailMqProducer::getPriKey, producer.getPriKey());
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getMqType()), SnailMqProducer::getMqType, producer.getMqType());
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getExchange()), SnailMqProducer::getExchange, producer.getExchange());
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getQueue()), SnailMqProducer::getQueue, producer.getQueue());
        queryWrapper.eq(StringUtils.isNotEmpty(producer.getCreateUserId()), SnailMqProducer::getCreateUserId, producer.getCreateUserId());
        queryWrapper.like(StringUtils.isNotEmpty(producer.getCreateUserName()), SnailMqProducer::getCreateUserName, producer.getCreateUserName());
        queryWrapper.ge(producer.getCreateTime() != null, SnailMqProducer::getCreateTime, producer.getCreateTime());
        queryWrapper.le(producer.getCreateTime() != null, SnailMqProducer::getCreateTime, producer.getCreateTime());
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public <T> Long selectMqProducerCount(MqDto<T> mqDto) {
        LambdaQueryWrapper<SnailMqProducer> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SnailMqProducer::getTopic, mqDto.getTopic());
        queryWrapper.eq(SnailMqProducer::getPriKey, mqDto.getPriKey());
        queryWrapper.eq(StringUtils.isNotEmpty(mqDto.getExchange()), SnailMqProducer::getExchange, mqDto.getExchange());
        queryWrapper.eq(StringUtils.isNotEmpty(mqDto.getQueue()), SnailMqProducer::getQueue, mqDto.getQueue());
        return baseMapper.selectCount(queryWrapper);
    }

    /**
     * 插入消息生产记录
     *
     * @param mqDto    消息体
     * @param result   发送结果
     * @param errorMsq 错误信息
     */
    @Override
    public <T> void insertProducerInfo(MqDto<T> mqDto, Boolean result, String errorMsq) {
        SnailMqProducer producer = new SnailMqProducer();
        producer.setPriKey(mqDto.getPriKey());
        producer.setMqType(mqDto.getMqType());
        producer.setTopic(mqDto.getTopic());
        producer.setExchange(mqDto.getExchange());
        producer.setQueue(mqDto.getQueue());
        producer.setContent(JSON.toJSONString(mqDto.getContent()));
        producer.setCreateUserId(mqDto.getUserId());
        producer.setCreateUserName(mqDto.getUserName());
        producer.setCreateTime(LocalDateTime.now());
        this.save(producer);
    }

    @Override
    public <T> void deleteMqProducer(MqDto<T> mqDto) {
        LambdaUpdateWrapper<SnailMqProducer> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SnailMqProducer::getTopic, mqDto.getTopic());
        updateWrapper.eq(SnailMqProducer::getPriKey, mqDto.getPriKey());
        updateWrapper.eq(StringUtils.isNotEmpty(mqDto.getExchange()), SnailMqProducer::getExchange, mqDto.getExchange());
        updateWrapper.eq(StringUtils.isNotEmpty(mqDto.getQueue()), SnailMqProducer::getQueue, mqDto.getQueue());
        this.baseMapper.delete(updateWrapper);
    }
}
