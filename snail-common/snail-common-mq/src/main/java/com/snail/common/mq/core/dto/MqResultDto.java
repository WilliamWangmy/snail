package com.snail.common.mq.core.dto;

import lombok.Data;

/**
 * @Description: 消息结果
 * @Author: Snail
 * @CreateDate: 2023/11/20 10:27
 * @Version: V1.0
 */
@Data
public class MqResultDto {

    public MqResultDto() {
        this.result = Boolean.FALSE;
    }

    /**
     * 结果
     */
    private Boolean result;

    /**
     * 错误信息
     */
    private String errorMsq;
}
