package com.snail.common.mq.exception;

/**
 * @Description: MQ全局异常
 * @Author: Snail
 * @CreateDate: 2022/7/14 17:54
 * @Version: V1.0
 */
public class MqException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;


    /**
     * 空构造方法，避免反序列化问题
     */
    public MqException() {
    }

    public MqException(String message) {
        this.message = message;
    }


    public MqException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public MqException setMessage(String message) {
        this.message = message;
        return this;
    }
}
