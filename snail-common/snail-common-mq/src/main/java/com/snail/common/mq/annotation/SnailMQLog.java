package com.snail.common.mq.annotation;

import java.lang.annotation.*;

/**
 * MQ日志
 * @author Snail
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SnailMQLog {

    /**
     * 日志类型,
     */
    String type();

    /**
     * mq类型
     */
    String mq();
}
