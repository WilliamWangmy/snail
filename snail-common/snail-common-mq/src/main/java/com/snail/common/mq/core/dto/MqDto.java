package com.snail.common.mq.core.dto;

import lombok.Data;

/**
 * @Description: 消息生产对象
 * @Author: Snail
 * @CreateDate: 2023/11/20 10:32
 * @Version: V1.0
 */
@Data
public class MqDto<T> {

    /**
     * 消息的唯一标识
     */
    private String priKey;
    /**
     * 消息类型：RabbitMq，Kafka
     */
    private String mqType;
    /**
     * 消息主题
     */
    private String topic;

    /**
     * 分区
     */
    private Integer partition;

    /**
     * 交换器
     */
    private String exchange;
    /**
     * 队列
     */
    private String queue;

    /**
     * 路由键
     */
    private String routingKey;

    /**
     * 消息内容
     */
    private T content;
    /**
     * 消息发送人
     */
    private String userId;
    /**
     * 消息发送人名称
     */
    private String userName;
}
