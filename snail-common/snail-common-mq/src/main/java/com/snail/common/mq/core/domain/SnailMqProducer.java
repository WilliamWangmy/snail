package com.snail.common.mq.core.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @Description: MQ消息生产记录
 * @Author: Snail
 * @CreateDate: 2023/11/20 14:15
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@ApiModel(value = "MQ消息生产记录")
@TableName("snail_mq_producer")
public class SnailMqProducer {

    /**
     * 主键
     */
    @ApiModelProperty(name = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String producerId;
    /**
     * 消息唯一标识
     */
    @ApiModelProperty(name = "消息唯一标识")
    private String priKey;
    /**
     * 消息类型：RabbitMq，Kafka
     */
    @ApiModelProperty(name = "消息类型：RabbitMq，Kafka")
    private String mqType;
    /**
     * 消息主题
     */
    @ApiModelProperty(name = "消息主题")
    private String topic;
    /**
     * 交换器
     */
    @ApiModelProperty(name = "交换器")
    private String exchange;
    /**
     * 队列
     */
    @ApiModelProperty(name = "队列")
    private String queue;
    /**
     * 内容
     */
    @ApiModelProperty(name = "内容")
    private String content;
    /**
     * 创建人id
     */
    @ApiModelProperty(name = "创建人id")
    private String createUserId;
    /**
     * 创建人
     */
    @ApiModelProperty(name = "创建人")
    private String createUserName;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
