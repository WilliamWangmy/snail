package com.snail.common.mq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.mq.core.domain.SnailMqConsumer;
import com.snail.common.mq.core.dto.MqDto;

import java.util.List;

/**
 * @Description: MQ消息记录service
 * @Author: Snail
 * @CreateDate: 2023/11/21 14:29
 * @Version: V1.0
 */
public interface ISnailMqConsumerService extends IService<SnailMqConsumer> {

    /**
     * 插入消息消费记录
     *
     * @param mqDto    消息体
     * @param result   发送结果
     * @param errorMsq 错误信息
     */
    <T> void insertConsumerInfo(MqDto<T> mqDto, Boolean result, String errorMsq);

    /**
     * 获取消费记录
     *
     * @param consumer 消费记录对象
     * @return 结果
     */
    List<SnailMqConsumer> selectMqConsumerList(SnailMqConsumer consumer);
}
