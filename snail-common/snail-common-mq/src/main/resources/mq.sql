create table snail_mq_consumer
(
    consumer_id      varchar(64)  not null comment '主键'
        primary key,
    pri_key          varchar(100) not null comment '消息唯一标识',
    mq_type          varchar(10)  not null comment '消息类型：RabbitMq，Kafka',
    topic            varchar(200) null comment '消息主题',
    exchange         varchar(200) null comment '交换器',
    queue            varchar(200) null comment '队列',
    content          longblob null comment '内容',
    status           varchar(255) not null comment '状态：1.成功，0.失败',
    error_msg        varchar(2000) null comment '错误信息',
    create_user_id   varchar(64) null comment '创建人id',
    create_user_name varchar(20) null comment '创建人',
    create_time      datetime     not null comment '创建时间',
    retry_count      int null comment '重试次数'
) comment 'mq消息消费记录';

create table snail_mq_producer
(
    producer_id      varchar(64)  not null comment '主键'
        primary key,
    pri_key          varchar(100) not null comment '消息唯一标识',
    mq_type          varchar(10)  not null comment '消息类型：RabbitMq，Kafka',
    topic            varchar(200) null comment '消息主题',
    exchange         varchar(200) null comment '交换器',
    queue            varchar(200) null comment '队列',
    content          longblob null comment '内容',
    create_user_id   varchar(64) null comment '创建人id',
    create_user_name varchar(20) null comment '创建人',
    create_time      datetime     not null comment '创建时间'
) comment 'mq消息生产记录';