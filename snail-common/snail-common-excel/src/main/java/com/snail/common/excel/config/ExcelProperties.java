package com.snail.common.excel.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: Excel配置参数
 * @Author: Snail
 * @CreateDate: 2023/8/15 16:45
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties("snail.excel")
public class ExcelProperties {

    /**
     * 全部导入
     * 为false时可以部分导入成功
     * 为true时整体失败或者整体成功
     */
    public boolean importAll = false;

    /**
     * 每次导入多少条
     * 为0则一次性全部导入
     */
    public Integer importNum = 200;

}
