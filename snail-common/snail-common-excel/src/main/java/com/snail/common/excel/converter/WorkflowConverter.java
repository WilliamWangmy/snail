package com.snail.common.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.snail.common.core.enums.BizStatus;
import com.snail.common.core.enums.WorkflowStatus;

/**
 * @Description: 流程状态转换器
 * @Author: Snail
 * @CreateDate: 2023/8/17 15:26
 * @Version: V1.0
 */
public class WorkflowConverter implements Converter<String> {



    @Override
    public String convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        if (cellData == null) {
            return "";
        }
        return WorkflowStatus.converterCode(cellData.toString());
    }
    @Override
    public WriteCellData<?> convertToExcelData(String value, ExcelContentProperty contentProperty,
                                               GlobalConfiguration globalConfiguration) {
        return new WriteCellData<>(WorkflowStatus.converterInfo(value));
    }

    @Override
    public Class<?> supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }
}
