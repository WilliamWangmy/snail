package com.snail.common.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据有效性
 * @author Snail
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelDataValidation {

    /**
     * 有效性范围
     * @return String[]
     */
    String[] value();

    /**
     * 错误提示
     * @return String
     */
    String msg();


}
