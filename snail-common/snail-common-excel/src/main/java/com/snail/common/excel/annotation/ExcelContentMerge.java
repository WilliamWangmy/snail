package com.snail.common.excel.annotation;

import com.snail.common.excel.constants.ExcelConstants;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Snail
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelContentMerge {

    /**
     * 合并策略
     */
    String mergeStrategy() default ExcelConstants.MERGE_SAME_CONTENT_PRE;


}
