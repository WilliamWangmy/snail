package com.snail.common.excel.constants;

/**
 * @Description: Excel导入、导出常量
 * @Author: Snail
 * @CreateDate: 2023/8/16 9:01
 * @Version: V1.0
 */
public class ExcelConstants {

    /**
     * Excel导入存放错误信息的key
     */
    public static final String ERROR_MSG_KEY = "errorMsg";

    public static final String ALL_ERROR_MSG = "导入失败,共{}条数据格式不正确,错误如下：{}";
    public static final String ERROR_MSG = "导入完成,导入成功{}条,导入失败{}条,失败信息如下：{}";

    public static final String SUCCESS_KEY = "successList";
    public static final String FAILURE_KEY = "failureList";
    public static final String SUCCESS_NUM_KEY = "successNum";
    public static final String FAILURE_NUM_KEY = "failureNum";
    /**
     * 合并相同的内容
     */
    public static final String MERGE_SAME_CONTENT = "merge_same_content";
    /**
     * 根据前一单元格合并相同的内容
     * 如果前一单元格合并那么当前单元格相同内容才合并
     */
    public static final String MERGE_SAME_CONTENT_PRE = "merge_same_content_pre";

}
