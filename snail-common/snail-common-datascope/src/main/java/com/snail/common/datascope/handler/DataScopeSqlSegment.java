package com.snail.common.datascope.handler;

import com.baomidou.mybatisplus.core.conditions.ISqlSegment;
import net.sf.jsqlparser.expression.Expression;

/**
 * @Description: 数据权限sql片段
 * @Author: Snail
 * @CreateDate: 2023/8/11 15:27
 * @Version: V1.0
 */
public class DataScopeSqlSegment implements ISqlSegment {
    private Expression expression;

    public DataScopeSqlSegment(Expression expression) {
        this.expression = expression;
    }

    @Override
    public String getSqlSegment() {
        return " and " + expression.toString();
    }

}
