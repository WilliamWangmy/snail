package com.snail.common.datascope.annotation;

import java.lang.annotation.*;

/**
 * 数据权限过滤注解
 *
 * @author snail
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScope {
    /**
     * 主表别名
     */
    String alias() default "";

    /**
     * 创建人字段名称
     */
    String userColumn() default "create_user_id";

    /**
     * 权限eq字段
     */
    String deptColumn() default "dept_id";
    /**
     * 权限like字段
     */
    String deptFullColumn() default "dept_full_id";

    /**
     * 权限字符（用于多个角色匹配符合要求的权限）默认根据权限注解@RequiresPermissions获取，多个权限用逗号分隔开来
     */
    String permission() default "";

    /**
     * 需要数据权限过滤的方法名称
     */
    String[] includeMethod() default {};
}
