package com.snail.common.datascope.interceptor;

import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import com.snail.common.core.constant.CommonConstants;
import com.snail.common.core.web.domain.BaseQuery;
import com.snail.common.datascope.constants.DataScopeConstants;
import com.snail.common.datascope.handler.DataScopePermissionHandler;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @Description: 数据权限拦截器
 * @Author: Snail
 * @CreateDate: 2023/8/10 17:23
 * @Version: V1.0
 */
public class DataScopePermissionInterceptor extends DataPermissionInterceptor {

    private final DataScopePermissionHandler dataScopePermissionHandler;

    public DataScopePermissionInterceptor(DataScopePermissionHandler dataScopePermissionHandler){
        this.dataScopePermissionHandler = dataScopePermissionHandler;
    }
    @Override
    protected void setWhere(PlainSelect plainSelect, String whereSegment) {
        Expression sqlSegment = this.dataScopePermissionHandler.getSqlSegment(plainSelect, whereSegment);
        if (null != sqlSegment) {
            plainSelect.setWhere(sqlSegment);
        }
    }
}
