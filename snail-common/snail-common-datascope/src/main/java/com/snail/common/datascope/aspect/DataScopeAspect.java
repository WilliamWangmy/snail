//package com.snail.common.datascope.aspect;
//
//import com.snail.common.core.context.SecurityContextHolder;
//import com.snail.common.core.utils.StringUtils;
//import com.snail.common.core.web.domain.BaseQuery;
//import com.snail.common.datascope.annotation.DataScope;
//import com.snail.common.datascope.constants.DataScopeConstants;
//import com.snail.common.datascope.utils.DataScopeUtils;
//import com.snail.common.security.utils.SecurityUtils;
//import com.snail.system.api.model.LoginUser;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.stereotype.Component;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 数据过滤处理
// *
// * @author snail
// */
//@Aspect
//@Component
//public class DataScopeAspect {
//
//    @Before("@annotation(controllerDataScope)")
//    public void doBefore(JoinPoint point, DataScope controllerDataScope) throws Throwable {
//        clearDataScope(point);
//        handleDataScope(point, controllerDataScope);
//    }
//
//    protected void handleDataScope(final JoinPoint joinPoint, DataScope dataScope) {
//        // 如果是超级管理员，则不过滤数据
//        if (!SecurityUtils.isAdminUser()) {
//            dataScopeFilter(joinPoint, dataScope);
//        }
//    }
//
//    /**
//     * 数据范围过滤
//     *
//     * @param joinPoint 切点
//     * @param dataScope 权限范围
//     */
//    public void dataScopeFilter(JoinPoint joinPoint, DataScope dataScope) {
//        Object params = joinPoint.getArgs()[0];
//        if (StringUtils.isNull(params) || !(params instanceof BaseQuery)) {
//            return;
//        }
//        BaseQuery baseQuery = (BaseQuery) params;
//        Map<String, Object> queryParam = new HashMap<>(64);
//        queryParam.put(DataScopeConstants.DATA_SCOPE, DataScopeUtils.authExpression(dataScope));
//        baseQuery.setParams(queryParam);
//    }
//
//    /**
//     * 拼接权限sql前先清空params.dataScope参数防止注入
//     */
//    private void clearDataScope(final JoinPoint joinPoint) {
//        Object params = joinPoint.getArgs()[0];
//        if (StringUtils.isNotNull(params) && params instanceof BaseQuery) {
//            BaseQuery baseQuery = (BaseQuery) params;
//            baseQuery.getParams().remove(DataScopeConstants.DATA_SCOPE);
//        }
//    }
//}
