package com.snail.common.mq.kafka.config;

import com.snail.common.mq.aspect.MqLogAspect;
import com.snail.common.mq.constants.MqConstants;
import com.snail.common.mq.core.IMqProducerAndConsumerHandler;
import com.snail.common.mq.core.SnailMqProperties;
import com.snail.common.mq.kafka.handler.KafkaProducerAndConsumerHandler;
import com.snail.common.mq.service.ISnailMqConsumerService;
import com.snail.common.mq.service.ISnailMqProducerService;
import com.snail.common.mq.service.impl.SnailMqConsumerServiceImpl;
import com.snail.common.mq.service.impl.SnailMqProducerServiceImpl;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Description: MQ配置类
 * @Author: Snail
 * @CreateDate: 2023/11/27 9:49
 * @Version: V1.0
 */
@Configuration
@EnableConfigurationProperties(SnailMqProperties.class)
@ConditionalOnClass(KafkaAutoConfiguration.class)
public class SnailKafkaAutoConfig {

    @Bean("kafkaProducerAndConsumerHandler")
    public IMqProducerAndConsumerHandler kafkaProducerAndConsumerHandler() {
        return new KafkaProducerAndConsumerHandler();
    }


    @Configuration
    @ConditionalOnProperty(prefix = "snail.mq.log", name = "enable", havingValue = "true", matchIfMissing = true)
    @MapperScan("com.snail.common.mq.mapper")
    @Import({MqLogAspect.class})
    static class SnailMqLogAutoConfig {

        @Bean
        public ISnailMqProducerService snailMqProducerService() {
            return new SnailMqProducerServiceImpl();
        }

        @Bean
        public ISnailMqConsumerService snailMqConsumerService() {
            return new SnailMqConsumerServiceImpl();
        }

    }

}
