## Snail-common-mq-Kafka使用方法

1、引用模块

```xml
<dependency>
    <groupId>com.snail</groupId>
    <artifactId>snail-common-mq-kafka</artifactId>
</dependency>
```

添加Kafka配置

```yml
spring:
  kafka:
    bootstrap-servers: localhost:9092 #这个是kafka的地址,对应你server.properties中配置的
    producer:
      #批次大小16K(16384)
      batch-size: 16384 #批量大小
      acks: -1 #应答级别:多少个分区副本备份完成时向生产者发送ack确认(可选0、1、all/-1)
      retries: 5 # 消息发送重试次数
      #transaction-id-prefix: transaction
      #缓冲区大小32M(33554432)
      buffer-memory: 33554432
      key-serializer: org.apache.kafka.common.serialization.StringSerializer
      value-serializer: org.apache.kafka.common.serialization.StringSerializer
      properties:
        max:
          request:
            size: 104857600
        linger:
          ms: 1000 #提交延迟
          #partitioner: #指定分区器
          #class: pers.zhang.config.CustomerPartitionHandler
    consumer:
      group-id: testGroup #默认的消费组ID
      enable-auto-commit: true #是否自动提交offset
      auto-commit-interval: 2000 #提交offset延时
      # 当kafka中没有初始offset或offset超出范围时将自动重置offset
      # earliest:重置为分区中最小的offset;
      # latest:重置为分区中最新的offset(消费分区中新产生的数据);
      # none:只要有一个分区不存在已提交的offset,就抛出异常;
      auto-offset-reset: latest
      max-poll-records: 500 #单次拉取消息的最大条数
      key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
      value-deserializer: org.apache.kafka.common.serialization.StringDeserializer
      properties:
        max:
          partition:
            fetch:
              bytes: 104857600
        session:
          timeout:
            ms: 120000 # 消费会话超时时间（超过这个时间 consumer 没有发送心跳，就会触发 rebalance 操作）
        request:
          timeout:
            ms: 18000 # 消费请求的超时时间
    listener:
      missing-topics-fatal: false # consumer listener topics 不存在时，启动项目就会报错
```

2、自定义mq业务处理类

```java
@Component
@KafkaListener(topics = {KafkaConsumerService.TOPIC})
public class KafkaConsumerService  extends AbstractKafkaMqFactory<LeaveApply> {

    public static final String TOPIC = "OA_LEAVE_APPLY";
    /**
     * 这里可以注入自定义handler进行处理
     * @param kafkaProducerAndConsumerHandler Kafka消息处理handler
     */
    public KafkaConsumerService(IMqProducerAndConsumerHandler kafkaProducerAndConsumerHandler) {
        super(kafkaProducerAndConsumerHandler);
    }


    /**
     * 业务消费处理
     *
     * @param mqDto 消息体
     * @throws ServiceException 业务异常
     * @throws MqException      mq异常
     */
    @Override
    protected void consumeMq(MqDto<LeaveApply> mqDto) throws ServiceException, MqException {
        //自定义业务处理
        System.out.println("Kafka消息监听："+mqDto);
    }
}

```

3、业务端进行调用发送消息

```java
@Override
    public MqResultDto sendMq(String applyId) {
        LeaveApply apply = getById(applyId);
        if(apply == null){
            throw new ServiceException("申请信息为空!");
        }
        MqDto<LeaveApply> mqDto = new MqDto<>();
        //设置业务唯一标识
        mqDto.setPriKey(applyId);
        //设置消息主题
        mqDto.setTopic(KafkaConsumerService.TOPIC);
        //设置消息内容
        mqDto.setContent(apply);
        //设置发送人id
        mqDto.setUserId(SecurityUtils.getUserId());
        //设置发送人名称
        mqDto.setUserName(SecurityUtils.getNickName());
        //发送mq消息
        return kafkaConsumerService.sendMq(mqDto);
    }
```

