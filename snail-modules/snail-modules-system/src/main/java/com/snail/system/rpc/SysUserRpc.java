package com.snail.system.rpc;

import com.snail.common.core.domain.R;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.security.annotation.InnerAuth;
import com.snail.system.api.domain.SysUser;
import com.snail.system.api.dto.SysUserDto;
import com.snail.system.service.ISysConfigService;
import com.snail.system.service.ISysPermissionService;
import com.snail.system.service.ISysUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @Description: 用户信息接口RPC
 * @Author: Snail
 * @CreateDate: 2023/8/7 10:52
 * @Version: V1.0
 */
@Api(value = "用户信息RPC", tags = "用户信息RPC")
@RestController
@RequestMapping("/user/rpc")
public class SysUserRpc {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysPermissionService permissionService;

    @Autowired
    private ISysConfigService configService;

    /**
     * 根据用户id获取用户信息
     *
     * @param userId 用户id
     * @return 结果
     */
    @InnerAuth
    @GetMapping("/{userId}")
    public R<SysUserDto> getUserById(@PathVariable("userId") String userId) {
        return R.ok(sysUserService.selectUserInfoById(userId));
    }

    /**
     * 根据用户id批量获取用户信息
     *
     * @param userIds 用户id
     * @return 结果
     */
    @InnerAuth
    @GetMapping("/select/{userIds}")
    public R<List<SysUserDto>> getUserList(@PathVariable("userIds") String userIds) {
        return R.ok(sysUserService.selectUserInfoByIds(userIds));
    }

    /**
     * 根据角色获取用户信息
     *
     * @param roleIds 角色id
     * @return 结果
     */
    @InnerAuth
    @GetMapping("/role/{roleIds}")
    public R<List<SysUserDto>> getRoleUserInfo(@PathVariable("roleIds") String roleIds) {
        return R.ok(sysUserService.selectRoleUserInfo(roleIds));
    }

    /**
     * 获取当前用户信息
     */
    @InnerAuth
    @GetMapping("/info/{username}")
    public R<SysUserDto> getUserByUserName(@PathVariable("username") String username) {
        SysUserDto sysUser = sysUserService.selectUserInfoByUserName(username);
        if (StringUtils.isNull(sysUser)) {
            return R.fail("用户名或密码错误");
        }
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(sysUser.getUserId());
        sysUser.setPermissions(permissions);
        //角色数据权限
        List<String> deptPermission = permissionService.getDeptPermission(sysUser.getUserId());
        sysUser.setDeptPermission(deptPermission);
        return R.ok(sysUser);
    }

    /**
     * 获取当前用户信息
     */
    @InnerAuth
    @GetMapping("/info/phone/{phone}")
    public R<SysUserDto> getUserByPhone(@PathVariable("phone") String phone) {
        SysUserDto sysUser = sysUserService.selectUserInfoByPhone(phone);
        if (StringUtils.isNull(sysUser)) {
            return R.fail("用户名或密码错误");
        }
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(sysUser.getUserId());
        sysUser.setPermissions(permissions);
        //角色数据权限
        List<String> deptPermission = permissionService.getDeptPermission(sysUser.getUserId());
        sysUser.setDeptPermission(deptPermission);
        return R.ok(sysUser);
    }

    /**
     * 注册用户信息
     */
    @InnerAuth
    @PostMapping("/register")
    public R<Boolean> register(@RequestBody SysUser sysUser) {
        String username = sysUser.getUserName();
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser")))) {
            return R.fail("当前系统没有开启注册功能！");
        }
        if (!sysUserService.checkUserNameUnique(sysUser)) {
            return R.fail("保存用户'" + username + "'失败，注册账号已存在");
        }
        return R.ok(sysUserService.registerUser(sysUser));
    }
}
