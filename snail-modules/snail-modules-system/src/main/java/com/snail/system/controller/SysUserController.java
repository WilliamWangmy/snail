package com.snail.system.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.controller.BaseController;
import com.snail.common.core.web.domain.AjaxResult;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.common.security.utils.SecurityUtils;
import com.snail.system.api.domain.SysRole;
import com.snail.system.api.domain.SysUser;
import com.snail.system.api.dto.SysUserDto;
import com.snail.system.api.query.SysDeptQuery;
import com.snail.system.api.query.SysUserQuery;
import com.snail.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用户信息
 *
 * @author snail
 */
@Api(value = "用户信息", tags = "用户信息")
@RestController
@RequestMapping("/user")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysPermissionService permissionService;

    /**
     * 获取用户列表
     */
    @ApiOperation(value = "分页查询用户信息")
    @Log(title = "用户管理", businessType = BusinessType.QUERY)
    @RequiresPermissions("system:user:list")
    @GetMapping("/list")
    public R<PageResult<SysUserDto>> list(SysUserQuery query) {
        return R.ok(userService.selectUserPage(query));
    }

    /**
     * 获取当前登录用户信息
     *
     * @return 用户信息
     */
    @ApiOperation(value = "获取当前登录用户信息")
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        SysUser user = userService.selectUserById(SecurityUtils.getUserId());
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user.getUserId());
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 根据用户编号获取详细信息
     */
    @ApiOperation(value = "根据用户编号获取详细信息")
    @RequiresPermissions("system:user:query")
    @GetMapping(value = {"/", "/{userId}"})
    public AjaxResult getInfo(@PathVariable(value = "userId", required = false) String userId) {
        userService.checkUserDataScope(userId);
        AjaxResult ajax = AjaxResult.success();
        List<SysRole> roles = roleService.selectRoleAll();
        ajax.put("roles",roles.stream().filter(r -> !SecurityUtils.isAdminRole(r.getRoleId())).collect(Collectors.toList()));
        ajax.put("posts", postService.selectPostAll());
        if (StringUtils.isNotNull(userId)) {
            SysUserDto sysUser = userService.selectUserInfoById(userId);
            ajax.put(AjaxResult.DATA_TAG, sysUser);
            ajax.put("postIds", postService.selectPostListByUserId(userId));
            List<SysRole> sysRoles = roleService.selectRolesByUserId(userId);
            ajax.put("roleIds", sysRoles.stream().map(SysRole::getRoleId).collect(Collectors.toList()));
        }
        return ajax;
    }

    /**
     * 新增用户
     */
    @ApiOperation(value = "新增用户")
    @RequiresPermissions("system:user:add")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysUserDto userDto) {
        return toAjax(userService.insertUser(userDto));
    }

    /**
     * 修改用户
     */
    @ApiOperation(value = "修改用户")
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysUserDto userDto) {
        return toAjax(userService.updateUser(userDto));
    }

    /**
     * 删除用户
     */
    @ApiOperation(value = "删除用户")
    @RequiresPermissions("system:user:remove")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable String[] userIds) {
        if (ArrayUtils.contains(userIds, SecurityUtils.getUserId())) {
            return error("当前用户不能删除");
        }
        return toAjax(userService.deleteUserByIds(userIds));
    }

    @ApiOperation(value = "下载导入模板")
    @PostMapping("/downTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException {

    }

    /**
     * 重置密码
     */
    @ApiOperation(value = "重置密码")
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public AjaxResult resetPwd(@RequestBody SysUser user) {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        return toAjax(userService.resetPwd(user));
    }

    /**
     * 状态修改
     */
    @ApiOperation(value = "状态修改")
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysUser user) {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        return toAjax(userService.updateUserStatus(user));
    }

    /**
     * 根据用户id查询授权已授权角色
     */
    @ApiOperation(value = "查询已授权角色")
    @RequiresPermissions("system:user:query")
    @GetMapping("/authRole/{userId}")
    public AjaxResult authRole(@PathVariable("userId") String userId) {
        AjaxResult ajax = AjaxResult.success();
        SysUserDto user = userService.selectUserInfoById(userId);
        List<SysRole> roles = roleService.selectRolesByUserId(userId);
        user.setRoles(roles);
        //所有角色
        List<SysRole> roleAll = roleService.selectRoleAll();
        ajax.put("user", user);
        ajax.put("roles", SecurityUtils.isAdminUser(userId) ? roleAll : roleAll.stream().filter(r -> !SecurityUtils.isAdminRole(r.getRoleId())).collect(Collectors.toList()));
        return ajax;
    }


    /**
     * 根据用户编号获取授权角色
     */
    @ApiOperation(value = "查询未授权角色")
    @RequiresPermissions("system:user:query")
    @GetMapping("/unAuthRole/{userId}")
    public AjaxResult unAuthRole(@PathVariable("userId") String userId) {
        AjaxResult ajax = AjaxResult.success();
        SysUser user = userService.selectUserById(userId);
        //授权角色
        List<SysRole> roles = roleService.selectRolesByUserId(userId);
        //所有角色
        List<SysRole> roleAll = roleService.selectRoleAll();
        //未授权角色
        List<SysRole> roleList = roleAll.stream().filter(item -> roles.stream().noneMatch(e -> e.getRoleId().equals(item.getRoleId()))).collect(Collectors.toList());
        ajax.put("user", user);
        ajax.put("roles", roleList.stream().filter(r -> !SecurityUtils.isAdminRole(r.getRoleId())).collect(Collectors.toList()));
        return ajax;
    }

    /**
     * 用户授权角色
     */
    @ApiOperation(value = "用户授权角色")
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.GRANT)
    @PutMapping("/authRole")
    public AjaxResult insertAuthRole(String userId, String[] roleIds) {
        userService.checkUserDataScope(userId);
        userService.insertUserAuth(userId, roleIds);
        return success();
    }

    /**
     * 获取部门树列表
     */
    @ApiOperation(value = "获取部门树列表")
    @RequiresPermissions("system:user:list")
    @GetMapping("/deptTree")
    public AjaxResult deptTree(SysDeptQuery dept) {
        return success(deptService.selectDeptTreeList(dept));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("system:user:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "用户信息导入模板", SysUserDto.class);
    }

    @ApiOperation(value = "导出数据")
    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:user:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, SysUserQuery query) {
        EasyExcelUtils.writeExcel(response, "用户信息", SysUserDto.class, userService.selectUserList(query));
    }

    @ApiOperation(value = "导入数据")
    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:user:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, SysUserDto.class, userService);
    }


    @ApiOperation(value = "选择用户信息")
    @GetMapping("/selectUser")
    public R<List<SysUserDto>> getSysUser(SysUserQuery query){
        return R.ok(this.userService.selectUserList(query));
    }
}
