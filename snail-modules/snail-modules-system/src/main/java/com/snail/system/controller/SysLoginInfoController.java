package com.snail.system.controller;

import com.snail.common.core.constant.CacheConstants;
import com.snail.common.core.utils.poi.ExcelUtil;
import com.snail.common.core.web.controller.BaseController;
import com.snail.common.core.web.domain.AjaxResult;
import com.snail.common.core.web.page.TableDataInfo;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.redis.service.RedisService;
import com.snail.common.security.annotation.InnerAuth;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.system.api.domain.SysLoginInfo;
import com.snail.system.api.query.SysLoginInfoQuery;
import com.snail.system.service.ISysLoginInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 系统访问记录
 *
 * @author snail
 */
@Api(value = "系统登录日志", tags = "系统登录日志")
@RestController
@RequestMapping("/loginInfo")
public class SysLoginInfoController extends BaseController {
    @Autowired
    private ISysLoginInfoService loginInfoService;

    @Autowired
    private RedisService redisService;

    @ApiOperation(value = "分页查询系统登录日志")
    @RequiresPermissions("system:loginInfo:list")
    @GetMapping("/list")
    public TableDataInfo list(SysLoginInfoQuery loginInfo) {
        startPage();
        List<SysLoginInfo> list = loginInfoService.selectLoginInfoList(loginInfo);
        return getDataTable(list);
    }

    @ApiOperation(value = "导出登录日志")
    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:loginInfo:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysLoginInfoQuery loginInfo) {
        List<SysLoginInfo> list = loginInfoService.selectLoginInfoList(loginInfo);
        ExcelUtil<SysLoginInfo> util = new ExcelUtil<SysLoginInfo>(SysLoginInfo.class);
        util.exportExcel(response, list, "登录日志");
    }

    @ApiOperation(value = "删除登录日志")
    @RequiresPermissions("system:loginInfo:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public AjaxResult remove(@PathVariable String[] infoIds) {
        return toAjax(loginInfoService.deleteLoginInfoByIds(infoIds));
    }

    @ApiOperation(value = "清空登录日志")
    @RequiresPermissions("system:loginInfo:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/clean")
    public AjaxResult clean() {
        loginInfoService.cleanLoginInfo();
        return success();
    }

    @ApiOperation(value = "账户解锁")
    @RequiresPermissions("system:loginInfo:unlock")
    @Log(title = "账户解锁", businessType = BusinessType.OTHER)
    @GetMapping("/unlock/{userName}")
    public AjaxResult unlock(@PathVariable("userName") String userName) {
        redisService.deleteObject(CacheConstants.PWD_ERR_CNT_KEY + userName);
        return success();
    }

    @InnerAuth
    @PostMapping
    public AjaxResult add(@RequestBody SysLoginInfo loginInfo) {
        return toAjax(loginInfoService.insertLoginInfo(loginInfo));
    }
}
