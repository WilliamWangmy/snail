package com.snail.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysConfig;
import com.snail.system.api.dto.SysTenantDto;
import com.snail.system.api.query.SysConfigQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 参数配置 数据层
 *
 * @author snail
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    /**
     * 查询参数配置信息
     *
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public SysConfig selectConfig(@Param("config") SysConfig config);


    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<SysConfig> selectConfigList(@Param("config") SysConfigQuery config);

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数配置信息
     */
    public SysConfig checkConfigKeyUnique(@Param("configKey") String configKey);

    /**
     * 获取多租户消息
     * @return 多租户信息
     */
    List<SysTenantDto> selectTenantList();

}