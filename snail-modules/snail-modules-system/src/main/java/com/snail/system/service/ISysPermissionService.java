package com.snail.system.service;

import java.util.List;
import java.util.Set;

import com.snail.system.api.domain.SysUser;

/**
 * 权限信息 服务层
 * 
 * @author snail
 */
public interface ISysPermissionService
{
    /**
     * 获取角色数据权限
     * 
     * @param user 用户
     * @return 角色权限信息
     */
    public Set<String> getRolePermission(SysUser user);

    /**
     * 获取菜单数据权限
     * 
     * @param userId 用户Id
     * @return 菜单权限信息
     */
    public Set<String> getMenuPermission(String userId);

    /**
     * 获取部门权限
     * @param userId 用户id
     * @return 部门id
     */
    List<String> getDeptPermission(String userId);
}
