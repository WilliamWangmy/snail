package com.snail.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.system.api.domain.SysTenant;
import com.snail.system.api.query.SysTenantQuery;
import com.snail.workflow.api.common.WorkflowBasicService;
import com.snail.common.excel.listener.IExcelService;

import java.util.List;

/**
 * @Description: 租户管理Service接口
 * @Author: snail
 * @CreateDate: 2024-04-30
 * @Version: V1.0
 */
public interface ISysTenantService extends IService<SysTenant>, IExcelService<SysTenant> {

    /**
     * 查询租户管理列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<SysTenant> selectSysTenantPage(SysTenantQuery query);

    /**
     * 根据id获取租户管理详情
     *
     * @param id 主键id
     * @return 结果
     */
    SysTenant getSysTenantById(Long id);

    /**
     * 保存租户管理数据
     *
     * @param sysTenant 租户管理信息
     * @return 结果
     */
    Long insertSysTenant(SysTenant sysTenant);

    /**
     * 更新租户管理数据
     *
     * @param sysTenant 租户管理信息
     * @return 结果
     */
    boolean updateSysTenant(SysTenant sysTenant);

    /**
     * 根据id批量删除租户管理数据
     *
     * @param ids 主键ids
     * @return 结果
     */
    boolean deleteSysTenantByIds(Long[] ids);

    /**
     * 查询租户管理数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<SysTenant> selectSysTenantList(SysTenantQuery query);

    /**
     * 更新租户状态
     *
     * @param sysTenant 租户信息
     * @return 结果
     */
    boolean updateStatus(SysTenant sysTenant);
}
