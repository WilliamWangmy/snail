package com.snail.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.system.api.domain.SysSequenceRule;
import com.snail.system.api.dto.SequenceRuleDto;
import com.snail.system.api.query.SysSequenceRuleQuery;


/**
 * @Description: 序列号规则业务接口
 * @Author: Snail
 * @CreateDate: 2023/8/21 10:52
 * @Version: V1.0
 */
public interface ISysSequenceRuleService extends IService<SysSequenceRule> {
    /**
     * 生成序列号
     *
     * @param sequenceCode 序列号编码
     * @return 结果
     */
    String generate(String sequenceCode);

    /**
     * 批量生成序列号
     *
     * @param sequenceCode 序列号编码
     * @param generateNum  批量生成数量
     * @return 结果
     */
    boolean generateBatch(String sequenceCode, Long generateNum);

    /**
     * 新增序列号规则
     *
     * @param rule 规则
     * @return 结果
     */
    String insertSequenceRule(SysSequenceRule rule);


    /**
     * 更新序列号规则
     *
     * @param rule 序列号规则
     * @return 结果
     */
    boolean updateSequenceRule(SysSequenceRule rule);

    /**
     * 查询序列号规则列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<SysSequenceRule> selectSysSequenceRulePage(SysSequenceRuleQuery query);

    /**
     * 根据id获取序列号规则详情
     *
     * @param ruleId 主键id
     * @return 结果
     */
    SysSequenceRule getSysSequenceRuleById(String ruleId);

    /**
     * 根据id批量删除序列号规则数据
     *
     * @param ruleIds 主键ids
     * @return 结果
     */
    boolean deleteSysSequenceRuleByIds(String[] ruleIds);

    /**
     * 创建子序列号规则并返回第一个子序列号
     *
     * @param sequenceRuleDto 序列号信息
     * @return 序列号
     */
    String createChildrenSequenceRule(SequenceRuleDto sequenceRuleDto);
}
