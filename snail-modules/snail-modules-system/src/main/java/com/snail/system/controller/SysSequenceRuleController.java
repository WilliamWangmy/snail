package com.snail.system.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.InnerAuth;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.common.sequence.utils.SequenceUtils;
import com.snail.system.api.domain.SysSequenceRule;
import com.snail.system.api.dto.SequenceRuleDto;
import com.snail.system.api.query.SysSequenceRuleQuery;
import com.snail.system.service.ISysSequenceRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 序列Controller
 * @Author: Snail
 * @CreateDate: 2023/8/21 10:02
 * @Version: V1.0
 */
@Api(value = "序列管理", tags = "序列管理")
@RestController
@RequestMapping("/sequence")
public class SysSequenceRuleController {

    @Autowired
    private ISysSequenceRuleService sysSequenceRuleService;
    @Autowired
    private SequenceUtils sequenceUtils;

    @Log(title = "查询序列号规则分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询序列号规则列表")
//    @RequiresPermissions("system:sequence:query")
    @GetMapping("/page")
    public R<PageResult<SysSequenceRule>> getSysSequenceRulePage(SysSequenceRuleQuery query) {
        return R.ok(sysSequenceRuleService.selectSysSequenceRulePage(query));
    }

    @Log(title = "获取序列号规则详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取序列号规则详细信息")
    @RequiresPermissions("system:sequence:query")
    @GetMapping(value = "/{ruleId}")
    public R<SysSequenceRule> getSysSequenceRuleById(@PathVariable("ruleId") String ruleId) {
        return R.ok(sysSequenceRuleService.getSysSequenceRuleById(ruleId));
    }

    @ApiOperation(value = "新增序列号规则")
    @RequiresPermissions("system:sequence:add")
    @Log(title = "新增序列号规则", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveSysSequenceRule(@RequestBody SysSequenceRule sysSequenceRule) {
        return R.ok(sysSequenceRuleService.insertSequenceRule(sysSequenceRule));
    }

    @ApiOperation(value = "修改序列号规则")
    @RequiresPermissions("system:sequence:edit")
    @Log(title = "修改序列号规则", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateSysSequenceRule(@RequestBody SysSequenceRule sysSequenceRule) {
        return R.ok(sysSequenceRuleService.updateSequenceRule(sysSequenceRule));
    }


    @ApiOperation(value = "删除序列号规则")
    @RequiresPermissions("system:sequence:delete")
    @Log(title = "删除序列号规则", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ruleIds}")
    public R<Boolean> deleteSysSequenceRuleByIds(@PathVariable String[] ruleIds) {
        return R.ok(sysSequenceRuleService.deleteSysSequenceRuleByIds(ruleIds));
    }

    @InnerAuth
    @ApiOperation(value = "根据规则编号生成子规则")
    @PostMapping("/children")
    public R<String> createChildrenSequenceRule(@RequestBody SequenceRuleDto sequenceRuleDto) {
        return R.ok(sysSequenceRuleService.createChildrenSequenceRule(sequenceRuleDto));
    }

    @InnerAuth
    @ApiOperation("生成序列")
    @GetMapping("/generate")
    public String generateSequence(String sequenceCode) {
        return sysSequenceRuleService.generate(sequenceCode);
    }

    @InnerAuth
    @ApiOperation("批量生成序列")
    @GetMapping("/generateBatch")
    public Boolean generateBatchSequence(@RequestParam("sequenceCode") String sequenceCode, @RequestParam("generateNum") Long generateNum) {
        return sysSequenceRuleService.generateBatch(sequenceCode, generateNum);
    }


}
