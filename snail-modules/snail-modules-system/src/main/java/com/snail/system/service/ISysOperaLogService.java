package com.snail.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.system.api.domain.SysOperaLog;
import com.snail.system.api.query.SysOperaLogQuery;

/**
 * 操作日志 服务层
 *
 * @author snail
 */
public interface ISysOperaLogService extends IService<SysOperaLog> {
    /**
     * 新增操作日志
     *
     * @param operaLog 操作日志对象
     * @return 结果
     */
    int insertOperaLog(SysOperaLog operaLog);

    /**
     * 查询系统操作日志集合
     *
     * @param query 操作日志对象
     * @return 操作日志集合
     */
    PageResult<SysOperaLog> selectOperaLogPage(SysOperaLogQuery query);

    /**
     * 批量删除系统操作日志
     *
     * @param operaIds 需要删除的操作日志ID
     * @return 结果
     */
    int deleteOperaLogByIds(String[] operaIds);

    /**
     * 查询操作日志详细
     *
     * @param operaId 操作ID
     * @return 操作日志对象
     */
    SysOperaLog selectOperaLogById(String operaId);

    /**
     * 清空操作日志
     */
    void cleanOperaLog();
}
