package com.snail.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.system.api.domain.SysOperaLog;
import com.snail.system.api.query.SysOperaLogQuery;
import com.snail.system.mapper.SysOperaLogMapper;
import com.snail.system.service.ISysOperaLogService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * 操作日志 服务层处理
 *
 * @author snail
 */
@Service
public class SysOperaLogServiceImpl extends ServiceImpl<SysOperaLogMapper, SysOperaLog> implements ISysOperaLogService {

    /**
     * 新增操作日志
     *
     * @param operaLog 操作日志对象
     * @return 结果
     */
    @Override
    public int insertOperaLog(SysOperaLog operaLog) {
        operaLog.setOperTime(LocalDateTime.now());
        return baseMapper.insert(operaLog);
    }

    /**
     * 查询系统操作日志集合
     *
     * @param query 操作日志对象
     * @return 操作日志集合
     */
    @Override
    public PageResult<SysOperaLog> selectOperaLogPage(SysOperaLogQuery query) {
        Page<SysOperaLog> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<SysOperaLog> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(query.getTitle()), SysOperaLog::getTitle, query.getTitle());
        queryWrapper.like(StringUtils.isNotEmpty(query.getOperName()), SysOperaLog::getOperName, query.getOperName());
        queryWrapper.eq(query.getStatus() != null, SysOperaLog::getStatus, query.getStatus());
        queryWrapper.eq(query.getBusinessType() != null, SysOperaLog::getBusinessType, query.getBusinessType());
        queryWrapper.in(query.getBusinessTypes() != null, SysOperaLog::getBusinessTypes, query.getBusinessTypes());
        queryWrapper.orderByDesc(SysOperaLog::getOperTime);
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    /**
     * 批量删除系统操作日志
     *
     * @param operaIds 需要删除的操作日志ID
     * @return 结果
     */
    @Override
    public int deleteOperaLogByIds(String[] operaIds) {
        return baseMapper.deleteBatchIds(Arrays.asList(operaIds));
    }

    /**
     * 查询操作日志详细
     *
     * @param operaId 操作ID
     * @return 操作日志对象
     */
    @Override
    public SysOperaLog selectOperaLogById(String operaId) {
        return baseMapper.selectById(operaId);
    }

    /**
     * 清空操作日志
     */
    @Override
    public void cleanOperaLog() {
        baseMapper.cleanOperLog();
    }
}
