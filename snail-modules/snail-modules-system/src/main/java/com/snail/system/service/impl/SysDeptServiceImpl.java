package com.snail.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.constant.CommonConstants;
import com.snail.common.core.constant.UserConstants;
import com.snail.common.core.exception.ServiceException;
import com.snail.common.core.text.Convert;
import com.snail.common.core.utils.SpringUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.security.utils.SecurityUtils;
import com.snail.system.api.constants.DeptConstants;
import com.snail.system.api.domain.SysDept;
import com.snail.system.api.domain.SysRole;
import com.snail.system.api.dto.SysDeptDto;
import com.snail.system.api.query.SysDeptQuery;
import com.snail.system.api.vo.TreeSelect;
import com.snail.system.mapper.SysDeptMapper;
import com.snail.system.mapper.SysRoleMapper;
import com.snail.system.service.ISysDeptService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理 服务实现
 *
 * @author snail
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 查询部门管理数据
     *
     * @param query 部门信息
     * @return 部门信息集合
     */
    @Override
    public List<SysDept> selectDeptList(SysDeptQuery query) {
        return baseMapper.selectDeptList(query);
    }

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    @Override
    public List<TreeSelect> selectDeptTreeList(SysDeptQuery dept) {
        return buildDeptTreeSelect(selectDeptDtoList(dept));
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    @Override
    public List<SysDeptDto> buildDeptTree(List<SysDeptDto> depts) {
        List<SysDeptDto> returnList = new ArrayList<>();
        List<String> tempList = depts.stream().map(SysDept::getDeptId).collect(Collectors.toList());
        for (SysDeptDto dept : depts) {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId())) {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty()) {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<SysDeptDto> depts) {
        List<SysDeptDto> deptTrees = buildDeptTree(depts);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<String> selectDeptListByRoleId(String roleId) {
        SysRole role = sysRoleMapper.selectById(roleId);
        return baseMapper.selectDeptListByRoleId(roleId, role.isDeptCheckStrictly());
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Override
    public SysDept selectDeptById(String deptId) {
        return baseMapper.selectById(deptId);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    @Override
    public int selectNormalChildrenDeptById(String deptId) {
        return baseMapper.selectNormalChildrenDeptById(deptId);
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public boolean hasChildByDeptId(String deptId) {
        int result = baseMapper.hasChildByDeptId(deptId);
        return result > 0;
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(String deptId) {
        int result = baseMapper.checkDeptExistUser(deptId);
        return result > 0;
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public boolean checkDeptNameUnique(SysDept dept) {
        SysDept info = baseMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && !info.getDeptId().equals(dept.getDeptId())) {
            return !UserConstants.NOT_UNIQUE;
        }
        return !UserConstants.UNIQUE;
    }

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    @Override
    public void checkDeptDataScope(String deptId) {
        if (!SecurityUtils.isAdminUser()) {
            SysDeptQuery dept = new SysDeptQuery();
            dept.setDeptId(deptId);
            List<SysDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
            if (StringUtils.isEmpty(depts)) {
                throw new ServiceException("没有权限访问部门数据！");
            }
        }
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDept dept) {
        if (this.checkDeptNameUnique(dept)) {
            throw new ServiceException("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        SysDept info = baseMapper.selectById(dept.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
            throw new ServiceException("部门停用，不允许新增");
        }
        dept.setDeptId(IdWorker.get32UUID());
        buildFullDept(dept);
        return baseMapper.insert(dept);
    }

    /**
     * 封装组织全路径
     *
     * @param dept
     */
    private void buildFullDept(SysDept dept) {
        //是否根组织
        if (DeptConstants.ROOT_ID.equals(dept.getDeptId())) {
            dept.setDeptFullName(dept.getDeptName());
            dept.setDeptFullId(dept.getDeptId().toString());
            return;
        }
        //获取父组织
        SysDept parent = this.getById(dept.getParentId());
        dept.setDeptFullId(parent.getDeptFullId() + DeptConstants.SPLIT_ID + dept.getDeptId());
        dept.setDeptFullName(parent.getDeptFullName() + DeptConstants.SPLIT_NAME + dept.getDeptName());
        dept.setAncestors(parent.getAncestors() + "," + dept.getParentId());
        //获取当前组织下de所有组织
        LambdaQueryWrapper<SysDept> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(SysDept::getDeptFullId, dept.getDeptId());
        List<SysDept> sysDepts = baseMapper.selectList(queryWrapper);
        //更新子组织的全路径名称
        List<SysDept> sysDeptList = buildDeptFullName(dept, sysDepts);
        this.updateBatchById(sysDeptList);
    }

    /**
     * 封装组织全路径
     *
     * @param dept
     * @param sysDepts
     */
    private List<SysDept> buildDeptFullName(SysDept dept, List<SysDept> sysDepts) {
        return sysDepts.stream()
                .filter(item -> dept.getDeptId().equals(item.getParentId()))
                .map(item -> {
                    item.setDeptFullName(dept.getDeptFullName() + DeptConstants.SPLIT_NAME + item.getDeptName());
                    buildDeptFullName(item, sysDepts);
                    return item;
                }).collect(Collectors.toList());
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int updateDept(SysDept dept) {
        //数据校验
        String deptId = dept.getDeptId();
        this.checkDeptDataScope(deptId);
        if (this.checkDeptNameUnique(dept)) {
            throw new ServiceException("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        } else if (dept.getParentId().equals(deptId)) {
            throw new ServiceException("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
        } else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus()) && this.selectNormalChildrenDeptById(deptId) > 0) {
            throw new ServiceException("该部门包含未停用的子部门！");
        }
        SysDept newParentDept = baseMapper.selectById(dept.getParentId());
        SysDept oldDept = baseMapper.selectById(dept.getDeptId());
        if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept)) {
            String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
        }
        //封装组织全路径信息
        buildFullDept(dept);
        int result = baseMapper.updateById(dept);
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && StringUtils.isNotEmpty(dept.getAncestors())
                && !StringUtils.equals("0", dept.getAncestors())) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatusNormal(dept);
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private void updateParentDeptStatusNormal(SysDept dept) {
        String ancestors = dept.getAncestors();
        String[] deptIds = Convert.toStrArray(ancestors);
        baseMapper.updateDeptStatusNormal(deptIds);
    }

    /**
     * 修改子元素关系
     *
     * @param deptId       被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(String deptId, String newAncestors, String oldAncestors) {
        List<SysDept> children = baseMapper.selectChildrenDeptById(deptId);
        for (SysDept child : children) {
            child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
        }
        if (children.size() > 0) {
            baseMapper.updateDeptChildren(children);
        }
    }

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(String deptId) {
        if (this.hasChildByDeptId(deptId)) {
            throw new ServiceException("存在下级部门,不允许删除");
        }
        if (this.checkDeptExistUser(deptId)) {
            throw new ServiceException("部门存在用户,不允许删除");
        }
        this.checkDeptDataScope(deptId);
        return baseMapper.deleteById(deptId);
    }

    @Override
    public List<SysDeptDto> selectDeptDtoList(SysDeptQuery query) {
        return baseMapper.selectDeptDtoList(query);
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<SysDeptDto> list, SysDeptDto t) {
        // 得到子节点列表
        List<SysDeptDto> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDeptDto tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDeptDto> getChildList(List<SysDeptDto> list, SysDeptDto t) {
        List<SysDeptDto> tlist = new ArrayList<>();
        Iterator<SysDeptDto> it = list.iterator();
        while (it.hasNext()) {
            SysDeptDto n = it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().equals(t.getDeptId())) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDeptDto> list, SysDeptDto t) {
        return getChildList(list, t).size() > 0;
    }

    @Override
    public String checkExcelData(SysDeptDto sysDeptDto) {
        StringBuilder error = new StringBuilder();
        //1.判断编码是否存在
        String deptCode = sysDeptDto.getDeptCode();
        if (StringUtils.isBlank(deptCode)) {
            error.append("部门编码为空!");
        } else {
            SysDept sysDept = selectDeptByCode(deptCode);
            if (sysDept != null) {
                error.append("部门编码").append(deptCode).append("已经存在！");
            }
        }
        //2.判断名称是否为空
        if (StringUtils.isBlank(sysDeptDto.getDeptName())) {
            error.append("部门名称为空！");
        } else {
            if (checkDeptNameUnique(sysDeptDto)) {
                error.append("部门名称[").append(sysDeptDto.getDeptName()).append("已经存在!");
            }
        }
        //3.父部门编码
        String parentCode = sysDeptDto.getParentCode();
        if (StringUtils.isBlank(parentCode)) {
            error.append("父部门编码为空！");
        } else {
            SysDept sysDept = selectDeptByCode(parentCode);
            if (sysDept == null) {
                error.append("部门编码").append(parentCode).append("不存存在！");
            } else {
                // 如果父节点不为正常状态,则不允许新增子节点
                if (!UserConstants.DEPT_NORMAL.equals(sysDept.getStatus())) {
                    error.append("父部门停用，不允许新增！");
                }
                sysDeptDto.setParentId(sysDept.getDeptId());
            }
        }
        return error.toString();
    }

    @Override
    public void saveExcelData(List<SysDeptDto> dataList) {
        List<SysDept> deptList = dataList.stream().map(item -> {
            SysDept sysDept = new SysDept();
            BeanUtils.copyProperties(item, sysDept);
            sysDept.setDeptId(IdWorker.get32UUID());
            sysDept.setStatus(CommonConstants.STATUS_YES);
            buildFullDept(sysDept);
            return sysDept;
        }).collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(deptList)) {
            saveBatch(deptList);
        }
    }


    /**
     * 根据部门编码获取部门信息
     *
     * @param deptCode 部门编码
     * @return 结果
     */
    @Override
    public SysDept selectDeptByCode(String deptCode) {
        LambdaQueryWrapper<SysDept> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysDept::getDeptCode, deptCode);
        return baseMapper.selectOne(queryWrapper);
    }
}
