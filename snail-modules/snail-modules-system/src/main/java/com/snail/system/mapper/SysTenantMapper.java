package com.snail.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysTenant;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 租户管理Mapper接口
 * @Author: snail
 * @CreateDate: 2024-04-30
 * @Version: V1.0
 */
@Mapper
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
