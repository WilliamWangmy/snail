package com.snail.system.service.impl;

import com.snail.common.core.constant.UserConstants;
import com.snail.common.security.utils.SecurityUtils;
import com.snail.system.api.domain.SysRole;
import com.snail.system.api.domain.SysUser;
import com.snail.system.mapper.SysRoleDeptMapper;
import com.snail.system.service.ISysMenuService;
import com.snail.system.service.ISysPermissionService;
import com.snail.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户权限处理
 *
 * @author snail
 */
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {
    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysRoleDeptMapper sysRoleDeptMapper;

    /**
     * 获取角色数据权限
     *
     * @param user 用户Id
     * @return 角色权限信息
     */
    @Override
    public Set<String> getRolePermission(SysUser user) {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (SecurityUtils.isAdminUser(user.getUserId())) {
            roles.add("admin");
        } else {
            roles.addAll(roleService.selectRolePermissionByUserId(user.getUserId()));
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     *
     * @param userId 用户
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getMenuPermission(String userId) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (SecurityUtils.isAdminUser(userId)) {
            perms.add("*:*:*");
        } else {
            List<SysRole> roles = roleService.selectRolesByUserId(userId);
            if (!CollectionUtils.isEmpty(roles)) {
                // 多角色设置permissions属性，以便数据权限匹配权限
                for (SysRole role : roles) {
                    Set<String> rolePerms = menuService.selectMenuPermsByRoleId(role.getRoleId());
                    role.setPermissions(rolePerms);
                    perms.addAll(rolePerms);
                }
            } else {
                perms.addAll(menuService.selectMenuPermsByUserId(userId));
            }
        }
        return perms;
    }

    @Override
    public List<String> getDeptPermission(String userId) {
        return sysRoleDeptMapper.getDeptPermission(userId);
    }
}
