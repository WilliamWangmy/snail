package com.snail.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.excel.listener.IExcelService;
import com.snail.system.api.domain.SysDept;
import com.snail.system.api.dto.SysDeptDto;
import com.snail.system.api.query.SysDeptQuery;
import com.snail.system.api.vo.TreeSelect;

import java.util.List;

/**
 * 部门管理 服务层
 *
 * @author snail
 */
public interface ISysDeptService extends IService<SysDept>, IExcelService<SysDeptDto> {
    /**
     * 查询部门管理数据
     *
     * @param query 部门信息
     * @return 部门信息集合
     */
    List<SysDept> selectDeptList(SysDeptQuery query);

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    List<TreeSelect> selectDeptTreeList(SysDeptQuery dept);

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    List<SysDeptDto> buildDeptTree(List<SysDeptDto> depts);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    List<TreeSelect> buildDeptTreeSelect(List<SysDeptDto> depts);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    List<String> selectDeptListByRoleId(String roleId);

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    SysDept selectDeptById(String deptId);

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    int selectNormalChildrenDeptById(String deptId);

    /**
     * 是否存在部门子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    boolean hasChildByDeptId(String deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    boolean checkDeptExistUser(String deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    boolean checkDeptNameUnique(SysDept dept);

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    void checkDeptDataScope(String deptId);

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    int insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    int updateDept(SysDept dept);

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    int deleteDeptById(String deptId);

    /**
     * 获取部门dto数据
     *
     * @param query 查询参数
     * @return 结果
     */
    List<SysDeptDto> selectDeptDtoList(SysDeptQuery query);

    /**
     * 根据部门编码获取部门信息
     *
     * @param deptCode 部门编码
     * @return 结果
     */
    SysDept selectDeptByCode(String deptCode);
}
