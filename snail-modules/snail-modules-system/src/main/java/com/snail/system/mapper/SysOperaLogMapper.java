package com.snail.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysOperaLog;
import com.snail.system.api.query.SysOperaLogQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 操作日志 数据层
 *
 * @author snail
 */
@Mapper
public interface SysOperaLogMapper extends BaseMapper<SysOperaLog> {

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    List<SysOperaLog> selectOperLogList(@Param("operLog") SysOperaLogQuery operLog);

    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
