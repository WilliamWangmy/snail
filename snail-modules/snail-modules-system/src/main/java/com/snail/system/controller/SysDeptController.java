package com.snail.system.controller;

import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.controller.BaseController;
import com.snail.common.core.web.domain.AjaxResult;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.system.api.domain.SysDept;
import com.snail.system.api.dto.SysDeptDto;
import com.snail.system.api.query.SysDeptQuery;
import com.snail.system.service.ISysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 部门信息
 *
 * @author snail
 */
@Api(value = "部门信息", tags = "部门信息")
@RestController
@RequestMapping("/dept")
public class SysDeptController extends BaseController {
    @Autowired
    private ISysDeptService deptService;

    /**
     * 获取部门列表
     */
    @ApiOperation(value = "获取部门列表")
    @RequiresPermissions("system:dept:list")
    @GetMapping("/list")
    public AjaxResult list(SysDeptQuery query) {
        return success(deptService.selectDeptList(query));
    }

    /**
     * 查询部门列表（排除节点）
     */
    @ApiOperation(value = "查询部门列表（排除节点）")
    @RequiresPermissions("system:dept:list")
    @GetMapping("/list/exclude/{deptId}")
    public AjaxResult excludeChild(@PathVariable(value = "deptId", required = false) String deptId) {
        List<SysDept> depts = deptService.selectDeptList(new SysDeptQuery());
        depts.removeIf(d -> d.getDeptId().equals(deptId) || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""));
        return success(depts);
    }

    /**
     * 根据部门编号获取详细信息
     */
    @ApiOperation(value = "根据部门编号获取详细信息")
    @RequiresPermissions("system:dept:query")
    @GetMapping(value = "/{deptId}")
    public AjaxResult getInfo(@PathVariable String deptId) {
        deptService.checkDeptDataScope(deptId);
        return success(deptService.selectDeptById(deptId));
    }

    /**
     * 获取部门下拉树列表
     */
    @ApiOperation(value = "获取部门下拉树列表")
    @GetMapping("/treeselect")
    public AjaxResult treeselect(SysDeptQuery dept) {
        List<SysDeptDto> depts = deptService.selectDeptDtoList(dept);
        return AjaxResult.success(deptService.buildDeptTreeSelect(depts));
    }

    /**
     * 加载对应角色部门列表树
     */
    @ApiOperation(value = "加载对应角色部门列表树")
    @GetMapping(value = "/roleDeptTreeselect/{roleId}")
    public AjaxResult roleDeptTreeselect(@PathVariable("roleId") String roleId) {
        List<SysDeptDto> depts = deptService.selectDeptDtoList(new SysDeptQuery());
        AjaxResult ajax = AjaxResult.success();
        ajax.put("checkedKeys", deptService.selectDeptListByRoleId(roleId));
        ajax.put("depts", deptService.buildDeptTreeSelect(depts));
        return ajax;
    }

    /**
     * 新增部门
     */
    @ApiOperation(value = "新增部门")
    @RequiresPermissions("system:dept:add")
    @Log(title = "部门管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysDept dept) {
        return toAjax(deptService.insertDept(dept));
    }

    /**
     * 修改部门
     */
    @ApiOperation(value = "修改部门")
    @RequiresPermissions("system:dept:edit")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysDept dept) {
        return toAjax(deptService.updateDept(dept));
    }

    /**
     * 删除部门
     */
    @ApiOperation(value = "删除部门")
    @RequiresPermissions("system:dept:remove")
    @Log(title = "部门管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{deptId}")
    public AjaxResult remove(@PathVariable String deptId) {
        return toAjax(deptService.deleteDeptById(deptId));
    }


    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("system:dept:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "部门信息导入模板",SysDeptDto.class);
    }

    @ApiOperation(value = "导出数据")
    @Log(title = "部门管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:dept:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response,SysDeptQuery query) {
        EasyExcelUtils.writeExcel(response, "部门信息",SysDeptDto.class,deptService.selectDeptDtoList(query));
    }

    @ApiOperation(value = "导入数据")
    @Log(title = "部门管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:dept:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, SysDeptDto.class,deptService);
    }

}
