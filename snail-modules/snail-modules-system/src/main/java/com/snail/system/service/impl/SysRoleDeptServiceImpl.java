package com.snail.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.system.api.domain.SysRoleDept;
import com.snail.system.mapper.SysRoleDeptMapper;
import com.snail.system.service.ISysRoleDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

/**
 * @Description: 角色部门权限
 * @Author: Snail
 * @CreateDate: 2023/8/10 17:02
 * @Version: V1.0
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRoleDeptByRoleId(String[] roleIds) {
        LambdaUpdateWrapper<SysRoleDept> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(SysRoleDept::getRoleId, Arrays.asList(roleIds));
        baseMapper.delete(updateWrapper);
    }
}
