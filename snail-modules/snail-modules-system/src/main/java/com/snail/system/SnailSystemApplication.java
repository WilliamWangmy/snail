package com.snail.system;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import com.snail.common.core.utils.ServerUtil;
import org.springframework.boot.SpringApplication;

/**
 * 系统模块
 *
 * @author snail
 */
@SnailCloudApplication
public class SnailSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SnailSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail系统管理服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _                       _\n" +
                "                   (_)| |                      | |\n" +
                " ___  _ ___   __ _  _ | |      ___  _   _  ___ | |_   ___  _ __ ___\n" +
                "/ __|| '_  \\ / _` || || |     / __|| | | |/ __|| __| / _ \\| '_ ` _ \\\n" +
                "\\__ \\| | | || (_| || || |     \\__ \\| |_| |\\__ \\| |_ |  __/| | | | | |\n" +
                "|___/|_| |_| \\__,_||_||_|     |___/ \\__, ||___/ \\__| \\___||_| |_| |_|\n" +
                "                                     __/ |\n" +
                "                                    |___/\n" +
                ServerUtil.printInfo());
    }
}
