package com.snail.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.system.api.domain.SysRoleDept;

/**
 * @Description: 角色部门权限
 * @Author: Snail
 * @CreateDate: 2023/8/10 17:02
 * @Version: V1.0
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {
    /**
     * 根据角色id删除角色部门权限
     *
     * @param roleIds 角色id
     */
    void deleteRoleDeptByRoleId(String[] roleIds);
}
