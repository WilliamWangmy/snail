package com.snail.system.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.web.controller.BaseController;
import com.snail.common.core.web.domain.AjaxResult;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.InnerAuth;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.system.api.domain.SysOperaLog;
import com.snail.system.api.query.SysOperaLogQuery;
import com.snail.system.service.ISysOperaLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 操作日志记录
 *
 * @author snail
 */
@Api(value = "操作日志记录", tags = "操作日志记录")
@RestController
@RequestMapping("/operlog")
public class SysOperlogController extends BaseController {
    @Autowired
    private ISysOperaLogService operLogService;

    @ApiOperation(value = "分页查询操作日志")
    @RequiresPermissions("system:operlog:list")
    @GetMapping("/list")
    public R<PageResult<SysOperaLog>> list(SysOperaLogQuery query) {
        return R.ok(operLogService.selectOperaLogPage(query));
    }

    @ApiOperation(value = "导出操作日志")
    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:operlog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysOperaLogQuery operLog) {
//        List<SysOperaLog> list = operLogService.selectOperLogList(operLog);
//        ExcelUtil<SysOperaLog> util = new ExcelUtil<SysOperaLog>(SysOperaLog.class);
//        util.exportExcel(response, list, "操作日志");
    }

    @ApiOperation(value = "删除操作日志")
    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:operlog:remove")
    @DeleteMapping("/{operIds}")
    public AjaxResult remove(@PathVariable String[] operIds) {
        return toAjax(operLogService.deleteOperaLogByIds(operIds));
    }

    @ApiOperation(value = "清空操作日志")
    @RequiresPermissions("system:operlog:remove")
    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public AjaxResult clean() {
        operLogService.cleanOperaLog();
        return success();
    }

    @InnerAuth
    @PostMapping
    public AjaxResult add(@RequestBody SysOperaLog operLog) {
        return toAjax(operLogService.insertOperaLog(operLog));
    }
}
