package com.snail.system.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.system.api.domain.SysTenant;
import com.snail.system.api.query.SysTenantQuery;
import com.snail.system.service.ISysTenantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 租户管理Controller
 * @Author: snail
 * @CreateDate: 2024-04-30
 * @Version: V1.0
 */
@Api(value = "租户管理", tags = "租户管理")
@RestController
@RequestMapping("/tenant")
public class SysTenantController {
    @Autowired
    private ISysTenantService sysTenantService;

    @Log(title = "查询租户管理分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询租户管理列表")
    @RequiresPermissions("system:tenant:query")
    @GetMapping("/page")
    public R<PageResult<SysTenant>> getSysTenantPage(SysTenantQuery query) {
        return R.ok(sysTenantService.selectSysTenantPage(query));
    }

    @Log(title = "获取租户管理详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取租户管理详细信息")
    @RequiresPermissions("system:tenant:query")
    @GetMapping(value = "/{id}")
    public R<SysTenant> getSysTenantById(@PathVariable("id") Long id) {
        return R.ok(sysTenantService.getSysTenantById(id));
    }

    @ApiOperation(value = "新增租户管理")
    @RequiresPermissions("system:tenant:add")
    @Log(title = "新增租户管理", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<Long> saveSysTenant(@Validated @RequestBody SysTenant sysTenant) {
        return R.ok(sysTenantService.insertSysTenant(sysTenant));
    }

    @ApiOperation(value = "修改租户管理")
    @RequiresPermissions("system:tenant:edit")
    @Log(title = "修改租户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateSysTenant(@Validated @RequestBody SysTenant sysTenant) {
        return R.ok(sysTenantService.updateSysTenant(sysTenant));
    }


    @ApiOperation(value = "更新租户状态")
    @RequiresPermissions("system:tenant:edit")
    @Log(title = "更新租户状态", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    public R<Boolean> updateStatus(@RequestBody SysTenant sysTenant) {
        return R.ok(sysTenantService.updateStatus(sysTenant));
    }


    @ApiOperation(value = "删除租户管理")
    @RequiresPermissions("system:tenant:delete")
    @Log(title = "删除租户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public R<Boolean> deleteSysTenantByIds(@PathVariable Long[] ids) {
        return R.ok(sysTenantService.deleteSysTenantByIds(ids));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("system:tenant:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "租户管理导入模板", SysTenant.class);
    }

    @ApiOperation(value = "导出租户管理数据")
    @Log(title = "导出租户管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:tenant:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, SysTenantQuery query) {
        EasyExcelUtils.writeExcel(response, "租户管理信息",SysTenant.class,sysTenantService.selectSysTenantList(query));
    }

    @ApiOperation(value = "导入租户管理数据")
    @Log(title = "导入租户管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:tenant:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, SysTenant.class,sysTenantService);
    }

}
