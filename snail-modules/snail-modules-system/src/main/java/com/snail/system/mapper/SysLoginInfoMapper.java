package com.snail.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysLoginInfo;
import com.snail.system.api.query.SysLoginInfoQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author snail
 */
@Mapper
public interface SysLoginInfoMapper extends BaseMapper<SysLoginInfo> {

    /**
     * 查询系统登录日志集合
     *
     * @param loginInfo 访问日志对象
     * @return 登录记录集合
     */
    List<SysLoginInfo> selectLoginInfoList(@Param("loginInfo") SysLoginInfoQuery loginInfo);

    /**
     * 清空系统登录日志
     *
     * @return 结果
     */
    int cleanLoginInfo();
}
