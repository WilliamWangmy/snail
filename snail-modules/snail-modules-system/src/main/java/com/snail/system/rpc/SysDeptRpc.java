package com.snail.system.rpc;

import com.snail.common.core.domain.R;
import com.snail.common.security.annotation.InnerAuth;
import com.snail.system.service.ISysDeptService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 部门信息RPC
 * @Author: Snail
 * @CreateDate: 2023/8/7 10:52
 * @Version: V1.0
 */
@Api(value = "部门信息RPC", tags = "部门信息RPC")
@RestController
@RequestMapping("/dept/rpc")
public class SysDeptRpc {

    @Autowired
    private ISysDeptService sysDeptService;

    @InnerAuth
    @GetMapping("/role/{roleId}")
    public R<List<String>> selectDeptRole(@PathVariable("roleId") String roleId){
        return R.ok(sysDeptService.selectDeptListByRoleId(roleId));
    }
}
