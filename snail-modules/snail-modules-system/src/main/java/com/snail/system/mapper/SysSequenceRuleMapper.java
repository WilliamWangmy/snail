package com.snail.system.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysSequenceRule;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 序列号规则Mapper
 * @Author: Snail
 * @CreateDate: 2023/8/21 10:51
 * @Version: V1.0
 */
@Mapper
/**TODO InterceptorIgnore详解**/
@InterceptorIgnore(illegalSql = "true", tenantLine = "true")
public interface SysSequenceRuleMapper extends BaseMapper<SysSequenceRule> {
}
