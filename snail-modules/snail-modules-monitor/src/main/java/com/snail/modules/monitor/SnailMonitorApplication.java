package com.snail.modules.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 监控中心
 * 
 * @author snail
 */
@EnableAdminServer
@SpringBootApplication
public class SnailMonitorApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SnailMonitorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  监控中心启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "                    _  _                                _  _\n" +
                "                   (_)| |                              (_)| |\n" +
                " ___  _ ___   __ _  _ | |      _ __ ___    ___   _ __   _ | |_   ___   _ __\n" +
                "/ __|| '_  \\ / _` || || |     | '_ ` _ \\  / _ \\ | '_ \\ | || __| / _ \\ | '__|\n" +
                "\\__ \\| | | || (_| || || |     | | | | | || (_) || | | || || |_ | (_) || |\n" +
                "|___/|_| |_| \\__,_||_||_|     |_| |_| |_| \\___/ |_| |_||_| \\__| \\___/ |_|"
                );
    }
}
