package com.snail.system.api.factory;

import com.snail.common.core.domain.R;
import com.snail.system.api.RemoteDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户服务降级处理
 *
 * @author snail
 */
@Component
public class RemoteDeptFallbackFactory implements FallbackFactory<RemoteDeptService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDeptFallbackFactory.class);

    @Override
    public RemoteDeptService create(Throwable throwable) {
        log.error("系统管理(部门)服务调用失败:{}", throwable.getMessage());
        return new RemoteDeptService() {
            @Override
            public R<List<String>> selectDeptRole(String roleId, String source) {
                return R.fail("获取角色部门失败:" + throwable.getMessage());
            }
        };
    }
}
