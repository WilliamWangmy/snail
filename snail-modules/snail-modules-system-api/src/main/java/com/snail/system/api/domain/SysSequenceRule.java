package com.snail.system.api.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 序列号规则对象 sys_sequence_rule
 * @Author: Snail
 * @CreateDate: 2023-08-31
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "序列号规则")
@TableName("sys_sequence_rule")
public class SysSequenceRule implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规则id
     */
    @ApiModelProperty(value = "规则id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String ruleId;

    /**
     * 序列号名称
     */
    @ApiModelProperty(value = "序列号名称")
    @Excel(value = "序列号名称")
    private String sequenceName;

    /**
     * 序列号编码
     */
    @ApiModelProperty(value = "序列号编码")
    @Excel(value = "序列号编码")
    private String sequenceCode;

    /**
     * 前缀
     */
    @ApiModelProperty(value = "前缀")
    @Excel(value = "前缀")
    private String prefix;

    /**
     * 后缀
     */
    @ApiModelProperty(value = "后缀")
    @Excel(value = "后缀")
    private String suffix;

    /**
     * 策略:
     * 1.纯数字，前缀+数字生成策略+后缀。eg:SQ0001SQ
     * 2.日期+数字，前缀+日期+数字+后缀。eg:SQ202306060001SQ
     */
    @ApiModelProperty(value = "策略")
    @Excel(value = "策略")
    private String strategy;

    /**
     * 日期格式
     */
    @ApiModelProperty(value = "日期格式")
    private String dateFormat;

    /**
     * 最小值
     */
    @ApiModelProperty(value = "最小值")
    @Excel(value = "最小值")
    private Long minValue;

    /**
     * 最大值
     */
    @ApiModelProperty(value = "最大值")
    @Excel(value = "最大值")
    private Long maxValue;

    /**
     * 当前值
     */
    @ApiModelProperty(value = "当前值")
    @Excel(value = "当前值")
    private Long currentValue;

    /**
     * 每次生成多少个
     */
    @ApiModelProperty(value = "每次生成多少个")
    private Long generateNum;

    /**
     * 是否重置
     */
    @ApiModelProperty(value = "是否重置")
    private String reset;

    /**
     * 补值
     */
    @ApiModelProperty(value = "补值")
    private String repairValue;

    /**
     * 阈值
     */
    @ApiModelProperty(value = "阈值")
    private Long threshold;

    /**
     * 序列号样例
     */
    @ApiModelProperty(value = "序列号样例")
    @Excel(value = "序列号样例")
    private String maxSequence;

    /**
     * 父序列号编码
     */
    @ApiModelProperty(value = "父序列号编码")
    private String parentSequenceCode;


    /**
     * 是否逻辑删除
     */
    @ApiModelProperty(value = "是否逻辑删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建人名称
     */
    @ApiModelProperty(value = "创建人名称")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "创建人名称")
    private String createUserName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新人名称
     */
    @ApiModelProperty(value = "更新人名称")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserName;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人组织id
     */
    @ApiModelProperty(value = "创建人组织id")
    @TableField(fill = FieldFill.INSERT)
    private String deptId;

    /**
     * 创建人组织名称
     */
    @ApiModelProperty(value = "创建人组织名称")
    @TableField(fill = FieldFill.INSERT)
    private String deptName;

    /**
     * 创建人组织全路径id
     */
    @ApiModelProperty(value = "创建人组织全路径id")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullId;

    /**
     * 创建人组织全路径名称
     */
    @ApiModelProperty(value = "创建人组织全路径名称")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "创建人组织全路径名称")
    private String deptFullName;


}
