package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 角色和部门关联 sys_role_dept
 *
 * @author snail
 */
@Data
@ToString
@ApiModel(value = "角色和部门关联")
@TableName("sys_role_dept")
public class SysRoleDept {
    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    private String roleId;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    private String deptId;
}
