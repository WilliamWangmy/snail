package com.snail.system.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.system.api.domain.SysLoginInfo;
import com.snail.system.api.domain.SysOperaLog;
import com.snail.system.api.factory.RemoteLogFallbackFactory;

/**
 * 日志服务
 *
 * @author snail
 */
@FeignClient(contextId = "remoteLogService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteLogService {
    /**
     * 保存系统日志
     *
     * @param sysOperaLog 日志实体
     * @param source     请求来源
     * @return 结果
     */
    @PostMapping("/operlog")
    public R<Boolean> saveLog(@RequestBody SysOperaLog sysOperaLog, @RequestHeader(SecurityConstants.FROM_SOURCE) String source) throws Exception;

    /**
     * 保存访问记录
     *
     * @param sysLoginInfo 访问实体
     * @param source       请求来源
     * @return 结果
     */
    @PostMapping("/loginInfo")
    public R<Boolean> saveLogininfor(@RequestBody SysLoginInfo sysLoginInfo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
