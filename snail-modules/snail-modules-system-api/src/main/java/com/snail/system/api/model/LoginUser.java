package com.snail.system.api.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.snail.system.api.domain.SysDept;
import com.snail.system.api.domain.SysRole;
import com.snail.system.api.domain.SysUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户信息
 *
 * @author snail
 */
@Data
public class LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户唯一标识
     */
    @ApiModelProperty(value = "用户唯一标识")
    private String token;

    /**
     * 用户名id
     */
    @ApiModelProperty(value = "用户名id")
    private String userId;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 登录时间
     */
    @ApiModelProperty(value = "登录时间")
    private Long loginTime;

    /**
     * 过期时间
     */
    @ApiModelProperty(value = "过期时间")
    private Long expireTime;

    /**
     * 登录IP地址
     */
    @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;

    /**
     * 权限列表
     */
    @ApiModelProperty(value = "权限列表")
    private Set<String> permissions;

    /**
     * 角色列表
     */
    @ApiModelProperty(value = "角色列表")
    private Set<String> roles;

    /**
     * 用户信息
     */
    @ApiModelProperty(value = "用户信息")
    private SysUser sysUser;

    /**
     * 部门信息
     */
    @ApiModelProperty(value = "部门信息")
    private SysDept sysDept;
    /**
     * 角色部门权限
     */
    @ApiModelProperty(value = "角色部门权限")
    private List<String> deptPermission;
    /**
     * 角色对象
     */
    @ApiModelProperty(value = "角色对象")
    private List<SysRole> sysRoles;



}
