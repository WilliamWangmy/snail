package com.snail.system.api.factory;

import com.snail.system.api.dto.SysUserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import com.snail.common.core.domain.R;
import com.snail.system.api.RemoteUserService;
import com.snail.system.api.domain.SysUser;

import java.util.List;

/**
 * 用户服务降级处理
 *
 * @author snail
 */
@Component
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteUserFallbackFactory.class);

    @Override
    public RemoteUserService create(Throwable throwable) {
        log.error("用户服务调用失败:{}", throwable.getMessage());
        return new RemoteUserService() {
            @Override
            public R<SysUserDto> getUserByUserName(String username, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }

            @Override
            public R<SysUserDto> getUserByPhone(String phone, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> registerUserInfo(SysUser sysUser, String source) {
                return R.fail("注册用户失败:" + throwable.getMessage());
            }

            @Override
            public R<SysUserDto> getUserById(String userId, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserDto>> getUserList(String userIds, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysUserDto>> getRoleUserInfo(String roleIds, String source) {
                return R.fail("获取用户失败:" + throwable.getMessage());
            }
        };
    }
}
