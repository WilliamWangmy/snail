package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 角色信息查询对象
 * @Author: Snail
 * @CreateDate: 2022/7/7 14:06
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "角色信息查询对象")
public class SysRoleQuery extends BaseQuery {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色序号")
    private String roleId;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    /**
     * 角色权限
     */
    @ApiModelProperty(value = "角色权限")
    private String roleKey;

    /**
     * 角色状态（0正常 1停用）
     */
    @ApiModelProperty(value = "角色状态:0=正常,1=停用")
    private String status;
}
