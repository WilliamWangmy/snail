package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author snail
 */
@Data
@ApiModel(value = "用户和岗位关联")
@TableName("sys_user_post")
public class SysUserPost {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /**
     * 岗位ID
     */
    @ApiModelProperty(value = "岗位ID")
    private String postId;

}
