package com.snail.system.api.dto;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.snail.common.excel.annotation.Excel;
import com.snail.system.api.domain.SysDept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.poi.ss.usermodel.Font;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: Snail
 * @CreateDate: 2023/8/15 11:08
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户dto对象")
public class SysDeptDto extends SysDept {


    @HeadFontStyle(color = Font.COLOR_RED)
    @Excel(value = "父部门编码",index = 2,template = true)
    @ApiModelProperty("父部门编码")
    private String parentCode;

    /**
     * 父部门名称
     */
    @HeadFontStyle(color = Font.COLOR_RED)
    @Excel(value = "父部门名称",index = 3,template = true)
    @ApiModelProperty(value = "父部门名称")
    private String parentName;

    /**
     * 子部门
     */
    @ApiModelProperty(value = "子部门")
    private List<SysDeptDto> children = new ArrayList<>();
}
