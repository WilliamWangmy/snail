package com.snail.system.api.dto;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.snail.common.excel.annotation.Excel;
import com.snail.system.api.domain.SysDept;
import com.snail.system.api.domain.SysRole;
import com.snail.system.api.domain.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.poi.ss.usermodel.Font;

import java.util.List;
import java.util.Set;

/**
 * @Description: 用户dto对象
 * @Author: Snail
 * @CreateDate: 2023/8/3 16:21
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@HeadFontStyle(color = Font.COLOR_RED)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户dto对象")
public class SysUserDto extends SysUser {

    /**
     * 角色组
     */
    @ApiModelProperty(value = "角色组")
    private String[] roleIds;

    /**
     * 岗位组
     */
    @ApiModelProperty(value = "岗位组")
    private String[] postIds;

    /**
     * 角色对象
     */
    @ApiModelProperty(value = "角色对象")
    private List<SysRole> roles;

    /**
     * 部门对象
     */
    @ApiModelProperty(value = "部门对象")
    private SysDept dept;

    /**
     * 权限列表
     */
    @ApiModelProperty(value = "权限列表")
    private Set<String> permissions;
    /**
     * 角色部门权限
     */
    @ApiModelProperty(value = "角色部门权限")
    private List<String> deptPermission;

    /**
     * 组织全路径
     */
    @ApiModelProperty(value = "组织全路径")
    @Excel(value = "组织全路径",index = 8)
    private String deptFullName;

    /**
     * 所属部门编码
     */
    @ApiModelProperty(value = "所属部门编码")
    @Excel(value = "所属部门编码",index = 6,template = true)
    private String deptCode;

    /**
     * 所属部门名称
     */
    @ApiModelProperty(value = "所属部门名称")
    @Excel(value = "所属部门名称",index = 7,template = true)
    private String deptName;

}
