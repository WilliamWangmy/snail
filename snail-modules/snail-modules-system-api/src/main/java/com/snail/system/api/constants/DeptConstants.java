package com.snail.system.api.constants;

/**
 * @Description: 部门常量
 * @Author: Snail
 * @CreateDate: 2023/8/3 16:21
 * @Version: V1.0
 */
public class DeptConstants {

    /**
     * 根组织id
     */
    public static final String ROOT_ID = "100";

    /**
     * 组织名称分隔符
     */
    public static final String SPLIT_NAME = "/";
    /**
     * 组织id分隔符
     */
    public static final String SPLIT_ID= ",";

}
