package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 岗位信息查询对象
 * @Author: Snail
 * @CreateDate: 2022/7/7 14:05
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "岗位信息查询对象")
public class SysPostQuery extends BaseQuery {

    /**
     * 岗位编码
     */
    @ApiModelProperty(value = "岗位编码")
    private String postCode;

    /**
     * 岗位名称
     */
    @ApiModelProperty(value = "岗位名称")
    private String postName;

    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty(value = "状态:0=正常,1=停用")
    private String status;
}
