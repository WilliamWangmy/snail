package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 系统配置查询对象
 * @Author: Snail
 * @CreateDate: 2022/7/7 13:46
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "系统配置查询对象")
public class SysConfigQuery extends BaseQuery {

    /**
     * 参数名称
     */
    @ApiModelProperty(value = "参数名称")
    private String configName;

    /**
     * 参数键名
     */
    @ApiModelProperty(value = "参数键名")
    private String configKey;

    /**
     * 系统内置（Y是 N否）
     */
    @ApiModelProperty(value = "系统内置:Y=是,N=否")
    private String configType;

}
