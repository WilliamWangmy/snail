package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 文件查询参数
 * @Author:Snail
 * @CreateDate: 2023/9/26 15:44
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "文件查询参数")
public class SysFileQuery extends BaseQuery {

    /**
     * 业务标识
     */
    @ApiModelProperty(value = "业务标识")
    private String bizKey;

    /**
     * 文件名称
     */
    @ApiModelProperty(value = "文件名称")
    private String fileName;
}
