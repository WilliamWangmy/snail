package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.core.annotation.Excel;
import com.snail.common.core.annotation.Excel.ColumnType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 岗位表 sys_post
 *
 * @author snail
 */
@Data
@ApiModel(value = "岗位信息")
@TableName("sys_post")
public class SysPost implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 岗位序号
     */
    @ApiModelProperty(value = "岗位序号")
    @TableId(type = IdType.ASSIGN_UUID)
    private String postId;

    /**
     * 岗位编码
     */
    @ApiModelProperty(value = "岗位编码")
    @NotBlank(message = "岗位编码不能为空")
    @Size(min = 0, max = 64, message = "岗位编码长度不能超过64个字符")
    private String postCode;

    /**
     * 岗位名称
     */
    @ApiModelProperty(value = "岗位名称")
    @NotBlank(message = "岗位名称不能为空")
    @Size(min = 0, max = 50, message = "岗位名称长度不能超过50个字符")
    private String postName;

    /**
     * 岗位排序
     */
    @ApiModelProperty(value = "岗位排序")
    @NotBlank(message = "显示顺序不能为空")
    private String postSort;

    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty(value = "状态:0=正常,1=停用")
    private String status;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;


    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 用户是否存在此岗位标识 默认不存在
     */
    @ApiModelProperty(value = "用户是否存在此岗位标识 默认不存在")
    @TableField(exist = false)
    private boolean flag = false;
}
