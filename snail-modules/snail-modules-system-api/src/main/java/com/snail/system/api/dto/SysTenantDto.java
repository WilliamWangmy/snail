package com.snail.system.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 多租户dto对象
 * @Author: Snail
 * @CreateDate: 2024/2/29 21:56
 * @Version: V1.0
 */
@Data
public class SysTenantDto {

    @ApiModelProperty("多租户标识")
    private String tenantId;

    @ApiModelProperty("多租户名称")
    private String tenantName;
}
