package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.core.annotation.Excel;
import com.snail.common.core.annotation.Excel.ColumnType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author snail
 */
@Data
@ApiModel(value = "系统登录日志")
@TableName("sys_login_info")
public class SysLoginInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @Excel(name = "序号", cellType = ColumnType.NUMERIC)
    @ApiModelProperty(value = "序号")
    @TableId(type = IdType.ASSIGN_UUID)
    private String infoId;

    /**
     * 用户账号
     */
    @Excel(name = "用户账号")
    @ApiModelProperty(value = "用户账号")
    private String userName;

    /**
     * 状态 0成功 1失败
     */
    @Excel(name = "状态", readConverterExp = "0=成功,1=失败")
    @ApiModelProperty(value = "状态:0=成功,1=失败")
    private String status;

    /**
     * 地址
     */
    @Excel(name = "地址")
    @ApiModelProperty(value = "地址")
    private String ipaddr;

    /**
     * 描述
     */
    @Excel(name = "描述")
    @ApiModelProperty(value = "描述")
    private String msg;
    /**
     * 浏览器类型
     */
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    /**
     * ip归属地
     */
    @ApiModelProperty(value = "ip归属地")
    private String location;
    /**
     * 客户端系统
     */
    @ApiModelProperty(value = "客户端系统")
    private String systemOs;
    /**
     * 访问时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "访问时间")
    private LocalDateTime accessTime;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    private String tenantId;

}