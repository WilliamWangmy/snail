package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 序列号规则查询对象
 * @Author: Snail
 * @CreateDate: 2023-08-31
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "序列号规则")
public class SysSequenceRuleQuery extends BaseQuery {
    @ApiModelProperty(value = "序列号名称")
    private String sequenceName;
    @ApiModelProperty(value = "序列号编码")
    private String sequenceCode;
}
