package com.snail.system.api;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.system.api.factory.RemoteDeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * 部门
 *
 * @author snail
 */
@FeignClient(contextId = "remoteDeptService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteDeptService {

    /**
     * 批量获取角色部门信息
     *
     * @param roleId 角色id
     * @return 结果
     */
    @GetMapping("/dept/rpc/role/{roleId}")
    R<List<String>> selectDeptRole(@PathVariable("roleId") String roleId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
