package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.core.xss.Xss;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.converter.UserStatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户对象 sys_user
 *
 * @author snail
 */
@Data
@NoArgsConstructor
@ApiModel(value = "用户信息")
@TableName("sys_user")
public class SysUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户序号")
    @TableId(type = IdType.ASSIGN_UUID)
    private String userId;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门编号")
    @NotBlank(message = "用户部门不能为空")
    private String deptId;

    /**
     * 用户账号
     */
    @Excel(value = "用户账号",index = 1,template = true)
    @ApiModelProperty(value = "登录名称")
    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    private String userName;

    /**
     * 用户编码
     */
    @Excel(value = "用户编码",index = 0,template = true)
    @ApiModelProperty(value = "用户编码")
    @NotBlank(message = "用户编码不能为空")
    private String userCode;

    /**
     * 用户昵称
     */
    @Excel(value = "用户名称",index = 2,template = true)
    @ApiModelProperty(value = "用户名称")
    @Xss(message = "用户昵称不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    private String nickName;

    /**
     * 用户邮箱
     */
    @Excel(value = "用户邮箱",index = 5,template = true)
    @ApiModelProperty(value = "用户邮箱")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
//    @Desensitization(type = DesensitizationEnum.EMAIL)
    private String email;

    /**
     * 手机号码
     */
    @Excel(value = "手机号码",index = 4,template = true)
    @ApiModelProperty(value = "手机号码")
    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
//    @Desensitization(type = DesensitizationEnum.PHONE)
    private String phoneNumber;

    /**
     * 用户性别
     */
    @Excel(value = "用户性别",index = 3,template = true)
    @ApiModelProperty(value = "用户性别:0=男,1=女,2=未知")
    private String sex;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 密码修改时间
     */
    @ApiModelProperty(value = "密码修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime passwordModifyTime;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Excel(value = "帐号状态",converter = UserStatusConverter.class)
    @ApiModelProperty(value = "帐号状态:0=正常,1=停用")
    private String status;

    /**
     * 租户标识
     */
    @Excel(value = "租户标识",index = 8)
    @ApiModelProperty(value = "租户标识")
    private String tenantId;

    /**
     * 最后登录IP
     */
    @Excel(value = "最后登录IP")
    @ApiModelProperty(value = "最后登录IP")
    private String loginIp;

    /**
     * 最后登录时间
     */
    @Excel(value = "最后登录时间")
    @ApiModelProperty(value = "最后登录时间")
    private LocalDateTime loginDate;

    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @ApiModelProperty(value = "删除标志（0代表存在 1代表删除）")
    @TableLogic
    private String delFlag;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建时间
     */
    @Excel(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新时间
     */
    @Excel(value = "更新时间")
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;


    public SysUser(String userId) {
        this.userId = userId;
    }
}
