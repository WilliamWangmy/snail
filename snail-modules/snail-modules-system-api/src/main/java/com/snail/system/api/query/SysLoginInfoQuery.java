package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description:
 * @Author: Snail
 * @CreateDate: 2022/7/7 14:05
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "登录日志查询对象")
public class SysLoginInfoQuery extends BaseQuery {

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String ipaddr;

    /**
     * 用户账号
     */
    @ApiModelProperty(value = "用户账号")
    private String userName;

    /**
     * 状态 0成功 1失败
     */
    @ApiModelProperty(value = "状态:0=成功,1=失败")
    private String status;
}
