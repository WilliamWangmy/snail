package com.snail.system.api.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.converter.BizStatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 租户管理对象 sys_tenant
 * @Author: snail
 * @CreateDate: 2024-04-30
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "租户管理")
@TableName("sys_tenant")
public class SysTenant implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 租户号
     */
    @ApiModelProperty(value = "租户号")
    @Excel(value = "租户号", index = 0, template = true)
    @NotEmpty(message = "租户号不能为空")
    private String tenantId;

    /**
     * 租户名
     */
    @ApiModelProperty(value = "租户名")
    @Excel(value = "租户名", index = 1, template = true)
    @NotEmpty(message = "租户名不能为空!")
    private String tenantName;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @Excel(value = "状态", index = 2, template = true, converter = BizStatusConverter.class)
    @NotEmpty(message = "状态不能为空!")
    private String status;

    /**
     * 序号
     */
    @ApiModelProperty(value = "排序")
    @Excel(value = "排序", index = 3, template = true)
    @NotNull(message = "排序不能为空")
    private Long sortNum;

    /**
     * 有效期（起）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "有效期（起）")
    @Excel(value = "有效期（起）", index = 4, template = true)
    @NotNull(message = "有效期（起）不能为空")
    private LocalDateTime startTime;

    /**
     * 有效期（止）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "有效期（止）")
    @Excel(value = "有效期（止）", index = 5, template = true)
    @NotNull(message = "有效期（止）不能为空")
    private LocalDateTime endTime;


}
