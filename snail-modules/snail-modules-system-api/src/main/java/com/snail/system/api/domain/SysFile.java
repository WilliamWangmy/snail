package com.snail.system.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 文件信息对象 sys_file
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
@Data
@ApiModel(value = "文件信息")
@TableName("sys_file")
public class SysFile implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 文件id
     */
    @ApiModelProperty(value = "文件id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String fileId;

    /**
     * 业务标识
     */
    @ApiModelProperty(value = "业务标识")
    private String bizKey;

    /**
     * 文件名称
     */
    @ApiModelProperty(value = "文件名称")
    private String fileName;

    /**
     * 文件地址
     */
    @ApiModelProperty(value = "文件地址")
    private String fileUrl;

    /**
     * 文件大小
     */
    @ApiModelProperty(value = "文件大小")
    private BigDecimal fileSize;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;


}
