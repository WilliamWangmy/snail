package com.snail.system.api.domain;

import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import com.alibaba.excel.converters.AutoConverter;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.converter.UserStatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.apache.poi.ss.usermodel.Font;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 部门表 sys_dept
 *
 * @author snail
 */
@Data
@ToString
@ApiModel(value = "部门信息")
@TableName("sys_dept")
public class SysDept implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID")
    @TableId(type = IdType.ASSIGN_UUID)
    private String deptId;

    /**
     * 父部门ID
     */
    @ApiModelProperty(value = "父部门ID")
    private String parentId;

    /**
     * 祖级列表
     */
    @ApiModelProperty(value = "祖级列表")
    private String ancestors;

    /**
     * 部门编码
     */
    @HeadFontStyle(color = Font.COLOR_RED)
    @Excel(value = "部门编码",index = 0,template = true,converter = AutoConverter.class)
    @ApiModelProperty(value = "部门编码")
    @NotBlank(message = "部门编码不能为空")
    private String deptCode;

    /**
     * 部门名称
     */
    @HeadFontStyle(color = Font.COLOR_RED)
    @Excel(value = "部门名称",index = 1,template = true)
    @ApiModelProperty(value = "部门名称")
    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符")
    private String deptName;
    /**
     * 租户标识
     */
    @Excel(value = "租户标识",index = 4,template = true)
    @ApiModelProperty(value = "租户标识")
    private String tenantId;

    /**
     * 组织全路径
     */
    @Excel(value = "组织全路径",index = 5)
    @ApiModelProperty(value = "组织全路径")
    private String deptFullName;
    /**
     * 组织全路径id
     */
    @ApiModelProperty(value = "组织全路径id")
    private String deptFullId;

    /**
     * 显示顺序
     */
    @ApiModelProperty(value = "显示顺序")
    @NotNull(message = "显示顺序不能为空")
    private Integer orderNum;

    /**
     * 负责人
     */
    @Excel(value = "负责人",template = true)
    @ApiModelProperty(value = "负责人")
    private String leader;

    /**
     * 联系电话
     */
    @Excel(value = "联系电话",template = true)
    @ApiModelProperty(value = "联系电话")
    @Size(min = 0, max = 11, message = "联系电话长度不能超过11个字符")
    private String phone;

    /**
     * 邮箱
     */
    @Excel(value = "邮箱",template = true)
    @ApiModelProperty(value = "邮箱")
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    private String email;

    /**
     * 部门状态:0正常,1停用
     */
    @Excel(value = "部门状态",converter = UserStatusConverter.class)
    @ApiModelProperty(value = "部门状态:0正常,1停用")
    private String status;

    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @ApiModelProperty(value = "删除标志（0代表存在 1代表删除）")
    @TableLogic
    private String delFlag;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建时间
     */
    @Excel(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新时间
     */
    @Excel(value = "更新时间")
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

}
