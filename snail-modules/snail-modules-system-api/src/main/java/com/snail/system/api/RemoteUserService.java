package com.snail.system.api;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.system.api.domain.SysUser;
import com.snail.system.api.dto.SysUserDto;
import com.snail.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户服务
 *
 * @author snail
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService {
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source   请求来源
     * @return 结果
     */
    @GetMapping("/user/rpc/info/{username}")
    R<SysUserDto> getUserByUserName(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 通过电话号码查询用户信息
     *
     * @param phone  电话号码
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/user/rpc/info/phone/{phone}")
    R<SysUserDto> getUserByPhone(@PathVariable("phone") String phone, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source  请求来源
     * @return 结果
     */
    @PostMapping("/user/rpc/register")
    R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据用户id获取用户信息
     *
     * @param userId
     * @param source
     * @return
     */
    @GetMapping("/user/rpc/{userId}")
    R<SysUserDto> getUserById(@PathVariable("userId") String userId, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 批量获取用户信息
     *
     * @param userIds
     * @return
     */
    @GetMapping("/user/rpc/select/{userIds}")
    R<List<SysUserDto>> getUserList(@PathVariable("userIds") String userIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 批量获取角色用户信息
     *
     * @param roleIds
     * @return
     */
    @GetMapping("/user/rpc/role/{roleIds}")
    R<List<SysUserDto>> getRoleUserInfo(@PathVariable("roleIds") String roleIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
