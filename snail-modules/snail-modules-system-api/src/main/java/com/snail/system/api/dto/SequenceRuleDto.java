package com.snail.system.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 序列号规则DTO对象
 * @Author: Snail
 * @CreateDate: 2024/4/29
 * @Version: V1.0
 */
@Data
@ApiModel(value = "序列号规则DTO对象")
public class SequenceRuleDto {

    @ApiModelProperty(value = "序列号规则编号")
    private String sequenceCode;
    @ApiModelProperty(value = "子序列号规则编号")
    private String childSequenceCode;

}
