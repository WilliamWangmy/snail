package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.snail.common.core.utils.StringUtils;


/**
 * @Description: 租户管理查询对象
 * @Author: snail
 * @CreateDate: 2024-04-30
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "租户管理")
public class SysTenantQuery extends BaseQuery {
    @ApiModelProperty(value = "租户号")
    private String tenantId;
    @ApiModelProperty(value = "租户名")
    private String tenantName;
    @ApiModelProperty(value = "状态")
    private String status;
}
