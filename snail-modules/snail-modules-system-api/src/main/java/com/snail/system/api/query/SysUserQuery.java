package com.snail.system.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 用户信息查询对象
 * @Author: Snail
 * @CreateDate: 2022/7/7 14:06
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "用户信息查询对象")
public class SysUserQuery extends BaseQuery {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户序号")
    private String userId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private String[] userIds;

    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门编号")
    private String deptId;
    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门编号")
    private String[] deptIds;

    /**
     * 用户账号
     */
    @ApiModelProperty(value = "登录名称")
    private String userName;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phoneNumber;

    /**
     * 帐号状态（0正常 1停用）
     */
    @ApiModelProperty(value = "帐号状态:1=正常,0=停用")
    private String status;

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色序号")
    private String roleId;
    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    private String[] roleIds;
    /**
     * 组织过滤类型:1.当前组织、2.当前组织及下属组织、3.上级组织，4.上级组织及下属组织,5.指定组织
     */
    @ApiModelProperty(value = "组织过滤类型")
    private String orgFilterType;

}
