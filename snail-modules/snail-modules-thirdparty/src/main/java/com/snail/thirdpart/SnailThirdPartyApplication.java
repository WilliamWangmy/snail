package com.snail.thirdpart;

import com.snail.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Description: 第三方接口服务
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 11:15
 * @Version: V1.0
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SnailThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailThirdPartyApplication.class, args);
    }
}
