package com.snail.thirdpart.business.sms.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponseBody;
import com.google.gson.Gson;
import com.snail.common.core.exception.ServiceException;
import com.snail.common.core.utils.http.HttpUtils;
import com.snail.thirdpart.business.sms.service.SmsService;
import com.snail.thirdpart.config.SmsProperties;
import com.snail.thirdparty.api.dto.SmsDto;
import darabonba.core.client.ClientOverrideConfiguration;
import lombok.SneakyThrows;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @Description: 短信处理
 * @Author: Snail
 * @CreateDate: 2024/4/17
 * @Version: V1.0
 */
@Service
public class SmsServiceImpl implements SmsService {

    @Autowired
    private SmsProperties smsProperties;

    @SneakyThrows
    @Override
    public boolean sendSmsCode(SmsDto smsDto) {
        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(smsProperties.getAccessKeyId())
                .accessKeySecret(smsProperties.getAccessKeySecret())
                .build());
        AsyncClient client = AsyncClient.builder()
                .region(smsProperties.getRegion())
                .credentialsProvider(provider)
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride(smsProperties.getRegionEndpoint())
                )
                .build();

        JSONObject param = new JSONObject();
        param.put("code", smsDto.getCode());

        SendSmsRequest sendSmsRequest = SendSmsRequest.builder()
                .signName(smsProperties.getSignName())
                .templateCode(smsProperties.getTemplateCode())
                .phoneNumbers(smsDto.getPhone())
                .templateParam(JSON.toJSONString(param))
                .build();

        CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);
        SendSmsResponse resp = response.get();
        SendSmsResponseBody body = resp.getBody();
        client.close();
        if ("OK".equals(body.getCode())) {
            return true;
        } else {
            throw new ServiceException("短信发送失败：" + body.getMessage());
        }
    }

}
