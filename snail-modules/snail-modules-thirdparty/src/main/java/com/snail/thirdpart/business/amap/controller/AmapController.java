package com.snail.thirdpart.business.amap.controller;

import com.snail.common.core.domain.R;
import com.snail.thirdpart.business.amap.service.AmapService;
import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.dto.GeoCodeDto;
import com.snail.thirdparty.api.dto.IpLocalDto;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.thirdparty.api.query.GeoQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 高德接口控制层
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 11:22
 * @Version: V1.0
 */
@Api(tags = "高德接口管理", value = "高德接口管理")
@RestController
@RequestMapping("/amap")
public class AmapController {

    @Autowired
    private AmapService amapService;

    @ApiOperation(value = "获取全国行政区域")
    @GetMapping("/district")
    private R<List<DistrictDto>> getDistrict(DistrictQuery query){
        return R.ok(amapService.getDistrict(query));
    }

    /**
     * 获取IP定位信息,不传默认当前ip
     * @param ip ip
     * @return
     */
    @ApiOperation(value = "获取IP定位信息")
    @GetMapping("/iplocal/{ip}")
    private R<IpLocalDto> getIpLocal(@PathVariable("ip") String ip){
        return R.ok(amapService.getIpLocal(ip));
    }


    @ApiOperation(value = "根据详细地址获取地理位置信息")
    @GetMapping("/geo")
    private R<List<GeoCodeDto>> getGeoCode(GeoQuery query){
        return R.ok(amapService.getGeoCode(query));
    }

    @ApiOperation(value = "根据经纬度获取地址位置信息")
    @GetMapping("/regeo")
    private R<String> getReGeoCode(String location){
        return R.ok(amapService.getReGeoCode(location));
    }

}
