package com.snail.thirdpart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 第三方接口配置
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 11:28
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties("snail.thirdparty.amap")
public class AmapProperties {

    /**
     * key需要去高德开放平台自己申请
     */
    private String key;

    /**
     * 行政区域查询接口地址
     */
    private String districtUrl = "https://restapi.amap.com/v3/config/district";

    /**
     * ip定位查询接口地址
     */
    private String ipUrl = "https://restapi.amap.com/v3/ip";
    /**
     * 获取地理编码信息
     */
    private String geoUrl = "https://restapi.amap.com/v3/geocode/geo";
    /**
     * 获取详细地址信息
     */
    private String reGeoUrl = "https://restapi.amap.com/v3/geocode/regeo";

}
