package com.snail.thirdpart.business.amap.service;

import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.dto.GeoCodeDto;
import com.snail.thirdparty.api.dto.IpLocalDto;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.thirdparty.api.query.GeoQuery;

import java.util.List;

/**
 * @Description: 高德接口Service
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 13:56
 * @Version: V1.0
 */
public interface AmapService {
    /**
     * 获取全国行政区域
     *
     * @param query 查询参数
     * @return 结果
     */
    List<DistrictDto> getDistrict(DistrictQuery query);

    /**
     * 根据ip获取定位
     *
     * @param ip ip地址
     * @return 结果
     */
    IpLocalDto getIpLocal(String ip);

    /**
     * 获取地址的地理编码位置
     *
     * @param query 查询参数
     * @return 结果
     */
    List<GeoCodeDto> getGeoCode(GeoQuery query);

    /**
     * 根据经纬度获取详细地址
     *
     * @param location 经纬度
     * @return 结果
     */
    String getReGeoCode(String location);
}
