package com.snail.thirdpart.business.sms.controller;

import com.snail.common.core.domain.R;
import com.snail.thirdpart.business.sms.service.SmsService;
import com.snail.thirdparty.api.dto.SmsDto;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description: 短信管理
 * @Author: Snail
 * @CreateDate: 2024/4/17
 * @Version: V1.0
 */
@Api(tags = "短信接口管理", value = "短信接口管理")
@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    @ApiOperation(value = "发送短信")
    @PostMapping("/sendCode")
    public R<Boolean> sendSmsCode(@RequestBody SmsDto smsDto) {
        return R.ok(smsService.sendSmsCode(smsDto));
    }
}
