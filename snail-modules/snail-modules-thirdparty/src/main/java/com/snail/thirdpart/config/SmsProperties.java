package com.snail.thirdpart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 短信配置
 * @Author: Snail
 * @CreateDate: 2024/4/17
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties("snail.thirdparty.sms")
public class SmsProperties {

    /**
     * accessKeyId
     */
    private String accessKeyId;
    /**
     * accessKeySecret
     */
    private String accessKeySecret;
    /**
     * 地区
     */
    private String region;
    /**
     * 地区地址
     */
    private String regionEndpoint;

    /**
     * 签名
     */
    private String signName;

    /**
     * 模板编号
     */
    private String templateCode;

}
