package com.snail.thirdpart.business.sms.service;

import com.snail.thirdparty.api.dto.SmsDto;

/**
 * @Description: 短信Service
 * @Author: Snail
 * @CreateDate: 2024/4/17
 * @Version: V1.0
 */
public interface SmsService {
    /**
     * 发送短信验证码
     *
     * @param smsDto 短信信息
     * @return 结果
     */
    boolean sendSmsCode(SmsDto smsDto);
}
