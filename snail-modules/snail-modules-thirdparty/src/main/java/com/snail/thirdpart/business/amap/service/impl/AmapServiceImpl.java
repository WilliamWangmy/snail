package com.snail.thirdpart.business.amap.service.impl;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.snail.thirdpart.business.amap.service.AmapService;
import com.snail.thirdpart.config.AmapProperties;
import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.dto.GeoCodeDto;
import com.snail.thirdparty.api.dto.IpLocalDto;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.thirdparty.api.query.GeoQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 高德接口Service实现
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 13:56
 * @Version: V1.0
 */
@Service
public class AmapServiceImpl implements AmapService {

    private static final Logger log = LoggerFactory.getLogger(AmapServiceImpl.class);


    @Autowired
    private AmapProperties properties;

    @Override
    public List<DistrictDto> getDistrict(DistrictQuery query) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", properties.getKey());
        param.put("keywords", query.getKeywords());
        param.put("subdistrict", query.getSubdistrict());
        log.debug("全国行政区域输出参数：" + JSONUtil.toJsonStr(param));
        String string = HttpUtil.get(properties.getDistrictUrl(), param);
        log.debug("全国行政区域查询结果：" + string);
        JSONObject jsonObject = JSONUtil.parseObj(string);
        Object districts = jsonObject.get("districts");
        return JSONUtil.toList(JSONUtil.parseArray(districts), DistrictDto.class);
    }

    @Override
    public IpLocalDto getIpLocal(String ip) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", properties.getKey());
        if(!ip.contains("127.0.0.1")){
            param.put("ip", ip);
        }
        log.debug("IP定位输出参数：" + JSONUtil.toJsonStr(param));
        String string = HttpUtil.get(properties.getIpUrl(), param);
        log.debug("IP定位查询结果：" + string);
        System.out.println("IP定位查询结果：" + string);
        JSONObject jsonObject = JSONUtil.parseObj(string);
        return JSONUtil.toBean(jsonObject, IpLocalDto.class);
    }

    @Override
    public List<GeoCodeDto> getGeoCode(GeoQuery query) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", properties.getKey());
        param.put("city", query.getCity());
        param.put("address", query.getAddress());
        log.debug("获取地理位置输出参数：" + JSONUtil.toJsonStr(param));
        String string = HttpUtil.get(properties.getGeoUrl(), param);
        log.debug("地理位置查询结果：" + string);
        JSONObject jsonObject = JSONUtil.parseObj(string);
        Object geocodes = jsonObject.get("geocodes");
        return JSONUtil.toList(JSONUtil.parseArray(geocodes), GeoCodeDto.class);
    }

    @Override
    public String getReGeoCode(String location) {
        Map<String, Object> param = new HashMap<>();
        param.put("key", properties.getKey());
        param.put("location", location);
        log.debug("详细地址获取经纬度输出参数：" + JSONUtil.toJsonStr(param));
        String string = HttpUtil.get(properties.getReGeoUrl(), param);
        log.debug("取经纬度查询结果：" + string);
        JSONObject jsonObject = JSONUtil.parseObj(string);
        Object regeocode = jsonObject.get("regeocode");
        JSONObject object = JSONUtil.parseObj(regeocode);
        return ObjUtil.toString(object.get("formatted_address"));
    }
}
