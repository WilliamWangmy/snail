package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Description: 历史活动实例对象
 * @Author: Snail
 * @CreateDate: 2022/8/10 11:02
 * @Version: V1.0
 */
@ApiModel("历史活动实例对象")
@Data
public class HisActivityInstanceDto {

    private String id;
    private Integer rev;
    private String procDefId;
    private String procInstId;
    private String executionId;
    private String actId;
    private String taskId;
    private String callProcInstId;
    private String actName;
    private String actType;
    private String assignee;
    private Date startTime;
    private Date endTime;
    private Integer transactionOrder;
    private Long duration;
    private String deleteReason;
    private String tenantId;

}
