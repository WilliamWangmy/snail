package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 流程定义对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 21:18
 * @Version: V1.0
 */
@ApiModel("流程定义对象")
@Data
public class FlowDefinitionDto implements Serializable {

    /**
     * 流程名称
     */
    @ApiModelProperty(value = "流程名称")
    private String wfName;

    /**
     * 流程分类
     */
    @ApiModelProperty(value = "流程分类")
    private String wfCategory;

    /**
     * xml 文件
     */
    @ApiModelProperty(value = "流程xml")
    private String xml;
}
