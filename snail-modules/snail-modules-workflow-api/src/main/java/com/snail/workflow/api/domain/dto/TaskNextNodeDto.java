package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 流程下一节点数据
 * @Author: Snail
 * @CreateDate: 2022/7/27 10:17
 * @Version: V1.0
 */
@ApiModel("流程下一节点数据")
@Data
public class TaskNextNodeDto {

    /**
     * 下一节点key
     */
    @ApiModelProperty(value = "下一节点key")
    private String nextNodeKey;

    /**
     * 下一节点key
     */
    @ApiModelProperty(value = "下一节点名称")
    private String nextNodeName;

    /**
     * 是否会签
     */
    @ApiModelProperty(value = "是否会签")
    private String multiInstance = "N";

    /**
     * 下一节点key
     */
    @ApiModelProperty(value = "默认审批人id")
    private List<String> userIds;

    /**
     * 下一节点key
     */
    @ApiModelProperty(value = "是否默认人审批")
    private String defaultApprove = "N";

    /**
     * 是否结束节点
     */
    @ApiModelProperty(value = "是否结束节点")
    private String isEndNode = "N";

    /**
     * 组织过滤类型:1.当前组织、2.当前组织及下属组织、3.上级组织，4.上级组织及下属组织,5.指定组织
     */
    @ApiModelProperty(value = "组织过滤类型")
    private String orgFilterType = "1";

    /**
     * 过滤组织id。仅支持指定组织时有值
     */
    @ApiModelProperty(value = "过滤组织")
    private List<String> filterOrgIds;

    /**
     * 角色
     */
    @ApiModelProperty(value = "角色")
    private List<String> roleIds;

}
