package com.snail.workflow.api;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.workflow.api.domain.dto.*;
import com.snail.workflow.api.factory.RemoteWorkflowFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @Description: 流程服务接口
 * @Author: Snail
 * @CreateDate: 2022/8/3 9:47
 * @Version: V1.0
 */
@FeignClient(contextId = "remoteWorkflowService", value = ServiceNameConstants.WORKFLOW_SERVICE, fallbackFactory = RemoteWorkflowFallbackFactory.class)
public interface RemoteWorkflowService {

    /**
     * 初始化流程任务
     *
     * @param taskInitWorkflowDto 初始化参数
     * @param source              接口标识
     * @return 结果
     */
    @PostMapping("/task/initWorkflow")
    R<WorkflowDto> initWorkflow(@RequestBody TaskInitWorkflowDto taskInitWorkflowDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取任务下一节点信息
     *
     * @param taskInitNextDto 初始化下一节点
     * @param source          接口标识
     * @return 结果
     */
    @GetMapping("/task/initNextNode")
    R<TaskParamDto> initNextNode(@SpringQueryMap TaskInitNextDto taskInitNextDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 审批流程任务
     *
     * @param taskSubmitDto 提交参数
     * @param source        接口标识
     * @return 结果
     */
    @PostMapping("/task/submitTask")
    R<WorkflowDto> submitTask(@RequestBody TaskSubmitDto taskSubmitDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 驳回流程任务
     *
     * @param taskSubmitDto 提交参数
     * @param source        接口标识
     * @return 结果
     */
    @PostMapping("/task/rejectTask")
    R<WorkflowDto> rejectTask(@RequestBody TaskSubmitDto taskSubmitDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 取消流程任务
     *
     * @param taskSubmitDto 提交参数
     * @param source        接口标识
     * @return 结果
     */
    @PostMapping("/task/cancelTask")
    R<WorkflowDto> cancelTask(@RequestBody TaskSubmitDto taskSubmitDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 委派流程任务
     *
     * @param taskSubmitDto 任务信息
     * @param source        接口标识
     * @return 流程信息
     */
    @PostMapping("/task/delegateTask")
    R<WorkflowDto> delegateTask(@RequestBody TaskSubmitDto taskSubmitDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 委派流程任务
     *
     * @param taskSubmitDto 任务信息
     * @param source        接口标识
     * @return 流程信息
     */
    @PostMapping("/task/turnToDoTask")
    R<WorkflowDto> turnToDoTask(@RequestBody TaskSubmitDto taskSubmitDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
    /**
     * 委派流程任务
     *
     * @param taskWithdrawDto 任务信息
     * @param source        接口标识
     * @return 流程信息
     */
    @PostMapping("/task/withdrawTask")
    R<Boolean> withdrawTask(@RequestBody TaskWithdrawDto taskWithdrawDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
