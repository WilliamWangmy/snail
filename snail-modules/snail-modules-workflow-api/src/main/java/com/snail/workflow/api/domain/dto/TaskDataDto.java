package com.snail.workflow.api.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.workflow.api.domain.entity.WorkflowBusiness;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @Description: 流程任务数据对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:36
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("流程任务数据对象")
@Data
public class TaskDataDto extends WorkflowBusiness {

    @ApiModelProperty("任务编号")
    private String taskId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务节点")
    private String nodeName;

    @ApiModelProperty("任务处理人Id")
    private String assigneeId;

    @ApiModelProperty("任务处理人名称")
    private String assigneeName;

    @ApiModelProperty("任务处理人部门")
    private String assigneeDeptName;

    @ApiModelProperty("流程发起人Id")
    private String startUserId;

    @ApiModelProperty("流程发起人名称")
    private String startUserName;

    @ApiModelProperty("流程发起人部门")
    private String startDeptName;

    @ApiModelProperty("流程分类")
    private String wfCategory;

    @ApiModelProperty("流程名称")
    private String wfName;

    @ApiModelProperty("任务创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime taskCreateTime;

    @ApiModelProperty("任务结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime taskFinishTime;

}
