package com.snail.workflow.api.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description: 审批记录数据对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:40
 * @Version: V1.0
 */
@ApiModel("审批记录数据对象")
@Data
public class TaskRecordsDto {

    @ApiModelProperty("任务编号")
    private String taskId;

    @ApiModelProperty("任务节点")
    private String nodeName;

    @ApiModelProperty("任务处理人Id")
    private String assigneeId;

    @ApiModelProperty("任务处理人名称")
    private String assigneeName;

    @ApiModelProperty("任务处理人部门")
    private String assigneeDeptName;

    @ApiModelProperty("任务结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime taskFinishTime;

    @ApiModelProperty("审批耗时(毫秒)")
    private Long durationTime;

    @ApiModelProperty("审批耗时")
    private String duration;

    @ApiModelProperty("审批意见")
    private String comment;
}
