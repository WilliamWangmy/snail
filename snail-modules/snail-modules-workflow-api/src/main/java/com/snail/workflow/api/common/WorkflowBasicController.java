package com.snail.workflow.api.common;

import com.snail.common.core.domain.R;
import com.snail.workflow.api.constant.WorkflowConstants;
import com.snail.workflow.api.domain.dto.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description: 流程公共接口
 * @Author: Snail
 * @CreateDate: 2022/8/3 10:58
 * @Version: V1.0
 */
public interface WorkflowBasicController {

    WorkflowBasicService getService();

    /**
     * 初始化流程任务
     *
     * @param dto 业务id
     * @return
     */
    @ApiOperation(value = "初始化流程任务")
    @PostMapping("/initWorkflow")
    default R<WorkflowDto> initWorkflow(@RequestBody TaskInitWorkflowDto dto) {
        return R.ok(this.getService().initWorkflow(dto));
    }


    /**
     * 获取下一审批节点
     *
     * @param taskInitNextDto
     * @return
     */
    @ApiOperation(value = "获取下一审批节点")
    @GetMapping("/initNextNode")
    default R<TaskParamDto> initNextNode(TaskInitNextDto taskInitNextDto) {
        return R.ok(this.getService().initNextNode(taskInitNextDto));
    }

    /**
     * 审批流程任务
     *
     * @param taskSubmitDto 提交参数
     * @return 结果
     */
    @ApiOperation(value = "审批流程任务")
    @PostMapping("/submitTask")
    default R<Boolean> submitTask(@RequestBody TaskSubmitDto taskSubmitDto) {
        boolean flag;
        switch (taskSubmitDto.getSubmitType()) {
            //提交
            case WorkflowConstants.TASK_TYPE_SUBMIT:
                flag = this.getService().submitTask(taskSubmitDto);
                break;
            //驳回
            case WorkflowConstants.TASK_TYPE_REJECT:
                flag = this.getService().rejectTask(taskSubmitDto);
                break;
            //取消、作废
            case WorkflowConstants.TASK_TYPE_CANCEL:
                flag = this.getService().cancelTask(taskSubmitDto);
                break;
            //委派
            case WorkflowConstants.TASK_TYPE_DELEGATE:
                flag = this.getService().delegateTask(taskSubmitDto);
                break;
            //转办
            case WorkflowConstants.TASK_TYPE_TURN_TO_DO:
                flag = this.getService().turnToDoTask(taskSubmitDto);
                break;
            default:
                flag = false;
        }
        return R.ok(flag);
    }

    @ApiOperation(value = "撤回流程任务")
    @PostMapping("/withdrawTask")
    default R<Boolean> withdrawTask(@RequestBody TaskWithdrawDto taskWithdrawDto) {
        return R.ok(this.getService().withdrawTask(taskWithdrawDto));
    }


}
