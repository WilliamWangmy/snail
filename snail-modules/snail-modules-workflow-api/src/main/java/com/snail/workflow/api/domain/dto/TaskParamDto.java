package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 流程任务参数对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:26
 * @Version: V1.0
 */
@ApiModel("流程任务参数对象")
@Data
public class TaskParamDto {

    @ApiModelProperty("业务id")
    private String bizId;

    @ApiModelProperty("任务编号")
    private String taskId;

    @ApiModelProperty(value = "当前节点key")
    private String nodeKey;

    @ApiModelProperty(value = "当前节点名称")
    private String nodeName;

    @ApiModelProperty("流程实例Id")
    private String instanceId;

    @ApiModelProperty("当前审批人id")
    private String userId;

    @ApiModelProperty("常用审批意见")
    private List<String> taskContexts;

    @ApiModelProperty("下一节点信息")
    private List<TaskNextNodeDto> nextNodes;

}
