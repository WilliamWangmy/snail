package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Description: 初始化任务下一节点
 * @Author: Snail
 * @CreateDate: 2022/7/29 8:28
 * @Version: V1.0
 */
@ApiModel("初始化任务下一节点")
@Data
public class TaskInitNextDto {

    @ApiModelProperty("任务id")
    @NotEmpty(message = "任务id不能为空")
    private String taskId;

    @ApiModelProperty("业务id")
    @NotEmpty(message = "业务id不能为空")
    private String bizId;

    @ApiModelProperty("用户id")
    @NotEmpty(message = "用户id不能为空")
    private String userId;

    @ApiModelProperty("操作类型：S提交、R驳回、C作废/取消")
    @NotEmpty(message = "操作类型不能为空")
    private String type;
}
