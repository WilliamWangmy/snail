package com.snail.workflow.api.domain.dto;

import com.snail.workflow.api.constant.WorkflowConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 流程DTO对象
 * @Author: Snail
 * @CreateDate: 2022/7/29 16:24
 * @Version: V1.0
 */
@Data
@ApiModel("流程DTO对象")
public class WorkflowDto {

    /**
     * 业务表单id
     */
    @ApiModelProperty(value = "业务id")
    private String bizId;
    /**
     * 流程实例id
     */
    @ApiModelProperty(value = "流程实例id")
    private String instanceId;
    /**
     * 流程任务id
     */
    @ApiModelProperty("流程任务id")
    private String taskId;
    /**
     * 流程状态
     */
    @ApiModelProperty(value = "流程状态")
    private String workflowStatus;

    /**
     * 当前任务节点ID
     */
    @ApiModelProperty(value = "当前任务节点ID")
    private String taskNodeId;

    /**
     * 当前任务节点名称
     */
    @ApiModelProperty(value = "当前任务节点名称")
    private String taskNodeName;

    /**
     * 可处理类型：
     */
    @ApiModelProperty(value = "可处理类型")
    private List<String> handleTypes;


    /**
     * 是否拟稿状态
     *
     * @return 结果
     */
    public boolean isDraft() {
        return WorkflowConstants.STATUS_DRAFT.equals(workflowStatus);
    }

    /**
     * 是否归档
     *
     * @return 结果
     */
    public boolean isCompete() {
        return WorkflowConstants.STATUS_COMPLETE.equals(workflowStatus);
    }

    /**
     * 是否取消
     *
     * @return 结果
     */
    public boolean isCancel() {
        return WorkflowConstants.TASK_TYPE_CANCEL.equals(workflowStatus);
    }

}
