package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @Description: 历史任务DTO
 * @Author: Snail
 * @CreateDate: 2023/9/7 14:59
 * @Version: V1.0
 */
@ApiModel("历史任务DTO对象")
@Data
public class HisTaskInstDto {

    private String id;
    private Integer rev;
    private String procDefId;
    private String taskDefId;
    private String taskDefKey;
    private String procInstId;
    private String executionId;
    private String scopeId;
    private String subScopeId;
    private String scopeType;
    private String scopeDefinitionId;
    private String propagatedStageInstId;
    private String name;
    private String parentTaskId;
    private String description;
    private String owner;
    private String assignee;
    private Date startTime;
    private Date claimTime;
    private Date endTime;
    private Long duration;
    private String deleteReason;
    private Integer priority;
    private Date dueDate;
    private String formKey;
    private String category;
    private String tenantId;
    private Date lastUpdatedTime;

}
