package com.snail.workflow.api.constant;

/**
 * @Description: 流程常量
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:55
 * @Version: V1.0
 */
public class WorkflowConstants {

    /**
     * 流程状态草稿
     */
    public static final String STATUS_DRAFT = "draft";
    /**
     * 流程状态流程中
     */
    public static final String STATUS_IN_PROCESS = "in_process";
    /**
     * 流程状态作废/取消
     */
    public static final String STATUS_CANCEL = "cancel";
    /**
     * 流程状态完成
     */
    public static final String STATUS_COMPLETE = "complete";
    /**
     * 流程状态驳回
     */
    public static final String STATUS_REJECT = "reject";

    /**
     * 流程发起人
     */
    public static final String INITIATOR = "INITIATOR";

    /**
     * nameapace
     */
    public static final String NAMASPASE = "http://flowable.org/bpmn";

    /**
     * 自定义属性 userType
     */
    public static final String PROCESS_CUSTOM_USER_TYPE = "userType";
    /**
     * 自定义属性 filterOrgType
     */
    public static final String PROCESS_CUSTOM_ORG_TYPE = "filterOrgType";
    /**
     * 自定义属性 filterOrg
     */
    public static final String PROCESS_CUSTOM_ORG_IDS = "filterOrg";
    /**
     * 单个审批人
     */
    public static final String USER_TYPE_ASSIGNEE = "assignee";
    /**
     * 多个个审批人
     */
    public static final String USER_TYPE_ASSIGNEE_LIST = "assigneeList";
    /**
     * 候选人
     */
    public static final String USER_TYPE_USERS = "candidateUsers";
    /**
     * 审批组(角色)
     */
    public static final String USER_TYPE_GROUPS = "candidateGroups";
    /**
     * 正常流程线
     */
    public static final String SEQUENCE_FLOW_TYPE_NORMAL = "normal";
    /**
     * 驳回流程线
     */
    public static final String SEQUENCE_FLOW_TYPE_REJECT = "reject";
    /**
     * 任务类型:审批（提交）、驳回、取消/作废、委托、转办
     */
    public static final String TASK_TYPE_SUBMIT = "S";
    public static final String TASK_TYPE_REJECT = "R";
    public static final String TASK_TYPE_CANCEL = "C";
    public static final String TASK_TYPE_DELEGATE = "D";
    public static final String TASK_TYPE_TURN_TO_DO = "T";
    public static final String TASK_TYPE_WITHDRAW = "W";
    /**
     * 提交类型
     */
    public static final String TASK_SUBMIT_TYPE = "submitType";
    /**
     * 任务处理类型
     */
    public static final String TASK_HANDLE_TYPE = "handleType";
    /**
     * 流程进入任务线
     */
    public static final String SEQUENCE_FLOW_INCOMING_FLOWS = "incoming";
    /**
     * 流程出任务先
     */
    public static final String SEQUENCE_FLOW_OUTGOING_FLOWS = "outgoing";
    /**
     * 开始节点前缀
     */
    public static final String START_NODE_PREFIX = "start";
    /**
     * 结束节点前缀
     */
    public static final String END_NODE_PREFIX = "end";

    /**
     * 会签实例总数
     */
    public static final String MULTI_INSTANCE_TOTAL = "nrOfInstances";
    /**
     * 当前活动的（即未完成的），实例数量。对于顺序多实例，这个值总为1。
     */
    public static final String MULTI_INSTANCE_ACTIVE = "nrOfActiveInstances";
    /**
     * 已完成的实例数量
     */
    public static final String MULTI_INSTANCE_COMPLETED = "nrOfCompletedInstances";
    /**
     * 给定实例在for-each循环中的index
     */
    public static final String MULTI_INSTANCE_LOOP_COUNTER = "loopCounter";
    /**
     * 组织过滤类型:1.当前组织、2.当前组织及下属组织、3.上级组织，4.上级组织及下属组织,5.指定组织
     */
    public static final String FILTER_ORG_TYPE_1 = "1";
    public static final String FILTER_ORG_TYPE_2 = "2";
    public static final String FILTER_ORG_TYPE_3 = "3";
    public static final String FILTER_ORG_TYPE_4 = "4";
    public static final String FILTER_ORG_TYPE_5 = "5";
    /**
     * 业务数据缓存key
     */
    public static final String BUSINESS_CACHE_KEY = "snail:workflow:info:business:{}";
    /**
     * 流程定义数据缓存key
     */
    public static final String PROCESS_DEFINITION_CACHE_KEY = "snail:workflow:info:processDef:{}";

    /**
     * 流程任务锁
     */
    public static final String TASK_LOCK_KEY = "snail:workflow:lock:task:{}";
    /**
     * 流程业务锁
     */
    public static final String BIZ_LOCK_KEY = "snail:workflow:lock:biz:{}";
}
