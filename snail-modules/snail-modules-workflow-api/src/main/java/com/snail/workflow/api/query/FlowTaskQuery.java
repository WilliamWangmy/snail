package com.snail.workflow.api.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 待办/已办查询对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:31
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel("待办/已办查询对象")
@Data
public class FlowTaskQuery extends BaseQuery {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("流程分类")
    private String wfCategory;

    @ApiModelProperty("流程名称")
    private String wfName;

    @ApiModelProperty("任务创建时间(起)")
    private String beginTaskCreateTime;

    @ApiModelProperty("任务创建时间(止)")
    private String endTaskCreateTime;
}
