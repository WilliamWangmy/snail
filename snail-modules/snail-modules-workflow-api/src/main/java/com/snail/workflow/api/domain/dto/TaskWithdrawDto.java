package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Description: 任务撤回DTO对象
 * @Author: Snail
 * @CreateDate: 2023/9/15 22:32
 * @Version: V1.0
 */
@ApiModel("任务撤回DTO对象")
@Data
public class TaskWithdrawDto {

    @ApiModelProperty("业务id")
    @NotEmpty(message = "业务id不能为空")
    private String bizId;

    @ApiModelProperty("撤回人id")
    @NotEmpty(message = "撤回人id不能为空")
    private String userId;

    @ApiModelProperty("撤回说明")
    @NotEmpty(message = "撤回说明不能为空")
    private String comment;
}
