package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 流程提交对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:24
 * @Version: V1.0
 */
@ApiModel("流程提交对象")
@Data
public class TaskSubmitDto {

    @ApiModelProperty("业务id")
    @NotEmpty(message = "业务id不能为空")
    private String bizId;

    @ApiModelProperty("任务Id")
    @NotEmpty(message = "任务Id不能为空")
    private String taskId;

    @ApiModelProperty("当前审批人id")
    @NotEmpty(message = "当前审批人id不能为空")
    private String userId;

    @ApiModelProperty("流程实例Id")
    @NotEmpty(message = "流程实例Id不能为空")
    private String instanceId;

    @ApiModelProperty("任务意见")
    @NotEmpty(message = "任务意见不能为空")
    private String comment;

    @ApiModelProperty("节点")
    private String targetKey;

    @ApiModelProperty(value = "是否会签")
    private String multiInstance;

    @ApiModelProperty(value = "是否结束")
    private String isEndNode;

    @ApiModelProperty(value = "提交类型:S.提交、R.驳回、C.取消、D.委托、T.转办")
    private String submitType;

    @ApiModelProperty("下一节点审批人")
    private List<String> assignee = new ArrayList<>();

    @ApiModelProperty("流程变量信息")
    private Map<String, Object> variables = new HashMap<>();
}
