package com.snail.workflow.api.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

/**
 * @Description: 流程启动参数对象
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:22
 * @Version: V1.0
 */
@ApiModel("流程启动参数对象")
@Data
public class TaskInitWorkflowDto {

    /**
     * 业务表单id
     */
    @ApiModelProperty(value = "业务id")
    @NotEmpty(message = "业务id不能为空")
    private String bizId;
    /**
     * 表单名称
     */
    @ApiModelProperty(value = "业务名称")
    @NotEmpty(message = "业务名称不能为空")
    private String bizName;

    /**
     * 业务url
     */
    @ApiModelProperty(value = "业务查看路由url")
    @NotEmpty(message = "业务查看路由url不能为空")
    private String bizUrl;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    @NotEmpty(message = "用户id不能为空")
    private String userId;

    /**
     * 流程定义Key
     */
    @ApiModelProperty(value = "流程定义Key")
    @NotEmpty(message = "流程定义Key不能为空")
    private String procDefKey;

    /**
     * 流程定义Id
     */
    @ApiModelProperty(value = "流程定义Id")
    private String procDefId;

    @ApiModelProperty("流程变量信息")
    private Map<String, Object> variables;

}
