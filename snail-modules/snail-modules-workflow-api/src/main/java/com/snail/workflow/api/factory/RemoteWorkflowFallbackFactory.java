package com.snail.workflow.api.factory;

import com.snail.common.core.domain.R;
import com.snail.workflow.api.RemoteWorkflowService;
import com.snail.workflow.api.domain.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 用户服务降级处理
 *
 * @author snail
 */
@Component
public class RemoteWorkflowFallbackFactory implements FallbackFactory<RemoteWorkflowService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteWorkflowFallbackFactory.class);

    @Override
    public RemoteWorkflowService create(Throwable throwable) {
        log.error("流程服务调用失败:{}", throwable.getMessage());
        return new RemoteWorkflowService() {
            @Override
            public R<WorkflowDto> initWorkflow(TaskInitWorkflowDto taskInitWorkflowDto, String source) {
                return R.fail("初始化流程失败:" + throwable.getMessage());
            }

            @Override
            public R<TaskParamDto> initNextNode(TaskInitNextDto taskInitNextDto, String source) {
                return R.fail("获取任务节点失败:" + throwable.getMessage());
            }

            @Override
            public R<WorkflowDto> submitTask(TaskSubmitDto taskSubmitDto, String source) {
                return R.fail("提交流程任务失败:" + throwable.getMessage());
            }

            @Override
            public R<WorkflowDto> rejectTask(TaskSubmitDto taskSubmitDto, String source) {
                return R.fail("驳回流程任务失败:" + throwable.getMessage());
            }

            @Override
            public R<WorkflowDto> cancelTask(TaskSubmitDto taskSubmitDto, String source) {
                return R.fail("取消流程任务失败:" + throwable.getMessage());
            }

            @Override
            public R<WorkflowDto> delegateTask(TaskSubmitDto taskSubmitDto, String source) {
                return R.fail("委托流程任务失败:" + throwable.getMessage());
            }

            @Override
            public R<WorkflowDto> turnToDoTask(TaskSubmitDto taskSubmitDto, String source) {
                return R.fail("委托流程任务失败:" + throwable.getMessage());
            }

            @Override
            public R<Boolean> withdrawTask(TaskWithdrawDto taskWithdrawDto, String source) {
                return R.fail("撤回流程任务失败:" + throwable.getMessage());
            }
        };
    }
}
