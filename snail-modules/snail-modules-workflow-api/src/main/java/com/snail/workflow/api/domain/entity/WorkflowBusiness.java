package com.snail.workflow.api.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 流程表单信息
 * @Author: Snail
 * @CreateDate: 2022/7/20 16:42
 * @Version: V1.0
 */
@Data
@ApiModel(value = "流程表单信息")
@TableName("w_workflow_business")
public class WorkflowBusiness implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 流程表单id
     */
    @ApiModelProperty(value = "流程表单id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String wfId;
    /**
     * 业务表单id
     */
    @ApiModelProperty(value = "业务id")
    private String bizId;
    /**
     * 表单名称
     */
    @ApiModelProperty(value = "业务名称")
    private String bizName;
    /**
     * 业务url
     */
    @ApiModelProperty(value = "业务url")
    private String bizUrl;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 流程状态
     */
    @ApiModelProperty(value = "流程状态")
    private String workflowStatus;
    /**
     * 流程实例id
     */
    @ApiModelProperty(value = "流程实例id")
    private String instanceId;

    /**
     * 流程定义id
     */
    @ApiModelProperty(value = "流程定义id")
    private String procDefId;
    /**
     * 流程定义id
     */
    @ApiModelProperty(value = "流程定义key")
    private String procDefKey;
    /**
     * 流程定义名称
     */
    @ApiModelProperty(value = "流程定义名称")
    private String procDefName;
    /**
     * 任务节点key
     */
    @ApiModelProperty(value = "任务节点key")
    private String taskNodeKey;
    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    private String createUserId;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    /**
     * 创建人部门
     */
    @ApiModelProperty(value = "创建人部门")
    private String createDeptName;

}
