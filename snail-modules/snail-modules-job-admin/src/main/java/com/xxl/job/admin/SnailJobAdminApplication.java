package com.xxl.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication
public class SnailJobAdminApplication {

	public static void main(String[] args) {
        SpringApplication.run(SnailJobAdminApplication.class, args);
	}

}