package com.snail.file.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minio 配置信息
 *
 * @author snail
 */
@Configuration
public class MinioFileConfig {

    @Autowired
    private FileConfigProperties properties;

    @Bean
    public MinioClient getMinioClient() {
        return MinioClient.builder().endpoint(properties.getMinio().getUrl())
                .credentials(properties.getMinio().getAccessKey(), properties.getMinio().getSecretKey()).build();
    }
}
