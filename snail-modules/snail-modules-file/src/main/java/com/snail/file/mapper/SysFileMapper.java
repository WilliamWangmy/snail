package com.snail.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.system.api.domain.SysFile;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 文件信息Mapper接口
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
@Mapper
public interface SysFileMapper extends BaseMapper<SysFile> {

}
