package com.snail.file.property;


import lombok.Data;

/**
 * @Description: 文件配置
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
@Data
public class LocalFile {

    /**
     * 上传文件存储在本地的根路径
     */
    private String path;

    /**
     * 资源映射路径 前缀
     */
    public String prefix;

    /**
     * 域名或本机访问地址
     */
    public String domain;


}
