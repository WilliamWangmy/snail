package com.snail.file.property;


import lombok.Data;

/**
 * @Description: 文件配置
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
@Data
public class FastDfsFile {

    /**
     * 域名或本机访问地址
     */
    public String domain;

    /**
     * 分组名称
     */
    public String groupName;


}
