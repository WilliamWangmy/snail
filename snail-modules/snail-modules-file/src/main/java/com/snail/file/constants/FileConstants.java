package com.snail.file.constants;

public class FileConstants {

    public static final String LOCAL = "local";
    public static final String MINIO = "minio";
    public static final String FAST_DFS = "FastDFS";
}
