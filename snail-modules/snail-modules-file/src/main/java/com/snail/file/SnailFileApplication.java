package com.snail.file;

import com.snail.common.core.utils.ServerUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 文件服务
 * 
 * @author snail
 */
@SpringBootApplication
@MapperScan("com.snail.file.mapper")
public class SnailFileApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SnailFileApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail文件服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _        __  _  _\n" +
                "                   (_)| |      / _|(_)| |\n" +
                " ___  _ ___   __ _  _ | |     | |_  _ | |  ___\n" +
                "/ __|| '_  \\ / _` || || |     |  _|| || | / _ \\\n" +
                "\\__ \\| | | || (_| || || |     | |  | || ||  __/\n" +
                "|___/|_| |_| \\__,_||_||_|     |_|  |_||_| \\___|\n" +
                ServerUtil.printInfo());
    }
}
