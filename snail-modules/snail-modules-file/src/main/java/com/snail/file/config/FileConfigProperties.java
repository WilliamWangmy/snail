package com.snail.file.config;

import com.snail.file.property.FastDfsFile;
import com.snail.file.property.LocalFile;
import com.snail.file.property.MinioFile;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 文件信息配置属性
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "snail.file")
public class FileConfigProperties {

    /**
     * 服务器类型：local、FastDFS、minio
     */
    private String serverType;

    /**
     * 可上传的文件类型多个用逗号分隔
     */
    private String fileType;

    /**
     * 本地服务配置
     */
    private LocalFile local;

    /**
     * minio配置
     */
    private MinioFile minio;

    /**
     * FastDFS配置
     */
    private FastDfsFile fastDfs;





}
