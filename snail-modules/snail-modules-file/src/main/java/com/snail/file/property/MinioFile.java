package com.snail.file.property;

import lombok.Data;

/**
 * Minio 配置信息
 *
 * @author snail
 */
@Data
public class MinioFile {
    /**
     * 服务地址
     */
    private String url;

    /**
     * 用户名
     */
    private String accessKey;

    /**
     * 密码
     */
    private String secretKey;

    /**
     * 存储桶名称
     */
    private String bucketName;
}
