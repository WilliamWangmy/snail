package com.snail.file.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.system.api.domain.SysFile;
import com.snail.system.api.query.SysFileQuery;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: 文件信息Service接口
 * @Author: snail
 * @CreateDate: 2023-09-20
 * @Version: V1.0
 */
public interface ISysFileService extends IService<SysFile> {


    /**
     * 上传文件
     *
     * @param file   文件
     * @param bizKey 业务key
     * @return 结果
     */
    SysFile uploadFile(MultipartFile file, String bizKey);

    /**
     * 分页查询附件信息
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<SysFile> getSysFilePage(SysFileQuery query);

    /**
     * 列表查询附件信息
     *
     * @param query 查询参数
     * @return 结果
     */
    List<SysFile> selectSysFileList(SysFileQuery query);

    /**
     * 下载文件
     *
     * @param response response
     * @param fileId   文件id
     */
    void download(HttpServletResponse response, String fileId);

    /**
     * 删除文件
     *
     * @param fileId 文件id
     * @return 结果
     */
    boolean deleteFile(String fileId);
}
