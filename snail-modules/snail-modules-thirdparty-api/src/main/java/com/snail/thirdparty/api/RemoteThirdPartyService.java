package com.snail.thirdparty.api;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.dto.GeoCodeDto;
import com.snail.thirdparty.api.dto.IpLocalDto;
import com.snail.thirdparty.api.dto.SmsDto;
import com.snail.thirdparty.api.factory.RemoteThirdPartyFallbackFactory;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.thirdparty.api.query.GeoQuery;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: 第三方接口Feign
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 15:32
 * @Version: V1.0
 */
@FeignClient(contextId = "remoteThirdPartyService", value = ServiceNameConstants.THIRDPARTY_SERVICE, fallbackFactory = RemoteThirdPartyFallbackFactory.class)
public interface RemoteThirdPartyService {

    /**
     * 获取IP定位
     * 不传默认当前ip
     *
     * @param ip     ip
     * @param source 内部接口标识
     * @return 结果
     */
    @GetMapping("/amap/iplocal/{ip}")
    R<IpLocalDto> getIpLocal(@PathVariable("ip") String ip, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取全国行政区域
     *
     * @param query 查询参数
     * @param source      内部接口标识
     * @return 结果
     */
    @GetMapping("/amap/district")
    R<List<DistrictDto>> getDistrict(@SpringQueryMap DistrictQuery query, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 根据详细地址获取地理位置
     *
     * @param query  查询参数
     * @param source 内部接口标识
     * @return 结果
     */
    @GetMapping("/amap/geo")
    R<List<GeoCodeDto>> getGeoCode(@SpringQueryMap GeoQuery query, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 根据经纬度获取地址位置信息
     *
     * @param location 经纬度
     * @param source   内部接口标识
     * @return 结果
     */
    @GetMapping("/amap/regeo")
    R<String> getReGeoCode(String location, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 发送短信验证码
     * @param smsDto 输入参数
     * @param source  内部接口标识
     * @return 结果
     */
    @PostMapping("/sms/sendCode")
    R<Boolean> sendSmsCode(@RequestBody SmsDto smsDto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
