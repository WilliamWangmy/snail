package com.snail.thirdparty.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 短信DTO
 * @Author: Snail
 * @CreateDate: 2024/4/25 21:11
 * @Version: V1.0
 */
@Data
public class SmsDto {

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "验证码")
    private String code;
}
