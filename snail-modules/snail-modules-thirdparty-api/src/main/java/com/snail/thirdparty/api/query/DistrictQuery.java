package com.snail.thirdparty.api.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 行政区域查询参数
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Data
@ApiModel("行政区域查询参数")
public class DistrictQuery {
    @ApiModelProperty("查询关键字")
    private String keywords;
    @ApiModelProperty("子级行政区")
    private String subdistrict;

}
