package com.snail.thirdparty.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: ip定位Dto
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 15:05
 * @Version: V1.0
 */
@ApiModel("ip定位Dto")
@Data
public class IpLocalDto {

    @ApiModelProperty("城市的adcode编码")
    private String adcode;
    @ApiModelProperty("城市名称")
    private String city;
    @ApiModelProperty("省份名称")
    private String province;
    @ApiModelProperty("所在城市矩形区域范围")
    private String rectangle;
}
