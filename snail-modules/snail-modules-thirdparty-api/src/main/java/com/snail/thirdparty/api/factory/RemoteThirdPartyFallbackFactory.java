package com.snail.thirdparty.api.factory;

import com.snail.common.core.domain.R;
import com.snail.thirdparty.api.RemoteThirdPartyService;
import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.dto.GeoCodeDto;
import com.snail.thirdparty.api.dto.IpLocalDto;
import com.snail.thirdparty.api.dto.SmsDto;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.thirdparty.api.query.GeoQuery;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description: 第三方接口服务降级处理
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 15:33
 * @Version: V1.0
 */
@Component
public class RemoteThirdPartyFallbackFactory implements FallbackFactory<RemoteThirdPartyService> {
    @Override
    public RemoteThirdPartyService create(Throwable cause) {
        return new RemoteThirdPartyService() {
            @Override
            public R<IpLocalDto> getIpLocal(String ip, String source) {
                return R.fail("获取IP定位失败:" + cause.getMessage());
            }

            @Override
            public R<List<DistrictDto>> getDistrict(DistrictQuery query, String source) {
                return R.fail("获取行政区域失败:" + cause.getMessage());
            }

            @Override
            public R<List<GeoCodeDto>> getGeoCode(GeoQuery query, String source) {
                return R.fail("获取地理位置失败:" + cause.getMessage());
            }

            @Override
            public R<String> getReGeoCode(String location, String source) {
                return R.fail("获取详细地址失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> sendSmsCode(SmsDto smsDto, String source) {
                return R.fail("发送验证码失败:" + cause.getMessage());
            }
        };
    }
}
