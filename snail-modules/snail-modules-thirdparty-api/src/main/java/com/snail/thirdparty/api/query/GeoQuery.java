package com.snail.thirdparty.api.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 地理位置信息查询参数
 * @Author: William.Wmy
 * @CreateDate: 2024/3/15 15:13
 * @Version: V1.0
 */
@ApiModel("地理位置信息查询参数")
@Data
public class GeoQuery {

    @ApiModelProperty("城市")
    private String city;
    @ApiModelProperty("详细地址")
    private String address;
}
