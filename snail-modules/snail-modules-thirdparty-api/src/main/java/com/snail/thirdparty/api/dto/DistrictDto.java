package com.snail.thirdparty.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Description: 行政区域DTO
 * @Author: William.Wmy
 * @CreateDate: 2024/3/14 13:49
 * @Version: V1.0
 */
@ApiModel("行政区域DTO")
@Data
public class DistrictDto {

    @ApiModelProperty("城市编码")
    private String citycode;

    @ApiModelProperty("区域编码")
    private String adcode;

    @ApiModelProperty("行政区名称")
    private String name;

    @ApiModelProperty("行政区边界坐标点")
    private String polyline;

    @ApiModelProperty("center")
    private String center;

    /**
     * country:国家
     * province:省份（直辖市会在province显示）
     * city:市（直辖市会在province显示）
     * district:区县
     * street:街道
     */
    @ApiModelProperty("行政区划级别")
    private String level;

    @ApiModelProperty("下级行政区列表")
    private List<DistrictDto> districts;
}

