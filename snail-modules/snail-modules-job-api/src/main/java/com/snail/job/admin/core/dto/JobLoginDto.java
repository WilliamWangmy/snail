package com.snail.job.admin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: job登录DTO对象
 * @Author: Snail
 * @CreateDate: 2024/9/24
 * @Version: V1.0
 */
@Data
@ApiModel(value = "job登录DTO对象")
public class JobLoginDto {

    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("密码")
    private String password;
}
