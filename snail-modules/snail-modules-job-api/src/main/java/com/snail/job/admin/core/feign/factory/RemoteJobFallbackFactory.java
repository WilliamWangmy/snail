package com.snail.job.admin.core.feign.factory;

import com.snail.common.core.domain.R;
import com.snail.job.admin.core.dto.JobLoginDto;
import com.snail.job.admin.core.feign.RemoteJobService;
import com.snail.job.admin.core.model.XxlJobGroup;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Description: TODO
 * @Author: Snail
 * @CreateDate: 2024/9/24
 * @Version: V1.0
 */
@Component
public class RemoteJobFallbackFactory implements FallbackFactory<RemoteJobService> {
    @Override
    public RemoteJobService create(Throwable throwable) {
        return new RemoteJobService() {
            @Override
            public R<String> login(JobLoginDto dto, String source) {
                return R.fail("job admin login fail:" + throwable.getMessage());
            }

            /**
             * 添加执行器
             *
             * @param jobGroup 执行器信息
             * @param source   来源
             * @return 登录结果
             */
            @Override
            public R<Integer> addExecutor(XxlJobGroup jobGroup, String source) {
                return R.fail("job admin addExecutor fail:" + throwable.getMessage());
            }
        };
    }
}
