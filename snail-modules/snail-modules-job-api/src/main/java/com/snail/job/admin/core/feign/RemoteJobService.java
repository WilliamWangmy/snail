package com.snail.job.admin.core.feign;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.constant.ServiceNameConstants;
import com.snail.common.core.domain.R;
import com.snail.job.admin.core.dto.JobLoginDto;
import com.snail.job.admin.core.feign.factory.RemoteJobFallbackFactory;
import com.snail.job.admin.core.model.XxlJobGroup;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @Description: Job Feign接口
 * @Author: Snail
 * @CreateDate: 2024/9/24
 * @Version: V1.0
 */
@FeignClient(contextId = "RemoteJobService", value = ServiceNameConstants.JOB_ADMIN_SERVICE,fallbackFactory = RemoteJobFallbackFactory.class)
public interface RemoteJobService {

    /**
     * 登录
     *
     * @param dto 登录信息
     * @param source   来源
     * @return 登录结果
     */
    @PostMapping("/rpc/login")
    R<String> login(@RequestBody JobLoginDto dto, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 添加执行器
     *
     * @param jobGroup 执行器信息
     * @param source   来源
     * @return 登录结果
     */
    @PostMapping("/jobgroup/rpc/add")
    R<Integer> addExecutor(@RequestBody XxlJobGroup jobGroup, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
