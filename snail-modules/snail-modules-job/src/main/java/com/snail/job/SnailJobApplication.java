package com.snail.job;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import com.snail.common.core.utils.ServerUtil;
import org.springframework.boot.SpringApplication;

/**
 * 定时任务
 *
 * @author snail
 */
@SnailCloudApplication
@Deprecated
public class SnailJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(SnailJobApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail定时任务服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _         _         _\n" +
                "                   (_)| |       (_)       | |\n" +
                " ___  _ ___   __ _  _ | |        _   ___  | |__\n" +
                "/ __|| '_  \\ / _` || || |       | | / _ \\ | '_ \\\n" +
                "\\__ \\| | | || (_| || || |       | || (_) || |_) |\n" +
                "|___/|_| |_| \\__,_||_||_|       | | \\___/ |_.__/\n" +
                "                               _/ |\n" +
                "                              |__/\n" +
                ServerUtil.printInfo());
    }
}
