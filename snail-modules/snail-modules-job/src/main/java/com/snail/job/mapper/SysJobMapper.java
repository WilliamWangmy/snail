package com.snail.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.job.domain.SysJob;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 调度任务信息 数据层
 *
 * @author snail
 */
@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {
    /**
     * 查询调度任务日志集合
     *
     * @param job 调度信息
     * @return 操作日志集合
     */
    public List<SysJob> selectJobList(SysJob job);

    /**
     * 查询所有调度任务
     *
     * @return 调度任务列表
     */
    public List<SysJob> selectJobAll();

}
