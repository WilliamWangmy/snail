package com.snail.gen.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DruidDataSourceCreator;
import com.baomidou.dynamic.datasource.ds.ItemDataSource;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.snail.common.core.constant.Constants;
import com.snail.common.core.exception.ServiceException;
import com.snail.common.core.utils.StringUtils;
import com.snail.gen.dto.DataSourceDto;
import com.snail.gen.query.DynamicTableQuery;
import com.snail.gen.service.DynamicTableService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: DynamicTableServiceImpl
 * =================================================
 * @Description: 动态业务表功能业务接口实现
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:40
 * @Version: V1.0
 */
@Service
@AllArgsConstructor
public class DynamicTableServiceImpl implements DynamicTableService {

    private final DataSource dataSource;
    private final DruidDataSourceCreator dataSourceCreator;


    @Override
    public List<DataSourceDto> listDataSource(DynamicTableQuery dynamicTableQuery) {
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        List<DataSourceDto> dtoList = new ArrayList<>();
        ds.getDataSources().forEach((k, v) -> {
            DataSourceDto dto = new DataSourceDto();
            ItemDataSource itemDataSource = (ItemDataSource) v;
            DruidDataSource druidDataSource = (DruidDataSource) itemDataSource.getRealDataSource();

            dto.setPollName(k);
            dto.setDbType(druidDataSource.getDbType());
            dtoList.add(dto);
        });
        if (StringUtils.isNotBlank(dynamicTableQuery.getPollName())) {
            return dtoList.stream().filter(item -> item.getPollName().contains(dynamicTableQuery.getPollName())).collect(Collectors.toList());
        }
        return dtoList;
    }


    @Override
    public boolean addDataSource(DataSourceDto dataSourceDto) {
        try {
            Class.forName(dataSourceDto.getDriverClassName());
            DriverManager.getConnection(dataSourceDto.getUrl(),dataSourceDto.getUsername(),dataSourceDto.getPassword());
        }catch (Exception e){
            throw new ServiceException("链接失败请查看数据源配置是否正确！", Constants.FAIL);
        }
        try {
            DataSourceProperty dataSourceProperty = new DataSourceProperty();
            BeanUtils.copyProperties(dataSourceDto, dataSourceProperty);
            DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
            DataSource dataSource = dataSourceCreator.createDataSource(dataSourceProperty);
            ds.addDataSource(dataSourceDto.getPollName(), dataSource);
        }catch (Exception e){
            throw new ServiceException("添加数据源异常："+e.getMessage(),Constants.FAIL);
        }
        return true;
    }
}
