package com.snail.gen.query;

import lombok.Data;

/**
 * @ClassName: DynamicTableQuery
 * =================================================
 * @Description:  动态业务表查询
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:44
 * @Version: V1.0
 */
@Data
public class DynamicTableQuery {

    /**
     * 连接池名称
     */
    private String pollName;
}
