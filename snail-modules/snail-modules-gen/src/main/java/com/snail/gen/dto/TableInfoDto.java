package com.snail.gen.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName: TableInfoDto
 * =================================================
 * @Description: 业务表信息
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:59
 * @Version: V1.0
 */
@Data
public class TableInfoDto {

    /**
     * 表名称
     */
    @ApiModelProperty("表名称")
    private String tableName;
    /**
     * 表描述
     */
    @ApiModelProperty("表描述")
    private String tableComment;
    /**
     * 表创建时间
     */
    @ApiModelProperty("表创建时间")
    private LocalDateTime createTime;
    /**
     * 表更新时间
     */
    @ApiModelProperty("表更新时间")
    private LocalDateTime updateTime;

}
