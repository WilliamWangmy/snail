package com.snail.gen.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.snail.gen.domain.GenTable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;


@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "业务表DTO")
public class GenTableDto extends GenTable {

    /** 其它生成选项 */
    @ApiModelProperty(value = "其它生成选项")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params;
}
