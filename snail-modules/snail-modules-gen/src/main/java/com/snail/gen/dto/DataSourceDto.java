package com.snail.gen.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: DataSourceDto
 * =================================================
 * @Description: 数据源对象
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:42
 * @Version: V1.0
 */
@Data
public class DataSourceDto {

    /**
     * 连接池名称
     */
    @ApiModelProperty(value = "连接池名称")
    private String pollName;

    @ApiModelProperty(value = "主机")
    private String host;

    @ApiModelProperty(value = "端口")
    private String port;

    @ApiModelProperty(value = "数据库名")
    private String dbName;

    /**
     * 链接驱动
     */
    @ApiModelProperty(value = "链接驱动")
    private String driverClassName;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String url;

    /**
     * 数据库类型
     */
    @ApiModelProperty(value = "数据库类型")
    private String dbType;


}
