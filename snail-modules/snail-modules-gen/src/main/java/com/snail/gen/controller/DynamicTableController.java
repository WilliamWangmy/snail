package com.snail.gen.controller;

import com.snail.common.core.domain.R;
import com.snail.gen.dto.DataSourceDto;
import com.snail.gen.query.DynamicTableQuery;
import com.snail.gen.service.DynamicTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: DynamicController
 * =================================================
 * @Description: 动态业务表功能控制层
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:34
 * @Version: V1.0
 */
@Api(tags = "动态数据源")
@RestController
@RequestMapping("/dynamic")
public class DynamicTableController {

    @Autowired
    private DynamicTableService dynamicTableService;

    @ApiOperation("获取当前配置的数据源")
    @GetMapping("/listDataSource")
    public R<List<DataSourceDto>> listDataSource(DynamicTableQuery dynamicTableQuery) {
        return R.ok(dynamicTableService.listDataSource(dynamicTableQuery));
    }

    @ApiOperation("添加数据源")
    @PostMapping("/addDataSource")
    public R<Boolean> addDataSource(@RequestBody DataSourceDto dataSourceDto) {
        return R.ok(dynamicTableService.addDataSource(dataSourceDto));
    }
}
