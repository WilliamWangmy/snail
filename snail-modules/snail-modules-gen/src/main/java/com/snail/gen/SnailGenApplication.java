package com.snail.gen;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import com.snail.common.core.utils.ServerUtil;
import org.springframework.boot.SpringApplication;

/**
 * 代码生成
 * 
 * @author snail
 */
@SnailCloudApplication
public class SnailGenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SnailGenApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail代码生成服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _\n" +
                "                   (_)| |\n" +
                " ___  _ ___   __ _  _ | |       __ _   ___  _ __\n" +
                "/ __|| '_  \\ / _` || || |      / _` | / _ \\| '_ \\\n" +
                "\\__ \\| | | || (_| || || |     | (_| ||  __/| | | |\n" +
                "|___/|_| |_| \\__,_||_||_|      \\__, | \\___||_| |_|\n" +
                "                                __/ |\n" +
                "                               |___/\n" +
                ServerUtil.printInfo());
    }
}
