package com.snail.gen.service;


import com.snail.gen.dto.DataSourceDto;
import com.snail.gen.query.DynamicTableQuery;

import java.util.List;

/**
 * @ClassName: DynamicTableService
 * =================================================
 * @Description: 动态业务表功能业务接口
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 22:39
 * @Version: V1.0
 */
public interface DynamicTableService {
    /**
     * 分页查询当前配置数据源
     *
     * @param dynamicTableQuery
     * @return
     */
    List<DataSourceDto> listDataSource(DynamicTableQuery dynamicTableQuery);

    /**
     * 添加数据源
     *
     * @param dataSourceDto
     * @return
     */
    boolean addDataSource(DataSourceDto dataSourceDto);
}
