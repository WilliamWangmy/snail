package com.snail.gen.mapper;

import com.snail.gen.dto.TableInfoDto;
import com.snail.gen.query.GenTableQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName: DynamicTableMapper
 * =================================================
 * @Description: 动态业务表功能eMapper接口
 * =================================================
 * CreateInfo:
 * @Author: Snail
 * @CreateDate: 2022/4/30 23:51
 * @Version: V1.0
 */
@Mapper
public interface DynamicTableMapper {

    /**
     * 获取表信息
     * @param genTable
     * @return
     */
    List<TableInfoDto> selectDbTableList(@Param("genTable") GenTableQuery genTable);
}
