package com.snail.gen.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 代码生成查询
 * @Author: Snail
 * @CreateDate: 2022/7/10 11:39
 * @Version: V1.0
 */
@Data
@ApiModel(value = "代码生成查询对象")
public class GenTableQuery extends BaseQuery {

    /**
     * 表名称
     */
    @ApiModelProperty(value = "表名称")
    private String tableName;

    /**
     * 表描述
     */
    @ApiModelProperty(value = "表描述")
    private String tableComment;

    /**
     * 连接池名称
     */
    @ApiModelProperty(value = "连接池名称")
    private String pollName;
}
