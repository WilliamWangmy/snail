package ${packageName}.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Description: ${functionName}对象 ${tableName}
 * @Author: ${author}
 * @CreateDate: ${datetime}
 * @Version: V1.0
 */
#if($table.crud || $table.sub)
#set($Entity="BaseEntity")
#elseif($table.tree)
#set($Entity="TreeEntity")
#end
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "${functionName}")
@TableName("${tableName}")
public class ${ClassName} implements Serializable{
    private static final long serialVersionUID = 1L;

#set($index=0)
#foreach ($column in $columns)
    /** $column.columnComment */
    @ApiModelProperty(value = "$column.columnComment")
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($parentheseIndex != -1)
#elseif($column.javaType == 'LocalDateTime')
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
#else
#end
#end
#if($column.pk)
    @TableId(type = IdType.ASSIGN_UUID)
#end
#if($column.delFlag == 1)
    @TableLogic
#end
#if($column.insertFill==1)
    @TableField(fill = FieldFill.INSERT)
#end
#if($column.updateFill==1)
    @TableField(fill = FieldFill.UPDATE)
#end
#if($column.isList == '1')
    @Excel(value = "${column.columnComment}",index = ${index},template = true)
    #set($index = $index+1)
#end
#if($column.isRequired == '1')
    @NotEmpty(message = "${column.columnComment}不能为空!")
#end
    private $column.javaType $column.javaField;
#end

}
