package ${packageName}.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.web.page.PageResult;
import ${packageName}.domain.${ClassName};
import ${packageName}.mapper.${ClassName}Mapper;
import ${packageName}.query.${ClassName}Query;
import ${packageName}.service.I${ClassName}Service;
import org.springframework.stereotype.Service;
import com.snail.common.core.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description:  ${functionName}Service业务层处理
 * @Author: ${author}
 * @CreateDate: ${datetime}
 * @Version: V1.0
 */
@Service
public class ${ClassName}ServiceImpl extends ServiceImpl<${ClassName}Mapper, ${ClassName}> implements I${ClassName}Service {

    @Override
    public PageResult<${ClassName}> select${ClassName}Page(${ClassName}Query query) {
        Page<${ClassName}> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<${ClassName}> queryWrapper = new LambdaQueryWrapper<>();
#foreach ($column in $columns)
    #if($column.isQuery == '1')
        //$column.columnComment
        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
        #if($column.javaType == 'String')
            #if($column.queryType == 'EQ')
                queryWrapper.eq(StringUtils.isNotEmpty(query.get${AttrName}()),${ClassName}::get${AttrName},query.get${AttrName}());
            #elseif($column.queryType == 'LIKE')
                queryWrapper.like(StringUtils.isNotEmpty(query.get${AttrName}()),${ClassName}::get${AttrName},query.get${AttrName}());
            #end
        #elseif($column.javaType == 'LocalDateTime')
            #if($column.queryType == 'GTE')
                queryWrapper.ge(StringUtils.isNotEmpty(query.get${AttrName}()),${ClassName}::get${AttrName},query.get${AttrName}());
            #elseif($column.queryType == 'LTE')
                queryWrapper.le(StringUtils.isNotEmpty(query.get${AttrName}()),${ClassName}::get${AttrName},query.get${AttrName}());
            #elseif($column.queryType == 'EQ')
                queryWrapper.eq(StringUtils.isNotEmpty(query.get${AttrName}()),${ClassName}::get${AttrName},query.get${AttrName}());
            #end
        #elseif($column.javaType == 'BigDecimal')
            #if($column.queryType == 'GTE')
                queryWrapper.ge(query.get${AttrName}()!=null,${ClassName}::get${AttrName},query.get${AttrName}());
            #elseif($column.queryType == 'LTE')
                queryWrapper.le(query.get${AttrName}()!=null,${ClassName}::get${AttrName},query.get${AttrName}());
            #elseif($column.queryType == 'EQ')
                queryWrapper.eq(query.get${AttrName}()!=null,${ClassName}::get${AttrName},query.get${AttrName}());
            #end
        #end
    #end
#end
        queryWrapper.ge(StringUtils.isNotEmpty(query.getBeginTime()),${ClassName}::getCreateTime,query.getBeginTime());
        queryWrapper.le(StringUtils.isNotEmpty(query.getEndTime()),${ClassName}::getCreateTime,query.getEndTime());
        queryWrapper.orderByDesc(${ClassName}::getCreateTime);
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public ${ClassName} get${ClassName}ById(String ${pkColumn.javaField}) {
        return getById(${pkColumn.javaField});
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insert${ClassName}(${ClassName} ${className}) {
        save(${className});
        #set($AttrName=$pkColumn.javaField.substring(0,1).toUpperCase() + ${pkColumn.javaField.substring(1)})
        return ${className}.get${AttrName}();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update${ClassName}(${ClassName} ${className}) {
        return updateById(${className});
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete${ClassName}ByIds(String[] ${pkColumn.javaField}s) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(${pkColumn.javaField}s));
        return batch == ${pkColumn.javaField}s.length;
    }

    @Override
    public List<${ClassName}> select${ClassName}List(${ClassName}Query query){
        LambdaQueryWrapper<${ClassName}> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<${ClassName}> dataList) {
        saveBatch(dataList);
    }
}
