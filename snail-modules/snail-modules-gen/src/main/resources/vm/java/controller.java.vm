package ${packageName}.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import ${packageName}.domain.${ClassName};
import ${packageName}.query.${ClassName}Query;
import ${packageName}.service.I${ClassName}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: ${functionName}Controller
 * @Author: ${author}
 * @CreateDate: ${datetime}
 * @Version: V1.0
 */
@Api(value = "${functionName}", tags = "${functionName}")
@RestController
@RequestMapping("/${businessName}")
public class ${ClassName}Controller {
    @Autowired
    private I${ClassName}Service ${className}Service;

    @Log(title = "查询${functionName}分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询${functionName}列表")
    @RequiresPermissions("${permissionPrefix}:query")
    @GetMapping("/page")
#if($table.crud || $table.sub)
    public R<PageResult<${ClassName}>> get${ClassName}Page(${ClassName}Query query)
    {
        return R.ok(${className}Service.select${ClassName}Page(query));
    }
#elseif($table.tree)
    public AjaxResult list(${ClassName} ${className})
    {
        List<${ClassName}> list = ${className}Service.select${ClassName}List(${className});
        return AjaxResult.success(list);
    }
#end

    @Log(title = "获取${functionName}详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取${functionName}详细信息")
    @RequiresPermissions("${permissionPrefix}:query")
    @GetMapping(value = "/{${pkColumn.javaField}}")
    public R<${ClassName}> get${ClassName}ById(@PathVariable("${pkColumn.javaField}") ${pkColumn.javaType} ${pkColumn.javaField})
    {
        return R.ok(${className}Service.get${ClassName}ById(${pkColumn.javaField}));
    }

    @ApiOperation(value = "新增${functionName}")
    @RequiresPermissions("${permissionPrefix}:add")
    @Log(title = "新增${functionName}", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> save${ClassName}(@Validated @RequestBody ${ClassName} ${className}) {
        return R.ok(${className}Service.insert${ClassName}(${className}));
    }

    @ApiOperation(value = "修改${functionName}")
    @RequiresPermissions("${permissionPrefix}:edit")
    @Log(title = "修改${functionName}", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> update${ClassName}(@Validated @RequestBody ${ClassName} ${className}) {
        return R.ok(${className}Service.update${ClassName}(${className}));
    }


    @ApiOperation(value = "删除${functionName}")
    @RequiresPermissions("${permissionPrefix}:delete")
    @Log(title = "删除${functionName}", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{${pkColumn.javaField}s}")
    public R<Boolean> delete${ClassName}ByIds(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s)
    {
        return R.ok(${className}Service.delete${ClassName}ByIds(${pkColumn.javaField}s));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("${permissionPrefix}:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "${functionName}导入模板", ${ClassName}.class);
    }

    @ApiOperation(value = "导出${functionName}数据")
    @Log(title = "导出${functionName}", businessType = BusinessType.EXPORT)
    @RequiresPermissions("${permissionPrefix}:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, ${ClassName}Query query) {
        EasyExcelUtils.writeExcel(response, "${functionName}信息",${ClassName}.class,${className}Service.select${ClassName}List(query));
    }

    @ApiOperation(value = "导入${functionName}数据")
    @Log(title = "导入${functionName}", businessType = BusinessType.IMPORT)
    @RequiresPermissions("${permissionPrefix}:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, ${ClassName}.class,${className}Service);
    }
}
