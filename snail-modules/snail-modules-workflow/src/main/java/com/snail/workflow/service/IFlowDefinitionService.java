package com.snail.workflow.service;

import com.snail.common.core.web.page.PageResult;
import com.snail.workflow.api.domain.dto.FlowDefinitionDto;
import com.snail.workflow.api.domain.dto.FlowProcDefDto;
import com.snail.workflow.api.query.FlowProcDefQuery;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Description: 流程定义接口
 * @Author: Snail
 * @CreateDate: 2022/7/26 21:14
 * @Version: V1.0
 */
public interface IFlowDefinitionService {
    /**
     * 部署流程（导入流程文件）
     *
     * @param wfName     流程名称
     * @param wfCategory 流程分类
     * @param file       流程文件
     * @return 成功/失败
     */
    boolean importFile(String wfName, String wfCategory, MultipartFile file);

    /**
     * 部署流程（导入流程xml）
     *
     * @param flowDefinitionDto 流程数据
     * @return 成功/失败
     */
    boolean importXml(FlowDefinitionDto flowDefinitionDto);

    /**
     * 读取流程xml信息
     *
     * @param deployId 流程定义id
     * @return 流程数据
     */
    FlowDefinitionDto readXml(String deployId);

    /**
     * 读取流程图片文件
     *
     * @param deployId 流程定义id
     * @return 流程文件输入流
     */
    InputStream readImage(String deployId);

    /**
     * 读取流程图片文件
     *
     * @param procDefKey 流程定义id
     * @return 流程文件输入流
     */
    InputStream readImageByKey(String procDefKey);

    /**
     * 查询流程定义列表
     *
     * @param flowProcDefQuery 流程定义查询参数
     * @return 流程定义数据
     */
    PageResult<FlowProcDefDto> listFlowProcDef(FlowProcDefQuery flowProcDefQuery);

    /**
     * 根据流程定义key查询流程定义
     * @param procDefKey
     * @return
     */
    List<FlowProcDefDto> listFlowProcDefByKey(String procDefKey);

    String deploy(String wfName, String wfCategory, InputStream in) throws IOException;

}
