package com.snail.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.workflow.api.domain.entity.WorkflowBusiness;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 流程表单信息;(w_workflow_business)表数据库访问层
 * @Author: Snail
 * @CreateDate: 2022/7/20 16:42
 * @Version: V1.0
 */
@Mapper
public interface WorkflowBusinessMapper extends BaseMapper<WorkflowBusiness> {
}
