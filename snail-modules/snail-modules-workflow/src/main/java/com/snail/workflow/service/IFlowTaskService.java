package com.snail.workflow.service;

import com.snail.common.core.web.page.PageResult;
import com.snail.workflow.api.domain.dto.HisActivityInstanceDto;
import com.snail.workflow.api.domain.dto.TaskDataDto;
import com.snail.workflow.api.domain.dto.TaskNextNodeDto;
import com.snail.workflow.api.domain.dto.TaskRecordsDto;
import com.snail.workflow.api.query.FlowTaskQuery;
import org.flowable.bpmn.model.EndEvent;
import org.flowable.engine.impl.persistence.entity.CommentEntityImpl;
import org.flowable.task.api.Task;
import org.flowable.task.service.impl.persistence.entity.HistoricTaskInstanceEntityImpl;

import java.util.List;

/**
 * @Description: 流程任务接口
 * @Author: Snail
 * @CreateDate: 2022/7/26 22:59
 * @Version: V1.0
 */
public interface IFlowTaskService {
    /**
     * 查询待办
     *
     * @param flowTaskQuery 查询参数
     * @return 待办数据
     */
    PageResult<TaskDataDto> findTodo(FlowTaskQuery flowTaskQuery);

    /**
     * 获取当前任务的下一节点信息
     *
     * @param task 当前任务
     * @param type 操作类型
     * @return 下一节点信息
     */
    List<TaskNextNodeDto> getNextNode(Task task, String type);

    /**
     * 查询已办
     *
     * @param flowTaskQuery 查询参数
     * @return 已办数据
     */
    PageResult<TaskDataDto> findDone(FlowTaskQuery flowTaskQuery);

    /**
     * 查询审批记录
     *
     * @param instanceId 流程实例id
     * @return 审批记录
     */
    List<TaskRecordsDto> findRecords(String instanceId);

    /**
     * 校验目标节点和当前节点是否存在链接先
     *
     * @param sourceElementKey  当前节点
     * @param targetElementKey  目标节点
     * @param processInstanceId 流程实例id
     * @return
     */
    boolean checkCurrentToTargetElement(String sourceElementKey, String targetElementKey, String processInstanceId);

    /**
     * 获取当前人常用审批意见
     *
     * @param userId 用户id
     * @param type   操作类型
     * @return
     */
    List<String> getCommonUseComment(String userId, String type);


    /**
     * 获取结束节点
     *
     * @param task
     * @return
     */
    EndEvent getEndEventNode(Task task);

    /**
     * 批量插入历史活动记录
     *
     * @param dtoList
     */
    void insertBatchHisActInstance(List<HisActivityInstanceDto> dtoList);

    /**
     * 根据流程实例获取流程任务
     *
     * @param instanceId 流程实例id
     * @param userId     用户id
     * @return 流程任务
     */
    Task getTaskByInsId(String instanceId, String userId);

    /**
     * 批量保存历史任务记录
     * @param dtoList
     */
    void insertBatchHisTaskInst(List<HistoricTaskInstanceEntityImpl> dtoList);

    /**
     * 保持历史任务记录
     * @param historicTaskInstance
     */
    void insertHisTaskInst(HistoricTaskInstanceEntityImpl historicTaskInstance);

    /**
     * 保持审批意见
     * @param commentEntity
     */
    void saveComment(CommentEntityImpl commentEntity);
}
