package com.snail.workflow;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import com.snail.common.core.utils.ServerUtil;
import org.springframework.boot.SpringApplication;

/**
 * @Description: 流程服务启动类
 * @Author: Snail
 * @CreateDate: 2023/7/10 14:08
 * @Version: V1.0
 */
@SnailCloudApplication
public class SnailWorkFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailWorkFlowApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail流程服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _                                        __  _\n" +
                "                   (_)| |                                _   _  / _|| |\n" +
                " ___  _ ___   __ _  _ | |     __   ____   __ ___   _ __ | | / /| |_ | |  ___ __   ____   __\n" +
                "/ __|| '_  \\ / _` || || |     \\ \\ / /\\ \\ / // _ \\ | '__|| |/ / |  _|| | / _ \\\\ \\ / /\\ \\ / /\n" +
                "\\__ \\| | | || (_| || || |      \\ V /  \\ V /| (_) || |   | |\\ \\ | |  | || (_) |\\ V /  \\ V /\n" +
                "|___/|_| |_| \\__,_||_||_|       \\_/    \\_/  \\___/ |_|   |_| \\_\\|_|  |_| \\___/  \\_/    \\_/\n" +
                ServerUtil.printInfo());
    }
}
