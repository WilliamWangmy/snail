package com.snail.workflow.service;

import com.snail.workflow.api.domain.dto.TaskInitWorkflowDto;
import org.flowable.engine.runtime.ProcessInstance;

/**
 * @Description: 流程实例接口
 * @Author: Snail
 * @CreateDate: 2022/7/26 16:30
 * @Version: V1.0
 */
public interface IFlowInstanceService {

    /**
     * 根据流程定义key启动流程实例
     *
     * @param taskInitWorkflowDto 流程表单数据
     * @return
     */
    ProcessInstance startWorkflowInstanceByDefKey(TaskInitWorkflowDto taskInitWorkflowDto);
}
