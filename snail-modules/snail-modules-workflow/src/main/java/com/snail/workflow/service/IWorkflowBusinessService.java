package com.snail.workflow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.workflow.api.domain.entity.WorkflowBusiness;

/**
 * @Description: 流程表单信息;(w_workflow_business)表服务接口
 * @Author: Snail
 * @CreateDate: 2022/7/20 16:46
 * @Version: V1.0
 */
public interface IWorkflowBusinessService extends IService<WorkflowBusiness> {

    /**
     * 根据表单id获取流程表单信息
     *
     * @param bizId 流程表单id
     * @return
     */
    WorkflowBusiness getWorkflowBusinessByBizId(String bizId);

    /**
     * 保存流程表单信息
     * @param workflowBusiness 流程表单信息
     */
    void createWorkflowBusiness(WorkflowBusiness workflowBusiness);

    /**
     * 更新流程业务数据
     * @param workflowBusiness 流程业务数据
     */
    void updateWorkflowBusiness(WorkflowBusiness workflowBusiness);
}
