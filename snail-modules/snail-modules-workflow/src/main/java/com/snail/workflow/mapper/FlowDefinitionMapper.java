package com.snail.workflow.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.snail.workflow.api.domain.dto.FlowProcDefDto;
import com.snail.workflow.api.query.FlowProcDefQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description: 流程定义mapper
 * @Author: Snail
 * @CreateDate: 2022/7/26 22:14
 * @Version: V1.0
 */
@Mapper
public interface FlowDefinitionMapper {
    /**
     * 查询流程定义列表
     * @param page
     * @param flowProcDefQuery
     * @return
     */
    Page<FlowProcDefDto> listFlowProcDef(@Param("page") Page<FlowProcDefDto> page, @Param("flowProcDefQuery") FlowProcDefQuery flowProcDefQuery);

    /**
     * 根据流程定义key查询流程定义
     * @param procDefKey
     * @return
     */
    List<FlowProcDefDto> listFlowProcDefByKey(@Param("procDefKey") String procDefKey);
}
