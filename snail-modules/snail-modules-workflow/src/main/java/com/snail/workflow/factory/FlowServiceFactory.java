package com.snail.workflow.factory;

import lombok.Getter;
import org.flowable.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Description: flowable流程引擎注入封装
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:50
 * @Version: V1.0
 */
@Getter
@Component
public class FlowServiceFactory {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    protected RepositoryService repositoryService;

    @Resource
    protected RuntimeService runtimeService;

    @Resource
    protected IdentityService identityService;

    @Resource
    protected TaskService taskService;

    @Resource
    protected FormService formService;

    @Resource
    protected HistoryService historyService;

    @Resource
    protected ManagementService managementService;

    @Qualifier("processEngine")
    @Resource
    protected ProcessEngine processEngine;
}
