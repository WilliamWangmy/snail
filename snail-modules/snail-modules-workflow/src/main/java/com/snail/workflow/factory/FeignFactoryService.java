package com.snail.workflow.factory;

import com.snail.common.core.constant.Constants;
import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.domain.R;
import com.snail.system.api.RemoteUserService;
import com.snail.system.api.dto.SysUserDto;
import com.snail.workflow.exception.WorkflowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description: feign接口调用工厂类
 * @Author: Snail
 * @CreateDate: 2022/8/8 9:08
 * @Version: V1.0
 */
@Component
public class FeignFactoryService {

    @Autowired
    private RemoteUserService remoteUserService;

    /**
     * 根据id获取用户信息
     * @param userId
     * @return
     */
    public SysUserDto getUserInfoById(String userId) {
        // TODO 三种方案：根据feign获取,根据缓存（redis、ES、MongoDB）获取,根据数据库同步工具（canal）直接到流程库获取
        R<SysUserDto> r = remoteUserService.getUserById(userId, SecurityConstants.INNER);
        if (!Constants.SUCCESS.equals(r.getCode())) {
            throw new WorkflowException("获取用户信息失败：" + r.getMsg());
        }
        if (r.getData() == null) {
            throw new WorkflowException("用户不存在!");
        }
        return r.getData();
    }

    /**
     * 获取用户信息
     *
     * @param userIds
     * @return
     */
    public List<SysUserDto> getUserInfoByIds(String userIds) {
        // TODO 三种方案：根据feign获取,根据缓存（redis、ES、MongoDB）获取,根据数据库同步工具（canal）直接到流程库获取
        R<List<SysUserDto>> r = remoteUserService.getUserList(userIds, SecurityConstants.INNER);
        if (!Constants.SUCCESS.equals(r.getCode())) {
            throw new WorkflowException("获取用户信息失败：" + r.getMsg());
        }
        if (r.getData() == null) {
            throw new WorkflowException("用户不存在!");
        }
        return r.getData();
    }

    /**
     * 根据角色获取用户信息
     *
     * @param roleIds
     * @return
     */
    public List<SysUserDto> selectUserRole(String roleIds) {
        // TODO 三种方案：根据feign获取,根据缓存（redis、ES、MongoDB）获取,根据数据库同步工具（canal）直接到流程库获取
        R<List<SysUserDto>> r = remoteUserService.getRoleUserInfo(roleIds, SecurityConstants.INNER);
        if (!Constants.SUCCESS.equals(r.getCode())) {
            throw new WorkflowException("获取用户信息失败：" + r.getMsg());
        }
        if (r.getData() == null) {
            throw new WorkflowException("用户不存在!");
        }
        return r.getData();
    }
}
