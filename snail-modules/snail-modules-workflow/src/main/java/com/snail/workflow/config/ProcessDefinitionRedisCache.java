package com.snail.workflow.config;

import com.snail.common.core.utils.StringUtils;
import com.snail.common.redis.service.RedisService;
import com.snail.workflow.api.constant.WorkflowConstants;
import org.flowable.common.engine.impl.persistence.deploy.DeploymentCache;
import org.flowable.engine.impl.persistence.deploy.ProcessDefinitionCacheEntry;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

/**
 * @Description: redis缓存策略
 * @Author: Snail
 * @CreateDate: 2023/9/11 13:52
 * @Version: V1.0
 */
public class ProcessDefinitionRedisCache implements DeploymentCache<ProcessDefinitionCacheEntry> {

    private RedisService redisService;

    public ProcessDefinitionRedisCache(RedisService redisService) {
        this.redisService = redisService;
    }

    @Override
    public ProcessDefinitionCacheEntry get(String id) {
        return redisService.getCacheObject(StringUtils.format(WorkflowConstants.PROCESS_DEFINITION_CACHE_KEY,id));
    }

    @Override
    public boolean contains(String id) {
        return redisService.hasKey(StringUtils.format(WorkflowConstants.PROCESS_DEFINITION_CACHE_KEY,id));
    }

    @Override
    public void add(String id, ProcessDefinitionCacheEntry processDefinitionCacheEntry) {
        redisService.setCacheObject(StringUtils.format(WorkflowConstants.PROCESS_DEFINITION_CACHE_KEY,id),processDefinitionCacheEntry);
    }

    @Override
    public void remove(String id) {
        redisService.deleteObject(StringUtils.format(WorkflowConstants.PROCESS_DEFINITION_CACHE_KEY,id));
    }

    @Override
    public void clear() {
        System.out.println("clear.................");
    }

    @Override
    public Collection<ProcessDefinitionCacheEntry> getAll() {
        System.out.println("getAll.................");
        return null;
    }

    @Override
    public int size() {
        return 0;
    }
}
