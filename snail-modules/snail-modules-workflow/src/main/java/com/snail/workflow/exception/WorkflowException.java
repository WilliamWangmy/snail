package com.snail.workflow.exception;

/**
 * @Description: 流程全局异常
 * @Author: Snail
 * @CreateDate: 2022/7/14 17:54
 * @Version: V1.0
 */
public class WorkflowException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     * <p>
     * 和 {@link CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public WorkflowException() {
    }

    public WorkflowException(String message) {
        this.message = message;
    }

    public WorkflowException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public WorkflowException setMessage(String message) {
        this.message = message;
        return this;
    }

    public WorkflowException setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
        return this;
    }
}
