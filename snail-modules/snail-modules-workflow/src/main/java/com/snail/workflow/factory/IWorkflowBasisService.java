package com.snail.workflow.factory;

import com.snail.workflow.api.domain.dto.TaskSubmitDto;

/**
 * @Description: 业务流程继承接口
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:58
 * @Version: V1.0
 */
public interface IWorkflowBasisService {

    /**
     * 流程提交前处理
     *
     * @return 成功/失败
     */
    default boolean beforeSubmit(TaskSubmitDto taskSubmitDto) {
        return true;
    }

    /**
     * 流程提交后处理
     *
     * @return 成功/失败
     */
    default boolean afterSubmit(TaskSubmitDto taskSubmitDto) {
        return true;
    }

    /**
     * 流程驳回前处理
     *
     * @return 成功/失败
     */
    default boolean beforeReject(TaskSubmitDto taskSubmitDto) {
        return true;
    }

    /**
     * 流程驳回后处理
     *
     * @return 成功/失败
     */
    default boolean afterReject(TaskSubmitDto taskSubmitDto) {
        return true;
    }

    /**
     * 流程作废/取消前处理
     *
     * @return 成功/失败
     */
    default boolean beforeCancel(TaskSubmitDto taskSubmitDto) {
        return true;
    }

    /**
     * 流程作废/取消后处理
     *
     * @return 成功/失败
     */
    default boolean afterCancel(TaskSubmitDto taskSubmitDto) {
        return true;
    }
}
