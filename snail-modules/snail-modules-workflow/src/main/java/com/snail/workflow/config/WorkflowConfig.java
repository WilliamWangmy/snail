package com.snail.workflow.config;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.snail.common.redis.service.RedisService;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 流程配置类
 * @Author: Snail
 * @CreateDate: 2022/8/10 10:18
 * @Version: V1.0
 */
@Configuration
public class WorkflowConfig implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {

    @Autowired
    private RedisService redisService;

    @Override
    public void configure(SpringProcessEngineConfiguration engineConfiguration) {
        engineConfiguration.setActivityFontName("宋体");
        engineConfiguration.setLabelFontName("宋体");
        engineConfiguration.setAnnotationFontName("宋体");
        //id生成策略
        engineConfiguration.setIdGenerator(IdWorker::get32UUID);
        //自定义缓存策略
//        engineConfiguration.setProcessDefinitionCache(new ProcessDefinitionRedisCache(redisService));
    }
}
