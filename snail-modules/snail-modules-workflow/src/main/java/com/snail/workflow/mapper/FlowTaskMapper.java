package com.snail.workflow.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.snail.workflow.api.domain.dto.HisActivityInstanceDto;
import com.snail.workflow.api.domain.dto.TaskDataDto;
import com.snail.workflow.api.domain.dto.TaskRecordsDto;
import com.snail.workflow.api.query.FlowTaskQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.flowable.engine.impl.persistence.entity.CommentEntityImpl;
import org.flowable.task.service.impl.persistence.entity.HistoricTaskInstanceEntityImpl;

import java.util.List;

/**
 * @Description: 流程任务数据层接口
 * @Author: Snail
 * @CreateDate: 2022/7/27 9:22
 * @Version: V1.0
 */
@Mapper
public interface FlowTaskMapper {

    /**
     * 查询待办
     *
     * @param page          分页
     * @param flowTaskQuery 查询参数
     * @return 分页数据
     */
    Page<TaskDataDto> findTodo(@Param("page") Page<TaskDataDto> page, @Param("query") FlowTaskQuery flowTaskQuery);

    /**
     * 查询已办
     *
     * @param page          分页
     * @param flowTaskQuery 查询参数
     * @return 分页数据
     */
    Page<TaskDataDto> findDone(@Param("page") Page<TaskDataDto> page, @Param("query") FlowTaskQuery flowTaskQuery);

    /**
     * 查询审批记录
     *
     * @param bizId 业务id
     * @return 审批记录
     */
    List<TaskRecordsDto> findRecords(@Param("bizId") String bizId);

    /**
     * 获取当前人常用审批意见
     * @param userId 用户id
     * @param type 操作类型
     * @return
     */
    List<String> getCommonUseComment(@Param("userId") String userId, @Param("type") String type);

    /**
     * 批量插入历史活动记录
     * @param dtoList
     */
    void insertBatchHisActInstance(@Param("dtoList") List<HisActivityInstanceDto> dtoList);
    /**
     * 批量保存历史任务记录
     * @param dtoList
     */
    void insertBatchHisTaskInst(@Param("dtoList") List<HistoricTaskInstanceEntityImpl> dtoList);

    /**
     * 保持历史人物记录
     * @param historicTaskInstance
     */
    void insertHisTaskInst(HistoricTaskInstanceEntityImpl historicTaskInstance);

    /**
     * 保持审批意见
     * @param commentEntity
     */
    void saveComment(CommentEntityImpl commentEntity);
}
