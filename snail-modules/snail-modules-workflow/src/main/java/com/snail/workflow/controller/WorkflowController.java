package com.snail.workflow.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.workflow.api.domain.dto.*;
import com.snail.workflow.api.domain.entity.WorkflowBusiness;
import com.snail.workflow.api.query.FlowTaskQuery;
import com.snail.workflow.service.IWorkflowBusinessService;
import com.snail.workflow.service.IWorkflowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * @Description: 流程任务管理
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:12
 * @Version: V1.0
 */
@Api(tags = "流程任务管理", value = "流程任务管理")
@RestController
@RequestMapping("/task")
public class WorkflowController {

    @Autowired
    private IWorkflowService workflowService;
    @Autowired
    private IWorkflowBusinessService workflowBusinessService;

    @ApiOperation(value = "获取流程数据")
    @GetMapping("/getWorkflow/{bizId}")
    public R<WorkflowBusiness> getWorkflow(@PathVariable String bizId){
        return R.ok(workflowBusinessService.getWorkflowBusinessByBizId(bizId));
    }

    @ApiOperation(value = "初始化流程任务")
    @PostMapping("/initWorkflow")
    public R<WorkflowDto> initWorkflow(@RequestBody @Valid TaskInitWorkflowDto taskInitWorkflowDto) {
        return R.ok(workflowService.initWorkflow(taskInitWorkflowDto));
    }

    @ApiOperation(value = "获取审批节点")
    @GetMapping("/initNextNode")
    public R<TaskParamDto> initNextNode(@Valid TaskInitNextDto taskInitNextDto) {
        return R.ok(workflowService.initNextNode(taskInitNextDto));
    }

    @ApiOperation(value = "审批流程任务")
    @PostMapping("/submitTask")
    public R<WorkflowDto> submitTask(@RequestBody @Valid TaskSubmitDto taskSubmitDto) {
        return R.ok(workflowService.submitTask(taskSubmitDto));
    }

    @ApiOperation(value = "驳回流程任务")
    @PostMapping("/rejectTask")
    public R<WorkflowDto> rejectTask(@RequestBody @Valid TaskSubmitDto taskSubmitDto) {
        return R.ok(workflowService.rejectTask(taskSubmitDto));
    }

    @ApiOperation(value = "取消流程任务")
    @PostMapping("/cancelTask")
    public R<WorkflowDto> cancelTask(@RequestBody @Valid TaskSubmitDto taskSubmitDto) {
        return R.ok(workflowService.cancelTask(taskSubmitDto));
    }

    @ApiOperation(value = "委派流程任务")
    @PostMapping("/delegateTask")
    public R<WorkflowDto> delegateTask(@RequestBody @Valid TaskSubmitDto taskSubmitDto) {
        return R.ok(workflowService.delegateTask(taskSubmitDto));
    }

    @ApiOperation(value = "转办流程任务")
    @PostMapping("/turnToDoTask")
    public R<WorkflowDto> turnToDoTask(@RequestBody @Valid TaskSubmitDto taskSubmitDto) {
        return R.ok(workflowService.turnToDoTask(taskSubmitDto));
    }

    @ApiOperation(value = "撤回流程任务")
    @PostMapping("/withdrawTask")
    public R<Boolean> withdrawTask(@RequestBody @Valid TaskWithdrawDto taskWithdrawDto){
        return R.ok(workflowService.withdrawTask(taskWithdrawDto));
    }

    @ApiOperation(value = "查询我的待办")
    @GetMapping("/findTodo")
    public R<PageResult<TaskDataDto>> findTodo(FlowTaskQuery flowTaskQuery) {
        return R.ok(workflowService.findTodo(flowTaskQuery));
    }

    @ApiOperation(value = "查询我的已办")
    @GetMapping("/findDone")
    public R<PageResult<TaskDataDto>> findDone(FlowTaskQuery flowTaskQuery) {
        return R.ok(workflowService.findDone(flowTaskQuery));
    }

    @ApiOperation(value = "查询审批记录")
    @GetMapping("/findRecords/{bizId}")
    public R<List<TaskRecordsDto>> findRecords(@PathVariable("bizId") String bizId) {
        return R.ok(workflowService.findRecords(bizId));
    }

    @ApiOperation(value = "获取流程图")
    @GetMapping("/image/{bizId}")
    public R<byte[]> readImage(@PathVariable("bizId") String bizId, HttpServletResponse response) {
        return R.ok(workflowService.readImage(bizId, response));
    }
}
