package com.snail.workflow.service;

import com.snail.common.core.web.page.PageResult;
import com.snail.workflow.api.domain.dto.*;
import com.snail.workflow.api.query.FlowTaskQuery;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: 流程任务业务接口类
 * @Author: Snail
 * @CreateDate: 2022/7/26 15:47
 * @Version: V1.0
 */
public interface IWorkflowService {
    /**
     * 启动流程任务
     *
     * @param taskInitWorkflowDto 流程任务对象
     * @return 流程数据
     */
    WorkflowDto initWorkflow(TaskInitWorkflowDto taskInitWorkflowDto);

    /**
     * 初始化流程提交参数
     *
     * @param taskInitNextDto 查询参数
     * @return 流程参数
     */
    TaskParamDto initNextNode(TaskInitNextDto taskInitNextDto);

    /**
     * 流程任务提交
     *
     * @param taskSubmitDto 流程提交参数
     * @return 流程数据
     */
    WorkflowDto submitTask(TaskSubmitDto taskSubmitDto);

    /**
     * 流程任务驳回
     *
     * @param taskSubmitDto 流程提交参数
     * @return 流程数据
     */
    WorkflowDto rejectTask(TaskSubmitDto taskSubmitDto);

    /**
     * 流程任务取消/作废
     *
     * @param taskSubmitDto 流程提交参数
     * @return 流程数据
     */
    WorkflowDto cancelTask(TaskSubmitDto taskSubmitDto);

    /**
     * 查询流程待办任务
     *
     * @param flowTaskQuery 查询参数
     * @return 待办数据
     */
    PageResult<TaskDataDto> findTodo(FlowTaskQuery flowTaskQuery);

    /**
     * 查询流程已办任务
     *
     * @param flowTaskQuery 查询参数
     * @return 已办数据
     */
    PageResult<TaskDataDto> findDone(FlowTaskQuery flowTaskQuery);

    /**
     * 查询流程审批记录
     *
     * @param bizId 业务id
     * @return 流程记录数据
     */
    List<TaskRecordsDto> findRecords(String bizId);

    /**
     * 委派流程任务
     *
     * @param taskSubmitDto 任务信息
     * @return 流程信息
     */
    WorkflowDto delegateTask(TaskSubmitDto taskSubmitDto);

    /**
     * 读取流程图
     *
     * @param bizId 业务id
     */
    byte[] readImage(String bizId, HttpServletResponse response);

    /**
     * 转办流程任务
     *
     * @param taskSubmitDto 提交参数
     * @return 流程信息
     */
    WorkflowDto turnToDoTask(TaskSubmitDto taskSubmitDto);

    /**
     * 撤回流程任务
     *
     * @param taskWithdrawDto 撤回数据对象
     * @return 结果
     */
    boolean withdrawTask(TaskWithdrawDto taskWithdrawDto);
}
