package com.snail.auth;

import com.snail.common.core.utils.ServerUtil;
import com.snail.common.security.annotation.EnableSnailFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 认证授权中心
 *
 * @author snail
 */
@EnableSnailFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SnailAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(SnailAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Snail认证中心服务启动成功   ლ(´ڡ`ლ)ﾞ\n" +
                "                    _  _                    _    _\n" +
                "                   (_)| |                   | |  | |\n" +
                " ___  _ ___   __ _  _ | |       __ _  _   _ | |_ | |__\n" +
                "/ __|| '_  \\ / _` || || |      / _` || | | || __|| '_ \\\n" +
                "\\__ \\| | | || (_| || || |     | (_| || |_| || |_ | | | |\n" +
                "|___/|_| |_| \\__,_||_||_|      \\__,_| \\__,_| \\__||_| |_|\n"+
                ServerUtil.printInfo());
    }
}
