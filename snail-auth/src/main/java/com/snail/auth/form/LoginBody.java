package com.snail.auth.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户登录对象
 *
 * @author snail
 */
@Data
@ApiModel(value = "登录form表单")
public class LoginBody {

    @ApiModelProperty(value = "登录类型：account.账号密码，mobile.手机号")
    private String type = "account";
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "短信验证码")
    private String smsCode;

}
