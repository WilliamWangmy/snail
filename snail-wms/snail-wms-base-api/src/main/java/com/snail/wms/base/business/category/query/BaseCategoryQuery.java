package com.snail.wms.base.business.category.query;

import com.snail.common.core.web.domain.BaseQuery;
import com.snail.wms.base.constants.BaseConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.snail.common.core.utils.StringUtils;


/**
 * @Description: 产品分类查询对象
 * @Author: snail
 * @CreateDate: 2024-04-24
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "产品分类")
public class BaseCategoryQuery extends BaseQuery {
    @ApiModelProperty(value = "分类编码")
    private String categoryCode;
    @ApiModelProperty(value = "分类名称")
    private String categoryName;
    @ApiModelProperty(value = "父分类id")
    private String parentCategoryId = BaseConstants.CATEGORY_ROOT_ID;
    @ApiModelProperty(value = "状态：1.启用，0.禁用")
    private String categoryStatus;
    @ApiModelProperty(value = "分类全路径")
    private String categoryFullName;
}
