package com.snail.wms.base.business.sku.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: sku属性对象 base_sku_attr_relation
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@ApiModel(value = "sku属性")
@TableName("base_sku_attr_relation")
public class BaseSkuAttrRelation implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String skuId;
    /**
     * 属性id
     */
    @ApiModelProperty(value = "属性id")
    @NotEmpty(message = "属性id不能为空!")
    private String attrId;
    /**
     * 属性名称
     */
    @ApiModelProperty(value = "属性名称")
    @NotEmpty(message = "属性名称不能为空!")
    private String attrName;
    /**
     * 属性值id
     */
    @ApiModelProperty(value = "属性值id")
    @NotEmpty(message = "属性值id不能为空!")
    private String attrValueId;
    /**
     * 属性值
     */
    @ApiModelProperty(value = "属性值")
    @NotEmpty(message = "属性值不能为空!")
    private String attrValue;

}
