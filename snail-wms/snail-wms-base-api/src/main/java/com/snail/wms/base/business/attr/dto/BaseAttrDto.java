package com.snail.wms.base.business.attr.dto;

import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.converter.ListStringConverter;
import com.snail.wms.base.business.attr.domain.BaseAttr;
import com.snail.wms.base.business.attr.domain.BaseAttrValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description: 分类属性
 * @Author: Snail
 * @CreateDate: 2024/6/4 22:05
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "分类属性DTO")
public class BaseAttrDto extends BaseAttr {

    @ApiModelProperty(value = "属性值")
    @Excel(value = "属性值", index = 5, template = true,converter = ListStringConverter.class)
    @NotNull(message = "分类属性不能为空!")
    private List<String> attrValues;

    @ApiModelProperty(value = "属性值")
    private List<BaseAttrValue> valueList;
}
