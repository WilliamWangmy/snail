package com.snail.wms.base.business.district.query;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 行政区域查询
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "行政区域查询")
public class BaseDistrictQuery extends BaseQuery {

    /**
     * 城市编码
     */
    @ApiModelProperty(value = "城市编码")
    private String cityCode;
    /**
     * 区域编码
     */
    @ApiModelProperty(value = "区域编码")
    private String adCode;
    /**
     * 行政区名称
     */
    @ApiModelProperty(value = "行政区名称")
    private String name;
    /**
     * 行政区划级别
     */
    @ApiModelProperty(value = "行政区划级别")
    private String level;
    /**
     * 父区域编码
     */
    @ApiModelProperty(value = "父区域编码")
    private String parentAdCode;
}
