package com.snail.wms.base.business.spu.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import com.baomidou.mybatisplus.annotation.*;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.annotation.ExcelContentMerge;
import com.snail.common.excel.annotation.ExcelDataValidation;
import com.snail.common.excel.converter.BizStatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: spu信息对象 base_spu
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "spu信息")
@TableName("base_spu")
public class BaseSpu implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String spuId;
    /**
     * spu编码
     */
    @ApiModelProperty(value = "spu编码")
    @Excel(value = {"SPU信息","spu编码"}, index = 0, template = true)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    private String spuCode;
    /**
     * spu名称
     */
    @ApiModelProperty(value = "spu名称")
    @Excel(value = {"SPU信息","spu名称"}, index = 1, template = true)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    @NotEmpty(message = "spu名称不能为空!")
    private String spuName;
    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id")
    @NotEmpty(message = "分类id不能为空!")
    private String categoryId;
    /**
     * 分类编码
     */
    @ApiModelProperty(value = "分类编码")
    @NotEmpty(message = "分类编码不能为空!")
    @Excel(value = {"SPU信息","分类编码"}, index = 2, template = true)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    private String categoryCode;
    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称")
    @NotEmpty(message = "分类名称不能为空!")
    @Excel(value = {"SPU信息","分类名称"}, index = 3, template = true)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    private String categoryName;
    /**
     * 分类全路径
     */
    @ApiModelProperty(value = "分类全路径")
    @Excel(value = {"SPU信息","分类全路径"}, index = 4)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    @NotEmpty(message = "分类全路径不能为空!")
    private String categoryFullName;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @ExcelDataValidation(value = {"启用","禁用"},msg = "数据不在有效范围内")
    @Excel(value = {"SPU信息","状态"}, index = 5, template = true,converter = BizStatusConverter.class)
    @ExcelContentMerge
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    @NotEmpty(message = "状态不能为空!")
    private String spuStatus;
    /**
     * 逻辑删除：1.删除，0.未删除
     */
    @ApiModelProperty(value = "逻辑删除：1.删除，0.未删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;
    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createUserName;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    @TableField(fill = FieldFill.INSERT)
    private String deptId;
    /**
     * 组织全路径id
     */
    @ApiModelProperty(value = "组织全路径id")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullId;
    /**
     * 组织全路径
     */
    @ApiModelProperty(value = "组织全路径")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullName;
    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserName;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

}
