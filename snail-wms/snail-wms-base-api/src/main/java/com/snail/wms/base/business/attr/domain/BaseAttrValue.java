package com.snail.wms.base.business.attr.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: 属性值对象 base_attr_value
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@ApiModel(value = "属性值")
@TableName("base_attr_value")
public class BaseAttrValue implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String attrValueId;
    /**
     * 属性id
     */
    @ApiModelProperty(value = "属性id")
    @NotEmpty(message = "属性id不能为空!")
    private String attrId;
    /**
     * 属性值
     */
    @ApiModelProperty(value = "属性值")
    @NotEmpty(message = "属性值不能为空!")
    private String attrValue;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @NotEmpty(message = "状态不能为空!")
    private String status;

}
