package com.snail.wms.base.business.spu.dto;

import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description: SPU信息
 * @Author: Snail
 * @CreateDate: 2024/6/10 14:22
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SPU信息")
public class BaseSpuDto extends BaseSpu {

    @ApiModelProperty(value = "SPU属性")
    private List<BaseSpuAttrRelation> spuAttrs;
}
