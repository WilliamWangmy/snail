package com.snail.wms.base.business.spu.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.poi.HorizontalAlignmentEnum;
import com.alibaba.excel.enums.poi.VerticalAlignmentEnum;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.annotation.ExcelContentMerge;
import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description: SpuExcelDto对象
 * @Author: Snail
 * @CreateDate: 2024/6/10 20:32
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SpuExcelDto对象")
public class SpuExcelDto extends BaseSpu {

    @ApiModelProperty(value = "属性名")
    @Excel(value = {"SPU属性","属性名"}, index = 6, template = true)
    @ContentStyle(verticalAlignment = VerticalAlignmentEnum.CENTER)
    @ExcelContentMerge
    private String attrName;

    @ApiModelProperty("属性值")
    @ContentStyle(horizontalAlignment = HorizontalAlignmentEnum.CENTER)
    @Excel(value = {"SPU属性","属性值"}, index = 7, template = true)
    private String attrValue;
}
