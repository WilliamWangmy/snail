package com.snail.wms.base.business.spu.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: spu信息查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "spu信息")
public class BaseSpuQuery extends BaseQuery {
    @ApiModelProperty(value = "spu编码")
    private String spuCode;
    @ApiModelProperty(value = "spu名称")
    private String spuName;
    @ApiModelProperty(value = "分类id")
    private String categoryId;
    @ApiModelProperty(value = "状态")
    private String spuStatus;
    @ApiModelProperty(value = "租户标识")
    private String tenantId;
}
