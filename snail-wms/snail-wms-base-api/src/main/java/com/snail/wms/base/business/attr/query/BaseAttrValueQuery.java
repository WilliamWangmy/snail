package com.snail.wms.base.business.att.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 属性值查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "属性值")
public class BaseAttrValueQuery extends BaseQuery {
    @ApiModelProperty(value = "属性id")
    private String attrId;
}
