package com.snail.wms.base.constants;

/**
 * @Description: 基础数据常量
 * @Author: Snail
 * @CreateDate: 2023/11/3 9:38
 * @Version: V1.0
 */
public interface BaseConstants {

    /**
     * 分类根目录
     */
    String CATEGORY_ROOT_ID = "-1";
    /**
     * 分类编码规则
     */
    String CATEGORY_CODE_RULE = "base_category_";
    /**
     * 仓库编码规则
     */
    String WAREHOUSE_CODE_RULE = "base_warehouse_";
    /**
     * SPU编码规则
     */
    String SPU_CODE_RULE = "base_spu_";
}
