package com.snail.wms.base.business.category.dto;

import com.snail.common.excel.annotation.Excel;
import com.snail.wms.base.business.category.domain.BaseCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Description: 产品分类DTO
 * @Author: Snail
 * @CreateDate: 2024/5/1 20:44
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "产品分类DTO")
public class BaseCategoryDto extends BaseCategory {

    /**
     * 父分类编码
     */
    @ApiModelProperty(value = "父分类编码")
    @Excel(value = "父分类编码", index = 1, template = true)
    private String parentCategoryCode;

    @ApiModelProperty(value = "子分类")
    private List<BaseCategoryDto> children;
}
