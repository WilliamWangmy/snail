package com.snail.wms.base.business.category.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.snail.common.excel.annotation.Excel;
import com.snail.common.excel.converter.BizStatusConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description: 产品分类对象 base_category
 * @Author: snail
 * @CreateDate: 2024-04-24
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "产品分类")
@TableName("base_category")
public class BaseCategory implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String categoryId;

    /**
     * 分类编码
     */
    @ApiModelProperty(value = "分类编码")
    @Excel(value = "分类编码")
    private String categoryCode;

    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称")
    @Excel(value = "分类名称", index = 0, template = true)
    @NotEmpty(message = "分类名称不能为空!")
    private String categoryName;

    /**
     * 分类层级
     */
    @NotNull(message = "分类层级不能为空")
    @ApiModelProperty(value = "分类层级")
    private Integer categoryLevel;

    /**
     * 父分类id
     */
    @ApiModelProperty(value = "父分类id")
    @NotEmpty(message = "父分类id不能为空!")
    private String parentId;

    /**
     * 状态：1.启用，0.禁用
     */
    @ApiModelProperty(value = "状态")
    @Excel(value = "状态", index = 2, template = true, converter = BizStatusConverter.class)
    @NotEmpty(message = "状态不能为空!")
    private String categoryStatus;

    /**
     * 分类全路径
     */
    @ApiModelProperty(value = "分类全路径")
    @Excel(value = "分类全路径")
    private String categoryFullName;

    /**
     * 逻辑删除：1.删除，0.未删除
     */
    @ApiModelProperty(value = "逻辑删除：1.删除，0.未删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createUserName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    @TableField(fill = FieldFill.INSERT)
    private String deptId;

    /**
     * 组织全路径id
     */
    @ApiModelProperty(value = "组织全路径id")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullId;

    /**
     * 组织全路径
     */
    @ApiModelProperty(value = "组织全路径")
    @TableField(fill = FieldFill.INSERT)
    private String deptFullName;

    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserId;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(fill = FieldFill.UPDATE)
    private String updateUserName;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


}
