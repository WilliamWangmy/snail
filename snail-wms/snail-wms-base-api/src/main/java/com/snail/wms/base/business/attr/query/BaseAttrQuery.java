package com.snail.wms.base.business.attr.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 属性查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "属性")
public class BaseAttrQuery extends BaseQuery {
    @ApiModelProperty(value = "属性名称")
    private String attrName;
    @ApiModelProperty(value = "分类id")
    private String categoryId;
    @ApiModelProperty(value = "分类编码")
    private String categoryCode;
    @ApiModelProperty(value = "分类名称")
    private String categoryName;
    @ApiModelProperty(value = "状态")
    private String status;
}
