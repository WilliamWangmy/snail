package com.snail.wms.base.business.spu.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: spu属性查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "spu属性")
public class BaseSpuAttrRelationQuery extends BaseQuery {
    @ApiModelProperty(value = "属性id")
    private String attrId;
    @ApiModelProperty(value = "属性名称")
    private String attrName;
}
