package com.snail.wms.base.business.sku.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.snail.common.core.utils.StringUtils;


/**
 * @Description: sku信息查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "sku信息")
public class BaseSkuQuery extends BaseQuery {
        @ApiModelProperty(value = "sku编码")
            private String skuCode;
        @ApiModelProperty(value = "sku名称")
            private String skuName;
        @ApiModelProperty(value = "主键")
            private String spuId;
        @ApiModelProperty(value = "spu编码")
            private String spuCode;
        @ApiModelProperty(value = "spu名称")
            private String spuName;
        @ApiModelProperty(value = "分类id")
            private String categoryId;
        @ApiModelProperty(value = "分类编码")
            private String categoryCode;
        @ApiModelProperty(value = "分类名称")
            private String categoryName;
        @ApiModelProperty(value = "分类全路径")
            private String categoryFullName;
        @ApiModelProperty(value = "状态")
            private String skuStatus;
        @ApiModelProperty(value = "更新人id")
            private String updateUserId;
        @ApiModelProperty(value = "更新人")
            private String updateUserName;
}
