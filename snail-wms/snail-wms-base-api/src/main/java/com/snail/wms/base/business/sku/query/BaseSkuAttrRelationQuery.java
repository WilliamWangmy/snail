package com.snail.wms.base.business.sku.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.snail.common.core.utils.StringUtils;


/**
 * @Description: sku属性查询对象
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "sku属性")
public class BaseSkuAttrRelationQuery extends BaseQuery {
        @ApiModelProperty(value = "属性id")
            private String attrId;
        @ApiModelProperty(value = "属性名称")
            private String attrName;
}
