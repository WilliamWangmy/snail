package com.snail.wms.base.business.district.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 行政区域
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Data
@ApiModel(value = "行政区域")
@TableName("base_district")
public class BaseDistrict {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 城市编码
     */
    @ApiModelProperty(value = "城市编码")
    private String cityCode;
    /**
     * 区域编码
     */
    @ApiModelProperty(value = "区域编码")
    private String adCode;
    /**
     * 行政区名称
     */
    @ApiModelProperty(value = "行政区名称")
    private String name;
    /**
     * 行政区边界坐标点
     */
    @ApiModelProperty(value = "行政区边界坐标点")
    private String polyline;
    /**
     * 行政区划级别
     */
    @ApiModelProperty(value = "行政区划级别")
    private String level;
    /**
     * 父区域编码
     */
    @ApiModelProperty(value = "父区域编码")
    private String parentAdCode;
}
