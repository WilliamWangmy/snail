package com.snail.wms.stock.business.stock.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: 库存维度对象 wms_stock_dimensions
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@Builder
@ApiModel(value = "库存维度")
@TableName("wms_stock_dimensions")
public class WmsStockDimensions implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String stockDimensionId;

    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    @NotEmpty(message = "spuId不能为空")
    private String spuId;

    /**
     * spu编码
     */
    @ApiModelProperty(value = "spu编码")
    @NotEmpty(message = "spu编码不能为空")
    private String spuCode;

    /**
     * spu名称
     */
    @ApiModelProperty(value = "spu名称")
    @NotEmpty(message = "spu名称不能为空")
    private String spuName;

    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    @NotEmpty(message = "skuId不能为空")
    private String skuId;

    /**
     * sku编码
     */
    @ApiModelProperty(value = "sku编码")
    @NotEmpty(message = "sku编码不能为空")
    private String skuCode;

    /**
     * sku名称
     */
    @ApiModelProperty(value = "sku名称")
    @NotEmpty(message = "sku名称不能为空")
    private String skuName;

    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id")
    @NotEmpty(message = "分类id不能为空")
    private String categoryId;

    /**
     * 分类编码
     */
    @ApiModelProperty(value = "分类编码")
    @NotEmpty(message = "分类编码不能为空")
    private String categoryCode;

    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称")
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;

    /**
     * 分类全路径
     */
    @ApiModelProperty(value = "分类全路径")
    @NotEmpty(message = "分类全路径不能为空")
    private String categoryFullName;

    /**
     * sku属性
     */
    @ApiModelProperty(value = "sku属性")
    private String skuAttr;


}
