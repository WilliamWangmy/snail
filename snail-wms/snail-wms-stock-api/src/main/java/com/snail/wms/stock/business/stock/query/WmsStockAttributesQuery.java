package com.snail.wms.stock.business.stock.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 库存属性查询对象
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库存属性")
public class WmsStockAttributesQuery extends BaseQuery {
    @ApiModelProperty(value = "生产批次号")
    private String productionBatch;
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;
    @ApiModelProperty(value = "入库批次")
    private String inBatch;
}
