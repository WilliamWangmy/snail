package com.snail.wms.stock.business.stock.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Description: 库存出库信息
 * @Author: snail
 * @CreateDate: 2024/3/18 17:15
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存出库信息")
public class StockOutputDto {

    /**
     * 单据id
     */
    @ApiModelProperty(value = "单据id")
    @NotEmpty(message = "单据id不能为空！")
    private String billId;

    /**
     * 单据编码
     */
    @ApiModelProperty(value = "单据编码")
    @NotEmpty(message = "单据编码不能为空！")
    private String billCode;

    /**
     * 单据明细id
     */
    @ApiModelProperty(value = "单据明细id")
    @NotEmpty(message = "单据明细id不能为空！")
    private String billDetailId;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    @NotNull(message = "数量不能为空！")
    private BigDecimal amount;

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @NotEmpty(message = "仓库id不能为空！")
    private String warehouseId;

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码")
    @NotEmpty(message = "仓库编码不能为空！")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称")
    @NotEmpty(message = "仓库名称不能为空！")
    private String warehouseName;

    /**
     * 库位id
     */
    @ApiModelProperty(value = "库位id")
    @NotEmpty(message = "库位id不能为空！")
    private String locationId;

    /**
     * 库位编码
     */
    @ApiModelProperty(value = "库位编码")
    @NotEmpty(message = "库位编码不能为空！")
    private String locationCode;

    /**
     * 库位名称
     */
    @ApiModelProperty(value = "库位名称")
    @NotEmpty(message = "库位名称不能为空！")
    private String locationName;

    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    @NotEmpty(message = "skuId不能为空！")
    private String skuId;

    /**
     * sku编码
     */
    @ApiModelProperty(value = "sku编码")
    @NotEmpty(message = "sku编码不能为空！")
    private String skuCode;

    /**
     * sku名称
     */
    @ApiModelProperty(value = "sku名称")
    @NotEmpty(message = "sku名称不能为空！")
    private String skuName;

    /**
     * 库存批次
     */
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;




}
