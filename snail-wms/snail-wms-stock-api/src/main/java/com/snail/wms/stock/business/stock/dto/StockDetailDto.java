package com.snail.wms.stock.business.stock.dto;

import com.snail.wms.stock.business.stock.domain.WmsStockAmount;
import com.snail.wms.stock.business.stock.domain.WmsStockAttributes;
import com.snail.wms.stock.business.stock.domain.WmsStockDimensions;
import com.snail.wms.stock.business.stock.domain.WmsStockWarehouse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 库存明细DTO对象
 * @Author: Snail
 * @CreateDate: 2024/4/3 22:00
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "库存明细DTO对象")
public class StockDetailDto extends WmsStockAmount {


    @ApiModelProperty(value = "库存维度")
    private WmsStockDimensions dimensions;

    @ApiModelProperty(value = "库存仓库")
    private WmsStockWarehouse warehouse;

    @ApiModelProperty(value = "库存属性")
    private WmsStockAttributes attributes;
}
