package com.snail.wms.stock.business.stock.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 库存数量对象 wms_stock_amount
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存数量")
@TableName("wms_stock_amount")
public class WmsStockAmount implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 总库存量
     */
    @ApiModelProperty(value = "总库存量")
    private BigDecimal totalAmount;

    /**
     * 预占库存量
     */
    @ApiModelProperty(value = "预占库存量")
    private BigDecimal preemptionAmount;

    /**
     * 冻结库存量
     */
    @ApiModelProperty(value = "冻结库存量")
    private BigDecimal freezeAmount;

    /**
     * 出库库存量
     */
    @ApiModelProperty(value = "出库库存量")
    private BigDecimal outAmount;

    /**
     * 库存仓库id
     */
    @ApiModelProperty(value = "库存仓库id")
    private String stockWarehouseId;

    /**
     * 库存维度id
     */
    @ApiModelProperty(value = "库存维度id")
    private String stockDimensionId;

    /**
     * 库存属性id
     */
    @ApiModelProperty(value = "库存属性id")
    private String stockAttributeId;


}
