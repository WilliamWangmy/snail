package com.snail.wms.stock.business.stock.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 库存属性对象 wms_stock_attributes
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@Builder
@ApiModel(value = "库存属性")
@TableName("wms_stock_attributes")
public class WmsStockAttributes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String stockAttributeId;

    /**
     * 生产批次号
     */
    @ApiModelProperty(value = "生产批次号")
    private String productionBatch;

    /**
     * 库存批次
     */
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;

    /**
     * 价格
     */
    @ApiModelProperty(value = "价格")
    @NotNull(message = "价格不能为空")
    private BigDecimal price;

    /**
     * 入库批次
     */
    @ApiModelProperty(value = "入库批次")
    @NotEmpty(message = "入库批次不能为空")
    private String inBatch;

    /**
     * 实际入库时间
     */
    @ApiModelProperty(value = "实际入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "实际入库时间不能为空")
    private LocalDateTime factInDate;
    /**
     * 入库时间
     */
    @ApiModelProperty(value = "入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime inDate;

    /**
     * 首次入库时间
     */
    @ApiModelProperty(value = "首次入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime firstInDate;

    /**
     * 首次入库id
     */
    @ApiModelProperty(value = "首次入库id")
    private String firstInId;

    /**
     * 首次入库单编码
     */
    @ApiModelProperty(value = "首次入库单编码")
    private String firstInCode;

    /**
     * 首次入库明细id
     */
    @ApiModelProperty(value = "首次入库明细id")
    private String firstInDetailId;

    /**
     * 入库id
     */
    @ApiModelProperty(value = "入库id")
    @NotEmpty(message = "入库id不能为空")
    private String inId;

    /**
     * 入库单编码
     */
    @ApiModelProperty(value = "入库单编码")
    @NotEmpty(message = "入库单编码不能为空")
    private String inCode;

    /**
     * 入库明细id
     */
    @ApiModelProperty(value = "入库明细id")
    @NotEmpty(message = "入库明细id不能为空")
    private String inDetailId;

    /**
     * 所属组织id
     */
    @ApiModelProperty(value = "所属组织id")
    @NotEmpty(message = "所属组织id不能为空")
    private String belongOrgId;

    /**
     * 所属组织名称
     */
    @ApiModelProperty(value = "所属组织名称")
    @NotEmpty(message = "所属组织名称不能为空")
    private String belongOrgName;

    /**
     * 所属组织全路径id
     */
    @ApiModelProperty(value = "所属组织全路径id")
    @NotEmpty(message = "所属组织全路径id不能为空")
    private String belongOrgFullId;

    /**
     * 所属组织全路径
     */
    @ApiModelProperty(value = "所属组织全路径")
    @NotEmpty(message = "所属组织全路径不能为空")
    private String belongOrgFullName;


}
