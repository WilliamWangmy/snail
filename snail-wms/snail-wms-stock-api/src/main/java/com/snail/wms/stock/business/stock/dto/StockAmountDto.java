package com.snail.wms.stock.business.stock.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 库存数量DTO对象
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存数量")
public class StockAmountDto {

    /**
     * 库存数量id
     */
    @ApiModelProperty(value = "库存数量id")
    private String stockAmountId;

    /**
     * 总库存量
     */
    @ApiModelProperty(value = "总库存量")
    private BigDecimal totalAmount;

    /**
     * 预占库存量
     */
    @ApiModelProperty(value = "预占库存量")
    private BigDecimal preemptionAmount;

    /**
     * 冻结库存量
     */
    @ApiModelProperty(value = "冻结库存量")
    private BigDecimal freezeAmount;

    /**
     * 出库库存量
     */
    @ApiModelProperty(value = "出库库存量")
    private BigDecimal outAmount;

    /**
     * 首次入库时间
     */
    @ApiModelProperty(value = "首次入库时间")
    private LocalDateTime firstInDate;

    /**
     * 库存批次
     */
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;


    /**
     * 库存仓库id
     */
    @ApiModelProperty(value = "库存仓库id")
    private String stockWarehouseId;

    /**
     * 库存维度id
     */
    @ApiModelProperty(value = "库存维度id")
    private String stockDimensionId;

    /**
     * 库存属性id
     */
    @ApiModelProperty(value = "库存属性id")
    private String stockAttributeId;



}
