package com.snail.wms.stock.business.stock.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description: 库存操作明细
 * @Author: Snail
 * @CreateDate: 2024/3/31 15:51
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存操作明细")
@TableName("wms_stock_operate_detail")
public class WmsStockOperateDetail {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 单据id
     */
    @ApiModelProperty(value = "单据id")
    private String billId;
    /**
     * 单据编号
     */
    @ApiModelProperty(value = "单据编号")
    private String billCode;
    /**
     * 单据明细id
     */
    @ApiModelProperty(value = "单据明细id")
    private String billDetailId;
    /**
     * 操作类型
     */
    @ApiModelProperty(value = "操作类型")
    private String type;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal amount;
    /**
     * 库存数量id
     */
    @ApiModelProperty(value = "库存数量id")
    private String stockAmountId;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
