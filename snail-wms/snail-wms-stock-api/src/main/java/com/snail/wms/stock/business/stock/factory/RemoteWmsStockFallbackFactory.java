package com.snail.wms.stock.business.stock.factory;

import com.snail.common.core.domain.R;
import com.snail.wms.stock.business.stock.feign.RemoteWmsStockService;
import com.snail.wms.stock.business.stock.mq.StockCallDto;
import com.snail.wms.stock.business.stock.mq.StockCallbackDto;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * @Description: 库存feign接口降级处理
 * @Author: snail
 * @CreateDate: 2024/3/18 10:10
 * @Version: V1.0
 */
public class RemoteWmsStockFallbackFactory implements FallbackFactory<RemoteWmsStockService> {
    @Override
    public RemoteWmsStockService create(Throwable throwable) {
        return new RemoteWmsStockService() {
            @Override
            public R<StockCallbackDto> inStock(StockCallDto dto, String source) {
                return R.fail("入库[" + dto.getType() + "]操作失败:" + throwable.getMessage());
            }

            @Override
            public R<StockCallbackDto> outStock(StockCallDto dto, String source) {
                return R.fail("出库[" + dto.getType() + "]操作失败:" + throwable.getMessage());
            }
        };
    }
}
