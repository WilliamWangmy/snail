package com.snail.wms.stock.business.stock.feign;

import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.domain.R;
import com.snail.wms.stock.business.stock.constants.WmsStockConstants;
import com.snail.wms.stock.business.stock.factory.RemoteWmsStockFallbackFactory;
import com.snail.wms.stock.business.stock.mq.StockCallDto;
import com.snail.wms.stock.business.stock.mq.StockCallbackDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @Description: 库存feign接口
 * @Author: snail
 * @CreateDate: 2024/3/18 10:10
 * @Version: V1.0
 */
@FeignClient(contextId = "remoteWmsStockService", value = WmsStockConstants.STOCK_SERVICE_NAME, fallbackFactory = RemoteWmsStockFallbackFactory.class)
public interface RemoteWmsStockService {

    /**
     * 入库操作
     * @param dto 输入信息
     * @return
     */
    @PostMapping("/stock/inStock")
    R<StockCallbackDto> inStock(@RequestBody StockCallDto dto,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 出库操作
     * @param dto 输入信息
     * @return
     */
    @PostMapping("/stock/outStock")
    R<StockCallbackDto> outStock(@RequestBody StockCallDto dto,@RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
