package com.snail.wms.stock.business.stock.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 库存仓库查询对象
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库存仓库")
public class WmsStockWarehouseQuery extends BaseQuery {
    @ApiModelProperty(value = "仓库id")
    private String warehouseId;
    @ApiModelProperty(value = "仓库编码")
    private String warehouseCode;
    @ApiModelProperty(value = "仓库名称")
    private String warehouseName;
    @ApiModelProperty(value = "区域id")
    private String areaId;
    @ApiModelProperty(value = "区域编码")
    private String areaCode;
    @ApiModelProperty(value = "区域名称")
    private String areaName;
    @ApiModelProperty(value = "货架id")
    private String shelvesId;
    @ApiModelProperty(value = "货架编码")
    private String shelvesCode;
    @ApiModelProperty(value = "货架名称")
    private String shelvesName;
    @ApiModelProperty(value = "库位id")
    private String locationId;
    @ApiModelProperty(value = "库位编码")
    private String locationCode;
    @ApiModelProperty(value = "库位名称")
    private String locationName;
}
