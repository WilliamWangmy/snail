package com.snail.wms.stock.business.stock.dto;

import com.snail.wms.stock.business.stock.domain.WmsStockAttributes;
import com.snail.wms.stock.business.stock.domain.WmsStockDimensions;
import com.snail.wms.stock.business.stock.domain.WmsStockWarehouse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Description: 库存DTO
 * @Author: snail
 * @CreateDate: 2024/3/18 16:54
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存DTO")
public class StockInputDto {


    @ApiModelProperty("入库数量")
    @NotNull(message = "入库数量为空！")
    @Min(message = "数量必须大于0",value = 0)
    private BigDecimal amount;

    /**
     * 库存仓库id
     * 有对应出库时填写对应的出库库存信息
     */
    @ApiModelProperty(value = "库存仓库id")
    private String stockWarehouseId;

    /**
     * 库存维度id
     * 有对应出库时填写对应的出库库存信息
     */
    @ApiModelProperty(value = "库存维度id")
    private String stockDimensionId;

    /**
     * 库存属性id
     * 有对应出库时填写对应的出库库存信息
     */
    @ApiModelProperty(value = "库存属性id")
    private String stockAttributeId;

    /**
     * 产品信息：
     * spu、sku、分类等信息
     */
    @ApiModelProperty("库存维度信息")
    private WmsStockDimensions stockDimensions;

    /**
     * 入库信息：
     * 入库单号、入库id、入库明细id、入库时间等信息
     */
    @ApiModelProperty("库存属性信息")
    private WmsStockAttributes stockAttributes;

    /**
     * 仓库信息：
     * 出库、区域、货架、库位等信息
     */
    @ApiModelProperty("库存仓库信息")
    private WmsStockWarehouse stockWarehouse;


}
