package com.snail.wms.stock.business.stock.constants;


/**
 * @Description: 库存管理业务常量
 * @Author: snail
 * @CreateDate: 2024/3/18 16:49
 * @Version: V1.0
 */
public class WmsStockConstants {

    /**
     * 分布式锁key
     */
    public static final String LOCK_KEY = "snail:lock:wms:stock:";

    /**
     * 操作类型：I.入库、P.预占、F.冻结、O.实占、RP.释放预占、RF.释放冻结、FO.冻结转实占、PO.预占转实占
     */
    public static final String OPERATE_TYPE_I = "I";
    public static final String OPERATE_TYPE_P = "P";
    public static final String OPERATE_TYPE_F = "F";
    public static final String OPERATE_TYPE_O = "O";
    public static final String OPERATE_TYPE_RP = "RP";
    public static final String OPERATE_TYPE_RF = "RF";
    public static final String OPERATE_TYPE_FO = "FO";
    public static final String OPERATE_TYPE_PO = "PO";

    /**
     * 服务名称
     */
    public static final String STOCK_SERVICE_NAME = "snail-wms-stock";
}
