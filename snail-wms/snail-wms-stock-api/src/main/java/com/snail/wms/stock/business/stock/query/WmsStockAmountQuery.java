package com.snail.wms.stock.business.stock.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 库存数量查询对象
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库存数量")
public class WmsStockAmountQuery extends BaseQuery {
    /**
     * 库存仓库id
     */
    @ApiModelProperty(value = "库存仓库id")
    private String stockWarehouseId;

    /**
     * 库存维度id
     */
    @ApiModelProperty(value = "库存维度id")
    private String stockDimensionId;

    /**
     * 库存属性id
     */
    @ApiModelProperty(value = "库存属性id")
    private String stockAttributeId;
}
