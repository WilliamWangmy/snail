package com.snail.wms.stock.business.stock.mq;

import com.snail.wms.stock.business.stock.dto.StockResultDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Description: 库存操作结果
 * @Author: snail
 * @CreateDate: 2024/3/18 10:10
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存操作结果")
public class StockCallbackDto {

    @ApiModelProperty(value = "操作结果")
    private Boolean result = Boolean.FALSE;

    @ApiModelProperty(value = "返回消息")
    private String msg;

    @ApiModelProperty(value = "库存返回信息")
    private List<StockResultDto> resultDtoList;

    /**
     * k：单据明细id
     * v：错误信息
     */
    @ApiModelProperty(value = "错误信息返回")
    private List<Map<String, String>> errorList;

}
