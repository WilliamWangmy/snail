package com.snail.wms.stock.business.stock.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 库存结果信息DTO
 * @Author: snail
 * @CreateDate: 2024/3/18 16:54
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存结果信息DTO")
public class StockResultDto {

    /**
     * 操作类型：P.预占、F.冻结、O.实占
     */
    @ApiModelProperty(value = "操作类型：P.预占、F.冻结、O.实占、RP.释放预占、RF.释放冻结、FO.冻结转实占、PO.预占转实占")
    private String type;

    /**
     * 单据id
     */
    @ApiModelProperty(value = "单据id")
    private String billId;
    /**
     * 单据编号
     */
    @ApiModelProperty(value = "单据编号")
    private String billCode;
    /**
     * 单据明细id
     */
    @ApiModelProperty(value = "单据明细id")
    private String billDetailId;
    /**
     * 库存仓库id
     */
    @ApiModelProperty(value = "库存仓库id")
    private String stockWarehouseId;

    /**
     * 库存维度id
     */
    @ApiModelProperty(value = "库存维度id")
    private String stockDimensionId;

    /**
     * 库存属性id
     */
    @ApiModelProperty(value = "库存属性id")
    private String stockAttributeId;

    /**
     * 库存数量id
     */
    @ApiModelProperty(value = "库存数量id")
    private String stockAmountId;

    /**
     * 库存批次
     */
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal amount;

}
