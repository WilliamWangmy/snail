package com.snail.wms.stock.business.stock.mq;

import com.snail.wms.stock.business.stock.dto.StockInputDto;
import com.snail.wms.stock.business.stock.dto.StockOutputDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Description: 库存入库DTO
 * @Author: snail
 * @CreateDate: 2024/3/18 10:09
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存入库DTO")
public class StockCallDto {


    /**
     * 单据id
     */
    @ApiModelProperty(value = "单据id")
    @NotEmpty(message = "单据id不能为空")
    private String billId;

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @NotEmpty(message = "仓库id不能为空")
    private String warehouseId;

    /**
     * 操作类型：I.入库、P.预占、F.冻结、O.实占、RP.释放预占、RF.释放冻结、FO.冻结转实占、PO.预占转实占
     */
    @ApiModelProperty(value = "操作类型：I.入库、P.预占、F.冻结、O.实占、RP.释放预占、RF.释放冻结、FO.冻结转实占、PO.预占转实占")
    @NotEmpty(message = "操作类型不能为空")
    private String type;


    @ApiModelProperty("入库信息")
    private List<StockInputDto> inputDtoList;

    @ApiModelProperty("出库信息")
    private List<StockOutputDto> outputDtoList;


}
