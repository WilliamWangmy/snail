package com.snail.wms.stock.business.stock.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 库存操作明细查询对象
 * @Author: Snail
 * @CreateDate: 2024/3/31 21:06
 * @Version: V1.0
 */
@Data
@ApiModel(value = "库存数量")
public class WmsStockOperateDetailQuery {

    /**
     * 单据id
     */
    @ApiModelProperty(value = "单据id")
    private String billId;

    /**
     * 单据明细id
     */
    @ApiModelProperty(value = "单据明细id")
    private String billDetailId;
    /**
     * 操作类型：P.预占、F.冻结、O.实占
     */
    @ApiModelProperty(value = "操作类型：P.预占、F.冻结、O.实占、RP.释放预占、RF.释放冻结、FO.冻结转实占、PO.预占转实占")
    private String type;
}
