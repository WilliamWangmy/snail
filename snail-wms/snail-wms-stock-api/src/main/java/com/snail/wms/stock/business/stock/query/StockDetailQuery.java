package com.snail.wms.stock.business.stock.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 库存明细查询对象
 * @Author: Snail
 * @CreateDate: 2024/4/3 22:03
 * @Version: V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "库存明细查询对象")
public class StockDetailQuery extends BaseQuery {

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    private String warehouseId;

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称")
    private String warehouseName;

    /**
     * 库区id
     */
    @ApiModelProperty(value = "库区id")
    private String areaId;

    /**
     * 库区编码
     */
    @ApiModelProperty(value = "库区编码")
    private String areaCode;

    /**
     * 库区名称
     */
    @ApiModelProperty(value = "库区名称")
    private String areaName;

    /**
     * 货架id
     */
    @ApiModelProperty(value = "货架id")
    private String shelvesId;

    /**
     * 货架编码
     */
    @ApiModelProperty(value = "货架编码")
    private String shelvesCode;

    /**
     * 货架名称
     */
    @ApiModelProperty(value = "货架名称")
    private String shelvesName;

    /**
     * 库位id
     */
    @ApiModelProperty(value = "库位id")
    private String locationId;

    /**
     * 库位编码
     */
    @ApiModelProperty(value = "库位编码")
    private String locationCode;

    /**
     * 库位名称
     */
    @ApiModelProperty(value = "库位名称")
    private String locationName;

    /**
     * 库存批次
     */
    @ApiModelProperty(value = "库存批次")
    private String stockBatch;

    /**
     * 入库批次
     */
    @ApiModelProperty(value = "入库批次")
    private String inBatch;

    /**
     * 入库单编码
     */
    @ApiModelProperty(value = "入库单编码")
    private String inCode;

    /**
     * 所属组织id
     */
    @ApiModelProperty(value = "所属组织id")
    private String belongOrgId;

    /**
     * spu
     */
    @ApiModelProperty(value = "spu")
    private String spuId;

    /**
     * spu编码
     */
    @ApiModelProperty(value = "spu编码")
    private String spuCode;

    /**
     * spu名称
     */
    @ApiModelProperty(value = "spu名称")
    private String spuName;

    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    private String skuId;

    /**
     * sku编码
     */
    @ApiModelProperty(value = "sku编码")
    private String skuCode;

    /**
     * sku名称
     */
    @ApiModelProperty(value = "sku名称")
    private String skuName;

    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id")
    private String categoryId;


    /**
     * sku属性
     */
    @ApiModelProperty(value = "sku属性")
    private String skuAttr;
}
