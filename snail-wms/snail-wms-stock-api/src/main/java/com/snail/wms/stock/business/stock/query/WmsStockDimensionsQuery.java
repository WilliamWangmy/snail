package com.snail.wms.stock.business.stock.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 库存维度查询对象
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库存维度")
public class WmsStockDimensionsQuery extends BaseQuery {
    @ApiModelProperty(value = "spu")
    private String spuId;
    @ApiModelProperty(value = "spu编码")
    private String spuCode;
    @ApiModelProperty(value = "spu名称")
    private String spuName;
    @ApiModelProperty(value = "sku")
    private String skuId;
    @ApiModelProperty(value = "sku编码")
    private String skuCode;
    @ApiModelProperty(value = "sku名称")
    private String skuName;
    @ApiModelProperty(value = "分类id")
    private String categoryId;
    @ApiModelProperty(value = "分类编码")
    private String categoryCode;
    @ApiModelProperty(value = "分类名称")
    private String categoryName;
    @ApiModelProperty(value = "分类全路径")
    private String categoryFullName;
    @ApiModelProperty(value = "sku属性")
    private String skuAttr;
}
