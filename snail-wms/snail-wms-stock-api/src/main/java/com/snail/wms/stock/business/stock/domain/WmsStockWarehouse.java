package com.snail.wms.stock.business.stock.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: 库存仓库对象 wms_stock_warehouse
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Data
@Builder
@ApiModel(value = "库存仓库")
@TableName("wms_stock_warehouse")
public class WmsStockWarehouse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String stockWarehouseId;

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @NotEmpty(message = "仓库id不能为空")
    private String warehouseId;

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码")
    @NotEmpty(message = "仓库编码不能为空")
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称")
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;

    /**
     * 库区id
     */
    @ApiModelProperty(value = "库区id")
    @NotEmpty(message = "库区id不能为空")
    private String areaId;

    /**
     * 库区编码
     */
    @ApiModelProperty(value = "库区编码")
    @NotEmpty(message = "库区编码不能为空")
    private String areaCode;

    /**
     * 库区名称
     */
    @ApiModelProperty(value = "库区名称")
    @NotEmpty(message = "库区名称不能为空")
    private String areaName;

    /**
     * 货架id
     */
    @ApiModelProperty(value = "货架id")
    private String shelvesId;

    /**
     * 货架编码
     */
    @ApiModelProperty(value = "货架编码")
    private String shelvesCode;

    /**
     * 货架名称
     */
    @ApiModelProperty(value = "货架名称")
    private String shelvesName;

    /**
     * 库位id
     */
    @ApiModelProperty(value = "库位id")
    @NotEmpty(message = "库位id不能为空")
    private String locationId;

    /**
     * 库位编码
     */
    @ApiModelProperty(value = "库位编码")
    @NotEmpty(message = "库位编码不能为空")
    private String locationCode;

    /**
     * 库位名称
     */
    @ApiModelProperty(value = "库位名称")
    @NotEmpty(message = "库位名称不能为空")
    private String locationName;


}
