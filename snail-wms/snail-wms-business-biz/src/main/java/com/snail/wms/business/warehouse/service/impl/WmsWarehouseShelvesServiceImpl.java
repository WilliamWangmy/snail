package com.snail.wms.business.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.business.warehouse.domain.WmsWarehouseShelves;
import com.snail.wms.business.warehouse.mapper.WmsWarehouseShelvesMapper;
import com.snail.wms.business.warehouse.query.WmsWarehouseShelvesQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseShelvesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 仓库货架Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Service
public class WmsWarehouseShelvesServiceImpl extends ServiceImpl<WmsWarehouseShelvesMapper, WmsWarehouseShelves> implements IWmsWarehouseShelvesService {

    @Override
    public PageResult<WmsWarehouseShelves> selectWmsWarehouseShelvesPage(WmsWarehouseShelvesQuery query) {
        Page<WmsWarehouseShelves> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<WmsWarehouseShelves> queryWrapper = new LambdaQueryWrapper<>();
        //仓库id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseId()), WmsWarehouseShelves::getWarehouseId, query.getWarehouseId());
        //区域id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAreaId()), WmsWarehouseShelves::getAreaId, query.getAreaId());
        //货架编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getShelvesCode()), WmsWarehouseShelves::getShelvesCode, query.getShelvesCode());
        //货架名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getShelvesName()), WmsWarehouseShelves::getShelvesName, query.getShelvesName());
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public WmsWarehouseShelves getWmsWarehouseShelvesById(String shelvesId) {
        return getById(shelvesId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertWmsWarehouseShelves(WmsWarehouseShelves wmsWarehouseShelves) {
        save(wmsWarehouseShelves);
        return wmsWarehouseShelves.getShelvesId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWmsWarehouseShelves(WmsWarehouseShelves wmsWarehouseShelves) {
        return updateById(wmsWarehouseShelves);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteWmsWarehouseShelvesByIds(String[] shelvesIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(shelvesIds));
        return batch == shelvesIds.length;
    }

    @Override
    public List<WmsWarehouseShelves> selectWmsWarehouseShelvesList(WmsWarehouseShelvesQuery query) {
        LambdaQueryWrapper<WmsWarehouseShelves> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<WmsWarehouseShelves> dataList) {
        saveBatch(dataList);
    }
}
