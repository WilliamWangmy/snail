package com.snail.wms.business.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.business.warehouse.domain.WmsWarehouseManager;
import com.snail.wms.business.warehouse.mapper.WmsWarehouseManagerMapper;
import com.snail.wms.business.warehouse.query.WmsWarehouseManagerQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 仓库管理人员Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Service
public class WmsWarehouseManagerServiceImpl extends ServiceImpl<WmsWarehouseManagerMapper, WmsWarehouseManager> implements IWmsWarehouseManagerService {

    @Override
    public PageResult<WmsWarehouseManager> selectWmsWarehouseManagerPage(WmsWarehouseManagerQuery query) {
        Page<WmsWarehouseManager> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<WmsWarehouseManager> queryWrapper = new LambdaQueryWrapper<>();
        //仓库id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseId()), WmsWarehouseManager::getWarehouseId, query.getWarehouseId());
        //人员名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getUserName()), WmsWarehouseManager::getUserName, query.getUserName());
        //联系电话
        queryWrapper.eq(StringUtils.isNotEmpty(query.getMobile()), WmsWarehouseManager::getMobile, query.getMobile());
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public WmsWarehouseManager getWmsWarehouseManagerById(String id) {
        return getById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertWmsWarehouseManager(WmsWarehouseManager wmsWarehouseManager) {
        save(wmsWarehouseManager);
        return wmsWarehouseManager.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWmsWarehouseManager(WmsWarehouseManager wmsWarehouseManager) {
        return updateById(wmsWarehouseManager);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteWmsWarehouseManagerByIds(String[] ids) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(ids));
        return batch == ids.length;
    }

    @Override
    public List<WmsWarehouseManager> selectWmsWarehouseManagerList(WmsWarehouseManagerQuery query) {
        LambdaQueryWrapper<WmsWarehouseManager> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<WmsWarehouseManager> dataList) {
        saveBatch(dataList);
    }
}
