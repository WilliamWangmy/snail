package com.snail.wms.business.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.business.warehouse.domain.WmsWarehouseLocation;
import com.snail.wms.business.warehouse.mapper.WmsWarehouseLocationMapper;
import com.snail.wms.business.warehouse.query.WmsWarehouseLocationQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseLocationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 仓库库位Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Service
public class WmsWarehouseLocationServiceImpl extends ServiceImpl<WmsWarehouseLocationMapper, WmsWarehouseLocation> implements IWmsWarehouseLocationService {

    @Override
    public PageResult<WmsWarehouseLocation> selectWmsWarehouseLocationPage(WmsWarehouseLocationQuery query) {
        Page<WmsWarehouseLocation> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<WmsWarehouseLocation> queryWrapper = new LambdaQueryWrapper<>();
        //仓库id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseId()), WmsWarehouseLocation::getWarehouseId, query.getWarehouseId());
        //区域id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAreaId()), WmsWarehouseLocation::getAreaId, query.getAreaId());
        //货架id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getShelvesId()), WmsWarehouseLocation::getShelvesId, query.getShelvesId());
        //库位编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getLocationCode()), WmsWarehouseLocation::getLocationCode, query.getLocationCode());
        //库位名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getLocationName()), WmsWarehouseLocation::getLocationName, query.getLocationName());
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public WmsWarehouseLocation getWmsWarehouseLocationById(String locationId) {
        return getById(locationId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertWmsWarehouseLocation(WmsWarehouseLocation wmsWarehouseLocation) {
        save(wmsWarehouseLocation);
        return wmsWarehouseLocation.getLocationId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWmsWarehouseLocation(WmsWarehouseLocation wmsWarehouseLocation) {
        return updateById(wmsWarehouseLocation);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteWmsWarehouseLocationByIds(String[] locationIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(locationIds));
        return batch == locationIds.length;
    }

    @Override
    public List<WmsWarehouseLocation> selectWmsWarehouseLocationList(WmsWarehouseLocationQuery query) {
        LambdaQueryWrapper<WmsWarehouseLocation> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<WmsWarehouseLocation> dataList) {
        saveBatch(dataList);
    }
}
