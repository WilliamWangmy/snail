package com.snail.wms.business.warehouse.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.business.warehouse.domain.WmsWarehouse;
import com.snail.wms.business.warehouse.query.WmsWarehouseQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 仓库信息Controller
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Api(value = "仓库信息", tags = "仓库信息")
@RestController
@RequestMapping("/warehouse")
public class WmsWarehouseController {
    @Autowired
    private IWmsWarehouseService wmsWarehouseService;

    @Log(title = "查询仓库信息分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询仓库信息列表")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping("/page")
    public R<PageResult<WmsWarehouse>> getWmsWarehousePage(WmsWarehouseQuery query) {
        return R.ok(wmsWarehouseService.selectWmsWarehousePage(query));
    }

    @Log(title = "获取仓库信息详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取仓库信息详细信息")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping(value = "/{warehouseId}")
    public R<WmsWarehouse> getWmsWarehouseById(@PathVariable("warehouseId") String warehouseId) {
        return R.ok(wmsWarehouseService.getWmsWarehouseById(warehouseId));
    }

    @ApiOperation(value = "新增仓库信息")
    @RequiresPermissions("business:warehouse:add")
    @Log(title = "新增仓库信息", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveWmsWarehouse(@Validated @RequestBody WmsWarehouse wmsWarehouse) {
        return R.ok(wmsWarehouseService.insertWmsWarehouse(wmsWarehouse));
    }

    @ApiOperation(value = "修改仓库信息")
    @RequiresPermissions("business:warehouse:edit")
    @Log(title = "修改仓库信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateWmsWarehouse(@Validated @RequestBody WmsWarehouse wmsWarehouse) {
        return R.ok(wmsWarehouseService.updateWmsWarehouse(wmsWarehouse));
    }


    @ApiOperation(value = "删除仓库信息")
    @RequiresPermissions("business:warehouse:delete")
    @Log(title = "删除仓库信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{warehouseIds}")
    public R<Boolean> deleteWmsWarehouseByIds(@PathVariable String[] warehouseIds) {
        return R.ok(wmsWarehouseService.deleteWmsWarehouseByIds(warehouseIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "仓库信息导入模板", WmsWarehouse.class);
    }

    @ApiOperation(value = "导出仓库信息数据")
    @Log(title = "导出仓库信息", businessType = BusinessType.EXPORT)
    @RequiresPermissions("business:warehouse:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, WmsWarehouseQuery query) {
        EasyExcelUtils.writeExcel(response, "仓库信息信息", WmsWarehouse.class, wmsWarehouseService.selectWmsWarehouseList(query));
    }

    @ApiOperation(value = "导入仓库信息数据")
    @Log(title = "导入仓库信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, WmsWarehouse.class, wmsWarehouseService);
    }
}
