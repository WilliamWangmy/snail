package com.snail.wms.business.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.business.warehouse.mapper.WmsWarehouseAreaMapper;
import com.snail.wms.business.warehouse.domain.WmsWarehouseArea;
import com.snail.wms.business.warehouse.query.WmsWarehouseAreaQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseAreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 库域信息Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Service
public class WmsWarehouseAreaServiceImpl extends ServiceImpl<WmsWarehouseAreaMapper, WmsWarehouseArea> implements IWmsWarehouseAreaService {

    @Override
    public PageResult<WmsWarehouseArea> selectWmsWarehouseAreaPage(WmsWarehouseAreaQuery query) {
        Page<WmsWarehouseArea> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<WmsWarehouseArea> queryWrapper = new LambdaQueryWrapper<>();
        //区域编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAreaCode()), WmsWarehouseArea::getAreaCode, query.getAreaCode());
        //区域名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getAreaName()), WmsWarehouseArea::getAreaName, query.getAreaName());
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public WmsWarehouseArea getWmsWarehouseAreaById(String areaId) {
        return getById(areaId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertWmsWarehouseArea(WmsWarehouseArea wmsWarehouseArea) {
        save(wmsWarehouseArea);
        return wmsWarehouseArea.getAreaId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWmsWarehouseArea(WmsWarehouseArea wmsWarehouseArea) {
        return updateById(wmsWarehouseArea);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteWmsWarehouseAreaByIds(String[] areaIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(areaIds));
        return batch == areaIds.length;
    }

    @Override
    public List<WmsWarehouseArea> selectWmsWarehouseAreaList(WmsWarehouseAreaQuery query) {
        LambdaQueryWrapper<WmsWarehouseArea> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<WmsWarehouseArea> dataList) {
        saveBatch(dataList);
    }
}
