package com.snail.wms.business.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.business.warehouse.domain.WmsWarehouseLocation;
import com.snail.wms.business.warehouse.query.WmsWarehouseLocationQuery;

import java.util.List;

/**
 * @Description: 仓库库位Service接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
public interface IWmsWarehouseLocationService extends IService<WmsWarehouseLocation>, IExcelService<WmsWarehouseLocation> {

    /**
     * 查询仓库库位列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<WmsWarehouseLocation> selectWmsWarehouseLocationPage(WmsWarehouseLocationQuery query);

    /**
     * 根据id获取仓库库位详情
     *
     * @param locationId 主键id
     * @return 结果
     */
    WmsWarehouseLocation getWmsWarehouseLocationById(String locationId);

    /**
     * 保存仓库库位数据
     *
     * @param wmsWarehouseLocation 仓库库位信息
     * @return 结果
     */
    String insertWmsWarehouseLocation(WmsWarehouseLocation wmsWarehouseLocation);

    /**
     * 更新仓库库位数据
     *
     * @param wmsWarehouseLocation 仓库库位信息
     * @return 结果
     */
    boolean updateWmsWarehouseLocation(WmsWarehouseLocation wmsWarehouseLocation);

    /**
     * 根据id批量删除仓库库位数据
     *
     * @param locationIds 主键ids
     * @return 结果
     */
    boolean deleteWmsWarehouseLocationByIds(String[] locationIds);

    /**
     * 查询仓库库位数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsWarehouseLocation> selectWmsWarehouseLocationList(WmsWarehouseLocationQuery query);

}
