package com.snail.wms.business.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.business.warehouse.domain.WmsWarehouse;
import org.apache.ibatis.annotations.Mapper;
import com.snail.common.datascope.annotation.DataScope;


/**
 * @Description: 仓库信息Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Mapper
@DataScope(includeMethod = {"selectPage", "selectList"})
public interface WmsWarehouseMapper extends BaseMapper<WmsWarehouse> {

}
