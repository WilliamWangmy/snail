package com.snail.wms.business.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.business.warehouse.domain.WmsWarehouseManager;
import com.snail.wms.business.warehouse.query.WmsWarehouseManagerQuery;

import java.util.List;

/**
 * @Description: 仓库管理人员Service接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
public interface IWmsWarehouseManagerService extends IService<WmsWarehouseManager>, IExcelService<WmsWarehouseManager> {

    /**
     * 查询仓库管理人员列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<WmsWarehouseManager> selectWmsWarehouseManagerPage(WmsWarehouseManagerQuery query);

    /**
     * 根据id获取仓库管理人员详情
     *
     * @param id 主键id
     * @return 结果
     */
    WmsWarehouseManager getWmsWarehouseManagerById(String id);

    /**
     * 保存仓库管理人员数据
     *
     * @param wmsWarehouseManager 仓库管理人员信息
     * @return 结果
     */
    String insertWmsWarehouseManager(WmsWarehouseManager wmsWarehouseManager);

    /**
     * 更新仓库管理人员数据
     *
     * @param wmsWarehouseManager 仓库管理人员信息
     * @return 结果
     */
    boolean updateWmsWarehouseManager(WmsWarehouseManager wmsWarehouseManager);

    /**
     * 根据id批量删除仓库管理人员数据
     *
     * @param ids 主键ids
     * @return 结果
     */
    boolean deleteWmsWarehouseManagerByIds(String[] ids);

    /**
     * 查询仓库管理人员数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsWarehouseManager> selectWmsWarehouseManagerList(WmsWarehouseManagerQuery query);

}
