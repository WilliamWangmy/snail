package com.snail.wms.business.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.business.warehouse.domain.WmsWarehouseArea;
import com.snail.wms.business.warehouse.query.WmsWarehouseAreaQuery;

import java.util.List;

/**
 * @Description: 库域信息Service接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
public interface IWmsWarehouseAreaService extends IService<WmsWarehouseArea>, IExcelService<WmsWarehouseArea> {

    /**
     * 查询库域信息列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<WmsWarehouseArea> selectWmsWarehouseAreaPage(WmsWarehouseAreaQuery query);

    /**
     * 根据id获取库域信息详情
     *
     * @param areaId 主键id
     * @return 结果
     */
    WmsWarehouseArea getWmsWarehouseAreaById(String areaId);

    /**
     * 保存库域信息数据
     *
     * @param wmsWarehouseArea 库域信息信息
     * @return 结果
     */
    String insertWmsWarehouseArea(WmsWarehouseArea wmsWarehouseArea);

    /**
     * 更新库域信息数据
     *
     * @param wmsWarehouseArea 库域信息信息
     * @return 结果
     */
    boolean updateWmsWarehouseArea(WmsWarehouseArea wmsWarehouseArea);

    /**
     * 根据id批量删除库域信息数据
     *
     * @param areaIds 主键ids
     * @return 结果
     */
    boolean deleteWmsWarehouseAreaByIds(String[] areaIds);

    /**
     * 查询库域信息数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsWarehouseArea> selectWmsWarehouseAreaList(WmsWarehouseAreaQuery query);

}
