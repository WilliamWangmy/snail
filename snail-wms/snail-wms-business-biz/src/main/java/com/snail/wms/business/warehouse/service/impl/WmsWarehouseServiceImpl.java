package com.snail.wms.business.warehouse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.business.warehouse.domain.WmsWarehouse;
import com.snail.wms.business.warehouse.mapper.WmsWarehouseMapper;
import com.snail.wms.business.warehouse.query.WmsWarehouseQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: 仓库信息Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Service
public class WmsWarehouseServiceImpl extends ServiceImpl<WmsWarehouseMapper, WmsWarehouse> implements IWmsWarehouseService {

    @Override
    public PageResult<WmsWarehouse> selectWmsWarehousePage(WmsWarehouseQuery query) {
        Page<WmsWarehouse> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<WmsWarehouse> queryWrapper = new LambdaQueryWrapper<>();
        //仓库编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseCode()), WmsWarehouse::getWarehouseCode, query.getWarehouseCode());
        //仓库名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getWarehouseName()), WmsWarehouse::getWarehouseName, query.getWarehouseName());
        //状态
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseStatus()), WmsWarehouse::getWarehouseStatus, query.getWarehouseStatus());
        //组织id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getDeptId()), WmsWarehouse::getDeptId, query.getDeptId());
        //组织全路径id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getDeptFullId()), WmsWarehouse::getDeptFullId, query.getDeptFullId());
        //组织全路径
        queryWrapper.like(StringUtils.isNotEmpty(query.getDeptFullName()), WmsWarehouse::getDeptFullName, query.getDeptFullName());
        //更新人id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getUpdateUserId()), WmsWarehouse::getUpdateUserId, query.getUpdateUserId());
        //更新人
        queryWrapper.like(StringUtils.isNotEmpty(query.getUpdateUserName()), WmsWarehouse::getUpdateUserName, query.getUpdateUserName());
        queryWrapper.ge(StringUtils.isNotEmpty(query.getBeginTime()), WmsWarehouse::getCreateTime, query.getBeginTime());
        queryWrapper.le(StringUtils.isNotEmpty(query.getEndTime()), WmsWarehouse::getCreateTime, query.getEndTime());
        queryWrapper.orderByDesc(WmsWarehouse::getCreateTime);
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public WmsWarehouse getWmsWarehouseById(String warehouseId) {
        return getById(warehouseId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertWmsWarehouse(WmsWarehouse wmsWarehouse) {
        save(wmsWarehouse);
        return wmsWarehouse.getWarehouseId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWmsWarehouse(WmsWarehouse wmsWarehouse) {
        return updateById(wmsWarehouse);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteWmsWarehouseByIds(String[] warehouseIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(warehouseIds));
        return batch == warehouseIds.length;
    }

    @Override
    public List<WmsWarehouse> selectWmsWarehouseList(WmsWarehouseQuery query) {
        LambdaQueryWrapper<WmsWarehouse> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<WmsWarehouse> dataList) {
        saveBatch(dataList);
    }
}
