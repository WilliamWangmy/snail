package com.snail.wms.business.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.common.datascope.annotation.DataScope;
import com.snail.wms.business.warehouse.domain.WmsWarehouseLocation;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 仓库库位Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Mapper
public interface WmsWarehouseLocationMapper extends BaseMapper<WmsWarehouseLocation> {

}
