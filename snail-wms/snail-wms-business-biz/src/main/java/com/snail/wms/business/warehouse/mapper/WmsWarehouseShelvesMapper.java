package com.snail.wms.business.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.business.warehouse.domain.WmsWarehouseShelves;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 仓库货架Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Mapper
public interface WmsWarehouseShelvesMapper extends BaseMapper<WmsWarehouseShelves> {

}
