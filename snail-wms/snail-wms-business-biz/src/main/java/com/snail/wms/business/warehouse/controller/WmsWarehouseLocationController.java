package com.snail.wms.business.warehouse.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.business.warehouse.domain.WmsWarehouseLocation;
import com.snail.wms.business.warehouse.query.WmsWarehouseLocationQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseLocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 仓库库位Controller
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Api(value = "仓库库位", tags = "仓库库位")
@RestController
@RequestMapping("/warehouse")
public class WmsWarehouseLocationController {
    @Autowired
    private IWmsWarehouseLocationService wmsWarehouseLocationService;

    @Log(title = "查询仓库库位分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询仓库库位列表")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping("/page")
    public R<PageResult<WmsWarehouseLocation>> getWmsWarehouseLocationPage(WmsWarehouseLocationQuery query)
    {
        return R.ok(wmsWarehouseLocationService.selectWmsWarehouseLocationPage(query));
    }

    @Log(title = "获取仓库库位详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取仓库库位详细信息")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping(value = "/{locationId}")
    public R<WmsWarehouseLocation> getWmsWarehouseLocationById(@PathVariable("locationId") String locationId)
    {
        return R.ok(wmsWarehouseLocationService.getWmsWarehouseLocationById(locationId));
    }

    @ApiOperation(value = "新增仓库库位")
    @RequiresPermissions("business:warehouse:add")
    @Log(title = "新增仓库库位", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveWmsWarehouseLocation(@Validated @RequestBody WmsWarehouseLocation wmsWarehouseLocation) {
        return R.ok(wmsWarehouseLocationService.insertWmsWarehouseLocation(wmsWarehouseLocation));
    }

    @ApiOperation(value = "修改仓库库位")
    @RequiresPermissions("business:warehouse:edit")
    @Log(title = "修改仓库库位", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateWmsWarehouseLocation(@Validated @RequestBody WmsWarehouseLocation wmsWarehouseLocation) {
        return R.ok(wmsWarehouseLocationService.updateWmsWarehouseLocation(wmsWarehouseLocation));
    }


    @ApiOperation(value = "删除仓库库位")
    @RequiresPermissions("business:warehouse:delete")
    @Log(title = "删除仓库库位", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{locationIds}")
    public R<Boolean> deleteWmsWarehouseLocationByIds(@PathVariable String[] locationIds)
    {
        return R.ok(wmsWarehouseLocationService.deleteWmsWarehouseLocationByIds(locationIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "仓库库位导入模板", WmsWarehouseLocation.class);
    }

    @ApiOperation(value = "导出仓库库位数据")
    @Log(title = "导出仓库库位", businessType = BusinessType.EXPORT)
    @RequiresPermissions("business:warehouse:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, WmsWarehouseLocationQuery query) {
        EasyExcelUtils.writeExcel(response, "仓库库位信息",WmsWarehouseLocation.class,wmsWarehouseLocationService.selectWmsWarehouseLocationList(query));
    }

    @ApiOperation(value = "导入仓库库位数据")
    @Log(title = "导入仓库库位", businessType = BusinessType.IMPORT)
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, WmsWarehouseLocation.class,wmsWarehouseLocationService);
    }
}
