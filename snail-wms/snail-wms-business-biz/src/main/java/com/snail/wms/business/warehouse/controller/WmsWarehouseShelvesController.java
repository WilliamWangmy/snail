package com.snail.wms.business.warehouse.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.business.warehouse.domain.WmsWarehouseShelves;
import com.snail.wms.business.warehouse.query.WmsWarehouseShelvesQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseShelvesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 仓库货架Controller
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Api(value = "仓库货架", tags = "仓库货架")
@RestController
@RequestMapping("/warehouse")
public class WmsWarehouseShelvesController {
    @Autowired
    private IWmsWarehouseShelvesService wmsWarehouseShelvesService;

    @Log(title = "查询仓库货架分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询仓库货架列表")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping("/page")
    public R<PageResult<WmsWarehouseShelves>> getWmsWarehouseShelvesPage(WmsWarehouseShelvesQuery query)
    {
        return R.ok(wmsWarehouseShelvesService.selectWmsWarehouseShelvesPage(query));
    }

    @Log(title = "获取仓库货架详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取仓库货架详细信息")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping(value = "/{shelvesId}")
    public R<WmsWarehouseShelves> getWmsWarehouseShelvesById(@PathVariable("shelvesId") String shelvesId)
    {
        return R.ok(wmsWarehouseShelvesService.getWmsWarehouseShelvesById(shelvesId));
    }

    @ApiOperation(value = "新增仓库货架")
    @RequiresPermissions("business:warehouse:add")
    @Log(title = "新增仓库货架", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveWmsWarehouseShelves(@Validated @RequestBody WmsWarehouseShelves wmsWarehouseShelves) {
        return R.ok(wmsWarehouseShelvesService.insertWmsWarehouseShelves(wmsWarehouseShelves));
    }

    @ApiOperation(value = "修改仓库货架")
    @RequiresPermissions("business:warehouse:edit")
    @Log(title = "修改仓库货架", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateWmsWarehouseShelves(@Validated @RequestBody WmsWarehouseShelves wmsWarehouseShelves) {
        return R.ok(wmsWarehouseShelvesService.updateWmsWarehouseShelves(wmsWarehouseShelves));
    }


    @ApiOperation(value = "删除仓库货架")
    @RequiresPermissions("business:warehouse:delete")
    @Log(title = "删除仓库货架", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{shelvesIds}")
    public R<Boolean> deleteWmsWarehouseShelvesByIds(@PathVariable String[] shelvesIds)
    {
        return R.ok(wmsWarehouseShelvesService.deleteWmsWarehouseShelvesByIds(shelvesIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "仓库货架导入模板", WmsWarehouseShelves.class);
    }

    @ApiOperation(value = "导出仓库货架数据")
    @Log(title = "导出仓库货架", businessType = BusinessType.EXPORT)
    @RequiresPermissions("business:warehouse:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, WmsWarehouseShelvesQuery query) {
        EasyExcelUtils.writeExcel(response, "仓库货架信息",WmsWarehouseShelves.class,wmsWarehouseShelvesService.selectWmsWarehouseShelvesList(query));
    }

    @ApiOperation(value = "导入仓库货架数据")
    @Log(title = "导入仓库货架", businessType = BusinessType.IMPORT)
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, WmsWarehouseShelves.class,wmsWarehouseShelvesService);
    }
}
