package com.snail.wms.business.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.business.warehouse.domain.WmsWarehouseArea;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 库域信息Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Mapper
public interface WmsWarehouseAreaMapper extends BaseMapper<WmsWarehouseArea> {

}
