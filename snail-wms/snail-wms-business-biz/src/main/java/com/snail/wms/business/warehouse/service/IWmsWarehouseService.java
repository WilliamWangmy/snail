package com.snail.wms.business.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.business.warehouse.domain.WmsWarehouse;
import com.snail.wms.business.warehouse.query.WmsWarehouseQuery;

import java.util.List;

/**
 * @Description: 仓库信息Service接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
public interface IWmsWarehouseService extends IService<WmsWarehouse>, IExcelService<WmsWarehouse> {

    /**
     * 查询仓库信息列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<WmsWarehouse> selectWmsWarehousePage(WmsWarehouseQuery query);

    /**
     * 根据id获取仓库信息详情
     *
     * @param warehouseId 主键id
     * @return 结果
     */
    WmsWarehouse getWmsWarehouseById(String warehouseId);

    /**
     * 保存仓库信息数据
     *
     * @param wmsWarehouse 仓库信息信息
     * @return 结果
     */
    String insertWmsWarehouse(WmsWarehouse wmsWarehouse);

    /**
     * 更新仓库信息数据
     *
     * @param wmsWarehouse 仓库信息信息
     * @return 结果
     */
    boolean updateWmsWarehouse(WmsWarehouse wmsWarehouse);

    /**
     * 根据id批量删除仓库信息数据
     *
     * @param warehouseIds 主键ids
     * @return 结果
     */
    boolean deleteWmsWarehouseByIds(String[] warehouseIds);

    /**
     * 查询仓库信息数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsWarehouse> selectWmsWarehouseList(WmsWarehouseQuery query);

}
