package com.snail.wms.business.warehouse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.business.warehouse.domain.WmsWarehouseManager;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 仓库管理人员Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Mapper
public interface WmsWarehouseManagerMapper extends BaseMapper<WmsWarehouseManager> {

}
