package com.snail.wms.business.warehouse.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.business.warehouse.domain.WmsWarehouseManager;
import com.snail.wms.business.warehouse.query.WmsWarehouseManagerQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 仓库管理人员Controller
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Api(value = "仓库管理人员", tags = "仓库管理人员")
@RestController
@RequestMapping("/warehouse")
public class WmsWarehouseManagerController {
    @Autowired
    private IWmsWarehouseManagerService wmsWarehouseManagerService;

    @Log(title = "查询仓库管理人员分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询仓库管理人员列表")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping("/page")
    public R<PageResult<WmsWarehouseManager>> getWmsWarehouseManagerPage(WmsWarehouseManagerQuery query)
    {
        return R.ok(wmsWarehouseManagerService.selectWmsWarehouseManagerPage(query));
    }

    @Log(title = "获取仓库管理人员详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取仓库管理人员详细信息")
    @RequiresPermissions("business:warehouse:query")
    @GetMapping(value = "/{id}")
    public R<WmsWarehouseManager> getWmsWarehouseManagerById(@PathVariable("id") String id)
    {
        return R.ok(wmsWarehouseManagerService.getWmsWarehouseManagerById(id));
    }

    @ApiOperation(value = "新增仓库管理人员")
    @RequiresPermissions("business:warehouse:add")
    @Log(title = "新增仓库管理人员", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveWmsWarehouseManager(@Validated @RequestBody WmsWarehouseManager wmsWarehouseManager) {
        return R.ok(wmsWarehouseManagerService.insertWmsWarehouseManager(wmsWarehouseManager));
    }

    @ApiOperation(value = "修改仓库管理人员")
    @RequiresPermissions("business:warehouse:edit")
    @Log(title = "修改仓库管理人员", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateWmsWarehouseManager(@Validated @RequestBody WmsWarehouseManager wmsWarehouseManager) {
        return R.ok(wmsWarehouseManagerService.updateWmsWarehouseManager(wmsWarehouseManager));
    }


    @ApiOperation(value = "删除仓库管理人员")
    @RequiresPermissions("business:warehouse:delete")
    @Log(title = "删除仓库管理人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public R<Boolean> deleteWmsWarehouseManagerByIds(@PathVariable String[] ids)
    {
        return R.ok(wmsWarehouseManagerService.deleteWmsWarehouseManagerByIds(ids));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "仓库管理人员导入模板", WmsWarehouseManager.class);
    }

    @ApiOperation(value = "导出仓库管理人员数据")
    @Log(title = "导出仓库管理人员", businessType = BusinessType.EXPORT)
    @RequiresPermissions("business:warehouse:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, WmsWarehouseManagerQuery query) {
        EasyExcelUtils.writeExcel(response, "仓库管理人员信息",WmsWarehouseManager.class,wmsWarehouseManagerService.selectWmsWarehouseManagerList(query));
    }

    @ApiOperation(value = "导入仓库管理人员数据")
    @Log(title = "导入仓库管理人员", businessType = BusinessType.IMPORT)
    @RequiresPermissions("business:warehouse:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, WmsWarehouseManager.class,wmsWarehouseManagerService);
    }
}
