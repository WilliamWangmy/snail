package com.snail.wms.business.warehouse.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.business.warehouse.domain.WmsWarehouseArea;
import com.snail.wms.business.warehouse.query.WmsWarehouseAreaQuery;
import com.snail.wms.business.warehouse.service.IWmsWarehouseAreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 库域信息Controller
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Api(value = "库域信息", tags = "库域信息")
@RestController
@RequestMapping("/warehouseArea")
public class WmsWarehouseAreaController {
    @Autowired
    private IWmsWarehouseAreaService wmsWarehouseAreaService;

    @Log(title = "查询库域信息分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询库域信息列表")
    @RequiresPermissions("business:warehouseArea:query")
    @GetMapping("/page")
    public R<PageResult<WmsWarehouseArea>> getWmsWarehouseAreaPage(WmsWarehouseAreaQuery query) {
        return R.ok(wmsWarehouseAreaService.selectWmsWarehouseAreaPage(query));
    }

    @Log(title = "获取库域信息详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取库域信息详细信息")
    @RequiresPermissions("business:warehouseArea:query")
    @GetMapping(value = "/{areaId}")
    public R<WmsWarehouseArea> getWmsWarehouseAreaById(@PathVariable("areaId") String areaId) {
        return R.ok(wmsWarehouseAreaService.getWmsWarehouseAreaById(areaId));
    }

    @ApiOperation(value = "新增库域信息")
    @RequiresPermissions("business:warehouseArea:add")
    @Log(title = "新增库域信息", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveWmsWarehouseArea(@Validated @RequestBody WmsWarehouseArea wmsWarehouseArea) {
        return R.ok(wmsWarehouseAreaService.insertWmsWarehouseArea(wmsWarehouseArea));
    }

    @ApiOperation(value = "修改库域信息")
    @RequiresPermissions("business:warehouseArea:edit")
    @Log(title = "修改库域信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateWmsWarehouseArea(@Validated @RequestBody WmsWarehouseArea wmsWarehouseArea) {
        return R.ok(wmsWarehouseAreaService.updateWmsWarehouseArea(wmsWarehouseArea));
    }


    @ApiOperation(value = "删除库域信息")
    @RequiresPermissions("business:warehouseArea:delete")
    @Log(title = "删除库域信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{areaIds}")
    public R<Boolean> deleteWmsWarehouseAreaByIds(@PathVariable String[] areaIds) {
        return R.ok(wmsWarehouseAreaService.deleteWmsWarehouseAreaByIds(areaIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("business:warehouseArea:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "库域信息导入模板", WmsWarehouseArea.class);
    }

    @ApiOperation(value = "导出库域信息数据")
    @Log(title = "导出库域信息", businessType = BusinessType.EXPORT)
    @RequiresPermissions("business:warehouseArea:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, WmsWarehouseAreaQuery query) {
        EasyExcelUtils.writeExcel(response, "库域信息信息", WmsWarehouseArea.class, wmsWarehouseAreaService.selectWmsWarehouseAreaList(query));
    }

    @ApiOperation(value = "导入库域信息数据")
    @Log(title = "导入库域信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("business:warehouseArea:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, WmsWarehouseArea.class, wmsWarehouseAreaService);
    }
}
