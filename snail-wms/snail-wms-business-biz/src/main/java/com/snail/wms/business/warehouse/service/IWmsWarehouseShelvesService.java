package com.snail.wms.business.warehouse.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.business.warehouse.domain.WmsWarehouseShelves;
import com.snail.wms.business.warehouse.query.WmsWarehouseShelvesQuery;

import java.util.List;

/**
 * @Description: 仓库货架Service接口
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
public interface IWmsWarehouseShelvesService extends IService<WmsWarehouseShelves>, IExcelService<WmsWarehouseShelves> {

    /**
     * 查询仓库货架列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<WmsWarehouseShelves> selectWmsWarehouseShelvesPage(WmsWarehouseShelvesQuery query);

    /**
     * 根据id获取仓库货架详情
     *
     * @param shelvesId 主键id
     * @return 结果
     */
    WmsWarehouseShelves getWmsWarehouseShelvesById(String shelvesId);

    /**
     * 保存仓库货架数据
     *
     * @param wmsWarehouseShelves 仓库货架信息
     * @return 结果
     */
    String insertWmsWarehouseShelves(WmsWarehouseShelves wmsWarehouseShelves);

    /**
     * 更新仓库货架数据
     *
     * @param wmsWarehouseShelves 仓库货架信息
     * @return 结果
     */
    boolean updateWmsWarehouseShelves(WmsWarehouseShelves wmsWarehouseShelves);

    /**
     * 根据id批量删除仓库货架数据
     *
     * @param shelvesIds 主键ids
     * @return 结果
     */
    boolean deleteWmsWarehouseShelvesByIds(String[] shelvesIds);

    /**
     * 查询仓库货架数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsWarehouseShelves> selectWmsWarehouseShelvesList(WmsWarehouseShelvesQuery query);

}
