package com.snail.wms.business.warehouse.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 库域信息对象 wms_warehouse_area
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "库域信息")
@TableName("wms_warehouse_area")
public class WmsWarehouseArea implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String areaId;

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @NotEmpty(message = "仓库id不能为空!")
    private String warehouseId;

    /**
     * 区域编码
     */
    @ApiModelProperty(value = "区域编码")
    @Excel(value = "区域编码", index = 0, template = true)
    @NotEmpty(message = "区域编码不能为空!")
    private String areaCode;

    /**
     * 区域名称
     */
    @ApiModelProperty(value = "区域名称")
    @Excel(value = "区域名称", index = 1, template = true)
    @NotEmpty(message = "区域名称不能为空!")
    private String areaName;

    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    @Excel(value = "长", index = 2, template = true)
    @NotEmpty(message = "长不能为空!")
    private BigDecimal length;

    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    @Excel(value = "宽", index = 3, template = true)
    @NotEmpty(message = "宽不能为空!")
    private BigDecimal width;

    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    @Excel(value = "高", index = 4, template = true)
    @NotEmpty(message = "高不能为空!")
    private BigDecimal height;

    /**
     * 逻辑删除：1.删除，0.未删除
     */
    @ApiModelProperty(value = "逻辑删除：1.删除，0.未删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @Excel(value = "状态", index = 5, template = true)
    @NotEmpty(message = "状态不能为空!")
    private String status;


}
