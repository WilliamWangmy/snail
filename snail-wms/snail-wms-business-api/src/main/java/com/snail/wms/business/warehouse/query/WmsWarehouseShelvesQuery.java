package com.snail.wms.business.warehouse.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 仓库货架查询对象
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "仓库货架")
public class WmsWarehouseShelvesQuery extends BaseQuery {
    @ApiModelProperty(value = "仓库id")
    private String warehouseId;
    @ApiModelProperty(value = "区域id")
    private String areaId;
    @ApiModelProperty(value = "货架编码")
    private String shelvesCode;
    @ApiModelProperty(value = "货架名称")
    private String shelvesName;
}
