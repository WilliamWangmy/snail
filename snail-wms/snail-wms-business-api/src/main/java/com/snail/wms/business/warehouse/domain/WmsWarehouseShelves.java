package com.snail.wms.business.warehouse.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 仓库货架对象 wms_warehouse_shelves
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "仓库货架")
@TableName("wms_warehouse_shelves")
public class WmsWarehouseShelves implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String shelvesId;
    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @Excel(value = "仓库id", index = 0, template = true)
    @NotEmpty(message = "仓库id不能为空!")
    private String warehouseId;
    /**
     * 区域id
     */
    @ApiModelProperty(value = "区域id")
    @Excel(value = "区域id", index = 1, template = true)
    @NotEmpty(message = "区域id不能为空!")
    private String areaId;
    /**
     * 货架编码
     */
    @ApiModelProperty(value = "货架编码")
    @Excel(value = "货架编码", index = 2, template = true)
    @NotEmpty(message = "货架编码不能为空!")
    private String shelvesCode;
    /**
     * 货架名称
     */
    @ApiModelProperty(value = "货架名称")
    @Excel(value = "货架名称", index = 3, template = true)
    @NotEmpty(message = "货架名称不能为空!")
    private String shelvesName;
    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    @Excel(value = "长", index = 4, template = true)
    private BigDecimal length;
    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    @Excel(value = "宽", index = 5, template = true)
    private BigDecimal width;
    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    @Excel(value = "高", index = 6, template = true)
    private BigDecimal height;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @Excel(value = "状态", index = 7, template = true)
    @NotEmpty(message = "状态不能为空!")
    private String status;
    /**
     * 逻辑删除：1.删除，0.未删除
     */
    @ApiModelProperty(value = "逻辑删除：1.删除，0.未删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    @NotEmpty(message = "逻辑删除：1.删除，0.未删除不能为空!")
    private String delFlag;

}
