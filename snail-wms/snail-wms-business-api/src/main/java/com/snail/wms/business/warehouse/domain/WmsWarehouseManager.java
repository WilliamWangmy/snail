package com.snail.wms.business.warehouse.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @Description: 仓库管理人员对象 wms_warehouse_manager
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "仓库管理人员")
@TableName("wms_warehouse_manager")
public class WmsWarehouseManager implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @Excel(value = "仓库id", index = 0, template = true)
    @NotEmpty(message = "仓库id不能为空!")
    private String warehouseId;
    /**
     * 人员id
     */
    @ApiModelProperty(value = "人员id")
    @Excel(value = "人员id", index = 1, template = true)
    @NotEmpty(message = "人员id不能为空!")
    private String userId;
    /**
     * 人员名称
     */
    @ApiModelProperty(value = "人员名称")
    @Excel(value = "人员名称", index = 2, template = true)
    @NotEmpty(message = "人员名称不能为空!")
    private String userName;
    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织id", index = 3, template = true)
    @NotEmpty(message = "组织id不能为空!")
    private String deptId;
    /**
     * 组织名称
     */
    @ApiModelProperty(value = "组织名称")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织名称", index = 4, template = true)
    @NotEmpty(message = "组织名称不能为空!")
    private String deptName;
    /**
     * 组织全路径
     */
    @ApiModelProperty(value = "组织全路径")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织全路径", index = 5, template = true)
    @NotEmpty(message = "组织全路径不能为空!")
    private String deptFullName;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    @Excel(value = "联系电话", index = 6, template = true)
    @NotEmpty(message = "联系电话不能为空!")
    private String mobile;
    /**
     * 角色编码
     */
    @ApiModelProperty(value = "角色编码")
    @Excel(value = "角色编码", index = 7, template = true)
    @NotEmpty(message = "角色编码不能为空!")
    private String roleCode;
    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    @Excel(value = "角色名称", index = 8, template = true)
    @NotEmpty(message = "角色名称不能为空!")
    private String roleName;

}
