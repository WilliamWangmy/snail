package com.snail.wms.business.warehouse.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 仓库管理人员查询对象
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "仓库管理人员")
public class WmsWarehouseManagerQuery extends BaseQuery {
    @ApiModelProperty(value = "仓库id")
    private String warehouseId;
    @ApiModelProperty(value = "人员名称")
    private String userName;
    @ApiModelProperty(value = "联系电话")
    private String mobile;
}
