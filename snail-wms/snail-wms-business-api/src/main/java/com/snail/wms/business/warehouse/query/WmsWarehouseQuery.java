package com.snail.wms.business.warehouse.query;

import com.snail.common.core.web.domain.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @Description: 仓库信息查询对象
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "仓库信息")
public class WmsWarehouseQuery extends BaseQuery {
    @ApiModelProperty(value = "仓库编码")
    private String warehouseCode;
    @ApiModelProperty(value = "仓库名称")
    private String warehouseName;
    @ApiModelProperty(value = "状态")
    private String warehouseStatus;
    @ApiModelProperty(value = "组织id")
    private String deptId;
    @ApiModelProperty(value = "组织全路径id")
    private String deptFullId;
    @ApiModelProperty(value = "组织全路径")
    private String deptFullName;
    @ApiModelProperty(value = "更新人id")
    private String updateUserId;
    @ApiModelProperty(value = "更新人")
    private String updateUserName;
}
