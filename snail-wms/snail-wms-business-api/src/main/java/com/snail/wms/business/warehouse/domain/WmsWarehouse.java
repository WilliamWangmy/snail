package com.snail.wms.business.warehouse.domain;

import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.snail.common.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import javax.validation.constraints.NotEmpty;

/**
 * @Description: 仓库信息对象 wms_warehouse
 * @Author: snail
 * @CreateDate: 2024-05-25
 * @Version: V1.0
 */
@Data
@HeadRowHeight(50)
@ContentRowHeight(25)
@ColumnWidth(20)
@ApiModel(value = "仓库信息")
@TableName("wms_warehouse")
public class WmsWarehouse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 仓库id
     */
    @ApiModelProperty(value = "仓库id")
    @TableId(type = IdType.ASSIGN_UUID)
    private String warehouseId;

    /**
     * 仓库编码
     */
    @ApiModelProperty(value = "仓库编码")
    @Excel(value = "仓库编码", index = 0, template = true)
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称")
    @Excel(value = "仓库名称", index = 1, template = true)
    @NotEmpty(message = "仓库名称不能为空!")
    private String warehouseName;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    @Excel(value = "状态", index = 2, template = true)
    @NotEmpty(message = "状态不能为空!")
    private String warehouseStatus;

    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    @Excel(value = "省", index = 3, template = true)
    @NotEmpty(message = "省不能为空!")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    @Excel(value = "市", index = 4, template = true)
    @NotEmpty(message = "市不能为空!")
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(value = "区")
    @Excel(value = "区", index = 5, template = true)
    @NotEmpty(message = "区不能为空!")
    private String district;

    /**
     * 仓库详细地址
     */
    @ApiModelProperty(value = "仓库详细地址")
    @Excel(value = "仓库详细地址", index = 6, template = true)
    @NotEmpty(message = "仓库详细地址不能为空!")
    private String warehouseAddress;

    /**
     * 面积
     */
    @ApiModelProperty(value = "面积")
    @Excel(value = "面积", index = 7, template = true)
    private BigDecimal area;

    /**
     * 经维度
     */
    @ApiModelProperty(value = "经维度")
    @Excel(value = "经维度", index = 8, template = true)
    private String location;

    /**
     * 长
     */
    @ApiModelProperty(value = "长")
    @Excel(value = "长", index = 9, template = true)
    @NotEmpty(message = "长不能为空!")
    private BigDecimal length;

    /**
     * 宽
     */
    @ApiModelProperty(value = "宽")
    @Excel(value = "宽", index = 10, template = true)
    @NotEmpty(message = "宽不能为空!")
    private BigDecimal width;

    /**
     * 高
     */
    @ApiModelProperty(value = "高")
    @Excel(value = "高", index = 11, template = true)
    @NotEmpty(message = "高不能为空!")
    private BigDecimal height;

    /**
     * 所属组织id
     */
    @ApiModelProperty(value = "所属组织id")
    @NotEmpty(message = "所属组织id不能为空!")
    private String belongOrgId;

    /**
     * 所属组织名称
     */
    @ApiModelProperty(value = "所属组织名称")
    @NotEmpty(message = "所属组织名称不能为空!")
    private String belongOrgName;

    /**
     * 所属组织全路径id
     */
    @ApiModelProperty(value = "所属组织全路径id")
    @NotEmpty(message = "所属组织全路径id不能为空!")
    private String belongOrgFullId;

    /**
     * 所属组织全路径
     */
    @ApiModelProperty(value = "所属组织全路径")
    @Excel(value = "所属组织全路径", index = 12, template = true)
    @NotEmpty(message = "所属组织全路径不能为空!")
    private String belongOrgFullName;

    /**
     * 仓库联系电话
     */
    @ApiModelProperty(value = "仓库联系电话")
    @NotEmpty(message = "仓库联系电话不能为空!")
    private String mobile;

    /**
     * 逻辑删除：1.删除，0.未删除
     */
    @ApiModelProperty(value = "逻辑删除：1.删除，0.未删除")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    @NotEmpty(message = "逻辑删除：1.删除，0.未删除不能为空!")
    private String delFlag;

    /**
     * 租户标识
     */
    @ApiModelProperty(value = "租户标识")
    @TableField(fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id")
    @TableField(fill = FieldFill.INSERT)
    private String createUserId;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField(fill = FieldFill.INSERT)
    private String createUserName;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织id", index = 13, template = true)
    private String deptId;

    /**
     * 组织全路径id
     */
    @ApiModelProperty(value = "组织全路径id")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织全路径id", index = 14, template = true)
    private String deptFullId;

    /**
     * 组织全路径
     */
    @ApiModelProperty(value = "组织全路径")
    @TableField(fill = FieldFill.INSERT)
    @Excel(value = "组织全路径", index = 15, template = true)
    private String deptFullName;

    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id")
    @TableField(fill = FieldFill.UPDATE)
    @Excel(value = "更新人id", index = 16, template = true)
    private String updateUserId;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(fill = FieldFill.UPDATE)
    @Excel(value = "更新人", index = 17, template = true)
    private String updateUserName;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;


}
