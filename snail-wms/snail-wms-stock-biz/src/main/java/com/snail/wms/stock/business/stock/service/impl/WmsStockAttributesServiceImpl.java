package com.snail.wms.stock.business.stock.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.wms.stock.business.stock.domain.WmsStockAttributes;
import com.snail.wms.stock.business.stock.mapper.WmsStockAttributesMapper;
import com.snail.wms.stock.business.stock.query.WmsStockAttributesQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockAttributesService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 库存属性Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Service
public class WmsStockAttributesServiceImpl extends ServiceImpl<WmsStockAttributesMapper, WmsStockAttributes> implements IWmsStockAttributesService {

    @Override
    public List<WmsStockAttributes> selectWmsStockAttributesList(WmsStockAttributesQuery query) {
        LambdaQueryWrapper<WmsStockAttributes> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }
}
