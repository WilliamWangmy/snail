package com.snail.wms.stock.business.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.stock.business.stock.domain.WmsStockWarehouse;
import com.snail.wms.stock.business.stock.query.WmsStockWarehouseQuery;

import java.util.List;

/**
 * @Description: 库存仓库Service接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
public interface IWmsStockWarehouseService extends IService<WmsStockWarehouse> {

    /**
     * 查询库存仓库数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsStockWarehouse> selectWmsStockWarehouseList(WmsStockWarehouseQuery query);

}
