package com.snail.wms.stock.business.stock.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.exception.ServiceException;
import com.snail.common.core.utils.StringUtils;
import com.snail.wms.stock.business.stock.domain.WmsStockOperateDetail;
import com.snail.wms.stock.business.stock.dto.StockResultDto;
import com.snail.wms.stock.business.stock.mapper.WmsStockOperateDetailMapper;
import com.snail.wms.stock.business.stock.query.WmsStockOperateDetailQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockOperateDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 库存操作明细业务实现
 * @Author: Snail
 * @CreateDate: 2024/3/31 17:16
 * @Version: V1.0
 */
@Service
public class WmsStockOperateDetailService extends ServiceImpl<WmsStockOperateDetailMapper, WmsStockOperateDetail> implements IWmsStockOperateDetailService {
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createStockOperateDetail(List<StockResultDto> resultDtoList) {
        if (CollUtil.isEmpty(resultDtoList)) {
            throw new ServiceException("库存操作明细数据为空!");
        }
        List<WmsStockOperateDetail> details = resultDtoList.stream().map(item -> {
            WmsStockOperateDetail detail = new WmsStockOperateDetail();
            detail.setBillId(item.getBillId());
            detail.setBillCode(item.getBillCode());
            detail.setBillDetailId(item.getBillDetailId());
            detail.setStockAmountId(item.getStockAmountId());
            detail.setCreateTime(LocalDateTime.now());
            detail.setType(item.getType());
            detail.setAmount(item.getAmount());
            return detail;
        }).collect(Collectors.toList());
        this.saveBatch(details);
    }

    @Override
    public List<WmsStockOperateDetail> selectStockOperateDetail(WmsStockOperateDetailQuery query) {
        LambdaQueryWrapper<WmsStockOperateDetail> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotEmpty(query.getBillId()), WmsStockOperateDetail::getBillId, query.getBillId());
        queryWrapper.eq(StringUtils.isNotEmpty(query.getType()), WmsStockOperateDetail::getType, query.getType());
        queryWrapper.eq(StringUtils.isNotEmpty(query.getBillDetailId()), WmsStockOperateDetail::getBillDetailId, query.getBillDetailId());
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteStockOperateDetail(String billId) {
        LambdaUpdateWrapper<WmsStockOperateDetail> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(WmsStockOperateDetail::getBillId, billId);
        this.baseMapper.delete(updateWrapper);
    }
}
