package com.snail.wms.stock.business.stock.service;

import com.snail.common.core.web.page.PageResult;
import com.snail.wms.stock.business.stock.dto.StockDetailDto;
import com.snail.wms.stock.business.stock.mq.StockCallDto;
import com.snail.wms.stock.business.stock.mq.StockCallbackDto;
import com.snail.wms.stock.business.stock.query.StockDetailQuery;

/**
 * @Description: 库存管理业务接口
 * @Author: snail
 * @CreateDate: 2024/3/18 16:48
 * @Version: V1.0
 */
public interface IWmsStockService {
    /**
     * 入库操作
     *
     * @param dto 输入信息
     * @return 结果
     */
    StockCallbackDto operateInStock(StockCallDto dto);

    /**
     * 出库操作
     *
     * @param callDto 输入信息
     * @return 结果
     */
    StockCallbackDto operateOutStock(StockCallDto callDto);

    /**
     * 分页查询库存信息
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<StockDetailDto> pageStockDetail(StockDetailQuery query);
}
