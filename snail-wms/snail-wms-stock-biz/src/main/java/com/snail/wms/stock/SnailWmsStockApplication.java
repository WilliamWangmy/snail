package com.snail.wms.stock;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import org.springframework.boot.SpringApplication;

/**
 * @Description: 库存服务启动类
 * @Author: snail
 * @CreateDate: 2024/3/7 9:13
 * @Version: V1.0
 */
@SnailCloudApplication
public class SnailWmsStockApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailWmsStockApplication.class,args);
    }
}
