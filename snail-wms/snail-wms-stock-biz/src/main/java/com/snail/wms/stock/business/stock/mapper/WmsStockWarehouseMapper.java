package com.snail.wms.stock.business.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.stock.business.stock.domain.WmsStockWarehouse;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 库存仓库Mapper接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Mapper
public interface WmsStockWarehouseMapper extends BaseMapper<WmsStockWarehouse> {

}
