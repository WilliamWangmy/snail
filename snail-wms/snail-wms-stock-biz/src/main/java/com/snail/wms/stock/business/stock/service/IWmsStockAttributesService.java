package com.snail.wms.stock.business.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.stock.business.stock.domain.WmsStockAttributes;
import com.snail.wms.stock.business.stock.query.WmsStockAttributesQuery;

import java.util.List;

/**
 * @Description: 库存属性Service接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
public interface IWmsStockAttributesService extends IService<WmsStockAttributes> {


    /**
     * 查询库存属性数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsStockAttributes> selectWmsStockAttributesList(WmsStockAttributesQuery query);

}
