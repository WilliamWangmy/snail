package com.snail.wms.stock.business.stock.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.stock.business.stock.domain.WmsStockAmount;
import com.snail.wms.stock.business.stock.dto.StockAmountDto;
import com.snail.wms.stock.business.stock.dto.StockDetailDto;
import com.snail.wms.stock.business.stock.mapper.WmsStockAmountMapper;
import com.snail.wms.stock.business.stock.query.StockDetailQuery;
import com.snail.wms.stock.business.stock.query.WmsStockAmountQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockAmountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 库存数量Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Service
public class WmsStockAmountServiceImpl extends ServiceImpl<WmsStockAmountMapper, WmsStockAmount> implements IWmsStockAmountService {

    @Override
    public List<WmsStockAmount> selectWmsStockAmountList(WmsStockAmountQuery query) {
        LambdaQueryWrapper<WmsStockAmount> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<StockAmountDto> selectStockAmount(String stockWarehouseId, String stockDimensionId, String stockAttributeId) {
        return baseMapper.selectStockAmount(stockWarehouseId, stockDimensionId, stockAttributeId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void preemptionStockAmount(List<StockAmountDto> amountDtoList) {
        List<WmsStockAmount> stockAmounts = amountDtoList.stream().map(item -> {
            WmsStockAmount stockAmount = new WmsStockAmount();
            stockAmount.setId(item.getStockAmountId());
            stockAmount.setPreemptionAmount(item.getPreemptionAmount());
            return stockAmount;
        }).collect(Collectors.toList());
        this.updateBatchById(stockAmounts);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void freezeStockAmount(List<StockAmountDto> amountDtoList) {
        List<WmsStockAmount> stockAmounts = amountDtoList.stream().map(item -> {
            WmsStockAmount stockAmount = new WmsStockAmount();
            stockAmount.setId(item.getStockAmountId());
            stockAmount.setFreezeAmount(item.getFreezeAmount());
            return stockAmount;
        }).collect(Collectors.toList());
        this.updateBatchById(stockAmounts);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void outStockAmount(List<StockAmountDto> amountDtoList) {
        List<WmsStockAmount> stockAmounts = amountDtoList.stream().map(item -> {
            WmsStockAmount stockAmount = new WmsStockAmount();
            stockAmount.setId(item.getStockAmountId());
            stockAmount.setOutAmount(item.getOutAmount());
            stockAmount.setPreemptionAmount(item.getPreemptionAmount());
            stockAmount.setFreezeAmount(item.getFreezeAmount());
            return stockAmount;
        }).collect(Collectors.toList());
        this.updateBatchById(stockAmounts);
    }

    @Override
    public PageResult<StockDetailDto> pageStockDetail(StockDetailQuery query) {
        Page<StockDetailDto> page = PageUtils.buildPage(query);
        page = this.baseMapper.pageStockDetail(page,query);
        return PageUtils.pageResult(page);
    }


}
