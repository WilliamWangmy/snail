package com.snail.wms.stock.business.stock.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.StringUtils;
import com.snail.wms.stock.business.stock.domain.WmsStockDimensions;
import com.snail.wms.stock.business.stock.mapper.WmsStockDimensionsMapper;
import com.snail.wms.stock.business.stock.query.WmsStockDimensionsQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockDimensionsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 库存维度Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Service
public class WmsStockDimensionsServiceImpl extends ServiceImpl<WmsStockDimensionsMapper, WmsStockDimensions> implements IWmsStockDimensionsService {

    @Override
    public List<WmsStockDimensions> selectWmsStockDimensionsList(WmsStockDimensionsQuery query) {
        LambdaQueryWrapper<WmsStockDimensions> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSkuId()),WmsStockDimensions::getSkuId,query.getSkuId());
        return baseMapper.selectList(queryWrapper);
    }

}
