package com.snail.wms.stock.business.stock.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.StringUtils;
import com.snail.wms.stock.business.stock.domain.WmsStockWarehouse;
import com.snail.wms.stock.business.stock.mapper.WmsStockWarehouseMapper;
import com.snail.wms.stock.business.stock.query.WmsStockWarehouseQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockWarehouseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 库存仓库Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Service
public class WmsStockWarehouseServiceImpl extends ServiceImpl<WmsStockWarehouseMapper, WmsStockWarehouse> implements IWmsStockWarehouseService {

    @Override
    public List<WmsStockWarehouse> selectWmsStockWarehouseList(WmsStockWarehouseQuery query) {
        LambdaQueryWrapper<WmsStockWarehouse> queryWrapper = new LambdaQueryWrapper<>();
        //库位
        queryWrapper.eq(StringUtils.isNotEmpty(query.getLocationId()),WmsStockWarehouse::getLocationId,query.getLocationId());
        //货架
        queryWrapper.eq(StringUtils.isNotEmpty(query.getShelvesId()),WmsStockWarehouse::getShelvesId,query.getShelvesId());
        //库区
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAreaId()),WmsStockWarehouse::getAreaId,query.getAreaId());
        //仓库
        queryWrapper.eq(StringUtils.isNotEmpty(query.getWarehouseId()),WmsStockWarehouse::getWarehouseId,query.getWarehouseId());
        return baseMapper.selectList(queryWrapper);
    }


}
