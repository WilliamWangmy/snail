package com.snail.wms.stock.business.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.stock.business.stock.domain.WmsStockDimensions;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 库存维度Mapper接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Mapper
public interface WmsStockDimensionsMapper extends BaseMapper<WmsStockDimensions> {

}
