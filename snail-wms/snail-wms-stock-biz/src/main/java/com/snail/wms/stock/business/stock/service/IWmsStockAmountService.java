package com.snail.wms.stock.business.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.stock.business.stock.domain.WmsStockAmount;
import com.snail.wms.stock.business.stock.dto.StockAmountDto;
import com.snail.wms.stock.business.stock.dto.StockDetailDto;
import com.snail.wms.stock.business.stock.query.StockDetailQuery;
import com.snail.wms.stock.business.stock.query.WmsStockAmountQuery;

import java.util.List;

/**
 * @Description: 库存数量Service接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
public interface IWmsStockAmountService extends IService<WmsStockAmount> {

    /**
     * 查询库存数量数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsStockAmount> selectWmsStockAmountList(WmsStockAmountQuery query);

    /**
     * 查询库存数量信息
     *
     * @param stockWarehouseId 库存仓库id
     * @param stockDimensionId 库存维度id
     * @param stockAttributeId 库存属性id
     * @return 结果
     */
    List<StockAmountDto> selectStockAmount(String stockWarehouseId, String stockDimensionId, String stockAttributeId);

    /**
     * 预占库存更新预占两
     *
     * @param amountDtoList 需要更新的数据
     */
    void preemptionStockAmount(List<StockAmountDto> amountDtoList);

    /**
     * 冻结库存数量
     *
     * @param amountDtoList 需要更新的数据
     */
    void freezeStockAmount(List<StockAmountDto> amountDtoList);

    /**
     * 实占库存数量
     *
     * @param amountDtoList 需要更新的数据
     */
    void outStockAmount(List<StockAmountDto> amountDtoList
    );

    /**
     * 分页查询库存明细
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<StockDetailDto> pageStockDetail(StockDetailQuery query);
}
