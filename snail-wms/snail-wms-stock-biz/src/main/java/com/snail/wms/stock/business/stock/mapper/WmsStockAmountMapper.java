package com.snail.wms.stock.business.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.snail.wms.stock.business.stock.domain.WmsStockAmount;
import com.snail.wms.stock.business.stock.dto.StockAmountDto;
import com.snail.wms.stock.business.stock.dto.StockDetailDto;
import com.snail.wms.stock.business.stock.query.StockDetailQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Description: 库存数量Mapper接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
@Mapper
public interface WmsStockAmountMapper extends BaseMapper<WmsStockAmount> {

    /**
     * 查询库存数量信息
     *
     * @param stockWarehouseId 库存仓库id
     * @param stockDimensionId 库存维度id
     * @param stockAttributeId 库存属性id
     * @return 结果
     */
    List<StockAmountDto> selectStockAmount(@Param("stockWarehouseId") String stockWarehouseId, @Param("stockDimensionId") String stockDimensionId, @Param("stockAttributeId") String stockAttributeId);

    /**
     * 分页查询库存明细
     *
     * @param page  分页参数
     * @param query 查询参数
     * @return 结果
     */
    Page<StockDetailDto> pageStockDetail(@Param("page") Page<StockDetailDto> page, @Param("query") StockDetailQuery query);
}
