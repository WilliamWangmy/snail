package com.snail.wms.stock.business.stock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.stock.business.stock.domain.WmsStockOperateDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 库存操作明细
 * @Author: Snail
 * @CreateDate: 2024/3/31 17:14
 * @Version: V1.0
 */
@Mapper
public interface WmsStockOperateDetailMapper extends BaseMapper<WmsStockOperateDetail> {
}
