package com.snail.wms.stock.business.stock.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.stock.business.stock.dto.StockDetailDto;
import com.snail.wms.stock.business.stock.mq.StockCallDto;
import com.snail.wms.stock.business.stock.mq.StockCallbackDto;
import com.snail.wms.stock.business.stock.query.StockDetailQuery;
import com.snail.wms.stock.business.stock.service.IWmsStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 库存Controller
 * @Author: snail
 * @CreateDate: 2024/3/18 10:08
 * @Version: V1.0
 */
@Api(value = "库存管理", tags = "库存管理")
@RestController
@RequestMapping("/stock")
public class WmsStockController {

    @Autowired
    private IWmsStockService wmsStockService;

    @ApiOperation(value = "入库操作")
    @PostMapping("/inStock")
    public R<StockCallbackDto> inStock(@RequestBody StockCallDto dto) {
        return R.ok(wmsStockService.operateInStock(dto));
    }

    @ApiOperation(value = "出库操作")
    @PostMapping("/outStock")
    public R<StockCallbackDto> outStock(@RequestBody StockCallDto dto) {
        return R.ok(wmsStockService.operateOutStock(dto));
    }

    @ApiOperation(value = "查询库存信息")
    @PostMapping("/page")
    public R<PageResult<StockDetailDto>> pageStockDetail(StockDetailQuery query){
        return R.ok(wmsStockService.pageStockDetail(query));
    }
}
