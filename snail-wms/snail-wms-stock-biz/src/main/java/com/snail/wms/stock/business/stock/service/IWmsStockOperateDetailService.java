package com.snail.wms.stock.business.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.stock.business.stock.domain.WmsStockOperateDetail;
import com.snail.wms.stock.business.stock.dto.StockResultDto;
import com.snail.wms.stock.business.stock.query.WmsStockOperateDetailQuery;

import java.util.List;

/**
 * @Description: 库存操作明细业务接口
 * @Author: Snail
 * @CreateDate: 2024/3/31 17:15
 * @Version: V1.0
 */
public interface IWmsStockOperateDetailService extends IService<WmsStockOperateDetail> {
    /**
     * 创建库存操作流水
     *
     * @param resultDtoList 库存操作明细
     */
    void createStockOperateDetail(List<StockResultDto> resultDtoList);

    /**
     * 获取库存操作流水
     *
     * @param query 查询参数
     * @return 结果
     */
    List<WmsStockOperateDetail> selectStockOperateDetail(WmsStockOperateDetailQuery query);

    /**
     * 删除操作明细
     *
     * @param billId 单据id
     */
    void deleteStockOperateDetail(String billId);

}
