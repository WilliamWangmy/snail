package com.snail.wms.stock.business.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.stock.business.stock.domain.WmsStockDimensions;
import com.snail.wms.stock.business.stock.query.WmsStockDimensionsQuery;

import java.util.List;

/**
 * @Description: 库存维度Service接口
 * @Author: snail
 * @CreateDate: 2024-03-17
 * @Version: V1.0
 */
public interface IWmsStockDimensionsService extends IService<WmsStockDimensions> {


    /**
     * 查询库存维度数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<WmsStockDimensions> selectWmsStockDimensionsList(WmsStockDimensionsQuery query);

}
