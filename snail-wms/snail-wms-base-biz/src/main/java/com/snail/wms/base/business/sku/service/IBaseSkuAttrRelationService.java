package com.snail.wms.base.business.sku.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.base.business.sku.domain.BaseSkuAttrRelation;
import com.snail.wms.base.business.sku.query.BaseSkuAttrRelationQuery;

import java.util.List;

/**
 * @Description: sku属性Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseSkuAttrRelationService extends IService<BaseSkuAttrRelation>, IExcelService<BaseSkuAttrRelation> {

    /**
     * 查询sku属性列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseSkuAttrRelation> selectBaseSkuAttrRelationPage(BaseSkuAttrRelationQuery query);

    /**
     * 根据id获取sku属性详情
     *
     * @param id 主键id
     * @return 结果
     */
    BaseSkuAttrRelation getBaseSkuAttrRelationById(String id);

    /**
     * 保存sku属性数据
     *
     * @param baseSkuAttrRelation sku属性信息
     * @return 结果
     */
    String insertBaseSkuAttrRelation(BaseSkuAttrRelation baseSkuAttrRelation);

    /**
     * 更新sku属性数据
     *
     * @param baseSkuAttrRelation sku属性信息
     * @return 结果
     */
    boolean updateBaseSkuAttrRelation(BaseSkuAttrRelation baseSkuAttrRelation);

    /**
     * 根据id批量删除sku属性数据
     *
     * @param ids 主键ids
     * @return 结果
     */
    boolean deleteBaseSkuAttrRelationByIds(String[] ids);

    /**
     * 查询sku属性数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<BaseSkuAttrRelation> selectBaseSkuAttrRelationList(BaseSkuAttrRelationQuery query);

}
