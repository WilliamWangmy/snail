package com.snail.wms.base.business.spu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;
import com.snail.wms.base.business.spu.mapper.BaseSpuAttrRelationMapper;
import com.snail.wms.base.business.spu.service.IBaseSpuAttrRelationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: spu属性Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Service
public class BaseSpuAttrRelationServiceImpl extends ServiceImpl<BaseSpuAttrRelationMapper, BaseSpuAttrRelation> implements IBaseSpuAttrRelationService {

    @Override
    public List<BaseSpuAttrRelation> getSpuAttrRelationBySpuId(String spuId) {
        LambdaQueryWrapper<BaseSpuAttrRelation> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseSpuAttrRelation::getSpuId,spuId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteSpuAttrRelationBySpuId(String spuId) {
        LambdaUpdateWrapper<BaseSpuAttrRelation> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(BaseSpuAttrRelation::getSpuId,spuId);
        this.baseMapper.delete(updateWrapper);
    }
}
