package com.snail.wms.base.business.sku.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.base.business.sku.domain.BaseSku;
import com.snail.wms.base.business.sku.query.BaseSkuQuery;

import java.util.List;

/**
 * @Description: sku信息Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseSkuService extends IService<BaseSku>, IExcelService<BaseSku> {

    /**
     * 查询sku信息列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseSku> selectBaseSkuPage(BaseSkuQuery query);

    /**
     * 根据id获取sku信息详情
     *
     * @param skuId 主键id
     * @return 结果
     */
    BaseSku getBaseSkuById(String skuId);

    /**
     * 保存sku信息数据
     *
     * @param baseSku sku信息信息
     * @return 结果
     */
    String insertBaseSku(BaseSku baseSku);

    /**
     * 更新sku信息数据
     *
     * @param baseSku sku信息信息
     * @return 结果
     */
    boolean updateBaseSku(BaseSku baseSku);

    /**
     * 根据id批量删除sku信息数据
     *
     * @param skuIds 主键ids
     * @return 结果
     */
    boolean deleteBaseSkuByIds(String[] skuIds);

    /**
     * 查询sku信息数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<BaseSku> selectBaseSkuList(BaseSkuQuery query);

}
