package com.snail.wms.base.business.district.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.wms.base.business.district.domain.BaseDistrict;
import com.snail.wms.base.business.district.query.BaseDistrictQuery;

import java.util.List;

/**
 * @Description: 行政区域业务接口
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
public interface IBaseDistrictService extends IService<BaseDistrict> {
    /**
     * 分页查询
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseDistrict> getBaseDistrictPage(BaseDistrictQuery query);

    /**
     * 接口查询
     *
     * @param query 查询参数
     * @return 结果
     */
    Boolean inquiryBaseDistrict(DistrictQuery query);

    /**
     * 根据区域编码获取行政区域
     *
     * @param adcode 区域编码
     * @return 结果
     */
    List<BaseDistrict> getBaseDistrictByParent(String adcode);
}
