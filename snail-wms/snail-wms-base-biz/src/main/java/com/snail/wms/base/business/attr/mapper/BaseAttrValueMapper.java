package com.snail.wms.base.business.attr.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.attr.domain.BaseAttrValue;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: 属性值Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Mapper
public interface BaseAttrValueMapper extends BaseMapper<BaseAttrValue> {

}
