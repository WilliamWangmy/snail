package com.snail.wms.base.business.spu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: spu属性Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Mapper
public interface BaseSpuAttrRelationMapper extends BaseMapper<BaseSpuAttrRelation> {

}
