package com.snail.wms.base.business.spu.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.dto.BaseSpuDto;
import com.snail.wms.base.business.spu.dto.SpuExcelDto;
import com.snail.wms.base.business.spu.query.BaseSpuQuery;
import com.snail.wms.base.business.spu.service.IBaseSpuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: spu信息Controller
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Api(value = "spu信息", tags = "spu信息")
@RestController
@RequestMapping("/spu")
public class BaseSpuController {
    @Autowired
    private IBaseSpuService baseSpuService;

    @Log(title = "查询spu信息分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询spu信息列表")
    @RequiresPermissions("base:spu:query")
    @GetMapping("/page")
    public R<PageResult<BaseSpu>> getBaseSpuPage(BaseSpuQuery query) {
        return R.ok(baseSpuService.selectBaseSpuPage(query));
    }


    @Log(title = "查询spu信息列表", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询spu信息列表")
    @GetMapping("/list")
    public R<List<BaseSpu>> getBaseSpuList(BaseSpuQuery query) {
        return R.ok(baseSpuService.selectBaseSpuList(query));
    }

    @Log(title = "获取spu信息详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取spu信息详细信息")
    @RequiresPermissions("base:spu:query")
    @GetMapping(value = "/{spuId}")
    public R<BaseSpuDto> getBaseSpuById(@PathVariable("spuId") String spuId) {
        return R.ok(baseSpuService.getBaseSpuById(spuId));
    }

    @ApiOperation(value = "新增spu信息")
    @RequiresPermissions("base:spu:add")
    @Log(title = "新增spu信息", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveBaseSpu(@Validated @RequestBody BaseSpuDto spuDto) {
        return R.ok(baseSpuService.insertBaseSpu(spuDto));
    }

    @ApiOperation(value = "修改spu信息")
    @RequiresPermissions("base:spu:edit")
    @Log(title = "修改spu信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateBaseSpu(@Validated @RequestBody BaseSpuDto spuDto) {
        return R.ok(baseSpuService.updateBaseSpu(spuDto));
    }

    @ApiOperation(value = "修改spu状态")
    @RequiresPermissions("base:spu:edit")
    @Log(title = "修改spu状态", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    public R<Boolean> updateStatus(@RequestBody BaseSpu baseSpu) {
        return R.ok(baseSpuService.updateStatus(baseSpu));
    }


    @ApiOperation(value = "删除spu信息")
    @RequiresPermissions("base:spu:delete")
    @Log(title = "删除spu信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{spuIds}")
    public R<Boolean> deleteBaseSpuByIds(@PathVariable String[] spuIds) {
        return R.ok(baseSpuService.deleteBaseSpuByIds(spuIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("base:spu:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "spu信息导入模板", SpuExcelDto.class);
    }

    @ApiOperation(value = "导出spu信息数据")
    @Log(title = "导出spu信息", businessType = BusinessType.EXPORT)
    @RequiresPermissions("base:spu:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, BaseSpuQuery query) {
        EasyExcelUtils.writeExcel(response, "spu信息信息", SpuExcelDto.class, baseSpuService.getBaseSpuExcelList(query));
    }

    @ApiOperation(value = "导入spu信息数据")
    @Log(title = "导入spu信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("base:spu:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, SpuExcelDto.class, baseSpuService);
    }
}
