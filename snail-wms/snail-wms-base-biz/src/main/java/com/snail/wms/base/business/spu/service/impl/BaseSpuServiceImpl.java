package com.snail.wms.base.business.spu.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.utils.bean.BeanUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.sequence.utils.SequenceUtils;
import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;
import com.snail.wms.base.business.spu.dto.BaseSpuDto;
import com.snail.wms.base.business.spu.dto.SpuExcelDto;
import com.snail.wms.base.business.spu.mapper.BaseSpuMapper;
import com.snail.wms.base.business.spu.query.BaseSpuQuery;
import com.snail.wms.base.business.spu.service.IBaseSpuAttrRelationService;
import com.snail.wms.base.business.spu.service.IBaseSpuService;
import com.snail.wms.base.constants.BaseConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: spu信息Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Service
public class BaseSpuServiceImpl extends ServiceImpl<BaseSpuMapper, BaseSpu> implements IBaseSpuService {

    @Autowired
    private SequenceUtils sequenceUtils;
    @Autowired
    private IBaseSpuAttrRelationService baseSpuAttrRelationService;

    @Override
    public PageResult<BaseSpu> selectBaseSpuPage(BaseSpuQuery query) {
        Page<BaseSpu> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<BaseSpu> queryWrapper = new LambdaQueryWrapper<>();
        //spu编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSpuCode()), BaseSpu::getSpuCode, query.getSpuCode());
        //spu名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getSpuName()), BaseSpu::getSpuName, query.getSpuName());
        //分类id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getCategoryId()), BaseSpu::getCategoryId, query.getCategoryId());
        //状态
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSpuStatus()), BaseSpu::getSpuStatus, query.getSpuStatus());
        //租户标识
        queryWrapper.eq(StringUtils.isNotEmpty(query.getTenantId()), BaseSpu::getTenantId, query.getTenantId());
        queryWrapper.ge(StringUtils.isNotEmpty(query.getBeginTime()), BaseSpu::getCreateTime, query.getBeginTime());
        queryWrapper.le(StringUtils.isNotEmpty(query.getEndTime()), BaseSpu::getCreateTime, query.getEndTime());
        queryWrapper.orderByDesc(BaseSpu::getCreateTime);
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public BaseSpuDto getBaseSpuById(String spuId) {
        BaseSpuDto spuDto = new BaseSpuDto();
        BaseSpu baseSpu = getById(spuId);
        BeanUtils.copyProperties(baseSpu,spuDto);
        List<BaseSpuAttrRelation> attrRelations =  baseSpuAttrRelationService.getSpuAttrRelationBySpuId(spuId);
        spuDto.setSpuAttrs(attrRelations);
        return spuDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertBaseSpu(BaseSpuDto spuDto) {
        BaseSpu baseSpu = new BaseSpu();
        BeanUtils.copyProperties(spuDto, baseSpu);
        String sequence = sequenceUtils.getSequence(BaseConstants.SPU_CODE_RULE);
        baseSpu.setSpuCode("SPU" + baseSpu.getCategoryCode() + sequence);
        save(baseSpu);
        List<BaseSpuAttrRelation> spuAttrs = spuDto.getSpuAttrs();
        if(CollUtil.isNotEmpty(spuAttrs)){
            spuAttrs.forEach(e -> e.setSpuId(baseSpu.getSpuId()));
            this.baseSpuAttrRelationService.saveBatch(spuAttrs);
        }
        return baseSpu.getSpuId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateBaseSpu(BaseSpuDto spuDto) {
        BaseSpu baseSpu = new BaseSpu();
        baseSpu.setSpuId(spuDto.getSpuId());
        baseSpu.setSpuCode(spuDto.getSpuCode());
        baseSpu.setSpuName(spuDto.getSpuName());
        baseSpu.setSpuStatus(spuDto.getSpuStatus());
        baseSpu.setCategoryId(spuDto.getCategoryId());
        baseSpu.setCategoryCode(spuDto.getCategoryCode());
        baseSpu.setCategoryName(spuDto.getCategoryName());
        baseSpu.setCategoryFullName(spuDto.getCategoryFullName());
        // 先删除属性
        this.baseSpuAttrRelationService.deleteSpuAttrRelationBySpuId(baseSpu.getSpuId());
        List<BaseSpuAttrRelation> spuAttrs = spuDto.getSpuAttrs();
        if(CollUtil.isNotEmpty(spuAttrs)){
            spuAttrs.forEach(e -> e.setSpuId(baseSpu.getSpuId()));
            this.baseSpuAttrRelationService.saveBatch(spuAttrs);
        }
        return updateById(baseSpu);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBaseSpuByIds(String[] spuIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(spuIds));
        return batch == spuIds.length;
    }

    @Override
    public List<BaseSpu> selectBaseSpuList(BaseSpuQuery query) {
        LambdaQueryWrapper<BaseSpu> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateStatus(BaseSpu baseSpu) {
        LambdaUpdateWrapper<BaseSpu> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(BaseSpu::getSpuStatus,baseSpu.getSpuStatus());
        updateWrapper.eq(BaseSpu::getSpuId,baseSpu.getSpuId());
        return this.update(updateWrapper);
    }

    @Override
    public List<SpuExcelDto> getBaseSpuExcelList(BaseSpuQuery query) {
        return baseMapper.getBaseSpuExcelList(query);
    }

    @Override
    public void saveExcelData(List<SpuExcelDto> dataList) {

    }
}
