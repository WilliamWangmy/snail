package com.snail.wms.base.business.district.controller;

import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.wms.base.business.district.domain.BaseDistrict;
import com.snail.wms.base.business.district.query.BaseDistrictQuery;
import com.snail.wms.base.business.district.service.IBaseDistrictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 行政区域对象功能接口
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Api(value = "行政区域", tags = "行政区域")
@RestController
@RequestMapping("/district")
public class BaseDistrictController {

    @Autowired
    private IBaseDistrictService baseDistrictService;

    @Log(title = "行政区域分页查询", businessType = BusinessType.QUERY)
    @ApiOperation(value = "分页查询")
    @RequiresPermissions("base:district:query")
    @GetMapping("/page")
    public R<PageResult<BaseDistrict>> getBaseDistrictPage(BaseDistrictQuery query) {
        return R.ok(baseDistrictService.getBaseDistrictPage(query));
    }


    @Log(title = "行政区域接口获取", businessType = BusinessType.INSERT)
    @ApiOperation(value = "接口获取")
//    @RequiresPermissions("base:district:add")
    @GetMapping("/interface")
    public R<Boolean>  inquiryBaseDistrict(DistrictQuery query) {
        return R.ok(baseDistrictService.inquiryBaseDistrict(query));
    }

    @Log(title = "获取行政区域", businessType = BusinessType.INSERT)
    @ApiOperation(value = "获取行政区域")
    @RequiresPermissions("base:district:query")
    @GetMapping("/{adcode}")
    public R<List<BaseDistrict>>  getBaseDistrictByParent(@PathVariable("adcode") String adcode) {
        return R.ok(baseDistrictService.getBaseDistrictByParent(adcode));
    }
}
