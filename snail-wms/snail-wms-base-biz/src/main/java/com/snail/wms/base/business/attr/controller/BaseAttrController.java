package com.snail.wms.base.business.attr.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.base.business.attr.domain.BaseAttr;
import com.snail.wms.base.business.attr.dto.BaseAttrDto;
import com.snail.wms.base.business.attr.query.BaseAttrQuery;
import com.snail.wms.base.business.attr.service.IBaseAttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: 属性Controller
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Api(value = "属性", tags = "属性")
@RestController
@RequestMapping("/attr")
public class BaseAttrController {
    @Autowired
    private IBaseAttrService baseAttrService;

    @Log(title = "查询属性分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询属性列表")
    @RequiresPermissions("base:attr:query")
    @GetMapping("/page")
    public R<PageResult<BaseAttrDto>> getBaseAttrPage(BaseAttrQuery query) {
        return R.ok(baseAttrService.selectBaseAttrPage(query));
    }


    @Log(title = "查询属性列表", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询属性列表")
    @RequiresPermissions("base:attr:query")
    @GetMapping("/list")
    public R<List<BaseAttrDto>> getBaseAttrList(BaseAttrQuery query) {
        return R.ok(baseAttrService.selectBaseAttrList(query));
    }

    @Log(title = "获取属性详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取属性详细信息")
    @RequiresPermissions("base:attr:query")
    @GetMapping(value = "/{attrId}")
    public R<BaseAttrDto> getBaseAttrById(@PathVariable("attrId") String attrId) {
        return R.ok(baseAttrService.getBaseAttrById(attrId));
    }

    @ApiOperation(value = "新增属性")
    @RequiresPermissions("base:attr:add")
    @Log(title = "新增属性", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveBaseAttr(@Validated @RequestBody BaseAttrDto attrDto) {
        return R.ok(baseAttrService.insertBaseAttr(attrDto));
    }

    @ApiOperation(value = "修改属性")
    @RequiresPermissions("base:attr:edit")
    @Log(title = "修改属性", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateBaseAttr(@Validated @RequestBody BaseAttrDto attrDto) {
        return R.ok(baseAttrService.updateBaseAttr(attrDto));
    }


    @ApiOperation(value = "修改属性状态")
    @RequiresPermissions("base:attr:edit")
    @Log(title = "修改属性状态", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    public R<Boolean> updateStatus(@RequestBody BaseAttr baseAttr) {
        return R.ok(baseAttrService.updateStatus(baseAttr));
    }


    @ApiOperation(value = "删除属性")
    @RequiresPermissions("base:attr:delete")
    @Log(title = "删除属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{attrIds}")
    public R<Boolean> deleteBaseAttrByIds(@PathVariable String[] attrIds) {
        return R.ok(baseAttrService.deleteBaseAttrByIds(attrIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("base:attr:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "属性导入模板", BaseAttrDto.class);
    }

    @ApiOperation(value = "导出属性数据")
    @Log(title = "导出属性", businessType = BusinessType.EXPORT)
    @RequiresPermissions("base:attr:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, BaseAttrQuery query) {
        EasyExcelUtils.writeExcel(response, "属性信息", BaseAttr.class, baseAttrService.selectBaseAttrList(query));
    }

    @ApiOperation(value = "导入属性数据")
    @Log(title = "导入属性", businessType = BusinessType.IMPORT)
    @RequiresPermissions("base:attr:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, BaseAttrDto.class, baseAttrService);
    }
}
