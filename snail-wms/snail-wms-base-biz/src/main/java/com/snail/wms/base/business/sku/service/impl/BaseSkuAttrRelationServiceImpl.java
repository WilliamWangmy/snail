package com.snail.wms.base.business.sku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.base.business.sku.domain.BaseSkuAttrRelation;
import com.snail.wms.base.business.sku.mapper.BaseSkuAttrRelationMapper;
import com.snail.wms.base.business.sku.query.BaseSkuAttrRelationQuery;
import com.snail.wms.base.business.sku.service.IBaseSkuAttrRelationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: sku属性Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Service
public class BaseSkuAttrRelationServiceImpl extends ServiceImpl<BaseSkuAttrRelationMapper, BaseSkuAttrRelation> implements IBaseSkuAttrRelationService {

    @Override
    public PageResult<BaseSkuAttrRelation> selectBaseSkuAttrRelationPage(BaseSkuAttrRelationQuery query) {
        Page<BaseSkuAttrRelation> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<BaseSkuAttrRelation> queryWrapper = new LambdaQueryWrapper<>();
        //属性id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAttrId()), BaseSkuAttrRelation::getAttrId, query.getAttrId());
        //属性名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getAttrName()), BaseSkuAttrRelation::getAttrName, query.getAttrName());
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public BaseSkuAttrRelation getBaseSkuAttrRelationById(String id) {
        return getById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertBaseSkuAttrRelation(BaseSkuAttrRelation baseSkuAttrRelation) {
        save(baseSkuAttrRelation);
        return baseSkuAttrRelation.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateBaseSkuAttrRelation(BaseSkuAttrRelation baseSkuAttrRelation) {
        return updateById(baseSkuAttrRelation);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBaseSkuAttrRelationByIds(String[] ids) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(ids));
        return batch == ids.length;
    }

    @Override
    public List<BaseSkuAttrRelation> selectBaseSkuAttrRelationList(BaseSkuAttrRelationQuery query) {
        LambdaQueryWrapper<BaseSkuAttrRelation> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<BaseSkuAttrRelation> dataList) {
        saveBatch(dataList);
    }
}
