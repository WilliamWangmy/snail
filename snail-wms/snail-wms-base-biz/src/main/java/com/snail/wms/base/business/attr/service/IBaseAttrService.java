package com.snail.wms.base.business.attr.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.base.business.attr.domain.BaseAttr;
import com.snail.wms.base.business.attr.dto.BaseAttrDto;
import com.snail.wms.base.business.attr.query.BaseAttrQuery;

import java.util.List;

/**
 * @Description: 属性Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseAttrService extends IService<BaseAttr>, IExcelService<BaseAttrDto> {

    /**
     * 查询属性列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseAttrDto> selectBaseAttrPage(BaseAttrQuery query);

    /**
     * 根据id获取属性详情
     *
     * @param attrId 主键id
     * @return 结果
     */
    BaseAttrDto getBaseAttrById(String attrId);

    /**
     * 保存属性数据
     *
     * @param attrDto 属性信息
     * @return 结果
     */
    String insertBaseAttr(BaseAttrDto attrDto);

    /**
     * 更新属性数据
     *
     * @param attrDto 属性信息
     * @return 结果
     */
    boolean updateBaseAttr(BaseAttrDto attrDto);

    /**
     * 根据id批量删除属性数据
     *
     * @param attrIds 主键ids
     * @return 结果
     */
    boolean deleteBaseAttrByIds(String[] attrIds);

    /**
     * 查询属性数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<BaseAttrDto> selectBaseAttrList(BaseAttrQuery query);

    /**
     * 修改属性状态
     *
     * @param baseAttr 属性
     * @return 结果
     */
    Boolean updateStatus(BaseAttr baseAttr);
}
