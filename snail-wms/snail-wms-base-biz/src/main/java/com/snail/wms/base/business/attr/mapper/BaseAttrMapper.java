package com.snail.wms.base.business.attr.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.snail.common.datascope.annotation.DataScope;
import com.snail.wms.base.business.attr.domain.BaseAttr;
import com.snail.wms.base.business.attr.dto.BaseAttrDto;
import com.snail.wms.base.business.attr.query.BaseAttrQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Description: 属性Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Mapper
public interface BaseAttrMapper extends BaseMapper<BaseAttr> {

    /**
     * 分页查询属性值
     *
     * @param page  分页参数
     * @param query 查询参数
     * @return 结果
     */
    Page<BaseAttrDto> selectBaseAttrPage(@Param("page") Page<BaseAttrDto> page, @Param("query") BaseAttrQuery query);

    /**
     * 查询属性列表
     *
     * @param query 查询参数
     * @return 结果
     */
    List<BaseAttrDto> selectBaseAttrList(@Param("query") BaseAttrQuery query);
}
