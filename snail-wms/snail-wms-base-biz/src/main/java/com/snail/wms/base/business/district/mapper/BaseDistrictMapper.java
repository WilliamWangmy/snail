package com.snail.wms.base.business.district.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.district.domain.BaseDistrict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: 行政区域Mapper
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Mapper
public interface BaseDistrictMapper extends BaseMapper<BaseDistrict> {
}
