package com.snail.wms.base.business.attr.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.base.business.attr.domain.BaseAttrValue;

import java.util.List;

/**
 * @Description: 属性值Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseAttrValueService extends IService<BaseAttrValue> {

    /**
     * 查询属性值数据
     *
     * @param attrId 查询条件
     * @return 结果
     */
    List<BaseAttrValue> getBaseAttrValueByAttrId(String attrId);

    /**
     * 根据属性id删除属性值
     *
     * @param attrId 属性id
     */
    void deleteValuesByAttrId(String attrId);
}
