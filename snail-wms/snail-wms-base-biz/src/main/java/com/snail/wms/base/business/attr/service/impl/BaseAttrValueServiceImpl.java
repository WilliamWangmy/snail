package com.snail.wms.base.business.attr.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.wms.base.business.attr.mapper.BaseAttrValueMapper;
import com.snail.wms.base.business.attr.domain.BaseAttrValue;
import com.snail.wms.base.business.attr.service.IBaseAttrValueService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 属性值Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Service
public class BaseAttrValueServiceImpl extends ServiceImpl<BaseAttrValueMapper, BaseAttrValue> implements IBaseAttrValueService {

    @Override
    public List<BaseAttrValue> getBaseAttrValueByAttrId(String attrId) {
        LambdaQueryWrapper<BaseAttrValue> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseAttrValue::getAttrId,attrId);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteValuesByAttrId(String attrId) {
        LambdaUpdateWrapper<BaseAttrValue> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(BaseAttrValue::getAttrId,attrId);
        this.baseMapper.delete(updateWrapper);
    }

}
