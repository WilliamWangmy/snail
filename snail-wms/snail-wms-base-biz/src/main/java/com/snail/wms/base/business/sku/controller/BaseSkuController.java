package com.snail.wms.base.business.sku.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.base.business.sku.domain.BaseSku;
import com.snail.wms.base.business.sku.query.BaseSkuQuery;
import com.snail.wms.base.business.sku.service.IBaseSkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Description: sku信息Controller
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Api(value = "sku信息", tags = "sku信息")
@RestController
@RequestMapping("/sku")
public class BaseSkuController {
    @Autowired
    private IBaseSkuService baseSkuService;

    @Log(title = "查询sku信息分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询sku信息列表")
    @RequiresPermissions("base:sku:query")
    @GetMapping("/page")
    public R<PageResult<BaseSku>> getBaseSkuPage(BaseSkuQuery query) {
        return R.ok(baseSkuService.selectBaseSkuPage(query));
    }

    @Log(title = "获取sku信息详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取sku信息详细信息")
    @RequiresPermissions("base:sku:query")
    @GetMapping(value = "/{skuId}")
    public R<BaseSku> getBaseSkuById(@PathVariable("skuId") String skuId) {
        return R.ok(baseSkuService.getBaseSkuById(skuId));
    }

    @ApiOperation(value = "新增sku信息")
    @RequiresPermissions("base:sku:add")
    @Log(title = "新增sku信息", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveBaseSku(@Validated @RequestBody BaseSku baseSku) {
        return R.ok(baseSkuService.insertBaseSku(baseSku));
    }

    @ApiOperation(value = "修改sku信息")
    @RequiresPermissions("base:sku:edit")
    @Log(title = "修改sku信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateBaseSku(@Validated @RequestBody BaseSku baseSku) {
        return R.ok(baseSkuService.updateBaseSku(baseSku));
    }


    @ApiOperation(value = "删除sku信息")
    @RequiresPermissions("base:sku:delete")
    @Log(title = "删除sku信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{skuIds}")
    public R<Boolean> deleteBaseSkuByIds(@PathVariable String[] skuIds) {
        return R.ok(baseSkuService.deleteBaseSkuByIds(skuIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("base:sku:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "sku信息导入模板", BaseSku.class);
    }

    @ApiOperation(value = "导出sku信息数据")
    @Log(title = "导出sku信息", businessType = BusinessType.EXPORT)
    @RequiresPermissions("base:sku:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, BaseSkuQuery query) {
        EasyExcelUtils.writeExcel(response, "sku信息信息", BaseSku.class, baseSkuService.selectBaseSkuList(query));
    }

    @ApiOperation(value = "导入sku信息数据")
    @Log(title = "导入sku信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("base:sku:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, BaseSku.class, baseSkuService);
    }
}
