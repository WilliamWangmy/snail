package com.snail.wms.base.business.category.controller;


import com.snail.common.core.domain.R;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.utils.EasyExcelUtils;
import com.snail.common.log.annotation.Log;
import com.snail.common.log.enums.BusinessType;
import com.snail.common.security.annotation.RequiresPermissions;
import com.snail.wms.base.business.category.domain.BaseCategory;
import com.snail.wms.base.business.category.dto.BaseCategoryDto;
import com.snail.wms.base.business.category.query.BaseCategoryQuery;
import com.snail.wms.base.business.category.service.IBaseCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: 产品分类Controller
 * @Author: snail
 * @CreateDate: 2024-04-24
 * @Version: V1.0
 */
@Api(value = "产品分类", tags = "产品分类")
@RestController
@RequestMapping("/category")
public class BaseCategoryController {
    @Autowired
    private IBaseCategoryService baseCategoryService;

    @Log(title = "查询产品分类分页", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询产品分类分页")
    @RequiresPermissions("base:category:query")
    @GetMapping("/page")
    public R<PageResult<BaseCategory>> getBaseCategoryPage(BaseCategoryQuery query) {
        return R.ok(baseCategoryService.selectBaseCategoryPage(query));
    }


    @Log(title = "查询产品分类列表", businessType = BusinessType.QUERY)
    @ApiOperation(value = "查询产品分类列表")
    @RequiresPermissions("base:category:query")
    @GetMapping("/tree")
    public R<List<BaseCategoryDto>> selectBaseCategoryTree(BaseCategoryQuery query) {
        return R.ok(baseCategoryService.selectBaseCategoryTree(query));
    }

    @Log(title = "获取产品分类详细信息", businessType = BusinessType.QUERY)
    @ApiOperation(value = "获取产品分类详细信息")
    @RequiresPermissions("base:category:query")
    @GetMapping(value = "/{categoryId}")
    public R<BaseCategory> getBaseCategoryById(@PathVariable("categoryId") String categoryId) {
        return R.ok(baseCategoryService.getBaseCategoryById(categoryId));
    }

    @ApiOperation(value = "新增产品分类")
    @RequiresPermissions("base:category:add")
    @Log(title = "新增产品分类", businessType = BusinessType.INSERT)
    @PostMapping("/save")
    public R<String> saveBaseCategory(@Validated @RequestBody BaseCategory baseCategory) {
        return R.ok(baseCategoryService.insertBaseCategory(baseCategory));
    }

    @ApiOperation(value = "修改产品分类")
    @RequiresPermissions("base:category:edit")
    @Log(title = "修改产品分类", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public R<Boolean> updateBaseCategory(@Validated @RequestBody BaseCategory baseCategory) {
        return R.ok(baseCategoryService.updateBaseCategory(baseCategory));
    }

    @ApiOperation(value = "修改分类状态")
    @RequiresPermissions("base:category:edit")
    @Log(title = "修改分类状态", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    public R<Boolean> updateStatus(@RequestBody BaseCategory baseCategory) {
        return R.ok(baseCategoryService.updateStatus(baseCategory));
    }


    @ApiOperation(value = "删除产品分类")
    @RequiresPermissions("base:category:delete")
    @Log(title = "删除产品分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{categoryIds}")
    public R<Boolean> deleteBaseCategoryByIds(@PathVariable String[] categoryIds) {
        return R.ok(baseCategoryService.deleteBaseCategoryByIds(categoryIds));
    }

    @ApiOperation(value = "下载导入模板")
    @RequiresPermissions("base:category:import")
    @PostMapping("/excel/template")
    public void downTemplate(HttpServletResponse response) {
        EasyExcelUtils.downTemplate(response, "产品分类导入模板", BaseCategoryDto.class);
    }

    @ApiOperation(value = "导出产品分类数据")
    @Log(title = "导出产品分类", businessType = BusinessType.EXPORT)
    @RequiresPermissions("base:category:export")
    @PostMapping("/excel/export")
    public void exportExcel(HttpServletResponse response, BaseCategoryQuery query) {
        EasyExcelUtils.writeExcel(response, "产品分类信息", BaseCategoryDto.class, baseCategoryService.selectBaseCategoryList(query));
    }

    @ApiOperation(value = "导入产品分类数据")
    @Log(title = "导入产品分类", businessType = BusinessType.IMPORT)
    @RequiresPermissions("base:category:import")
    @PostMapping("/excel/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) {
        EasyExcelUtils.readExcel(multipartFile, BaseCategoryDto.class, baseCategoryService);
    }
}
