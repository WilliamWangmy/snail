package com.snail.wms.base.business.category.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.base.business.category.domain.BaseCategory;
import com.snail.wms.base.business.category.dto.BaseCategoryDto;
import com.snail.wms.base.business.category.query.BaseCategoryQuery;

import java.util.List;

/**
 * @Description: 产品分类Service接口
 * @Author: snail
 * @CreateDate: 2024-04-24
 * @Version: V1.0
 */
public interface IBaseCategoryService extends IService<BaseCategory>, IExcelService<BaseCategoryDto> {

    /**
     * 查询产品分类列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseCategory> selectBaseCategoryPage(BaseCategoryQuery query);

    /**
     * 根据id获取产品分类详情
     *
     * @param categoryId 主键id
     * @return 结果
     */
    BaseCategory getBaseCategoryById(String categoryId);

    /**
     * 保存产品分类数据
     *
     * @param baseCategory 产品分类信息
     * @return 结果
     */
    String insertBaseCategory(BaseCategory baseCategory);

    /**
     * 更新产品分类数据
     *
     * @param baseCategory 产品分类信息
     * @return 结果
     */
    boolean updateBaseCategory(BaseCategory baseCategory);

    /**
     * 根据id批量删除产品分类数据
     *
     * @param categoryIds 主键ids
     * @return 结果
     */
    boolean deleteBaseCategoryByIds(String[] categoryIds);

    /**
     * 查询产品分类数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<BaseCategory> selectBaseCategoryList(BaseCategoryQuery query);

    /**
     * 更新分类状态
     *
     * @param baseCategory 分类
     * @return 结果
     */
    boolean updateStatus(BaseCategory baseCategory);

    /**
     * 获取分类树
     *
     * @param query 查询参数
     * @return 结果
     */
    List<BaseCategoryDto> selectBaseCategoryTree(BaseCategoryQuery query);

    /**
     * 根据分类编码获取分类信息
     *
     * @param categoryCode 分类编码
     * @return 结果
     */
    BaseCategory getBaseCategoryByCode(String categoryCode);
}
