package com.snail.wms.base.business.spu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.dto.SpuExcelDto;
import com.snail.wms.base.business.spu.query.BaseSpuQuery;
import org.apache.ibatis.annotations.Mapper;
import com.snail.common.datascope.annotation.DataScope;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Description: spu信息Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Mapper
public interface BaseSpuMapper extends BaseMapper<BaseSpu> {

    /**
     * 获取Excel导出数据
     * @param query 查询参数
     * @return 结果
     */
    List<SpuExcelDto> getBaseSpuExcelList(@Param("query") BaseSpuQuery query);
}
