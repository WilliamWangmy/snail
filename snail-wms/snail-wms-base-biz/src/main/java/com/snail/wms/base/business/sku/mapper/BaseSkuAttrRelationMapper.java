package com.snail.wms.base.business.sku.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.sku.domain.BaseSkuAttrRelation;
import org.apache.ibatis.annotations.Mapper;


/**
 * @Description: sku属性Mapper接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Mapper
public interface BaseSkuAttrRelationMapper extends BaseMapper<BaseSkuAttrRelation> {

}
