package com.snail.wms.base.business.district.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.constant.SecurityConstants;
import com.snail.common.core.domain.R;
import com.snail.common.core.exception.ServiceException;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.thirdparty.api.RemoteThirdPartyService;
import com.snail.thirdparty.api.dto.DistrictDto;
import com.snail.thirdparty.api.query.DistrictQuery;
import com.snail.wms.base.business.district.domain.BaseDistrict;
import com.snail.wms.base.business.district.mapper.BaseDistrictMapper;
import com.snail.wms.base.business.district.query.BaseDistrictQuery;
import com.snail.wms.base.business.district.service.IBaseDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Description: 行政区域业务处理
 * @Author: Snail
 * @CreateDate: 2024/5/31
 * @Version: V1.0
 */
@Service
public class BaseDistrictServiceImpl extends ServiceImpl<BaseDistrictMapper, BaseDistrict> implements IBaseDistrictService {


    @Autowired
    private RemoteThirdPartyService remoteThirdPartyService;


    /**
     * 分页查询
     *
     * @param query 查询参数
     * @return 结果
     */
    @Override
    public PageResult<BaseDistrict> getBaseDistrictPage(BaseDistrictQuery query) {
        Page<BaseDistrict> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<BaseDistrict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotEmpty(query.getCityCode()), BaseDistrict::getCityCode, query.getCityCode());
        queryWrapper.eq(StringUtils.isNotEmpty(query.getAdCode()), BaseDistrict::getAdCode, query.getAdCode());
        queryWrapper.like(StringUtils.isNotEmpty(query.getName()), BaseDistrict::getName, query.getName());
        queryWrapper.eq(StringUtils.isNotEmpty(query.getLevel()), BaseDistrict::getLevel, query.getLevel());
        queryWrapper.eq(StringUtils.isNotEmpty(query.getParentAdCode()), BaseDistrict::getParentAdCode, query.getParentAdCode());
        return PageUtils.pageResult(this.baseMapper.selectPage(page, queryWrapper));
    }

    /**
     * 接口查询
     *
     * @param query 查询参数
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean inquiryBaseDistrict(DistrictQuery query) {
        R<List<DistrictDto>> r = remoteThirdPartyService.getDistrict(query, SecurityConstants.FROM_SOURCE);
        if (!r.isOk()) {
            throw new ServiceException(r.getMsg());
        }
        List<DistrictDto> data = r.getData();

        List<BaseDistrict> baseDistricts = new ArrayList<>();
        Set<String> delSet = new HashSet<>();
        for (DistrictDto item : data) {
            BaseDistrict district = new BaseDistrict();
            district.setCityCode(item.getCitycode());
            district.setAdCode(item.getAdcode());
            district.setName(item.getName());
            district.setPolyline(item.getPolyline());
            district.setLevel(item.getLevel());
            baseDistricts.add(district);
            delSet.add(item.getAdcode());
            //子行政区域
            buildChildDistrict(item.getDistricts(), district, baseDistricts, delSet);
        }
        //删除
        LambdaUpdateWrapper<BaseDistrict> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(BaseDistrict::getAdCode, delSet.toArray());
        this.baseMapper.delete(updateWrapper);

        return this.saveOrUpdateBatch(baseDistricts);
    }

    @Override
    public List<BaseDistrict> getBaseDistrictByParent(String adcode) {
        LambdaQueryWrapper<BaseDistrict> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseDistrict::getParentAdCode, adcode);
        return this.baseMapper.selectList(queryWrapper);
    }

    /**
     * 获取子行政区域
     *
     * @param districts     接口数据
     * @param parent        父行政区域
     * @param baseDistricts 需要保存的数据
     */
    private void buildChildDistrict(List<DistrictDto> districts, BaseDistrict parent, List<BaseDistrict> baseDistricts, Set<String> delSet) {
        for (DistrictDto item : districts) {
            BaseDistrict district = new BaseDistrict();
            district.setCityCode(item.getCitycode());
            district.setAdCode(item.getAdcode());
            district.setName(item.getName());
            district.setPolyline(item.getPolyline());
            district.setLevel(item.getLevel());
            district.setParentAdCode(parent.getAdCode());
            baseDistricts.add(district);
            delSet.add(item.getAdcode());
            //子行政区域
            buildChildDistrict(item.getDistricts(), district, baseDistricts, delSet);
        }
    }
}
