package com.snail.wms.base.business.sku.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snail.common.core.utils.PageUtils;
import com.snail.common.core.utils.StringUtils;
import com.snail.common.core.web.page.PageResult;
import com.snail.wms.base.business.sku.domain.BaseSku;
import com.snail.wms.base.business.sku.mapper.BaseSkuMapper;
import com.snail.wms.base.business.sku.query.BaseSkuQuery;
import com.snail.wms.base.business.sku.service.IBaseSkuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @Description: sku信息Service业务层处理
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
@Service
public class BaseSkuServiceImpl extends ServiceImpl<BaseSkuMapper, BaseSku> implements IBaseSkuService {

    @Override
    public PageResult<BaseSku> selectBaseSkuPage(BaseSkuQuery query) {
        Page<BaseSku> page = PageUtils.buildPage(query);
        LambdaQueryWrapper<BaseSku> queryWrapper = new LambdaQueryWrapper<>();
        //sku编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSkuCode()), BaseSku::getSkuCode, query.getSkuCode());
        //sku名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getSkuName()), BaseSku::getSkuName, query.getSkuName());
        //主键
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSpuId()), BaseSku::getSpuId, query.getSpuId());
        //spu编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSpuCode()), BaseSku::getSpuCode, query.getSpuCode());
        //spu名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getSpuName()), BaseSku::getSpuName, query.getSpuName());
        //分类id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getCategoryId()), BaseSku::getCategoryId, query.getCategoryId());
        //分类编码
        queryWrapper.eq(StringUtils.isNotEmpty(query.getCategoryCode()), BaseSku::getCategoryCode, query.getCategoryCode());
        //分类名称
        queryWrapper.like(StringUtils.isNotEmpty(query.getCategoryName()), BaseSku::getCategoryName, query.getCategoryName());
        //分类全路径
        queryWrapper.like(StringUtils.isNotEmpty(query.getCategoryFullName()), BaseSku::getCategoryFullName, query.getCategoryFullName());
        //状态
        queryWrapper.eq(StringUtils.isNotEmpty(query.getSkuStatus()), BaseSku::getSkuStatus, query.getSkuStatus());
        //更新人id
        queryWrapper.eq(StringUtils.isNotEmpty(query.getUpdateUserId()), BaseSku::getUpdateUserId, query.getUpdateUserId());
        //更新人
        queryWrapper.like(StringUtils.isNotEmpty(query.getUpdateUserName()), BaseSku::getUpdateUserName, query.getUpdateUserName());
        queryWrapper.ge(StringUtils.isNotEmpty(query.getBeginTime()), BaseSku::getCreateTime, query.getBeginTime());
        queryWrapper.le(StringUtils.isNotEmpty(query.getEndTime()), BaseSku::getCreateTime, query.getEndTime());
        queryWrapper.orderByDesc(BaseSku::getCreateTime);
        page = baseMapper.selectPage(page, queryWrapper);
        return PageUtils.pageResult(page);
    }

    @Override
    public BaseSku getBaseSkuById(String skuId) {
        return getById(skuId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String insertBaseSku(BaseSku baseSku) {
        save(baseSku);
        return baseSku.getSkuId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateBaseSku(BaseSku baseSku) {
        return updateById(baseSku);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBaseSkuByIds(String[] skuIds) {
        int batch = baseMapper.deleteBatchIds(Arrays.asList(skuIds));
        return batch == skuIds.length;
    }

    @Override
    public List<BaseSku> selectBaseSkuList(BaseSkuQuery query) {
        LambdaQueryWrapper<BaseSku> queryWrapper = new LambdaQueryWrapper<>();
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveExcelData(List<BaseSku> dataList) {
        saveBatch(dataList);
    }
}
