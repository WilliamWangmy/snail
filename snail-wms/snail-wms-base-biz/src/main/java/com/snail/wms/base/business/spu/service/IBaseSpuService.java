package com.snail.wms.base.business.spu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.common.core.web.page.PageResult;
import com.snail.common.excel.listener.IExcelService;
import com.snail.wms.base.business.spu.domain.BaseSpu;
import com.snail.wms.base.business.spu.dto.BaseSpuDto;
import com.snail.wms.base.business.spu.dto.SpuExcelDto;
import com.snail.wms.base.business.spu.query.BaseSpuQuery;

import java.util.List;

/**
 * @Description: spu信息Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseSpuService extends IService<BaseSpu>, IExcelService<SpuExcelDto> {

    /**
     * 查询spu信息列表
     *
     * @param query 查询参数
     * @return 结果
     */
    PageResult<BaseSpu> selectBaseSpuPage(BaseSpuQuery query);

    /**
     * 根据id获取spu信息详情
     *
     * @param spuId 主键id
     * @return 结果
     */
    BaseSpuDto getBaseSpuById(String spuId);

    /**
     * 保存spu信息数据
     *
     * @param spuDto spu信息信息
     * @return 结果
     */
    String insertBaseSpu(BaseSpuDto spuDto);

    /**
     * 更新spu信息数据
     *
     * @param spuDto spu信息信息
     * @return 结果
     */
    boolean updateBaseSpu(BaseSpuDto spuDto);

    /**
     * 根据id批量删除spu信息数据
     *
     * @param spuIds 主键ids
     * @return 结果
     */
    boolean deleteBaseSpuByIds(String[] spuIds);

    /**
     * 查询spu信息数据
     *
     * @param query 查询条件
     * @return 结果
     */
    List<BaseSpu> selectBaseSpuList(BaseSpuQuery query);

    /**
     * 修改SPU状态
     *
     * @param baseSpu spu信息
     * @return 结果
     */
    Boolean updateStatus(BaseSpu baseSpu);

    /**
     * 获取Excel导出数据
     *
     * @param query 查询参数
     * @return 结果
     */
    List<SpuExcelDto> getBaseSpuExcelList(BaseSpuQuery query);
}
