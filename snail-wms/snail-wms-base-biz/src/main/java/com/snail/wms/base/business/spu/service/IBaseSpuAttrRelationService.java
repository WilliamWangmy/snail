package com.snail.wms.base.business.spu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.snail.wms.base.business.spu.domain.BaseSpuAttrRelation;

import java.util.List;

/**
 * @Description: spu属性Service接口
 * @Author: snail
 * @CreateDate: 2024-05-26
 * @Version: V1.0
 */
public interface IBaseSpuAttrRelationService extends IService<BaseSpuAttrRelation> {

    /**
     * 获取spu属性
     *
     * @param spuId spuId
     * @return 结果
     */
    List<BaseSpuAttrRelation> getSpuAttrRelationBySpuId(String spuId);

    /**
     * 删除spu属性
     * @param spuId spuId
     */
    void deleteSpuAttrRelationBySpuId(String spuId);
}
