package com.snail.wms.base;

import com.snail.common.cloud.annotation.SnailCloudApplication;
import org.springframework.boot.SpringApplication;

/**
 * @Description: 基础数据服务启动类
 * @Author: snail
 * @CreateDate: 2024/3/7 9:13
 * @Version: V1.0
 */
@SnailCloudApplication
public class SnailWmsBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnailWmsBaseApplication.class,args);
    }
}
