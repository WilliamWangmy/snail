package com.snail.wms.base.business.category.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.snail.wms.base.business.category.domain.BaseCategory;
import org.apache.ibatis.annotations.Mapper;
import com.snail.common.datascope.annotation.DataScope;


/**
 * @Description: 产品分类Mapper接口
 * @Author: snail
 * @CreateDate: 2024-04-24
 * @Version: V1.0
 */
@Mapper
@DataScope(includeMethod = {"selectPage","selectList"})
public interface BaseCategoryMapper extends BaseMapper<BaseCategory> {

}
